{{>partial_header}}

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using {{packageName}}.Client.Requests;
using {{packageName}}.Client.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace {{packageName}}.Client.Calls
{
  /// <summary>
  ///   API Request
  /// </summary>
  public class ApiCallStreamed<T> : ApiCall, IDisposable
  {
    private const int CACHED_ITEMS = 1000;
    private const int ABORT_TIMEOUT_MS = 30000;
    private const string RESULT_FIELD = "results";
    private readonly CancellationTokenSource cancellationTokenSource;
    private int currentRetry;

    protected StreamedApiEnumerator<T> enumerator;
    protected Exception exception;
    private RequestPreliminaryOutcome requestOutcome;

    private DateTime requestTime;

    internal BlockingCollection<JObject> resultObjects;
    private Task resultTask;

    /// <summary>
    /// The semaphore is locked at request start.
    /// The request is made in another thread and the main thread waits acquiring the semaphore.
    /// Once the request produces a response, it is consumed enough to determine if it is an error or read its metadata.
    /// The response data (requestOutcome/exception/metadata) is then populated.
    /// The semaphore is then released no matter what by the secondary thread.
    /// The main thread processes the request outcome and returns to the caller.
    /// If the request did not produce an error, it is consumed in the background.
    /// Note: no attempt is made to release the semaphore exactly once. It might be released more than once to simplify code.
    /// </summary>
    private Semaphore semaphore;

    /// <summary>
    ///   Constructs a new API call
    /// </summary>
    /// <param name="apiClient">The API client to use to execute the API call</param>
    /// <param name="apiRequest">The API call specification</param>
    /// <param name="apiMethodName">The name of the method executed, for error reporting</param>
    public ApiCallStreamed(ApiClient apiClient, ApiRequest apiRequest, string apiMethodName) : base(apiClient,
      apiRequest, apiMethodName)
    {
      resultObjects = new BlockingCollection<JObject>(CACHED_ITEMS);
      cancellationTokenSource = new CancellationTokenSource();
      enumerator = new StreamedApiEnumerator<T>(this);
      PrepareRequest();
    }

    public JObject metadata { get; private set; }

    /// <summary>
    ///   Disposes of the api call
    /// </summary>
    public void Dispose()
    {
      Abort(true);
      if (enumerator != null)
        enumerator.Dispose();
    }

    /// <summary>
    ///   Initializes:
    ///   the result collection
    ///   the token cancellation source
    ///   the request actions
    ///   the api response collection
    /// </summary>
    private void PrepareRequest()
    {
      ApiRequest.GetRequest().AdvancedResponseWriter = (responseStream, response) =>
      {
        try
        {
          //As soon as a response is received, the execution comes here
          ApiResponse = new ApiResponse(response, requestTime);

          if (HasRetryableError(ApiRequest, ApiResponse) &&
              ++currentRetry <= ApiClient.Configuration.RetryCount)
          {
            //The response contains a retryable error and we have still retries left
            requestOutcome = RequestPreliminaryOutcome.RETRY;
            Thread.Sleep(Delay(currentRetry));
            semaphore.Release();
            return;
          }

          //There are no retries left or the response has no retryable errors (either success or not retryable)
          try
          {
            CheckResponseStatus(ApiResponse, responseStream);
            //The response is successful
          }
          catch (Exception e)
          {
            requestOutcome = RequestPreliminaryOutcome.FAILURE;
            exception = e;
            semaphore.Release();
            return;
          }

          try
          {
            var serializer = new JsonSerializer();
            using (var streamReader = new StreamReader(responseStream))
            using (var textReader = new JsonTextReader(streamReader))
            {
              //A non-error response was produced. The caller is still waiting to be unlocked
              //ProcessMetadata will extract the metadata and no-matter-what release the semaphore
              bool hasData = ProcessMetadata(textReader, serializer);

              // Control is returned to the caller of Execute() here, because processMetadata has released the semaphore.
              // The rest is running in the background
              if (hasData)
              {
                ProcessData(textReader, serializer);
              }
              else
              {
                resultObjects.CompleteAdding();
              }
            }
          }
          catch (Exception e)
          {
            requestOutcome = RequestPreliminaryOutcome.FAILURE;
            exception = e;
            semaphore.Release();
            return;
          }
        }
        catch (Exception e)
        {
          requestOutcome = RequestPreliminaryOutcome.FAILURE;
          exception = e;
          semaphore.Release();
          return;
        }
      };
    }

    private bool ProcessMetadata(JsonTextReader textReader, JsonSerializer serializer)
    {
      try
      {
        metadata = new JObject();
        /* Parse metadata */
        ReadToken(textReader, JsonToken.StartObject);
        ReadToken(textReader, JsonToken.PropertyName);
        while (true)
        {
          var fieldName = textReader.Value.ToString();
          if (fieldName == RESULT_FIELD) /* Field with list of items */
            break;
          ReadToken(textReader);
          metadata.Add(fieldName, serializer.Deserialize<JToken>(textReader));
          ReadToken(textReader);
          if (textReader.TokenType == JsonToken.EndObject)
          {
            break; /* Only metadata */
          }
          if (textReader.TokenType != JsonToken.PropertyName)
            throw new ApiException("Unexpected token " + textReader.TokenType + " in metadata", this);
        }

        //Case 1: there is no result field (TO FIX)
        if (textReader.TokenType == JsonToken.EndObject)
        {
          requestOutcome = RequestPreliminaryOutcome.SUCCESS;
          return false;
        }
        //Case 2: there is a result field
        else if (textReader.TokenType == JsonToken.PropertyName)
        {
          ReadToken(textReader);
          if (textReader.TokenType == JsonToken.StartArray)
          {
            requestOutcome = RequestPreliminaryOutcome.SUCCESS;
            return true;
          }
          else if (textReader.TokenType == JsonToken.Null)
          {
            requestOutcome = RequestPreliminaryOutcome.SUCCESS;
            return false;
          }
          else
            throw new ApiException(
              "Unexpected token " + textReader.TokenType + ", expecting " +
              JsonToken.StartArray + " or " + JsonToken.Null, this);
        }
        else
          throw new ApiException(
            "Unexpected token " + textReader.TokenType + ", expecting " +
            JsonToken.EndObject + " or " + JsonToken.PropertyName, this);
      }
      catch (Exception e)
      {
        requestOutcome = RequestPreliminaryOutcome.FAILURE;
        exception = e;
        return false;
      }
      finally
      {
        semaphore.Release();
      }
    }

    private void ProcessData(JsonTextReader textReader, JsonSerializer serializer)
    {
      try
      {
        while (true)
        {
          ReadToken(textReader);
          if (textReader.TokenType == JsonToken.StartObject)
          {
            var obj = serializer.Deserialize<JObject>(textReader);
            resultObjects.Add(obj, cancellationTokenSource.Token); //Blocking call
          }
          else if (textReader.TokenType == JsonToken.EndArray)
          {
            break;
          }
          else
          {
            throw new ApiException(
              "Unexpected token " + textReader.TokenType + ", expecting " +
              JsonToken.EndArray + " or " + JsonToken.StartObject, this);
          }
        }

        ReadToken(textReader, JsonToken.EndObject);
        if (textReader.Read())
          throw new ApiException("Unexpected content at the end of response", this);
      }
      catch (OperationCanceledException)
      {
        //This exception is thrown if the user wants to Reset or Dispose the enumerator
        throw;
      }
      catch (Exception e)
      {
        //Other exceptions are thrown in case of communication errors
        var error = new JObject(
          new JProperty("__error__", true),
          new JProperty("status", 500),
          new JProperty("type", "Internal Error"),
          new JProperty("code", "STREAMING_ERROR"),
          new JProperty("message", e.ToString()));
        resultObjects.Add(error);
      }
      finally
      {
        //Signal any consumer that the stream has been fully produced
        resultObjects.CompleteAdding();
      }
    }



    private void ReadToken(JsonTextReader textReader, JsonToken? tokenType = null)
    {
      if (!textReader.Read())
        throw new ApiException("Nothing to read while looking for " + tokenType, this);
      if (tokenType != null && textReader.TokenType != tokenType)
        throw new ApiException("Unexpected token " + textReader.TokenType + " while looking for " + tokenType, this);
    }


    /// <summary>
    ///   Sends the request on the wire.
    ///   Starts appending result to the blocking result collection.
    ///   State: INITIALIZED -> VALID
    ///   No matter what, when the execution leaves this function, the semaphore is released.
    /// </summary>
    private void StartRequest()
    {
      resultTask = Task.Run(() =>
      {
        try
        {
          requestTime = DateTime.Now;
          ApiClient.Execute(ApiRequest);
        }
        catch (Exception e)
        {
          requestOutcome = RequestPreliminaryOutcome.FAILURE;
          exception = e;
          semaphore.Release();
        }
      }, cancellationTokenSource.Token);
    }

    internal void Abort(bool failIfCannotStop)
    {
      cancellationTokenSource.Cancel();

      if (!resultTask.Wait(ABORT_TIMEOUT_MS) && failIfCannotStop) //Waits for the call to terminate
        throw new ApiException("Unable to abort running iterator", this);
    }

    /// <summary>
    ///   Enumerator over the produced items
    /// </summary>
    //protected StreamedApiEnumerator<T> enumerator;
    private enum RequestPreliminaryOutcome
    {
      SUCCESS,
      RETRY,
      FAILURE
    }

    #region Execution

    internal override void Execute()
    {
      for (;;)
      {
        semaphore = new Semaphore(0, 1);
        StartRequest();
        semaphore.WaitOne();

        switch (requestOutcome)
        {
          case RequestPreliminaryOutcome.SUCCESS:
            return;
          case RequestPreliminaryOutcome.RETRY:
            break;
          case RequestPreliminaryOutcome.FAILURE:
            try
            {
              resultTask.Wait();
            }
            catch (Exception e)
            {
              throw new AggregateException(exception, e);
            }

            throw exception;
          default:
            throw new ApiException("Unexpected request outcome: " + requestOutcome, this);
        }
      }
    }

#pragma warning disable 1998
    internal override async Task ExecuteAsync()
#pragma warning restore 1998
    {
      Execute();
    }

    #endregion
  }
}