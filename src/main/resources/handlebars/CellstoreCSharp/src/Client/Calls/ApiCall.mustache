{{>partial_header}}

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using {{packageName}}.Client.Requests;
using {{packageName}}.Client.Responses;
using RestSharp;
using RestSharp.Extensions;

namespace {{packageName}}.Client.Calls
{
  /// <summary>
  ///   API Request
  /// </summary>
  public abstract class ApiCall
  {
    /// <summary>
    ///   Constructs a new API call
    /// </summary>
    /// <param name="apiClient">The API client to use to execute the API call</param>
    /// <param name="apiRequest">The API call specification</param>
    /// <param name="apiMethodName">The name of the method executed, for error reporting</param>
    protected ApiCall(ApiClient apiClient, ApiRequest apiRequest, string apiMethodName)
    {
      ApiClient = apiClient;
      ApiRequest = apiRequest;
      ApiMethodName = apiMethodName;
    }

    /// <summary>
    ///   The list of HTTP responses received from the server
    /// </summary>
    public ApiResponse ApiResponse { get; protected set; }

    /// <summary>
    ///   The specification of the request
    /// </summary>
    public ApiRequest ApiRequest { get; }

    /// <summary>
    ///   The API client to use to execute the request
    /// </summary>
    public ApiClient ApiClient { get; }

    /// <summary>
    ///   The name of the method executed, for error reporting
    /// </summary>
    public string ApiMethodName { get; }


    /// <summary>
    ///   Returns a string representation of this API Call
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var sb = new StringBuilder();
      sb.AppendLine("Request:");
      sb.AppendLine(ApiRequest.ToString());
      sb.AppendLine("-------------------------");
      sb.AppendLine("Response:");
      if (ApiResponse != null) sb.AppendLine(ApiResponse.ToString());

      if (ApiClient != null && ApiClient.Configuration != null)
      {
        sb.AppendLine("===Configuration===");
        sb.AppendLine(ApiClient.Configuration.ToString());
      }

      return sb.ToString();
    }

    internal abstract void Execute();

    internal abstract Task ExecuteAsync();

    #region Response checking

    /// <summary>
    ///   Checks an api call reply and throws an ApiException in case the reply corresponds to an error
    /// </summary>
    internal void CheckResponseStatus(ApiResponse response, Stream responseStream = null)
    {
      if (ApiClient.UnitTestConfiguration.AllRequestsHaveRetryableErrors)
        response.StatusCode = HttpStatusCode.ExpectationFailed;

      var status = (int) response.StatusCode;
      if (status >= 400)
      {
        if (responseStream != null)
        {
          var content = "";
          try
          {
#pragma warning disable 618
            var rawBytes = responseStream.ReadAsBytes();
            content = rawBytes.AsString();
#pragma warning restore 618
          }
          catch (Exception e)
          {
            throw new ApiException($"Error calling {ApiMethodName}", e, this);
          }

          throw new ApiException($"Error calling {ApiMethodName}: {content}", this);
        }

        throw new ApiException($"Error calling {ApiMethodName}: {response.Content}", this);
      }

      if (status == 0) throw new ApiException($"Error calling {ApiMethodName}: {response.ErrorMessage}", this);
    }

    /// <summary>
    ///   Computes the delay before retrying the request
    /// </summary>
    /// <param name="response">Received response</param>
    /// <param name="currentRetry">Current retry</param>
    /// <returns></returns>
    protected TimeSpan Delay(int currentRetry)
    {
      return TimeSpan.FromMilliseconds((currentRetry + 1) * 10  * 1000);
    }

    /// <summary>
    ///   Returns whether the request can be safely retried or not
    /// </summary>
    /// <param name="request">Request</param>
    /// <param name="response">Response</param>
    /// <returns>whether the request can be safely retried or not</returns>
    protected bool HasRetryableError(ApiRequest request, ApiResponse response)
    {
      if (ApiClient.UnitTestConfiguration.NextRequestHasRetryableError)
      {
        ApiClient.UnitTestConfiguration.NextRequestHasRetryableError = false;
        response.StatusCode = HttpStatusCode.ExpectationFailed;
        return true;
      }

      /*
       * Transport errors
       */
      if (IsSendFailure(response)) // Always retry if the request was not (fully) sent.
        return true;

      /*
       * Retry non side-effecting requests if the response was not (fully) received.
       */
      if (IsReceiveFailure(response))
        return IsReadOnlyRequest(request);

      return false;
    }

    private static bool IsSendFailure(ApiResponse response)
    {
      if (response.ErrorException != null)
      {
        if (response.ErrorException is WebException)
        {
          var status = ((WebException) response.ErrorException).Status;
          return IsSendFailureStatus(status);
        }

        if (response.ErrorException is IOException &&
            response.ErrorException.TargetSite != null &&
            response.ErrorException.TargetSite.Name != null)
          return response.ErrorException.TargetSite.Name == "InternalWrite";
      }

      return false;
    }

    private static bool IsSendFailureStatus(WebExceptionStatus status)
    {
      return status == WebExceptionStatus.ConnectFailure ||
             status == WebExceptionStatus.KeepAliveFailure ||
             status == WebExceptionStatus.NameResolutionFailure ||
             status == WebExceptionStatus.ProxyNameResolutionFailure ||
             status == WebExceptionStatus.SecureChannelFailure ||
             status == WebExceptionStatus.SendFailure;
    }

    private bool IsReceiveFailure(ApiResponse response)
    {
      if (ApiClient.UnitTestConfiguration.NextRequestHasReceiveError)
      {
        ApiClient.UnitTestConfiguration.NextRequestHasReceiveError = false;
        response.StatusCode = HttpStatusCode.ExpectationFailed;
        return true;
      }

      if (response.ErrorException != null)
      {
        if (response.ErrorException is WebException)
        {
          var status = ((WebException) response.ErrorException).Status;
          return IsReceiveFailureStatus(status);
        }

        if (response.ErrorException is IOException &&
            (response.ErrorException.TargetSite == null ||
             response.ErrorException.TargetSite.Name == null ||
             response.ErrorException.TargetSite.Name != "InternalWrite")
        )
          return true;
      }

      return false;
    }

    /// <summary>
    ///   Does the request alter data?
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    private static bool IsReadOnlyRequest(ApiRequest request)
    {
      return request.GetRequest().Method == Method.GET ||
             request.GetRequest().Method == Method.HEAD ||
             request.GetRequest().Method == Method.OPTIONS;
    }

    /// <summary>
    ///   The request has reached the server, the server has started producing an answer, but the connection failed.
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    private static bool IsReceiveFailureStatus(WebExceptionStatus status)
    {
      return status == WebExceptionStatus.ConnectionClosed ||
             status == WebExceptionStatus.PipelineFailure ||
             status == WebExceptionStatus.ReceiveFailure ||
             status == WebExceptionStatus.RequestCanceled;
    }

    #endregion
  }
}