{{>partial_header}}

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace {{packageName}}.Client
{
  public abstract class DiagnosticInfo
  {
    public CultureInfo CurrentCulture => CultureInfo.CurrentCulture;
    public CultureInfo CurrentUICulture => CultureInfo.CurrentUICulture;

    public string FrameworkDescription => RuntimeInformation.FrameworkDescription;
    public string ProcessArchitecture => RuntimeInformation.ProcessArchitecture.ToString();
    public string OSArchitecture => RuntimeInformation.OSArchitecture.ToString();
    public string OSDescription => RuntimeInformation.OSDescription;

    public static DiagnosticInfo Instance()
    {
      if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
      {
        if (Environment.OSVersion.Version.Major < 5)
          return new LegacyWindowsDiagnosticInfo();
        return new WindowsDiagnosticInfo();
      }

      if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
        return new LinuxDiagnosticInfo();
      if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
        return new MacOSDiagnosticInfo();
      return new UnsupportedOSDiagnosticInfo();
    }

    public abstract ulong TotalPhysicalMemory();
    public abstract ulong AvailablePhysicalMemory();
    public abstract ulong TotalVirtualMemory();
    public abstract ulong AvailableVirtualMemory();

    public long TryTotalPhysicalMemory()
    {
      try
      {
        return (long) TotalPhysicalMemory();
      }
      catch (PlatformNotSupportedException)
      {
        return -1;
      }
    }

    public long TryAvailablePhysicalMemory()
    {
      try
      {
        return (long) AvailablePhysicalMemory();
      }
      catch (PlatformNotSupportedException)
      {
        return -1;
      }
    }

    public long TryTotalVirtualMemory()
    {
      try
      {
        return (long) TotalVirtualMemory();
      }
      catch (PlatformNotSupportedException)
      {
        return -1;
      }
    }

    public long TryAvailableVirtualMemory()
    {
      try
      {
        return (long) AvailableVirtualMemory();
      }
      catch (PlatformNotSupportedException)
      {
        return -1;
      }
    }

    public long PeakWorkingSet()
    {
      return Process.GetCurrentProcess().PeakWorkingSet64;
    }

    public long PeakVirtualMemorySize()
    {
      return Process.GetCurrentProcess().PeakVirtualMemorySize64;
    }

    public long PeakPagedMemorySize()
    {
      return Process.GetCurrentProcess().PeakPagedMemorySize64;
    }

    public long WorkingSet()
    {
      return Process.GetCurrentProcess().WorkingSet64;
    }

    public long VirtualMemorySize()
    {
      return Process.GetCurrentProcess().VirtualMemorySize64;
    }

    public long PagedMemorySize()
    {
      return Process.GetCurrentProcess().PagedMemorySize64;
    }

    public override string ToString()
    {
      return
        $"Running C# library version {{{packageVersion}}}, built for the API specification {{{version}}} as a {ProcessArchitecture} process on {FrameworkDescription} in a {OSArchitecture} {OSDescription} OS using a {CurrentCulture} culture.\n" +
        $"System has {TryTotalPhysicalMemory() / 1024 / 1024} MB ({TryTotalVirtualMemory() / 1024 / 1024} MB virtual) in total, of which {TryAvailablePhysicalMemory() / 1024 / 1024} MB ({TryAvailableVirtualMemory() / 1024 / 1024} MB virtual) are currently free.\n" +
        $"Application is using {WorkingSet() / 1024 / 1024} MB ({VirtualMemorySize() / 1024 / 1024} MB virtual), and peaked at {PeakWorkingSet() / 1024 / 1024} MB ({PeakVirtualMemorySize() / 1024 / 1024} MB virtual).";
    }
  }

  internal class UnsupportedOSDiagnosticInfo : DiagnosticInfo
  {
    public override ulong AvailablePhysicalMemory()
    {
      throw new PlatformNotSupportedException("Function not available on unsupported OSes");
      ;
    }

    public override ulong AvailableVirtualMemory()
    {
      throw new PlatformNotSupportedException("Function not available on unsupported OSes");
      ;
    }

    public override ulong TotalPhysicalMemory()
    {
      throw new PlatformNotSupportedException("Function not available on unsupported OSes");
      ;
    }

    public override ulong TotalVirtualMemory()
    {
      throw new PlatformNotSupportedException("Function not available on unsupported OSes");
      ;
    }
  }

  internal class LegacyWindowsDiagnosticInfo : DiagnosticInfo
  {
    [DllImport("Kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern void GlobalMemoryStatus(ref MEMORYSTATUS lpBuffer);

    private MEMORYSTATUS GetMemoryStatus()
    {
      var memoryStatus = new MEMORYSTATUS();
      GlobalMemoryStatus(ref memoryStatus);
      return memoryStatus;
    }

    public override ulong TotalPhysicalMemory()
    {
      return GetMemoryStatus().dwTotalPhys;
    }

    public override ulong AvailablePhysicalMemory()
    {
      return GetMemoryStatus().dwAvailPhys;
    }

    public override ulong TotalVirtualMemory()
    {
      return GetMemoryStatus().dwTotalVirtual;
    }

    public override ulong AvailableVirtualMemory()
    {
      return GetMemoryStatus().dwAvailVirtual;
    }

    internal struct MEMORYSTATUS
    {
      internal uint dwLength;
      internal uint dwMemoryLoad;
      internal uint dwTotalPhys;
      internal uint dwAvailPhys;
      internal uint dwTotalPageFile;
      internal uint dwAvailPageFile;
      internal uint dwTotalVirtual;
      internal uint dwAvailVirtual;
    }
  }

  internal class WindowsDiagnosticInfo : DiagnosticInfo
  {
    [DllImport("Kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool GlobalMemoryStatusEx(ref MEMORYSTATUSEX lpBuffer);

    private MEMORYSTATUSEX GetMemoryStatusEx()
    {
      var memoryStatus = new MEMORYSTATUSEX();
      memoryStatus.Init();
      GlobalMemoryStatusEx(ref memoryStatus);
      return memoryStatus;
    }

    public override ulong TotalPhysicalMemory()
    {
      return GetMemoryStatusEx().ullTotalPhys;
    }

    public override ulong AvailablePhysicalMemory()
    {
      return GetMemoryStatusEx().ullAvailPhys;
    }

    public override ulong TotalVirtualMemory()
    {
      return GetMemoryStatusEx().ullTotalVirtual;
    }

    public override ulong AvailableVirtualMemory()
    {
      return GetMemoryStatusEx().ullAvailVirtual;
    }

    internal struct MEMORYSTATUSEX
    {
      internal uint dwLength;
      internal uint dwMemoryLoad;
      internal ulong ullTotalPhys;
      internal ulong ullAvailPhys;
      internal ulong ullTotalPageFile;
      internal ulong ullAvailPageFile;
      internal ulong ullTotalVirtual;
      internal ulong ullAvailVirtual;
      internal ulong ullAvailExtendedVirtual;

      internal void Init()
      {
        dwLength = checked((uint) Marshal.SizeOf(typeof(MEMORYSTATUSEX)));
      }
    }
  }

  internal class LinuxDiagnosticInfo : DiagnosticInfo
  {
    public override ulong TotalPhysicalMemory()
    {
      return GetBytesFromLine("MemTotal:");
    }

    public override ulong AvailablePhysicalMemory()
    {
      return GetBytesFromLine("MemFree:");
    }

    public override ulong TotalVirtualMemory()
    {
      return GetBytesFromLine("SwapTotal:");
    }

    public override ulong AvailableVirtualMemory()
    {
      return GetBytesFromLine("SwapFree:");
    }

    private static string[] GetProcMemInfoLines()
    {
      return File.ReadAllLines("/proc/meminfo");
    }

    private static ulong GetBytesFromLine(string token)
    {
      const string KbToken = "kB";
      var memTotalLine = GetProcMemInfoLines().FirstOrDefault(x => x.StartsWith(token))?.Substring(token.Length);
      if (memTotalLine != null && memTotalLine.EndsWith(KbToken) &&
          ulong.TryParse(memTotalLine.Substring(0, memTotalLine.Length - KbToken.Length), out var memKb))
        return memKb * 1024;
      throw new PlatformNotSupportedException("Cannot parse /proc/meminfo");
    }
  }

  internal class MacOSDiagnosticInfo : DiagnosticInfo
  {
    private static IntPtr SizeOfLineSize = (IntPtr) IntPtr.Size;

    public override ulong TotalPhysicalMemory()
    {
      return GetSysCtlIntegerByName("hw.memsize");
    }

    public override ulong AvailablePhysicalMemory()
    {
      throw new PlatformNotSupportedException("Function not available on MacOS");
    }

    public override ulong TotalVirtualMemory()
    {
      throw new PlatformNotSupportedException("Function not available on MacOS");
    }

    public override ulong AvailableVirtualMemory()
    {
      throw new PlatformNotSupportedException("Function not available on MacOS");
    }

    private static ulong GetSysCtlIntegerByName(string name)
    {
      sysctlbyname(name, out var lineSize, ref SizeOfLineSize, IntPtr.Zero, IntPtr.Zero);
      return (ulong) lineSize.ToInt64();
    }

    [DllImport("libc")]
    private static extern int sysctlbyname(string name, out IntPtr oldp, ref IntPtr oldlenp, IntPtr newp,
      IntPtr newlen);
  }
}