{{>partial_header}}

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace {{packageName}}.Client.Requests
{
  /// <summary>
  ///   API Request
  /// </summary>
  public class ApiRequest
  {
    public string BaseUrl { get; }

    private Method method;
    private string path;
    
    private bool assembled;
    private RestRequest request;

#pragma warning disable CS0618
    private List<Parameter> pathParameters = new List<Parameter>();
    private List<Parameter> queryParameters = new List<Parameter>();
    private List<Parameter> headerParameters = new List<Parameter>();
    private List<Parameter> bodyParameters = new List<Parameter>();
#pragma warning restore CS0618
    
    public ApiRequest(Method method, string baseUrl, string path)
    {
      this.method = method;
      BaseUrl = baseUrl;
      this.path = path;
    }

    public void AddPathParameter(string name, object value)
    {
      if (assembled)
        throw new InvalidOperationException("Request is already assembled");
      if (value == null)
        return;
#pragma warning disable CS0618
      if (value is string)
        pathParameters.Add(new Parameter(name, value, ParameterType.UrlSegment));
      else if (value is bool)
        pathParameters.Add(new Parameter(name, (bool) value ? "true" : "false", ParameterType.UrlSegment));
      else
        pathParameters.Add(new Parameter(name, Convert.ToString(value), ParameterType.UrlSegment));
#pragma warning restore CS0618
    }

    public void AddQueryParameter(string name, object value)
    {
      if (assembled)
        throw new InvalidOperationException("Request is already assembled");
      if (value == null)
        return;
#pragma warning disable CS0618
      if (value is string)
        queryParameters.Add(new Parameter(name, value, ParameterType.QueryString));
      else if (value is bool)
        queryParameters.Add(new Parameter(name, (bool) value ? "true" : "false", ParameterType.QueryString));
      else if (value is IEnumerable)
        foreach (var innerValue in (IEnumerable) value)
          AddQueryParameter(name, innerValue);
      else
        queryParameters.Add(new Parameter(name, Convert.ToString(value), ParameterType.QueryString));
#pragma warning restore CS0618
    }

    /// <summary>
    ///   Adds pattern-based parameters (after name validation) to a dictionary query parameters.
    /// </summary>
    /// <param name="parameters">Pattern parameters</param>
    /// <param name="pattern">Pattern</param>
    public void AddPatternQueryParameter<T>(IReadOnlyDictionary<string, T> parameters, string pattern)
    {
      if (assembled)
        throw new InvalidOperationException("Request is already assembled");
      var regex = new Regex(pattern);
      foreach (var entry in parameters)
        if (regex.IsMatch(entry.Key))
          AddQueryParameter(entry.Key, entry.Value);
        else
          throw new ApiException("Invalid parameter " + entry.Key + ", parameter name must match the regular expression \"" + pattern + "\"");
    }

    public void AddHeaderParameter(string name, string value)
    {
#pragma warning disable CS0618
      headerParameters.Add(new Parameter(name, value, ParameterType.HttpHeader));
#pragma warning restore CS0618
    }

    public void AddBodyParameter(string contentType, object value)
    {
      if (assembled)
        throw new InvalidOperationException("Request is already assembled");
#pragma warning disable CS0618
      bodyParameters.Add(new Parameter(contentType, SerializeBody(value), contentType, ParameterType.RequestBody));
#pragma warning restore CS0618
    }

    /// <summary>
    ///   Serialize data to be sent in the body of a request
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private static object SerializeBody(object data)
    {
      if (data == null)
        return null;
      if (data is byte[] || data is string)
        return data;
      if (data.GetType() == typeof(JObject))
        return ((JObject) data).ToString(Formatting.Indented);
      return JsonConvert.SerializeObject(data);
    }

    /// <summary>
    ///   If parameter is a boolean, return "true" or "false"
    ///   Otherwise just return its string conversion.
    /// </summary>
    /// <param name="obj">The parameter (header, path, query, form).</param>
    /// <returns>Formatted string.</returns>
    private string SingleParameterToString(object obj)
    {
      if (obj is bool)
        return (bool) obj ? "true" : "false";
      return Convert.ToString(obj);
    }

    private List<string> SerializeQueryParameter(bool value)
    {
      return new List<string> {value ? "true" : "false"};
    }

    private List<string> SerializeQueryParameter(int value)
    {
      return new List<string> {Convert.ToString(value)};
    }

    private List<string> SerializeQueryParameter(string value)
    {
      return new List<string> {value};
    }

    private List<string> SerializeQueryParameter(object obj)
    {
      if (obj is IEnumerable)
      {
        var ret = new List<string>();
        foreach (var param in (IEnumerable) obj)
          ret.Add(SingleParameterToString(param));
        return ret;
      }

      return new List<string> {SingleParameterToString(obj)};
    }

    private string BuildUrl() {
      string replacedPath = path;
#pragma warning disable CS0618
      foreach (Parameter parameter in pathParameters) {
        replacedPath = replacedPath.Replace("{" + parameter.Name + "}", Uri.EscapeDataString((string)parameter.Value));
      }
#pragma warning restore CS0618
      replacedPath = Regex.Replace(replacedPath, "/\\{[^}]+\\}", "");
      return replacedPath;
    }

    internal void Assemble()
    {
      if (assembled)
        throw new InvalidOperationException("Request is already assembled");
      request = new RestRequest(BuildUrl(), method);
      request.Parameters.AddRange(queryParameters);
      request.Parameters.AddRange(headerParameters);
      request.Parameters.AddRange(bodyParameters);
      assembled = true;
    }
    
    internal RestRequest GetRequest()
    {
      if (!assembled)
        throw new InvalidOperationException("Request is not assembled");
      return request;
    }

    /// <summary>
    ///   Returns a string representation of the request
    /// </summary>
    /// <returns>String representation</returns>
    public override string ToString()
    {
      var sb = new StringBuilder();

      var queryString = "";

      var uriEncodedParameters = queryParameters.Where(parameter =>
        parameter != null &&
        parameter.Name != null).Select(parameter =>
      {
        if (parameter.Name.Equals("token") || parameter.Name.Equals("password"))
          return parameter.Name + "=***";
        if (parameter.Value == null || parameter.Value is string value && value == "")
          return WebUtility.UrlEncode(parameter.Name);
        return WebUtility.UrlEncode(parameter.Name) + "=" + WebUtility.UrlEncode(parameter.Value.ToString());
      }).ToList();
      queryString = string.Join("&", uriEncodedParameters);
      if (!string.IsNullOrEmpty(queryString))
        queryString = "?" + queryString;
      
      sb.AppendLine(method + " " + BaseUrl + BuildUrl() + queryString);

      foreach (var parameter in headerParameters.Where(param => param.Type == ParameterType.HttpHeader))
        sb.AppendLine(parameter.Name + ": " + parameter.Value);
      return sb.ToString();
    }
  }
}
