package com.fcavalieri.openapi.codegen.python;

import com.fcavalieri.openapi.codegen.common.BaseGenerator;
import com.fcavalieri.openapi.codegen.common.Utils;
import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.generators.python.PythonClientCodegen;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.servers.Server;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static com.fcavalieri.openapi.codegen.common.BaseGenerator.setServerURL;

public class PythonGenerator extends PythonClientCodegen {
    public CodegenType getTag() {
        return CodegenType.CLIENT;
    }
    public String getName() {
        return "CellstorePython";
    }
    public String getHelp() {
        return "Generates a Cellstore Python client library.";
    }

    public PythonGenerator() {
        super();

        CodegenModelFactory.setTypeMapping(CodegenModelType.OPERATION, Operation.class);
        CodegenModelFactory.setTypeMapping(CodegenModelType.PARAMETER, Parameter.class);


        typeMapping.put("number", "Union[float, Decimal]");
        typeMapping.put("double", "Union[float, Decimal]");
        typeMapping.put("float", "Union[float, Decimal]");
    }


    @Override
    public void processOpts() {
        this.sortParamsByRequiredFlag = false;
        additionalProperties.put(CodegenConstants.EXCLUDE_TESTS, "true");
        String modelPackage = this.modelPackage;
        String apiPackage = this.apiPackage;
        super.processOpts();
        setTemplateFiles(modelPackage, apiPackage);
    }

    private void setTemplateFiles(String modelPackage, String apiPackage) {
        embeddedTemplateDir = customTemplateDir = templateDir = "handlebars/CellstorePython";

        apiTemplateFiles.clear();
        apiTestTemplateFiles.clear();
        apiDocTemplateFiles.clear();

        modelTemplateFiles.clear();
        modelTestTemplateFiles.clear();
        modelDocTemplateFiles.clear();

        supportingFiles.clear();

        //docs folder
        modelDocTemplateFiles.put("docs/model_doc.mustache", ".md");
        apiDocTemplateFiles.put("docs/api_doc.mustache", ".md");

        //src/api folder
        apiTemplateFiles.put("src/api/api.mustache", ".py");
        supportingFiles.add(new SupportingFile("src/api/api_base.mustache", packageName, "api/api_base.py"));
        supportingFiles.add(new SupportingFile("src/api/__init__.mustache", packageName + File.separatorChar + apiPackage, "__init__.py"));

        //src/model folder
        supportingFiles.add(new SupportingFile("src/model/__init__.mustache", packageName + File.separatorChar + modelPackage, "__init__.py"));


        //src folder
        supportingFiles.add(new SupportingFile("src/__init__.mustache", packageName, "__init__.py"));
        supportingFiles.add(new SupportingFile("src/client/__init__.mustache", packageName, "client/__init__.py"));
        supportingFiles.add(new SupportingFile("src/client/api_authentication.mustache", packageName, "client/api_authentication.py"));
        supportingFiles.add(new SupportingFile("src/client/api_exception.mustache", packageName, "client/api_exception.py"));
        supportingFiles.add(new SupportingFile("src/client/api_request.mustache", packageName, "client/api_request.py"));
        supportingFiles.add(new SupportingFile("src/configuration.mustache", packageName, "configuration.py"));
        supportingFiles.add(new SupportingFile("README.mustache", "", "README.md"));
        supportingFiles.add(new SupportingFile("requirements.mustache", "", "requirements.txt"));

        //root folder
        supportingFiles.add(new SupportingFile("setup.mustache", "", "setup.py"));


        if ("asyncio".equals(getLibrary())) {
            supportingFiles.add(new SupportingFile("src/client/api_client.asyncio.mustache", packageName, "client/api_client.py"));
            supportingFiles.add(new SupportingFile("src/client/api_call.asyncio.mustache", packageName, "client/api_call.py"));
            supportingFiles.add(new SupportingFile("src/client/api_response.asyncio.mustache", packageName, "client/api_response.py"));
            supportingFiles.add(new SupportingFile("src/model/sequences.asyncio.mustache", packageName, "models/sequences.py"));
            additionalProperties.put("asyncio", "true");
        } else {
            supportingFiles.add(new SupportingFile("src/client/api_client.urllib.mustache", packageName, "client/api_client.py"));
            supportingFiles.add(new SupportingFile("src/client/api_call.urllib.mustache", packageName, "client/api_call.py"));
            supportingFiles.add(new SupportingFile("src/client/api_response.urllib.mustache", packageName, "client/api_response.py"));
            supportingFiles.add(new SupportingFile("src/model/sequences.urllib.mustache", packageName, "models/sequences.py"));
            additionalProperties.remove("asyncio");
        }
    }

  @Override
  public void preprocessOpenAPI(OpenAPI openAPI) {
    super.preprocessOpenAPI(openAPI);

    Integer urlSegments = Utils.getValue(openAPI.getExtensions(), "x-url-segments", false, Integer.class);
    if (urlSegments != null)
      this.additionalProperties.put("urlSegments", urlSegments);
    else
        this.additionalProperties.put("urlSegments", 2);

    setServerURL(openAPI, this.additionalProperties);
  }

  @Override
  public CodegenOperation fromOperation(String path, String httpMethod, io.swagger.v3.oas.models.Operation operation, Map<String, Schema> schemas, OpenAPI openAPI) {
    List<io.swagger.v3.oas.models.parameters.Parameter> parameters = operation.getParameters();
    if (parameters != null) {
      parameters.removeIf(p -> ((Parameter)fromParameter(p, new HashSet<String>())).excluded);
    }

    Operation op = (Operation) super.fromOperation(path, httpMethod, operation, schemas, openAPI);
    op.init(this);

    return op;
  }

  @Override
  public CodegenParameter fromParameter(io.swagger.v3.oas.models.parameters.Parameter param, Set<String> imports) {
    Parameter p = (Parameter) super.fromParameter(param, imports);
    p.init(this);
    return p;
  }

  @Override
  public CodegenParameter fromRequestBody(RequestBody body, String name, Schema schema, Map<String, Schema> schemas, Set<String> imports) {
    Parameter p = (Parameter) super.fromRequestBody(body, name, schema, schemas, imports);
    p.init(this, body);
    return p;
  }

    @Override
    public Map<String, Object> postProcessOperations(Map<String, Object> objs) {

        List<CodegenOperation> ops = null;
        if (objs != null) {
            Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
            if (operations != null) {
                ops = (List<CodegenOperation>) operations.get("operation");
            }
        }

        if (ops != null) {
            ops.removeIf(op -> ((Operation) op).isExcluded());
            ops.forEach(op -> ((Operation)op).validate());
        }

        addSwitch(CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG,
                CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG_DESC,
                this.sortParamsByRequiredFlag);

        super.postProcessOperations(objs);
        processVariants(ops);

        return objs;
    }

    private void processVariants(List<CodegenOperation> ops) {
        List<CodegenOperation> newOps = new ArrayList<>();
        Set<CodegenOperation> oldOps = new HashSet<>();

        for (CodegenOperation op : ops) {
            if (op.vendorExtensions.containsKey("x-variants")) {
                oldOps.add(op);
                Object variants = op.vendorExtensions.get("x-variants");
                if (variants instanceof List) {
                    for (Object variant : (List) variants) {
                        Operation variantOp = Operation.generateVariant((Operation) op, (Map<String, Object>) variant, this);
                        BaseGenerator.sortOperationParameters(variantOp);
                        newOps.add(variantOp);
                    }
                } else
                    throw new RuntimeException("Invalid value for x-variants, if defined, it must be an array of objects");
            }
            else
            {
                BaseGenerator.sortOperationParameters(op);
            }
        }
        ops.addAll(newOps);
        ops.removeIf(p -> oldOps.contains(p));
    }
}
