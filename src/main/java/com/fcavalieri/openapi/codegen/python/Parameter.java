package com.fcavalieri.openapi.codegen.python;

import com.fcavalieri.openapi.codegen.common.BaseParameter;
import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.parameters.RequestBody;

import java.util.Map;

public class Parameter extends BaseParameter {

    private RequestBody rawBody;
    public RequestBody getRawBody() { return rawBody; }

    public void init(DefaultCodegenConfig codegen, RequestBody body) {
        rawBody = body;
        if (body.getContent().keySet().size() > 0) {
            String contentType = (String)body.getContent().keySet().stream().sorted().toArray()[0];
            initContentType(codegen, contentType);
        }
    }

    public void initContentType(DefaultCodegenConfig codegen, String contentType) {
        switch (contentType) {
            case "text/html":
            case "application/xhtml+xml":
            case "text/xml":
            case "application/xml":
                dataType = "str";
                break;
            case "application/json":
                dataType = "dict";
                break;
            case "application/zip":
                dataType = "bytes";
                break;
            default:
                dataType = "Any";
        }
        init(codegen, rawBody.getExtensions());
    }

    public void init(DefaultCodegenConfig codegen, Map<String, Object> extensions)
    {
        super.init(codegen, extensions, "python");

        switch(kind) {
            case PATTERN:
                dataType = "dict[str, " + dataType + "]";
                break;
            default:
                break;
        }
        if (!required)
            dataType = "Optional[" + dataType + "]";
    }

    public void validate() {
        super.validate();
        /*
        if (rawBody != null) {
            if (rawBody.getContent().size() == 0)
                throw new RuntimeException("No possible body types have been found. This is unsupported.");
            else if (rawBody.getContent().size() > 1)
                throw new RuntimeException("Multiple possible body types have been found. This is unsupported.");
        }
        if (dataType.equals("Any")) {
            throw new RuntimeException("Unsupported body type: " + (String) rawBody.getContent().keySet().toArray()[0]);
        }
        */
    }

    @Override
    public Parameter copy() {
        return new Cloner().deepClone(this);
    }
}
