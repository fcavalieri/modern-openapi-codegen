package com.fcavalieri.openapi.codegen.python;

import com.fcavalieri.openapi.codegen.common.BaseOperation;
import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;

import java.util.Map;

public class Operation extends BaseOperation {
    public String apiCallClass;
    public String getApiCallClass() { return apiCallClass; }

    public String itemType;
    public String getItemType() { return itemType; }

    public boolean returnTypeIterable = false;
    public boolean isReturnTypeIterable() { return returnTypeIterable; }

    @Override
    public void init(DefaultCodegenConfig codegen) {
        super.init(codegen);

        initExtension(codegen);
    }

    private void initExtension(DefaultCodegenConfig codegen) {
        String library = codegen.getLibrary();
        switch(kind)
        {
            case EMPTY:
                returnType = "None";
                apiCallClass = "ApiCallSingleton";
                itemType = "None";
                break;
            case XML:
                returnType = "str";
                apiCallClass = "ApiCallSingleton";
                itemType = "str";
                break;
            case BINARY:
                returnType = "bytes";
                apiCallClass = "ApiCallSingleton";
                itemType = "bytes";
                break;
            case OBJECT_LIST:
                returnType = "ISequence[dict[str, Any]]";
                apiCallClass = "ApiCallObjectList";
                returnTypeIterable = true;
                itemType = "dict";
                break;
            case OBJECT_SINGLE:
                returnType = "dict[str, Any]";
                apiCallClass = "ApiCallSingleton";
                itemType = "dict";
                break;
            case OBJECT_STREAM:
                returnType = "IStreamedSequence[dict[str, Any]]";
                apiCallClass = "ApiCallObjectStream";
                returnTypeIterable = true;
                itemType = "dict";
                break;
            default:
                throw new RuntimeException("Unsupported operation type: " + kind);
        }
    }

    public static Operation generateVariant(Operation template, Map<String, Object> variant, DefaultCodegenConfig codegen) {
        Operation ret = (Operation)template.copy();
        initVariant(ret, variant, codegen);
        ret.initExtension(codegen);
        return ret;
    }

    @Override
    public Operation copy() {
        return new Cloner().deepClone(this);
    }
}
