package com.fcavalieri.openapi.codegen.java;

import com.fcavalieri.openapi.codegen.common.BaseOperation;
import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;

import java.util.Map;

public class Operation extends BaseOperation {

    public String apiCallClass;
    public String getApiCallClass() { return apiCallClass; }

    public boolean returnTypeIterable = false;
    public boolean isReturnTypeIterable() { return returnTypeIterable; }

    @Override
    public void init(DefaultCodegenConfig codegen) {
        super.init(codegen);

        initExtension(codegen);
    }

    private void initExtension(DefaultCodegenConfig codegen) {
        String library = codegen.getLibrary();
        switch(kind)
        {
            case EMPTY:
                returnType = null;
                apiCallClass = "ApiCallSingleton<Void>";
                break;
            case XML:
                returnType = "String";
                apiCallClass = "ApiCallSingleton<String>";
                break;
            case BINARY:
                returnType = "byte[]";
                apiCallClass = "ApiCallSingleton<byte[]>";
                break;
            case OBJECT_LIST:
                if ("gson".equals(library)) {
                    returnType = "ISequence<JsonObject, JsonObject>";
                    apiCallClass = "ApiCallObjectList<JsonObject, JsonObject>";
                }
                else {
                    returnType = "ISequence<ObjectNode, ObjectNode>";
                    apiCallClass = "ApiCallObjectList<ObjectNode, ObjectNode>";
                }
                returnTypeIterable = true;
                break;
            case OBJECT_SINGLE:
                if ("gson".equals(library)) {
                    returnType = "JsonObject";
                    apiCallClass = "ApiCallSingleton<JsonObject>";
                }
                else {
                    returnType = "ObjectNode";
                    apiCallClass = "ApiCallSingleton<ObjectNode>";
                }
                break;
            case OBJECT_STREAM:
                if ("gson".equals(library)) {
                    returnType = "IStream<JsonObject, JsonObject>";
                    apiCallClass = "ApiCallObjectStream<JsonObject, JsonObject>";;
                }
                else {
                    returnType = "IStream<ObjectNode, ObjectNode>";
                    apiCallClass = "ApiCallObjectStream<ObjectNode, ObjectNode>";
                }
                returnTypeIterable = true;
                break;
            default:
                throw new RuntimeException("Unsupported operation type: " + kind);
        }
    }

    public static Operation generateVariant(Operation template, Map<String, Object> variant, DefaultCodegenConfig codegen) {
        Operation ret = (Operation)template.copy();
        initVariant(ret, variant, codegen);
        ret.initExtension(codegen);
        return ret;
    }

    @Override
    public Operation copy() { return new Cloner().deepClone(this); }
}
