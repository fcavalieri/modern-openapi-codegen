package com.fcavalieri.openapi.codegen.java;

import com.fcavalieri.openapi.codegen.common.BaseParameter;
import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.parameters.RequestBody;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Parameter extends BaseParameter {

    private boolean canBeNull = true;
    public boolean isCanBeNull() { return canBeNull; }

    private RequestBody rawBody;
    public RequestBody getRawBody() { return rawBody; }

    public void init(DefaultCodegenConfig codegen, RequestBody body) {
        rawBody = body;
        if (body.getContent().keySet().size() > 0) {
            String contentType = (String)body.getContent().keySet().stream().sorted().toArray()[0];
            initContentType(codegen, contentType);
        }
    }

    public void initContentType(DefaultCodegenConfig codegen, String contentType) {
        String library = codegen.getLibrary();
        switch (contentType)
        {
            case "text/html":
            case "application/xhtml+xml":
            case "text/xml":
            case "application/xml":
                dataType =  "String";
                break;
            case "application/json":
                if ("gson".equals(library))
                    dataType =  "JsonObject";
                else
                    dataType =  "ObjectNode";
                break;
            case "application/zip":
                dataType =  "byte[]";
                break;
            default:
                dataType =  "Object";
        }
        init(codegen, rawBody.getExtensions());
    }

    public void init(DefaultCodegenConfig codegen, Map<String, Object> extensions)
    {
        super.init(codegen, extensions, "java");

        switch(kind) {
            case PATTERN:
                dataType = "Map<String, " + dataType + ">";
                break;
            case NORMAL:
                Set<String> notNullTypes = new HashSet<String>(Arrays.asList("boolean", "byte", "char", "short", "int", "long", "float", "double"));

                canBeNull = !notNullTypes.contains(dataType);
                break;
            default:
                break;
        }
    }

    public void validate() {
        super.validate();
        /*
        if (rawBody != null) {
            if (rawBody.getContent().size() == 0)
                throw new RuntimeException("No possible body types have been found. This is unsupported.");
            else if (rawBody.getContent().size() > 1)
                throw new RuntimeException("Multiple possible body types have been found. This is unsupported.");
        }
        if (dataType.equals("Object")) {
            throw new RuntimeException("Unsupported body type: " + (String) rawBody.getContent().keySet().toArray()[0]);
        }
        */
    }

    @Override
    public Parameter copy() {
        return new Cloner().deepClone(this);
    }
}
