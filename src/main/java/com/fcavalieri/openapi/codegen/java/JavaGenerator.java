package com.fcavalieri.openapi.codegen.java;

import com.fcavalieri.openapi.codegen.common.BaseGenerator;
import com.fcavalieri.openapi.codegen.common.Utils;
import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.generators.java.JavaClientCodegen;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.servers.ServerVariable;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static com.fcavalieri.openapi.codegen.common.BaseGenerator.setServerURL;

public class JavaGenerator extends JavaClientCodegen {
    public CodegenType getTag() {
        return CodegenType.CLIENT;
    }
    public String getName() {
        return "CellstoreJava";
    }
    public String getHelp() {
        return "Generates a Cellstore Java client library.";
    }

    public JavaGenerator() {
        super();

        CodegenModelFactory.setTypeMapping(CodegenModelType.OPERATION, Operation.class);
        CodegenModelFactory.setTypeMapping(CodegenModelType.PARAMETER, Parameter.class);

        supportedLibraries.put("gson", "OkHTTP + Gson");
        supportedLibraries.put("jackson", "OkHTTP + Jackson");
    }

    @Override
    public void processOpts() {
        this.sortParamsByRequiredFlag = false;
        String library = getLibrary();
        setLibrary("okhttp-gson");
        setDateLibrary("java8");
        super.processOpts();
        supportedLibraries.clear();
        supportedLibraries.put("gson", "OkHTTP + Gson");
        supportedLibraries.put("jackson", "OkHTTP + Jackson");
        setLibrary(library); // Verifies the input library is really valid
        setTemplateFiles();
    }

    private void setTemplateFiles() {
        apiTemplateFiles.clear();
        apiTestTemplateFiles.clear();
        apiDocTemplateFiles.clear();

        modelTemplateFiles.clear();
        modelTestTemplateFiles.clear();
        modelDocTemplateFiles.clear();

        supportingFiles.clear();

        embeddedTemplateDir = customTemplateDir = templateDir = "handlebars/CellstoreJava";

        final String apiFolder = (sourceFolder + File.separator + apiPackage).replace(".", File.separator);
        final String invokerFolder = (sourceFolder + File.separator + invokerPackage).replace(".", File.separator);
        final String modelFolder = (sourceFolder + File.separator + modelPackage).replace(".", File.separator);

        //docs folder
        apiDocTemplateFiles.put("docs/api_doc.mustache", ".md");

        //src/.../api folder
        apiTemplateFiles.put("src/api/api.mustache", ".java");
        supportingFiles.add(new SupportingFile("src/api/ApiBase.mustache", apiFolder, "ApiBase.java"));

        //src/.../client folder
        supportingFiles.add(new SupportingFile("src/client/ApiCallback.mustache", invokerFolder, "ApiCallback.java"));
        supportingFiles.add(new SupportingFile("src/client/ApiClient.mustache", invokerFolder, "ApiClient.java"));
        supportingFiles.add(new SupportingFile("src/client/ApiException.mustache", invokerFolder, "ApiException.java"));
        supportingFiles.add(new SupportingFile("src/client/Configuration.mustache", invokerFolder, "Configuration.java"));
        supportingFiles.add(new SupportingFile("src/client/DiagnosticInfo.mustache", invokerFolder, "DiagnosticInfo.java"));
        supportingFiles.add(new SupportingFile("src/client/ProgressRequestBody.mustache", invokerFolder, "ProgressRequestBody.java"));
        supportingFiles.add(new SupportingFile("src/client/ProgressResponseBody.mustache", invokerFolder, "ProgressResponseBody.java"));
        supportingFiles.add(new SupportingFile("src/client/UnitTestConfiguration.mustache", invokerFolder, "UnitTestConfiguration.java"));

        //src/.../client/authentication folder
        supportingFiles.add(new SupportingFile("src/client/authentication/AuthenticationProvider.mustache", invokerFolder + "/authentication", "AuthenticationProvider.java"));
        supportingFiles.add(new SupportingFile("src/client/authentication/Unauthenticated.mustache", invokerFolder + "/authentication", "Unauthenticated.java"));

        //src/.../client/calls folder
        supportingFiles.add(new SupportingFile("src/client/calls/ApiCall.mustache", invokerFolder + "/calls", "ApiCall.java"));
        supportingFiles.add(new SupportingFile("src/client/calls/ApiCallObjectList.mustache", invokerFolder + "/calls", "ApiCallObjectList.java"));
        supportingFiles.add(new SupportingFile("src/client/calls/ApiCallObjectStream.mustache", invokerFolder + "/calls", "ApiCallObjectStream.java"));
        supportingFiles.add(new SupportingFile("src/client/calls/ApiCallSingleton.mustache", invokerFolder + "/calls", "ApiCallSingleton.java"));

        //src/.../client/requests folder
        supportingFiles.add(new SupportingFile("src/client/requests/ApiRequest.mustache", invokerFolder + "/requests", "ApiRequest.java"));

        //src/.../client/responses folder
        supportingFiles.add(new SupportingFile("src/client/responses/ApiResponse.mustache", invokerFolder + "/responses", "ApiResponse.java"));
        supportingFiles.add(new SupportingFile("src/client/responses/MaterializedSequence.mustache", invokerFolder + "/responses", "MaterializedSequence.java"));
        supportingFiles.add(new SupportingFile("src/client/responses/StreamedSequence.mustache", invokerFolder + "/responses", "StreamedSequence.java"));


        //src/.../model folder
        supportingFiles.add(new SupportingFile("src/model/ISequence.mustache", modelFolder, "ISequence.java"));
        supportingFiles.add(new SupportingFile("src/model/IStream.mustache", modelFolder, "IStream.java"));
        //root folder
        writeOptional(outputFolder, new SupportingFile("pom.mustache", "", "pom.xml"));
        writeOptional(outputFolder, new SupportingFile("README.mustache", "", "README.md"));
        if ("jackson".equals(getLibrary())) {
            supportingFiles.add(new SupportingFile("src/client/JSON.jackson.mustache", invokerFolder, "JSON.java"));
            supportingFiles.add(new SupportingFile("src/client/calls/ApiCallStreamed.jackson.mustache", invokerFolder + "/calls", "ApiCallStreamed.java"));
            supportingFiles.add(new SupportingFile("src/client/calls/StreamedApiIterator.jackson.mustache", invokerFolder + "/calls", "StreamedApiIterator.java"));
            additionalProperties.put("jackson", "true");
            additionalProperties.remove("gson");
        } else {
            supportingFiles.add(new SupportingFile("src/client/JSON.gson.mustache", invokerFolder, "JSON.java"));
            supportingFiles.add(new SupportingFile("src/client/calls/ApiCallStreamed.gson.mustache", invokerFolder + "/calls", "ApiCallStreamed.java"));
            supportingFiles.add(new SupportingFile("src/client/calls/StreamedApiIterator.gson.mustache", invokerFolder + "/calls", "StreamedApiIterator.java"));
            additionalProperties.put("gson", "true");
            additionalProperties.remove("jackson");

        }
    }

  @Override
  public void preprocessOpenAPI(OpenAPI openAPI) {
    super.preprocessOpenAPI(openAPI);

    Integer urlSegments = Utils.getValue(openAPI.getExtensions(), "x-url-segments", false, Integer.class);
    if (urlSegments != null)
        this.additionalProperties.put("urlSegments", urlSegments);
    else
        this.additionalProperties.put("urlSegments", 2);

    setServerURL(openAPI, this.additionalProperties);
  }

  @Override
  public CodegenOperation fromOperation(String path, String httpMethod, io.swagger.v3.oas.models.Operation operation, Map<String, Schema> schemas, OpenAPI openAPI) {
    List<io.swagger.v3.oas.models.parameters.Parameter> parameters = operation.getParameters();
    if (parameters != null) {
      parameters.removeIf(p -> ((Parameter)fromParameter(p, new HashSet<String>())).excluded);
    }

    Operation op = (Operation) super.fromOperation(path, httpMethod, operation, schemas, openAPI);
    op.init(this);

    return op;
  }

  @Override
  public CodegenParameter fromParameter(io.swagger.v3.oas.models.parameters.Parameter param, Set<String> imports) {
    Parameter p = (Parameter) super.fromParameter(param, imports);
    p.init(this);
    return p;
  }

  @Override
  public CodegenParameter fromRequestBody(RequestBody body, String name, Schema schema, Map<String, Schema> schemas, Set<String> imports) {
    Parameter p = (Parameter) super.fromRequestBody(body, name, schema, schemas, imports);
    p.init(this, body);
    return p;
  }

    @Override
    public Map<String, Object> postProcessOperations(Map<String, Object> objs) {

        List<CodegenOperation> ops = null;
        if (objs != null) {
            Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
            if (operations != null) {
                ops = (List<CodegenOperation>) operations.get("operation");
            }
        }

        if (ops != null) {
            ops.removeIf(op -> ((Operation) op).isExcluded());
            ops.forEach(op -> ((Operation)op).validate());
        }

        addSwitch(CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG,
                CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG_DESC,
                this.sortParamsByRequiredFlag);

        super.postProcessOperations(objs);
        processVariants(ops);

        return objs;
    }

    private void processVariants(List<CodegenOperation> ops) {
        List<CodegenOperation> newOps = new ArrayList<>();
        Set<CodegenOperation> oldOps = new HashSet<>();

        for (CodegenOperation op : ops) {
            if (op.vendorExtensions.containsKey("x-variants")) {
                oldOps.add(op);
                Object variants = op.vendorExtensions.get("x-variants");
                if (variants instanceof List) {
                    for (Object variant : (List) variants) {
                        Operation variantOp = Operation.generateVariant((Operation) op, (Map<String, Object>) variant, this);
                        BaseGenerator.sortOperationParameters(variantOp);
                        newOps.add(variantOp);
                    }
                } else
                    throw new RuntimeException("Invalid value for x-variants, if defined, it must be an array of objects");
            }
            else
            {
                BaseGenerator.sortOperationParameters(op);
            }
        }
        ops.addAll(newOps);
        ops.removeIf(p -> oldOps.contains(p));
    }
}
