package com.fcavalieri.openapi.codegen.common;

import java.util.ArrayList;
import java.util.Map;

public class Utils {
    public static <T> T getValue(Map<String, Object> map, String key, boolean mandatory, Class<T> desiredType) {
        if (map != null) {
            Object field = map.get(key);
            if (field != null) {
                if (!desiredType.isInstance(field))
                    throw new RuntimeException("The field " + key + " must be a " + desiredType.getSimpleName());
                else
                    return (T) field;
            }
        }
        if (mandatory)
            throw new RuntimeException("The field " + key + " is mandatory");
        else
            return null;
    }
}
