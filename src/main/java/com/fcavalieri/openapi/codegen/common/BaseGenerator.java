package com.fcavalieri.openapi.codegen.common;

import io.swagger.codegen.v3.CodegenOperation;
import io.swagger.codegen.v3.CodegenParameter;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.servers.ServerVariable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class BaseGenerator {
  public static void sortOperationParameters(CodegenOperation op) {
    Collections.sort(op.allParams, Comparator.comparing(CodegenParameter::getRequired).thenComparing(CodegenParameter::getIsPathParam).reversed());
    for (int i = 0; i < op.allParams.size(); i++) {
      op.allParams.get(i).getVendorExtensions().replace("x-has-more", i < op.allParams.size() - 1);
    }
  }

  public static void setServerURL(OpenAPI openAPI, Map<String, Object> additionalProperties) {
    List<Server> servers = openAPI.getServers();
    if (servers != null && !servers.isEmpty()) {
      Server server = servers.get(0);
      String replacedUrl = server.getUrl();
      for (Map.Entry<String, ServerVariable> entry: server.getVariables().entrySet()) {
        replacedUrl = replacedUrl.replace("{" + entry.getKey() + "}", entry.getValue().getDefault() != null ? entry.getValue().getDefault() : "");
      }
      additionalProperties.put("serverUrl", replacedUrl);
    }
  }
}
