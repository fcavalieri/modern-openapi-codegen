package com.fcavalieri.openapi.codegen.common;

import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.CodegenOperation;
import io.swagger.codegen.v3.CodegenParameter;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;

import java.util.*;

/*
 * The extended parameter class has the following extra features:
 * - Exclusion of parameters from the generated bindings:
 *   - x-excluded: true
 *   - x-obsolete: obsolete message
 */
public abstract class BaseOperation extends CodegenOperation {

    public List<CodegenParameter> patternQueryParams = new ArrayList<CodegenParameter>();
    public List<CodegenParameter> hardcodedQueryParams = new ArrayList<CodegenParameter>();
    public OperationKind kind;
    public Boolean isObsolete;
    public String obsoleteMessage;
    public Boolean excluded;

    public Boolean getIsEmpty() { return kind == OperationKind.EMPTY; }
    public Boolean getIsSingleton() { return kind == OperationKind.BINARY || kind == OperationKind.XML || kind == OperationKind.OBJECT_SINGLE; }
    public Boolean getIsSequence() { return kind == OperationKind.OBJECT_LIST || kind == OperationKind.OBJECT_STREAM; }

    public Boolean getIsObsolete() { return isObsolete; }
    public String getObsoleteMessage() { return obsoleteMessage; }
    public List<CodegenParameter> getPatternQueryParams() { return patternQueryParams; }
    public List<CodegenParameter> getHardcodedQueryParams() { return hardcodedQueryParams; }

    public void init(DefaultCodegenConfig codegen) {
        if (queryParams != null) {
            List<CodegenParameter> removeQueryParams = new ArrayList<>();
            for (CodegenParameter p : queryParams) {
                BaseParameter param = (BaseParameter) p;
                if (param.getKind() == BaseParameter.Kind.PATTERN) {
                    removeQueryParams.add(p);
                    patternQueryParams.add(param.copy());
                } else if (param.getKind() == BaseParameter.Kind.HARDCODED) {
                    removeQueryParams.add(p);
                    hardcodedQueryParams.add(param.copy());
                }
            }
            for (CodegenParameter p : removeQueryParams)
                queryParams.remove(p);
        }

        // remove hard coded and excluded params from all params
        if (allParams != null) {
            List<CodegenParameter> removeAllParams = new ArrayList<>();
            for (CodegenParameter p : allParams) {
                BaseParameter param = (BaseParameter) p;
                if (param.getKind() == BaseParameter.Kind.HARDCODED || param.excluded) {
                    removeAllParams.add(p);
                }
            }
            for (CodegenParameter p : removeAllParams)
                allParams.remove(p);
        }

        Boolean excludedRaw = Utils.getValue(vendorExtensions, "x-excluded", false, Boolean.class);
        excluded = excludedRaw == null ? false : excludedRaw.booleanValue();

        obsoleteMessage = Utils.getValue(vendorExtensions, "x-obsolete", false, String.class);
        isObsolete = obsoleteMessage != null;

        String kindRaw = Utils.getValue(vendorExtensions, "x-kind", false, String.class);
        if (kindRaw != null)
            kind = OperationKind.valueOf(kindRaw);
        else
            kind = OperationKind.OBJECT_SINGLE;

        if (bodyParam != null)
        {
            if (((BaseParameter)bodyParam).excluded)
                bodyParam = null;
        }
    }

    public void validate() {
        if (!excluded && !(vendorExtensions.get("x-variants") instanceof List) && bodyParam != null && (consumes == null || consumes.size() != 1))
        {
            String message = "Operation " + operationId + " has a body, but does not have a single consume (content-type)";
            throw new RuntimeException(message);
        }
    }

    public static void initVariant(BaseOperation ret, Map<String, Object> variant, DefaultCodegenConfig codegen) {
        String operationId = Utils.getValue(variant, "operationId", true, String.class);
        ret.operationId = codegen.toOperationId(operationId);
        ret.operationIdLowerCase = operationId.toLowerCase();
        ret.operationIdCamelCase = DefaultCodegenConfig.camelize(operationId);
        ret.operationIdSnakeCase = DefaultCodegenConfig.underscore(operationId);

        Map<String, Object> hardcodedParameters = Utils.getValue(variant, "hardcoded", false, Map.class);
        if (hardcodedParameters != null)
            overrideHardcoded(ret, hardcodedParameters);
        ArrayList<String> excludedParameters = Utils.getValue(variant, "excluded", false, ArrayList.class);
        if (excludedParameters != null)
            overrideExcluded(ret, excludedParameters);
        Map<String, Object> requiredParameters = Utils.getValue(variant, "required", false, Map.class);
        if (requiredParameters != null)
            overrideRequired(ret, requiredParameters);

        String obsoleteMessage = Utils.getValue(variant, "obsolete", false, String.class);
        if (obsoleteMessage != null) {
            ret.isObsolete = true;
            ret.obsoleteMessage = obsoleteMessage;
        }

        String kindRaw = Utils.getValue(variant, "kind", false, String.class);
        if (kindRaw != null)
            ret.kind = OperationKind.valueOf(kindRaw);

        Map<String, Object> requestBodyOverrides = Utils.getValue(variant, "requestBody", false, Map.class);
        if (requestBodyOverrides != null) {
            if (ret.bodyParam == null)
                throw new RuntimeException("Request body parameter was not found");
            if (ret.bodyParams.size() != 1)
                throw new RuntimeException("Unexpected number of body parameters");
            if (ret.allParams.stream().filter(CodegenParameter::getIsBodyParam).count() != 1)
                throw new RuntimeException("Unexpected number of body parameters in all parameters collection");
            if (ret.consumes == null || ret.consumes.size() == 0)
                throw new RuntimeException("Consumes was not found or is empty");
            String contentOverride = Utils.getValue(requestBodyOverrides, "content", true, String.class);

            Optional<Map<String, String>> chosenConsumes = ret.consumes.stream().filter(c -> c.get("mediaType").equals(contentOverride)).findFirst();
            if (!chosenConsumes.isPresent())
                throw new RuntimeException("The overridden content type " + contentOverride + " was not found");
            ret.consumes.clear();
            chosenConsumes.get().put("hasMore", null);
            ret.consumes.add(chosenConsumes.get());

            BaseParameter bodyParamA = (BaseParameter) ret.bodyParam;
            BaseParameter bodyParamB = (BaseParameter) ret.bodyParams.get(0);
            BaseParameter bodyParamC = (BaseParameter) ret.allParams.stream().filter(CodegenParameter::getIsBodyParam).findFirst().get();

            String nameOverride = Utils.getValue(requestBodyOverrides, "name", false, String.class);
            if (nameOverride != null) {
                bodyParamA.paramName = codegen.toParamName(nameOverride);
                bodyParamB.paramName = codegen.toParamName(nameOverride);
                bodyParamC.paramName = codegen.toParamName(nameOverride);
            }

            String descriptionOverride = Utils.getValue(requestBodyOverrides, "description", false, String.class);
            if (descriptionOverride != null) {
                bodyParamA.unescapedDescription = descriptionOverride;
                bodyParamA.description = codegen.escapeText(descriptionOverride);
                bodyParamB.unescapedDescription = descriptionOverride;
                bodyParamB.description = codegen.escapeText(descriptionOverride);
                bodyParamC.unescapedDescription = descriptionOverride;
                bodyParamC.description = codegen.escapeText(descriptionOverride);
            }

            bodyParamA.initContentType(codegen, contentOverride);
            bodyParamB.initContentType(codegen, contentOverride);
            bodyParamC.initContentType(codegen, contentOverride);
        }

        ret.vendorExtensions.remove("x-variants");
        ret.validate();
    }

    private static void overrideRequired(BaseOperation op, Map<String, Object> requiredParameters) {
        for (Map.Entry<String, Object> entry: requiredParameters.entrySet()) {
            String paramName = entry.getKey();
            Boolean isRequired = Utils.getValue(requiredParameters, paramName, true, Boolean.class);
            Optional<CodegenParameter> parameter = op.allParams.stream().filter(x -> x.baseName.equals(paramName)).findFirst();
            if (parameter.isPresent()) {
                List.of(op.allParams, op.bodyParams, op.pathParams, op.queryParams, op.headerParams, op.formParams, op.hardcodedQueryParams, op.patternQueryParams)
                    .stream().forEach(list -> list.stream().filter(x -> x.baseName.equals(paramName)).findFirst().ifPresent(x -> x.required = isRequired));
            }
            else {
                throw new RuntimeException("The " + paramName + " parameter must be present as query parameter (hardcoded or not) because it is referred in a variant set field");
            }
        }
    }

    private static void overrideExcluded(BaseOperation op, ArrayList<String> excludedParameters) {
        for (String paramName: excludedParameters) {
            List.of(op.allParams, op.bodyParams, op.pathParams, op.queryParams, op.headerParams, op.formParams, op.hardcodedQueryParams, op.patternQueryParams)
                    .stream().forEach(list -> list.removeIf(x -> x.baseName.equals(paramName)));
        }
    }

    private static void overrideHardcoded(BaseOperation op, Map<String, Object> hardcodedParameters) {
        for (Map.Entry<String, Object> entry: hardcodedParameters.entrySet()) {
            String paramName = entry.getKey();
            String paramValue = entry.getValue().toString();
            Optional<CodegenParameter> parameter = op.queryParams.stream().filter(x -> x.baseName.equals(paramName)).findFirst();
            if (!parameter.isPresent()) {
                parameter = op.hardcodedQueryParams.stream().filter(x -> x.baseName.equals(paramName)).findFirst();
            }
            if (!parameter.isPresent())
                throw new RuntimeException("The " + paramName + " parameter must be present as query parameter (hardcoded or not) because it is referred in a variant set field");
            List.of(op.allParams, op.bodyParams, op.pathParams, op.queryParams, op.headerParams, op.formParams, op.hardcodedQueryParams, op.patternQueryParams)
                    .stream().forEach(list -> list.removeIf(x -> x.baseName.equals(paramName)));

            BaseParameter hardcodedParam = (BaseParameter) parameter.get();
            hardcodedParam.vendorExtensions.put("x-value", paramValue);
            hardcodedParam.hardcodedValue = paramValue;
            op.hardcodedQueryParams.add(hardcodedParam);
        }
    }

    public boolean isExcluded() { return excluded; }

    public BaseOperation copy() {
        return new Cloner().deepClone(this);
    }
}
