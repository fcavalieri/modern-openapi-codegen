package com.fcavalieri.openapi.codegen.common;

public enum OperationKind { EMPTY, BINARY, XML, OBJECT_SINGLE, OBJECT_LIST, OBJECT_STREAM }