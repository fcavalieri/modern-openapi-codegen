package com.fcavalieri.openapi.codegen.common;

import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.CodegenParameter;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.parameters.RequestBody;

import java.util.*;

/*
 * The extended parameter class has the following extra features:
 * - Hardcoded parameters: Cannot be specified by the user, the value is decided by the swagger binding.
 *   - Specify: x-value: value
 * - Pattern parameters
 *   - Specify: x-pattern: pattern
 * - Overriding of common parameter fields:
 *   - x-name: name
 *   - x-description: description
 *   - x-type: type
 * - Exclusion of parameters from the generated bindings:
 *   - x-excluded: true
 */
public abstract class BaseParameter extends CodegenParameter {

    public enum Kind {
        NORMAL,
        HARDCODED,
        PATTERN
    }

    public Kind kind;
    public String pattern;
    public String hardcodedValue;
    public boolean excluded;

    public Kind getKind() { return kind; }
    public String getPattern() { return pattern; }
    public String getHardcodedValue() { return hardcodedValue; }
    public boolean isExcluded() { return excluded; }

    public void init(DefaultCodegenConfig codegen) {
        init(codegen, vendorExtensions);
    }

    protected abstract void init(DefaultCodegenConfig codegen, RequestBody body);

    protected abstract void init(DefaultCodegenConfig codegen, Map<String, Object> extensions);

    protected void init(DefaultCodegenConfig codegen, Map<String, Object> extensions, String language)
    {
        String xName = Utils.getValue(extensions, "x-name", false, String.class);
        if (xName != null)
            paramName = codegen.toParamName(xName);

        String xDescription = Utils.getValue(extensions, "x-description", false, String.class);
        if (xDescription != null) {
            unescapedDescription = xDescription;
            description = codegen.escapeText(xDescription);
        }

        String xDataType = Utils.getValue(extensions, "x-type-" + language , false, String.class);
        if (xDataType != null)
            dataType = xDataType;

        Boolean xRequired = Utils.getValue(extensions, "x-required", false, Boolean.class);
        if (xRequired != null)
            required = xRequired;

        String xValue = Utils.getValue(extensions, "x-value", false, String.class);
        String xPattern = Utils.getValue(extensions, "x-pattern", false, String.class);

        if (xValue != null && xPattern != null)
            throw new RuntimeException("x-value and x-pattern cannot be specified at the same time.");
        else if (xValue != null) {
            if (Utils.getValue(vendorExtensions, "x-is-primitive-type", true, Boolean.class) == false)
                throw new RuntimeException("hardcoded parameters must have a primitive type");
            kind = Kind.HARDCODED;
            hardcodedValue = xValue;
        }
        else if (xPattern != null) {
            kind = Kind.PATTERN;
            if (getDefaultValue() != null)
                throw new RuntimeException("No behaviour defined when a default value is specified for a pattern parameter");

            pattern = xPattern;
        }
        else {
            kind = Kind.NORMAL;
            if (getDefaultValue() != null && getRequired())
                throw new RuntimeException("No behaviour defined when a default value is specified for a required parameter");
        }

        Boolean excludedRaw = Utils.getValue(extensions, "x-excluded", false, Boolean.class);
        excluded = excludedRaw != null && excludedRaw;
    }

    protected abstract void initContentType(DefaultCodegenConfig codegen, String contentType);

    public void validate() {}

    @Override
    public CodegenParameter copy() {
        return new Cloner().deepClone(this);
    }

    @Override
    public String toString() {
        return String.format("%s(%s) R: %s", this.baseName, this.dataType, this.required);
    }
}
