package com.fcavalieri.openapi.codegen;

public class CLI {
    public static void main(String[] args) {
        try {
            io.swagger.codegen.v3.cli.SwaggerCodegen.main(args);
        } catch (Throwable e) {
            System.exit(1);
        }
    }
}
