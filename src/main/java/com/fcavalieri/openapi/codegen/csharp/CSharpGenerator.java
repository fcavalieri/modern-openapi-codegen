package com.fcavalieri.openapi.codegen.csharp;

import com.fcavalieri.openapi.codegen.common.BaseGenerator;
import com.fcavalieri.openapi.codegen.common.Utils;
import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.generators.dotnet.CSharpClientCodegen;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.servers.Server;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static com.fcavalieri.openapi.codegen.common.BaseGenerator.setServerURL;

public class CSharpGenerator extends CSharpClientCodegen {
  public CodegenType getTag() {
    return CodegenType.CLIENT;
  }
  public String getName() {
    return "CellstoreCSharp";
  }
  public String getHelp() {
    return "Generates a Cellstore C# client library.";
  }

  public CSharpGenerator() {
    super();

    CodegenModelFactory.setTypeMapping(CodegenModelType.OPERATION, Operation.class);
    CodegenModelFactory.setTypeMapping(CodegenModelType.PARAMETER, Parameter.class);

    typeMapping.put("array", "IEnumerable");
    typeMapping.put("list", "IEnumerable");
    typeMapping.put("object", "JObject");
    typeMapping.put("map", "IReadOnlyDictionary");

    instantiationTypes.put("array", "List");
    instantiationTypes.put("list", "List");
    instantiationTypes.put("object", "JObject");
  }

  @Override
  public void processOpts() {
    this.sortParamsByRequiredFlag = false;
    this.additionalProperties.put("excludeTests", true);
    super.processOpts();

    if (!this.targetFramework.equals("v5.0"))
      throw new Error("Support limited to .NET framework 4.5 and v5.0");

    this.setTargetFrameworkNuget("netstandard2.0");
    this.additionalProperties.put("targetFrameworkNuget", this.targetFrameworkNuget);

    embeddedTemplateDir = customTemplateDir = templateDir = "handlebars/CellstoreCSharp";
    setTemplateFiles();
  }

  private void setTemplateFiles() {
    apiTemplateFiles.clear();
    apiTestTemplateFiles.clear();
    apiDocTemplateFiles.clear();

    modelTemplateFiles.clear();
    modelTestTemplateFiles.clear();
    modelDocTemplateFiles.clear();

    supportingFiles.clear();

    String packageFolder = sourceFolder + File.separator + packageName;
    String clientPackageDir = packageFolder + File.separator + clientPackage;

    // docs Folder
    //modelDocTemplateFiles.put("docs/model_doc.mustache", ".md");
    apiDocTemplateFiles.put("docs/api_doc.mustache", ".md");

    // src/Project/Api Folder
    apiTemplateFiles.put("src/Api/api.mustache", ".cs");
    supportingFiles.add(new SupportingFile("src/Api/ApiBase.mustache", packageFolder + File.separator + "Api", "ApiBase.cs"));

    // src/Project/Client Folder
    supportingFiles.add(new SupportingFile("src/Client/ApiClient.mustache", clientPackageDir, "ApiClient.cs"));
    supportingFiles.add(new SupportingFile("src/Client/ApiException.mustache", clientPackageDir, "ApiException.cs"));

    supportingFiles.add(new SupportingFile("src/Client/Configuration.mustache", clientPackageDir, "Configuration.cs"));
    supportingFiles.add(new SupportingFile("src/Client/DiagnosticInfo.mustache", clientPackageDir, "DiagnosticInfo.cs"));
    supportingFiles.add(new SupportingFile("src/Client/UnitTestConfiguration.mustache", clientPackageDir, "UnitTestConfiguration.cs"));

    supportingFiles.add(new SupportingFile("src/Client/Authentication/AuthenticationProvider.mustache", clientPackageDir + File.separator + "Authentication", "AuthenticationProvider.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Authentication/Unauthenticated.mustache", clientPackageDir + File.separator + "Authentication", "Unauthenticated.cs"));

    supportingFiles.add(new SupportingFile("src/Client/Calls/ApiCall.mustache", clientPackageDir + File.separator + "Calls", "ApiCall.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Calls/ApiCallObjectList.mustache", clientPackageDir + File.separator + "Calls", "ApiCallObjectList.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Calls/ApiCallObjectStream.mustache", clientPackageDir + File.separator + "Calls", "ApiCallObjectStream.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Calls/ApiCallSingleton.mustache", clientPackageDir + File.separator + "Calls", "ApiCallSingleton.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Calls/ApiCallStreamed.mustache", clientPackageDir + File.separator + "Calls", "ApiCallStreamed.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Calls/StreamedApiEnumerator.mustache", clientPackageDir + File.separator + "Calls", "StreamedApiEnumerator.cs"));


    supportingFiles.add(new SupportingFile("src/Client/Requests/ApiRequest.mustache", clientPackageDir + File.separator + "Requests", "ApiRequest.cs"));

    supportingFiles.add(new SupportingFile("src/Client/Responses/ApiResponse.mustache", clientPackageDir + File.separator + "Responses", "ApiResponse.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Responses/MaterializedSequence.mustache", clientPackageDir + File.separator + "Responses", "MaterializedSequence.cs"));
    supportingFiles.add(new SupportingFile("src/Client/Responses/StreamedSequence.mustache", clientPackageDir + File.separator + "Responses", "StreamedSequence.cs"));

    // src/Project/Model Folder
    //modelTemplateFiles.put("src/Model/model.mustache", ".cs");
    supportingFiles.add(new SupportingFile("src/Model/ISequence.mustache", packageFolder + File.separator + "Model", "ISequence.cs"));
    supportingFiles.add(new SupportingFile("src/Model/IStream.mustache", packageFolder + File.separator + "Model", "IStream.cs"));
    supportingFiles.add(new SupportingFile("src/Model/Data.mustache", packageFolder + File.separator + "Model", "Data.cs"));

    // src/Project Folder
    supportingFiles.add(new SupportingFile("src/Project.mustache", packageFolder, packageName + ".csproj"));

    supportingFiles.add(new SupportingFile("README.mustache", "", "README.md"));
    supportingFiles.add(new SupportingFile("Solution.mustache", "", packageName + ".sln"));
  }

  @Override
  public void preprocessOpenAPI(OpenAPI openAPI) {
    super.preprocessOpenAPI(openAPI);

    Integer urlSegments = Utils.getValue(openAPI.getExtensions(), "x-url-segments", false, Integer.class);
    if (urlSegments != null)
      this.additionalProperties.put("urlSegments", urlSegments);
    else
      this.additionalProperties.put("urlSegments", 2);

    setServerURL(openAPI, this.additionalProperties);
  }

  @Override
  public CodegenOperation fromOperation(String path, String httpMethod, io.swagger.v3.oas.models.Operation operation, Map<String, Schema> schemas, OpenAPI openAPI) {
    List<io.swagger.v3.oas.models.parameters.Parameter> parameters = operation.getParameters();
    if (parameters != null) {
      parameters.removeIf(p -> ((Parameter)fromParameter(p, new HashSet<String>())).excluded);
    }

    Operation op = (Operation) super.fromOperation(path, httpMethod, operation, schemas, openAPI);
    op.init(this);

    return op;
  }

  @Override
  public CodegenParameter fromParameter(io.swagger.v3.oas.models.parameters.Parameter param, Set<String> imports) {
    Parameter p = (Parameter) super.fromParameter(param, imports);
    p.init(this);
    return p;
  }

  @Override
  public CodegenParameter fromRequestBody(RequestBody body, String name, Schema schema, Map<String, Schema> schemas, Set<String> imports) {
    Parameter p = (Parameter) super.fromRequestBody(body, name, schema, schemas, imports);
    p.init(this, body);
    return p;
  }

  @Override
  public Map<String, Object> postProcessOperations(Map<String, Object> objs) {

    List<CodegenOperation> ops = null;
    if (objs != null) {
      Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
      if (operations != null) {
        ops = (List<CodegenOperation>) operations.get("operation");
      }
    }

    if (ops != null) {
      ops.removeIf(op -> ((Operation) op).isExcluded());
      ops.forEach(op -> ((Operation)op).validate());
      ops.forEach(op -> ((Operation)op).allParams.forEach(p -> ((Parameter)p).validate()));
    }

    addSwitch(CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG,
            CodegenConstants.SORT_PARAMS_BY_REQUIRED_FLAG_DESC,
            this.sortParamsByRequiredFlag);

    super.postProcessOperations(objs);
    processVariants(ops);

    return objs;
  }

  private void processVariants(List<CodegenOperation> ops) {
    List<CodegenOperation> newOps = new ArrayList<>();
    Set<CodegenOperation> oldOps = new HashSet<>();

    for (CodegenOperation op : ops) {
      if (op.vendorExtensions.containsKey("x-variants")) {
        oldOps.add(op);
        Object variants = op.vendorExtensions.get("x-variants");
        if (variants instanceof List) {
          for (Object variant : (List) variants) {
            Operation variantOp = Operation.generateVariant((Operation) op, (Map<String, Object>) variant, this);
            BaseGenerator.sortOperationParameters(variantOp);
            newOps.add(variantOp);
          }
        } else
          throw new RuntimeException("Invalid value for x-variants, if defined, it must be an array of objects");
      }
      else
      {
        BaseGenerator.sortOperationParameters(op);
      }
    }
    ops.addAll(newOps);
    ops.removeIf(p -> oldOps.contains(p));
  }
}
