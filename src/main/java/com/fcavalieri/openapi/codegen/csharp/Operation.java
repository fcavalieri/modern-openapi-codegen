package com.fcavalieri.openapi.codegen.csharp;

import com.fcavalieri.openapi.codegen.common.BaseOperation;
import com.rits.cloning.Cloner;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;

import java.util.Map;

public class Operation extends BaseOperation {

    public String apiCallClass;
    public String getApiCallClass() { return apiCallClass; }

    public boolean returnTypeIterable = false;
    public boolean isReturnTypeIterable() { return returnTypeIterable; }

    @Override
    public void init(DefaultCodegenConfig codegen) {
        super.init(codegen);

        initExtension(codegen);
    }

    private void initExtension(DefaultCodegenConfig codegen) {
        switch(kind)
        {
            case EMPTY:
                returnType = null;
                apiCallClass = "ApiCallSingleton<string>";
                break;
            case XML:
                returnType = "string";
                apiCallClass = "ApiCallSingleton<string>";
                break;
            case BINARY:
                returnType = "byte[]";
                apiCallClass = "ApiCallSingleton<byte[]>";
                break;
            case OBJECT_LIST:
                returnType = "ISequence<JObject>";
                apiCallClass = "ApiCallObjectList<JObject>";
                returnTypeIterable = true;
                break;
            case OBJECT_SINGLE:
                returnType = "JObject";
                apiCallClass = "ApiCallSingleton<JObject>";
                break;
            case OBJECT_STREAM:
                returnType = "IStream<JObject>";
                apiCallClass = "ApiCallObjectStream<JObject>";
                returnTypeIterable = true;
                break;
            default:
                throw new RuntimeException("Unsupported operation type: " + kind);
        }
    }

    public static Operation generateVariant(Operation template, Map<String, Object> variant, DefaultCodegenConfig codegen) {
        Operation ret = (Operation)template.copy();
        initVariant(ret, variant, codegen);
        ret.initExtension(codegen);
        return ret;
    }

    @Override
    public Operation copy() {
        return new Cloner().deepClone(this);
    }
}
