#!/usr/bin/env python3

import argparse
import json

import sys
import os
import subprocess
import shutil
import time
import xml.etree.ElementTree as ElementTree


class Commands:

    PATH_BASE = os.path.dirname(os.path.realpath(__file__))
    PATH_SERVER = os.path.join(PATH_BASE, "test", "test-server")
    PATH_HANDLEBARS = os.path.join(PATH_BASE, "src", "main", "resources", "handlebars")

    PATH_OPENAPI_SPECIFICATION = os.path.join(PATH_BASE, "test", "resources", "index.json")

    PATH_CSHARP_HANDLEBARS = os.path.join(PATH_HANDLEBARS, "CellstoreCSharp")
    PATH_CSHARP_STANDARD_CODEGEN_OPTIONS = os.path.join(PATH_BASE, "test", "resources", "codegen-options-csharp-standard.json")
    PATH_CSHARP_STANDARD_EXPECTED_RESULTS = os.path.join(PATH_BASE, "test", "expected-results", "csharp", "standard")
    PATH_CSHARP_STANDARD_ACTUAL_RESULTS = os.path.join(PATH_BASE, "test", "actual-results", "csharp", "standard")
    PATH_CSHARP_STANDARD_TESTS = os.path.join(PATH_BASE, "test", "test-clients", "csharp", "standard")

    PATH_JAVA_HANDLEBARS = os.path.join(PATH_HANDLEBARS, "CellstoreJava")
    PATH_JAVA_GSON_CODEGEN_OPTIONS = os.path.join(PATH_BASE, "test", "resources", "codegen-options-java-gson.json")
    PATH_JAVA_GSON_EXPECTED_RESULTS = os.path.join(PATH_BASE, "test", "expected-results", "java", "gson")
    PATH_JAVA_GSON_ACTUAL_RESULTS = os.path.join(PATH_BASE, "test", "actual-results", "java", "gson")
    PATH_JAVA_GSON_TESTS = os.path.join(PATH_BASE, "test", "test-clients", "java", "gson")
    PATH_JAVA_JACKSON_CODEGEN_OPTIONS = os.path.join(PATH_BASE, "test", "resources", "codegen-options-java-jackson.json")
    PATH_JAVA_JACKSON_EXPECTED_RESULTS = os.path.join(PATH_BASE, "test", "expected-results", "java", "jackson")
    PATH_JAVA_JACKSON_ACTUAL_RESULTS = os.path.join(PATH_BASE, "test", "actual-results", "java", "jackson")
    PATH_JAVA_JACKSON_TESTS = os.path.join(PATH_BASE, "test", "test-clients", "java", "jackson")

    PATH_PYTHON_HANDLEBARS = os.path.join(PATH_HANDLEBARS, "CellstorePython")
    PATH_PYTHON_URLLIB_CODEGEN_OPTIONS = os.path.join(PATH_BASE, "test", "resources", "codegen-options-python-urllib.json")
    PATH_PYTHON_URLLIB_EXPECTED_RESULTS = os.path.join(PATH_BASE, "test", "expected-results", "python", "urllib")
    PATH_PYTHON_URLLIB_ACTUAL_RESULTS = os.path.join(PATH_BASE, "test", "actual-results", "python", "urllib")
    PATH_PYTHON_URLLIB_TESTS = os.path.join(PATH_BASE, "test", "test-clients", "python", "urllib")
    PATH_PYTHON_ASYNCIO_CODEGEN_OPTIONS = os.path.join(PATH_BASE, "test", "resources", "codegen-options-python-asyncio.json")
    PATH_PYTHON_ASYNCIO_EXPECTED_RESULTS = os.path.join(PATH_BASE, "test", "expected-results", "python", "asyncio")
    PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS = os.path.join(PATH_BASE, "test", "actual-results", "python", "asyncio")
    PATH_PYTHON_ASYNCIO_TESTS = os.path.join(PATH_BASE, "test", "test-clients", "python", "asyncio")

    PATH_DEPENDENCIES = os.path.join(PATH_BASE, "test", "test-clients", "csharp", "dependencies")
    PATH_NUGET = "/usr/bin/nuget.exe"

    VERSION = ElementTree.parse('pom.xml').getroot().find("{http://maven.apache.org/POM/4.0.0}version").text
    GENERATOR_COMMAND = f"java --add-opens=java.base/java.util=ALL-UNNAMED -jar target/modern-openapi-codegen-{VERSION}.jar"
    PIPENV_COMMAND = "python3 -m pipenv"

    def __init__(self):
        parser = argparse.ArgumentParser(
            usage='''./commands.py <command> [<args>]

Supported commands:
  compile                 Compile the generator and test server
  verifyTemplates         Verifies the templates
  generatePythonClients   Generate the python client libraries [--regenerate-expected]
  generateJavaClients     Generate the java client libraries [--regenerate-expected]
  generateCSharpClients   Generate the csharp client libraries [--regenerate-expected]  
  generateClients         Generate all the client libraries [--regenerate-expected]
  compilePythonClients    Compile the python client libraries
  compileJavaClients      Compile the java client libraries
  compileCSharpClients    Compile the csharp client libraries
  compileClients          Compile all the client libraries
  compilePythonTests      Compile the python tests
  compileJavaTests        Compile the java tests
  compileCSharpTests      Compile the csharp tests
  compileTests            Compile all the tests
  prepare                 All the above steps
  testsPythonAsyncio      Run the python asyncio tests
  testsPythonUrllib       Run the python urllib tests
  testsJavaGson           Run the java gson tests
  testsJavaJackson        Run the java jackson tests
  testsCSharpStandard     Run the csharp standard tests
  tests                   Run all tests
''')
        parser.add_argument('command', help='Subcommand to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        PATH_BASE = os.path.dirname(os.path.realpath(__file__))
        self.test_server_path = os.path.join(PATH_BASE, "test", "test-server")
        self.environ = os.environ.copy()
        self.environ["PIPENV_VENV_IN_PROJECT"] ="enabled"
        getattr(self, args.command)()

    def prepare(self):
        self.compile()
        self.verifyTemplates()
        self.generateClients()
        self.compileClients()
        self.compileTests()

    def compile(self):
        print("Compiling generator library")
        subprocess.run("mvn clean package -DskipTests=true", cwd=self.PATH_BASE, shell=True, check=True)
        print("Compiling test-server")
        subprocess.run("mvn clean package -DskipTests=true", cwd=self.PATH_SERVER, shell=True, check=True)

    def verifyTemplates(self):
        self.verifyTemplate(self.PATH_PYTHON_ASYNCIO_CODEGEN_OPTIONS, self.PATH_PYTHON_HANDLEBARS)
        self.verifyTemplate(self.PATH_PYTHON_URLLIB_CODEGEN_OPTIONS, self.PATH_PYTHON_HANDLEBARS)

        self.verifyTemplate(self.PATH_JAVA_GSON_CODEGEN_OPTIONS, self.PATH_JAVA_HANDLEBARS)
        self.verifyTemplate(self.PATH_JAVA_JACKSON_CODEGEN_OPTIONS, self.PATH_JAVA_HANDLEBARS)

        self.verifyTemplate(self.PATH_CSHARP_STANDARD_CODEGEN_OPTIONS, self.PATH_CSHARP_HANDLEBARS)

    def verifyTemplate(self, options_path, template_path):
        with open(options_path) as optionsFile:
            options = json.load(optionsFile)
            values = self.get_all_values(options, ["library"])
            for value in values:
                self.search_in_template(template_path, value)
        with open(self.PATH_OPENAPI_SPECIFICATION) as specFile:
            spec = json.load(specFile)
            values = self.get_all_values(spec["info"])
            for value in values:
                self.search_in_template(template_path, value)

    def get_all_values(self, val, excl=[], ret=None):
        if ret is None:
            ret = []
        if isinstance(val, dict):
            for key, value in val.items():
                if key in excl:
                    continue
                else:
                    self.get_all_values(value, excl, ret)
        elif isinstance(val, list):
            for value in val.items():
                self.get_all_values(value, excl, ret)
        elif isinstance(val, str):
            ret.append(val)
        return ret

    def search_in_template(self, template_path, value):
        result = subprocess.run(f"grep -R '{value}' .", cwd=template_path, shell=True, check=False)
        if result.returncode == 0:
            raise Exception(f"{value} was found in the mustache template at {template_path}. Leak?")
        elif result.returncode != 1:
            raise Exception(f"Error looking for {value} in the mustache template at {template_path}.")

    def generatePythonClients(self):
        parser = argparse.ArgumentParser(description='Client generation')
        parser.add_argument('--regenerate-expected', action='store_true', help='Regenerates the test results')
        args = parser.parse_args(sys.argv[2:])
        regenerate_expected = args.regenerate_expected

        print("Cleanup")
        self.clearFolder(self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS)
        self.clearFolder(self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS)

        if regenerate_expected:
            self.clearFolder(self.PATH_PYTHON_ASYNCIO_EXPECTED_RESULTS)
            self.clearFolder(self.PATH_PYTHON_URLLIB_EXPECTED_RESULTS)

        print("Generating python asyncio client")
        subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstorePython -c {self.PATH_PYTHON_ASYNCIO_CODEGEN_OPTIONS} -o {self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        if regenerate_expected:
            subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstorePython -c {self.PATH_PYTHON_ASYNCIO_CODEGEN_OPTIONS} -o {self.PATH_PYTHON_ASYNCIO_EXPECTED_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        print("Verifying python asyncio client")
        subprocess.run(f"diff -r {self.PATH_PYTHON_ASYNCIO_EXPECTED_RESULTS} {self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)

        print("Generating python urllib client")
        subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstorePython -c {self.PATH_PYTHON_URLLIB_CODEGEN_OPTIONS} -o {self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        if regenerate_expected:
            subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstorePython -c {self.PATH_PYTHON_URLLIB_CODEGEN_OPTIONS} -o {self.PATH_PYTHON_URLLIB_EXPECTED_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        print("Verifying python urllib client")
        subprocess.run(f"diff -r {self.PATH_PYTHON_URLLIB_EXPECTED_RESULTS} {self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)


    def generateJavaClients(self):
        parser = argparse.ArgumentParser(description='Client generation')
        parser.add_argument('--regenerate-expected', action='store_true', help='Regenerates the test results')
        args = parser.parse_args(sys.argv[2:])
        regenerate_expected = args.regenerate_expected

        print("Cleanup")
        self.clearFolder(self.PATH_JAVA_GSON_ACTUAL_RESULTS)
        self.clearFolder(self.PATH_JAVA_JACKSON_ACTUAL_RESULTS)

        if regenerate_expected:
            self.clearFolder(self.PATH_JAVA_GSON_EXPECTED_RESULTS)
            self.clearFolder(self.PATH_JAVA_JACKSON_EXPECTED_RESULTS)

        print("Generating java gson client")
        subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreJava -c {self.PATH_JAVA_GSON_CODEGEN_OPTIONS} -o {self.PATH_JAVA_GSON_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        if regenerate_expected:
            subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreJava -c {self.PATH_JAVA_GSON_CODEGEN_OPTIONS} -o {self.PATH_JAVA_GSON_EXPECTED_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        print("Verifying java client")
        subprocess.run(f"diff -r {self.PATH_JAVA_GSON_EXPECTED_RESULTS} {self.PATH_JAVA_GSON_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)

        print("Generating java jackson client")
        subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreJava -c {self.PATH_JAVA_JACKSON_CODEGEN_OPTIONS} -o {self.PATH_JAVA_JACKSON_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        if regenerate_expected:
            subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreJava -c {self.PATH_JAVA_JACKSON_CODEGEN_OPTIONS} -o {self.PATH_JAVA_JACKSON_EXPECTED_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        print("Verifying java client")
        subprocess.run(f"diff -r {self.PATH_JAVA_JACKSON_EXPECTED_RESULTS} {self.PATH_JAVA_JACKSON_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)

    def generateCSharpClients(self):
        parser = argparse.ArgumentParser(description='Client generation')
        parser.add_argument('--regenerate-expected', action='store_true', help='Regenerates the test results')
        args = parser.parse_args(sys.argv[2:])
        regenerate_expected = args.regenerate_expected

        print("Cleanup")
        self.clearFolder(self.PATH_CSHARP_STANDARD_ACTUAL_RESULTS)

        if regenerate_expected:
            self.clearFolder(self.PATH_CSHARP_STANDARD_EXPECTED_RESULTS)

        print("Generating csharp standard client")
        subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreCSharp -c {self.PATH_CSHARP_STANDARD_CODEGEN_OPTIONS} -o {self.PATH_CSHARP_STANDARD_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        if regenerate_expected:
            subprocess.run(f"{self.GENERATOR_COMMAND} generate -i {self.PATH_OPENAPI_SPECIFICATION} -l CellstoreCSharp -c {self.PATH_CSHARP_STANDARD_CODEGEN_OPTIONS} -o {self.PATH_CSHARP_STANDARD_EXPECTED_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)
        print("Verifying csharp standard client")
        subprocess.run(f"diff -r {self.PATH_CSHARP_STANDARD_EXPECTED_RESULTS} {self.PATH_CSHARP_STANDARD_ACTUAL_RESULTS}", cwd=self.PATH_BASE, shell=True, check=True)

    def generateClients(self):
        self.generatePythonClients()
        self.generateJavaClients()
        self.generateCSharpClients()

    def clearFolder(self, folder):
        if os.path.exists(folder) and os.path.isdir(folder):
            shutil.rmtree(folder)

    def compilePythonClients(self):
        print("Verifying python asyncio library (library)")
        self.clearFolder(os.path.join(self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS, ".venv"))
        subprocess.run(f"{self.PIPENV_COMMAND} install -r requirements.txt", cwd=self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS, shell=True, check=True, env=self.environ)
        subprocess.run('flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics --extend-exclude=.venv', cwd=self.PATH_PYTHON_ASYNCIO_ACTUAL_RESULTS, shell=True, check=True, env=self.environ)

        print("Verifying python urllib library (library)")
        self.clearFolder(os.path.join(self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS, ".venv"))
        subprocess.run(f"{self.PIPENV_COMMAND} install -r requirements.txt", cwd=self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS, shell=True, check=True, env=self.environ)
        subprocess.run('flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics --extend-exclude=.venv', cwd=self.PATH_PYTHON_URLLIB_ACTUAL_RESULTS, shell=True, check=True, env=self.environ)

    def compileJavaClients(self):
        print("Compiling java gson library (library)")
        subprocess.run('mvn package -DskipTests', cwd=self.PATH_JAVA_GSON_ACTUAL_RESULTS, shell=True, check=True)

        print("Compiling java jackson library (library)")
        subprocess.run('mvn package -DskipTests', cwd=self.PATH_JAVA_JACKSON_ACTUAL_RESULTS, shell=True, check=True)


    def compileCSharpClients(self):
        print("Compiling csharp standard library (library)")
        subprocess.run('dotnet build --configuration Release CellStore.sln', cwd=self.PATH_CSHARP_STANDARD_ACTUAL_RESULTS, shell=True, check=True)

    def compileClients(self):
        self.compilePythonClients()
        self.compileJavaClients()
        self.compileCSharpClients()

    def compilePythonTests(self):
        print("Verifying python asyncio tests")
        self.clearFolder(os.path.join(self.PATH_PYTHON_ASYNCIO_TESTS, ".venv"))
        subprocess.run(f"{self.PIPENV_COMMAND} install", cwd=self.PATH_PYTHON_ASYNCIO_TESTS, shell=True, check=True, env=self.environ)
        subprocess.run('flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics --extend-exclude=.venv', cwd=self.PATH_PYTHON_ASYNCIO_TESTS, shell=True, check=True)

        print("Verifying python urllib tests")
        self.clearFolder(os.path.join(self.PATH_PYTHON_URLLIB_TESTS, ".venv"))
        subprocess.run(f"{self.PIPENV_COMMAND} install", cwd=self.PATH_PYTHON_URLLIB_TESTS, shell=True, check=True, env=self.environ)
        subprocess.run('flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics --extend-exclude=.venv', cwd=self.PATH_PYTHON_URLLIB_TESTS, shell=True, check=True)

    def compileJavaTests(self):
        print("Compiling java gson tests")
        subprocess.run('mvn package -DskipTests', cwd=self.PATH_JAVA_GSON_TESTS, shell=True, check=True)

        print("Compiling java jackson tests")
        subprocess.run('mvn package -DskipTests', cwd=self.PATH_JAVA_JACKSON_TESTS, shell=True, check=True)

    def compileCSharpTests(self):
        print("Installing csharp standard packages (tests)")
        subprocess.run(f"dotnet restore", cwd=self.PATH_CSHARP_STANDARD_TESTS, shell=True, check=True)
        print("Compiling csharp standard tests")
        subprocess.run('dotnet build --configuration Release *.sln', cwd=self.PATH_CSHARP_STANDARD_TESTS, shell=True, check=True)


    def compileTests(self):
        self.compilePythonTests()
        self.compileJavaTests()
        self.compileCSharpTests()

    def tests(self):
        self.testsPythonAsyncio()
        self.testsPythonUrllib()
        self.testsJavaGson()
        self.testsJavaJackson()
        self.testsCSharpStandard()

    def testsPythonAsyncio(self):
        print("Running python asyncio tests")
        subprocess.run(f"{self.PIPENV_COMMAND} run pytest", cwd=self.PATH_PYTHON_ASYNCIO_TESTS, shell=True, check=True)

    def testsPythonUrllib(self):
        print("Running python urllib tests")
        subprocess.run(f"{self.PIPENV_COMMAND} run pytest", cwd=self.PATH_PYTHON_URLLIB_TESTS, shell=True, check=True)

    def testsJavaGson(self):
        print("Running java gson tests")
        subprocess.run("mvn test", cwd=self.PATH_JAVA_GSON_TESTS, shell=True, check=True)

    def testsJavaJackson(self):
        print("Running java jackson tests")
        subprocess.run("mvn test", cwd=self.PATH_JAVA_JACKSON_TESTS, shell=True, check=True)

    def testsCSharpStandard(self):
        print("Running csharp standard tests")
        subprocess.run(f"dotnet test -v n", cwd=self.PATH_CSHARP_STANDARD_TESTS, shell=True, check=True)

if __name__ == "__main__":
    Commands()
