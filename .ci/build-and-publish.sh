#!/bin/bash
set -e
set -u

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
IMAGE_NAME="registry.gitlab.com/fcavalieri/modern-openapi-codegen/ci"
IMAGE_VERSION="0.2.0"
TAG="$IMAGE_NAME:$IMAGE_VERSION"

(
  cd "$SCRIPT_DIR"
  docker build --no-cache -t $TAG .
  #docker build -t $TAG .
  docker push $TAG
)
