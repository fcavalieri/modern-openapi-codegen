# Modern OpenAPI codegen

* Supports OpenAPI v3.
* Generates Libraries for .NET Framework 4.5.2 and later and .NET Core 2.0 and later. 
* Libraries are produced in C#.
* Libraries rely on the latest version (at time of development) of RestSharp, Newtonsoft.Json, System.Diagnostics.Process (only .NET Framework), System.Runtime.InteropServices.RuntimeInformation (only .NET core).

## Supported extensions to the OpenAPI v3:

### Global options:
* `x-url-segments` (integer), e.g.: `2`. Specifies the number of segments in a valid endpoint path. `2` means `http://host/v1`, `3` means `http://host/endpoint/v1`.

### Parameters (including body):
* `x-name` (string), e.g.: `"myParameter"`. Overrides the variable name used in the library with the given one. Useful for bodies (they have no name) and pattern parameters (the name is a pattern). 
* `x-description` (string), e.g.: `"My parameter"`. Overrides the variable description used in the library with the given one. Useful to provide a description more suited to the library.
* `x-type` (string), e.g.: `"int?"`. Overrides the variable type used in the library with the given one. Useful in corner cases, to use a specific type.
* `x-excluded` (boolean), e.g.: `true`. If true, the parameter does not appear in the generated library. It is not sent to the server in the request.

### Pattern parameters:
A pattern parameter defines a family of parameters whose name matches the regular expression defined in `x-pattern`. 
For each specified parameter is that defined in the OpenAPI parameter.

* `x-pattern` (string), e.g.: `"^([^!:]+:[^:]+)(?:(?:::)(eq|gt|lt))?$"`. When specified, makes the parameter a pattern parameter. Defines the regular expression that characterizes the parameters names.

### Hardcoded parameters:
A hardcoded parameter is a parameter that do not appear in the generated library. Its value is specified in the OpenAPI definition and sent as is to the server in the request.

* `x-value` (string), e.g.: `"3"`. When specified, makes the parameter a hardcoded parameter. It is always string. It is sent as is to the server in the request.

### Path parameters:
* `x-required` (boolean), e.g.: `false`. Path parameters are mandatory in OpenAPI. To make a path parameter optional specify `x-required` to be `false`. 

### Operations:
* `x-kind` (string), e.g.: `"OBJECT_LIST"`. Defines the kind of data returned in the generated library, selecting a custom implementation. 
   One of BINARY, XML, OBJECT_SINGLE, OBJECT_LIST, OBJECT_STREAM. Default is OBJECT_SINGLE.
* `x-obsolete` (string), e.g.: `"Obsolete use another operation."`. If specified, the operation is marked as obsolete with the given message in the generated library.
* `x-excluded` (boolean), e.g.: `true`. If true, the operation does not appear in the generated library.

### Operation variants:
Certain endpoints might produce data in different formats, e.g. JSON and XML. 
For these endpoints it is useful to have different methods generated one for each kind, with a precisely defined return value.

Moreover for endpoints that produce a sequence of JSON objects it is useful to provide both a method that returns a list of object and one that returns a stream of object.

* `x-variants` (array of variant objects): Specifies the operation variants. The operation definition will be used only as a template for the different variants and will not be generated.

Each variant object has the following structure:
* `operationId` (string, mandatory), e.g.: `MyOperationVariant`. The name of the operation variant
* `obsolete` (string), e.g.: `"Obsolete use another operation."`. If specified, the operation is marked as obsolete with the given message in the generated library.
* `kind` (string), e.g.: `"OBJECT_LIST"`. Defines the kind of data returned in the generated library, selecting a custom implementation. 
   One of BINARY, XML, OBJECT_SINGLE, OBJECT_LIST, OBJECT_STREAM. Default is OBJECT_SINGLE.
* `hardcoded` (object[string->string]), e.g.: `{ "param1": "value1", "param2": "value2" }`. Makes the specified parameters hardcoded in the variant and specifies their value.
* `excluded` (array[string]), e.g.: ` ["param3", "param4"] }`. Makes the specified parameters excluded in the variant.
* `required` (object[string->boolean]), e.g.: ` { "param5": true, "param6": false] }`. Overrides the required attribute for parameters in the variant.
* `requestBody` (object)
   - `content` (string), e.g.: `application/zip`. Makes the operation use the specified content type.
   - `name` (string), e.g.: `zipArchive`. Overrides the body parameter name.
   - `description` (string): `Zip Archive`. Overrides the body parameter description.

## Feature implementation status

|                     | C# (.NET Standard) | Java (Gson) | Java (Jackson) |Python (AsyncIO) | Python (URLlib) |
|---------------------|--------------------|-------------|----------------|------------------|----------------|
|                     |                    |             |                |                  |                |
| **Parameters**      | 100%               | 100%        | 100%           | 100%             | 100%           |
| x-name              | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-description       | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-type              | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-excluded          | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-pattern           | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-value             | [x]                | [x]         | [x]            | [x]              | [x]            |
|                     |                    |             |                |                  |                |
| **Operations**      | 100%               | 100%        | 100%           | 100%             | 100%           |
| x-obsolete          | [x]                | [x]         | [x]            | [x]              | [x]            |
| x-excluded          | [x]                | [x]         | [x]            | [x]              | [x]            |
|                     |                    |             |                |                  |                |
| **Specializations** | 100%               | 100%        | 100%           | 100%             | 100%           |
| BINARY              | [x]                | [x]         | [x]            | [x]              | [x]            |
| XML                 | [x]                | [x]         | [x]            | [x]              | [x]            |
| OBJECT_SINGLE       | [x]                | [x]         | [x]            | [x]              | [x]            |
| OBJECT_LIST         | [x]                | [x]         | [x]            | [x]              | [x]            |
| OBJECT_STREAM       | [x]                | [x]         | [x]            | [x]              | [x]            |
|                     |                    |             |                |                  |                |
| **Variants**        | 100%               | 100%        | 100%           | 100%             | 100%           |
| operationId         | [x]                | [x]         | [x]            | [x]              | [x]            |
| obsolete            | [x]                | [x]         | [x]            | [x]              | [x]            |
| kind                | [x]                | [x]         | [x]            | [x]              | [x]            |
| hardcoded           | [x]                | [x]         | [x]            | [x]              | [x]            |
| excluded            | [x]                | [x]         | [x]            | [x]              | [x]            |
| required            | [x]                | [x]         | [x]            | [x]              | [x]            |
|                     |                    |             |                |                  |                |
| **Behavioural**     | 100%               | 100%        | 100%           | 100%             | 100%           |
| Useful exceptions   | [x]                | [x]         | [x]            | [x]              | [x]            |
| Network resilience  | [x]                | [x]         | [x]            | [x]              | [x]            |
| Streaming           | [x]                | [x]         | [x]            | [x]              | [x]            |
|                     |                    |             |                |                  |                |
| **Meta**            | 100%               | 50%         | 50%            | 50%              | 50%            |
| Decent code         | [x]                | [x]         | [x]            | [x]              | [x]            |
| API Stability       | Good               | Average     | Average        | Average          | Average        |
