# ParametersApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPathParameters**](ParametersApi.md#getPathParameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
[**getQueryParameters**](ParametersApi.md#getQueryParameters) | **GET** /query-parameters | Summary
[**postBinaryBody**](ParametersApi.md#postBinaryBody) | **POST** /binary-body | Summary
[**postBinaryBodyOptional**](ParametersApi.md#postBinaryBodyOptional) | **POST** /binary-body-optional | Summary
[**postExcludedBinaryBody**](ParametersApi.md#postExcludedBinaryBody) | **POST** /excluded-binary-body | Summary
[**postObjectBody**](ParametersApi.md#postObjectBody) | **POST** /object-body | Summary
[**postObjectBodyOptional**](ParametersApi.md#postObjectBodyOptional) | **POST** /object-body-optional | Summary
[**postOverriddenBinaryBody**](ParametersApi.md#postOverriddenBinaryBody) | **POST** /overridden-binary-body | Summary
[**postStringBody**](ParametersApi.md#postStringBody) | **POST** /string-body | Summary
[**postStringBodyOptional**](ParametersApi.md#postStringBodyOptional) | **POST** /string-body-optional | Summary
[**postVariantBodyZipArchive**](ParametersApi.md#postVariantBodyZipArchive) | **POST** /variant-body | Summary
[**postVariantBodyXHTML**](ParametersApi.md#postVariantBodyXHTML) | **POST** /variant-body | Summary
[**postVariantBodyHTML**](ParametersApi.md#postVariantBodyHTML) | **POST** /variant-body | Summary

<a name="getPathParameters"></a>
# **getPathParameters**
> ObjectNode getPathParameters(id1, id2, id3, id4, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
Integer id1 = ...; // Integer | Description
String id2 = ...; // String | Description
Boolean id3 = ...; // Boolean | Description
BigDecimal id4 = ...; // BigDecimal | Description
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.getPathParameters(id1, id2, id3, id4, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#getPathParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id1** | **Integer**| Description |
 **id2** | **String**| Description | [optional]
 **id3** | **Boolean**| Description | [optional]
 **id4** | **BigDecimal**| Description | [optional]
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getQueryParameters"></a>
# **getQueryParameters**
> ObjectNode getQueryParameters(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
Boolean booleanMandatoryWithoutDefault = ...; // Boolean | Description
Integer integerMandatoryWithoutDefault = ...; // Integer | Description
BigDecimal numberMandatoryWithoutDefault = ...; // BigDecimal | Description
String stringMandatoryWithoutDefault = ...; // String | Description
List<Boolean> booleanArrayMandatoryWithoutDefault = ...; // List<Boolean> | Description
List<Integer> integerArrayMandatoryWithoutDefault = ...; // List<Integer> | Description
List<BigDecimal> numberArrayMandatoryWithoutDefault = ...; // List<BigDecimal> | Description
List<String> stringArrayMandatoryWithoutDefault = ...; // List<String> | Description
Map<String, List<String>> patternParameterArrayMandatory = ...; // Map<String, List<String>> | Description
Map<String, String> patternParameterSingleMandatory = ...; // Map<String, String> | Description
int overriddenTypeMandatory = ...; // int | Description
Boolean fail = ...; // Boolean | Description
Boolean booleanOptionalWithDefault = ...; // Boolean | Description
Boolean booleanOptionalWithoutDefault = ...; // Boolean | Description
Integer integerOptionalWithDefault = ...; // Integer | Description
Integer integerOptionalWithoutDefault = ...; // Integer | Description
BigDecimal numberOptionalWithDefault = ...; // BigDecimal | Description
BigDecimal numberOptionalWithoutDefault = ...; // BigDecimal | Description
String stringOptionalWithDefault = ...; // String | Description
String stringOptionalWithoutDefault = ...; // String | Description
List<Boolean> booleanArrayOptionalWithDefault = ...; // List<Boolean> | Description
List<Boolean> booleanArrayOptionalWithoutDefault = ...; // List<Boolean> | Description
List<Integer> integerArrayOptionalWithDefault = ...; // List<Integer> | Description
List<Integer> integerArrayOptionalWithoutDefault = ...; // List<Integer> | Description
List<BigDecimal> numberArrayOptionalWithDefault = ...; // List<BigDecimal> | Description
List<BigDecimal> numberArrayOptionalWithoutDefault = ...; // List<BigDecimal> | Description
List<String> stringArrayOptionalWithDefault = ...; // List<String> | Description
List<String> stringArrayOptionalWithoutDefault = ...; // List<String> | Description
Map<String, List<String>> patternParameterArrayOptional = ...; // Map<String, List<String>> | Description
Map<String, String> patternParameterSingleOptional = ...; // Map<String, String> | Description
Integer overriddenTypeOptional = ...; // Integer | Description
try {
    ObjectNode result = apiInstance.getQueryParameters(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#getQueryParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **booleanMandatoryWithoutDefault** | **Boolean**| Description |
 **integerMandatoryWithoutDefault** | **Integer**| Description |
 **numberMandatoryWithoutDefault** | **BigDecimal**| Description |
 **stringMandatoryWithoutDefault** | **String**| Description |
 **booleanArrayMandatoryWithoutDefault** | [**List&lt;Boolean&gt;**](Boolean.md)| Description |
 **integerArrayMandatoryWithoutDefault** | [**List&lt;Integer&gt;**](Integer.md)| Description |
 **numberArrayMandatoryWithoutDefault** | [**List&lt;BigDecimal&gt;**](BigDecimal.md)| Description |
 **stringArrayMandatoryWithoutDefault** | [**List&lt;String&gt;**](String.md)| Description |
 **patternParameterArrayMandatory** | [**Map&lt;String, List&lt;String&gt;&gt;**](String.md)| Description |
 **patternParameterSingleMandatory** | **Map&lt;String, String&gt;**| Description |
 **overriddenTypeMandatory** | **int**| Description |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **booleanOptionalWithDefault** | **Boolean**| Description | [optional] [default to true]
 **booleanOptionalWithoutDefault** | **Boolean**| Description | [optional]
 **integerOptionalWithDefault** | **Integer**| Description | [optional] [default to 2]
 **integerOptionalWithoutDefault** | **Integer**| Description | [optional]
 **numberOptionalWithDefault** | **BigDecimal**| Description | [optional] [default to 2]
 **numberOptionalWithoutDefault** | **BigDecimal**| Description | [optional]
 **stringOptionalWithDefault** | **String**| Description | [optional] [default to def]
 **stringOptionalWithoutDefault** | **String**| Description | [optional]
 **booleanArrayOptionalWithDefault** | [**List&lt;Boolean&gt;**](Boolean.md)| Description | [optional]
 **booleanArrayOptionalWithoutDefault** | [**List&lt;Boolean&gt;**](Boolean.md)| Description | [optional]
 **integerArrayOptionalWithDefault** | [**List&lt;Integer&gt;**](Integer.md)| Description | [optional]
 **integerArrayOptionalWithoutDefault** | [**List&lt;Integer&gt;**](Integer.md)| Description | [optional]
 **numberArrayOptionalWithDefault** | [**List&lt;BigDecimal&gt;**](BigDecimal.md)| Description | [optional]
 **numberArrayOptionalWithoutDefault** | [**List&lt;BigDecimal&gt;**](BigDecimal.md)| Description | [optional]
 **stringArrayOptionalWithDefault** | [**List&lt;String&gt;**](String.md)| Description | [optional]
 **stringArrayOptionalWithoutDefault** | [**List&lt;String&gt;**](String.md)| Description | [optional]
 **patternParameterArrayOptional** | [**Map&lt;String, List&lt;String&gt;&gt;**](String.md)| Description | [optional]
 **patternParameterSingleOptional** | **Map&lt;String, String&gt;**| Description | [optional]
 **overriddenTypeOptional** | **Integer**| Description | [optional]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postBinaryBody"></a>
# **postBinaryBody**
> byte[] postBinaryBody(bodyName, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
byte[] bodyName = ...; // byte[] | Description
Boolean fail = ...; // Boolean | Description
try {
    byte[] result = apiInstance.postBinaryBody(bodyName, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postBinaryBody");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bodyName** | [**byte[]**](Object.md)| Description |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

[**byte[]**](.md)

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: Not defined

<a name="postBinaryBodyOptional"></a>
# **postBinaryBodyOptional**
> byte[] postBinaryBodyOptional(bodyName, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
byte[] bodyName = ...; // byte[] | Description
Boolean fail = ...; // Boolean | Description
try {
    byte[] result = apiInstance.postBinaryBodyOptional(bodyName, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postBinaryBodyOptional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bodyName** | [**byte[]**](Object.md)| Description | [optional]
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

[**byte[]**](.md)

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: Not defined

<a name="postExcludedBinaryBody"></a>
# **postExcludedBinaryBody**
> String postExcludedBinaryBody()

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
try {
    String result = apiInstance.postExcludedBinaryBody();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postExcludedBinaryBody");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

<a name="postObjectBody"></a>
# **postObjectBody**
> ObjectNode postObjectBody(archive, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
ObjectNode archive = ...; // ObjectNode | An archive item (JSON).
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postObjectBody(archive, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postObjectBody");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**ObjectNode**](Object.md)| An archive item (JSON). |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postObjectBodyOptional"></a>
# **postObjectBodyOptional**
> ObjectNode postObjectBodyOptional(archive, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
ObjectNode archive = ...; // ObjectNode | An archive item (JSON).
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postObjectBodyOptional(archive, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postObjectBodyOptional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**ObjectNode**](Object.md)| An archive item (JSON). | [optional]
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postOverriddenBinaryBody"></a>
# **postOverriddenBinaryBody**
> String postOverriddenBinaryBody(archive)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
byte[] archive = ...; // byte[] | An archive item (XML).
try {
    String result = apiInstance.postOverriddenBinaryBody(archive);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postOverriddenBinaryBody");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**byte[]**](Object.md)| An archive item (XML). |

### Return type

**String**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

<a name="postStringBody"></a>
# **postStringBody**
> String postStringBody(archive, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String archive = ...; // String | An archive item (XML).
Boolean fail = ...; // Boolean | Description
try {
    String result = apiInstance.postStringBody(archive, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postStringBody");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**String**](Object.md)| An archive item (XML). |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**String**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

<a name="postStringBodyOptional"></a>
# **postStringBodyOptional**
> String postStringBodyOptional(archive, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String archive = ...; // String | An archive item (XML).
Boolean fail = ...; // Boolean | Description
try {
    String result = apiInstance.postStringBodyOptional(archive, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postStringBodyOptional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**String**](Object.md)| An archive item (XML). | [optional]
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**String**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

<a name="postVariantBodyZipArchive"></a>
# **postVariantBodyZipArchive**
> ObjectNode postVariantBodyZipArchive(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
byte[] template = ...; // byte[] | A Zip Archive
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyZipArchive(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyZipArchive");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **byte[]**| A Zip Archive |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: application/json

<a name="postVariantBodyZipArchive"></a>
# **postVariantBodyZipArchive**
> ObjectNode postVariantBodyZipArchive(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
byte[] template = ...; // byte[] | A Zip Archive
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyZipArchive(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyZipArchive");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **byte[]**| A Zip Archive |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: application/json

<a name="postVariantBodyXHTML"></a>
# **postVariantBodyXHTML**
> ObjectNode postVariantBodyXHTML(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String template = ...; // String | A XHTML file
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyXHTML(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyXHTML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **String**| A XHTML file |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/xhtml+xml
 - **Accept**: application/json

<a name="postVariantBodyXHTML"></a>
# **postVariantBodyXHTML**
> ObjectNode postVariantBodyXHTML(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String template = ...; // String | A XHTML file
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyXHTML(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyXHTML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **String**| A XHTML file |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: application/xhtml+xml
 - **Accept**: application/json

<a name="postVariantBodyHTML"></a>
# **postVariantBodyHTML**
> ObjectNode postVariantBodyHTML(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String template = ...; // String | A HTML file
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyHTML(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyHTML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **String**| A HTML file |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: text/html
 - **Accept**: application/json

<a name="postVariantBodyHTML"></a>
# **postVariantBodyHTML**
> ObjectNode postVariantBodyHTML(template, fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ParametersApi;

ParametersApi apiInstance = new ParametersApi();
String template = ...; // String | A HTML file
Boolean fail = ...; // Boolean | Description
try {
    ObjectNode result = apiInstance.postVariantBodyHTML(template, fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ParametersApi#postVariantBodyHTML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **String**| A HTML file |
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**ObjectNode**

### HTTP request headers

 - **Content-Type**: text/html
 - **Accept**: application/json

