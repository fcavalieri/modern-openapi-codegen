/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.JSON;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.ApiResponse;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.concurrent.*;

/**
 * Streamed Api Call class.
 * @param <T> Type of the items
 * @param <U> Type of the metadata
  */
public class ApiCallStreamed<T, U> extends ApiCall implements AutoCloseable {
  public static ObjectNode POISON = JsonNodeFactory.instance.objectNode().put("__poison__", true);
  private final int CACHED_ITEMS = 1000;
  private final String RESULT_FIELD = "results";

  private final ExecutorService _executor = Executors.newSingleThreadExecutor();
  private BlockingQueue<ObjectNode> _resultObjects;
  private Future<?> _producer;

  /**
   * The response metadata
   */
  protected U _metadata;

  /**
   * The result iterator
   */
  protected StreamedApiIterator<T> _iterator;
  private ApiException _exception;
  private Type _metadataType;

  private RequestPreliminaryOutcome _requestOutcome;
  private int _currentRetry;

  /// <summary>
  /// The semaphore is locked at request start.
  /// The request is made in another thread and the main thread waits acquiring the semaphore.
  /// Once the request produces a response, it is consumed enough to determine if it is an error or read its metadata.
  /// The response data (requestOutcome/exception/metadata) is then populated.
  /// The semaphore is then released no matter what by the secondary thread.
  /// The main thread processes the request outcome and returns to the caller.
  /// If the request did not produce an error, it is consumed in the background.
  /// Note: no attempt is made to release the semaphore exactly once. It might be released more than once to simplify code.
  /// </summary>
  private Semaphore semaphore;

  public ApiCallStreamed(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type returnType, Type metadataType) {
    super(apiClient, apiRequest, apiMethodName, returnType);

    _metadataType = metadataType;
    _resultObjects = new LinkedBlockingDeque<ObjectNode>(CACHED_ITEMS);
    _iterator = new StreamedApiIterator<T>(this, _resultObjects, returnType);
  }

  /**
   * Gets the api call metadata
   * @return The api call metadata
   */
  public U getMetadata() {
    return _metadata;
  }

  private void startRequest() {
    try {
      LocalDateTime requestTime = LocalDateTime.now();
      Response response = _apiClient.execute(_apiRequest, _apiMethodName);
      _apiResponse = new ApiResponse(response, requestTime);
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      semaphore.release();
      return;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      semaphore.release();
      throw t;
    }

    //There are no retries left or the response has no retryable errors (either success or not retryable)
    try {
      checkResponseStatus(_apiResponse);
      //The response is successful
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      semaphore.release();
      return;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      semaphore.release();
      throw t;
    }

    _producer = _executor.submit(() -> {
      JSON json = new JSON();
      JsonFactory jsonFactory = new JsonFactory();
      //A non-error response was produced. The caller is still waiting to be unlocked
      try (JsonParser jsonParser = jsonFactory.setCodec(json.getMapper()).createParser(_apiResponse.body().charStream())) {
        //A non-error response was produced. The caller is still waiting to be unlocked
        //ProcessMetadata will extract the metadata and no-matter-what release the semaphore
        boolean hasData = processMetadata(jsonParser);

        // Control is returned to the caller of Execute() here, because processMetadata has released the semaphore.
        // The rest is running in the background. Errors are put in the stream
        //ProcessData will process the data items and no-matter-what add the POISON at the end
        if (hasData) {
          processData(jsonParser);
        } else {
          _resultObjects.put(POISON);
        }
      } catch (InterruptedException e) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, e, this);
        throw new RuntimeException(e);
      } catch (IOException e) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, e, this);
        return;
      } catch (Throwable t) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, t, this);
        throw t;
      } finally {
        semaphore.release();
      }
    });
  }

  private boolean processMetadata(JsonParser jsonParser) {
    try {
      _metadata = JSON.parse(readMetadata(jsonParser), _metadataType);
      JsonToken token = jsonParser.currentToken();
      if (JsonToken.END_OBJECT.equals(token)) {
        //Case 1: there is no result field
        _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
        return false;
      } else if (JsonToken.FIELD_NAME.equals(token)) {
        //Case 2: there is a result field
        token = jsonParser.nextToken();
        if (JsonToken.VALUE_NULL.equals(token)) {
          //Case 2a: there is a result field (null)
          readToken(jsonParser, JsonToken.END_OBJECT); // End of response object
          _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
          return false;
        } else if (JsonToken.START_ARRAY.equals(token)) {
          //Case 2b: there is a non-empty result field
          _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
          return true;
        } else {
          throw new ApiException(
                  "Unexpected token " + token + ", expecting " +
                          JsonToken.START_ARRAY + " or " + JsonToken.VALUE_NULL, this);
        }
      } else {
        throw new ApiException(
                "Unexpected token " + token + ", expecting " +
                        JsonToken.END_OBJECT + " or " + JsonToken.FIELD_NAME, this);
      }
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      return false;
    } catch (IOException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      return false;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      throw t;
    } finally {
      semaphore.release();
    }
  }

  private void processData(JsonParser jsonParser) throws InterruptedException {
    try {
        while (true) {
          JsonToken token = readToken(jsonParser, null);
          if (JsonToken.START_OBJECT.equals(token)) {
            ObjectNode obj = jsonParser.readValueAs(ObjectNode.class);
            _resultObjects.put(obj); //Blocking call
          } else if (JsonToken.END_ARRAY.equals(token)) {
            break;
          }
          else {
            throw new ApiException(
                    "Unexpected token " + token + ", expecting " +
                            JsonToken.START_OBJECT + " or " + JsonToken.END_ARRAY, this);
          }
        }

        readToken(jsonParser, JsonToken.END_OBJECT); // End of response object
        jsonParser.nextToken();
        if (!jsonParser.isClosed())
          throw new ApiException("Unexpected " + jsonParser.currentToken() + " at the end of response", this);
    } catch (InterruptedException e) {
      _resultObjects.put(throwableToJsonError(e));
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      throw e;
    } catch (Exception e) {
      //Other exceptions are thrown in case of communication errors
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      _resultObjects.put(throwableToJsonError(e));
    } catch (Throwable t) {
      //Other exceptions are thrown in case of communication errors
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      _resultObjects.put(throwableToJsonError(t));
      throw t;
    }

    finally {
      //Signal any consumer that the stream has been fully produced
      _resultObjects.put(POISON);
    }
  }

  private ObjectNode throwableToJsonError(Throwable t) {
    ObjectNode error = JsonNodeFactory.instance.objectNode()
      .put("__error__", true)
      .put("status", 500)
      .put("type", "Internal Error")
      .put("code", "STREAMING_ERROR")
      .put("message", t.toString());
    return error;
  }

  /**
   * Executes the api call
   * @throws ApiException api exception
   */
  public void execute() throws ApiException {
    for (; ; ) {
      semaphore = new Semaphore(0);
      startRequest();

      try {
        semaphore.acquire(); // Waits for the metadata/error to be produced
        switch (_requestOutcome)
        {
          case SUCCESS:
            return;
          case RETRY:
            break;
          case FAILURE:
            if (_producer != null) {
              _producer.cancel(true);
            }
            throw _exception;
          default:
            throw new ApiException("Unexpected request outcome: " + _requestOutcome, this);
        }

      } catch (InterruptedException e) {
        throw new ApiException("Error calling " + _apiMethodName, e, this);
      }

      return;
    }
  }

  private JsonToken readToken(JsonParser jsonParser, JsonToken tokenType) throws IOException, ApiException {
    JsonToken jsonToken = jsonParser.nextToken();
    if (jsonToken == null)
      throw new ApiException("Nothing to read while looking for " + tokenType, this);
    if (tokenType != null && !tokenType.equals(jsonToken))
      throw new ApiException("Unexpected token " + jsonToken + " while looking for " + tokenType, this);
    return jsonToken;
  }

  @SuppressWarnings("deprecation")
  private ObjectNode readMetadata(JsonParser jsonParser) throws ApiException {
    try {
      ObjectNode ret = JsonNodeFactory.instance.objectNode();
      readToken(jsonParser, JsonToken.START_OBJECT);
      while (true) {
        JsonToken nextToken = readToken(jsonParser, null);
        if (JsonToken.END_OBJECT.equals(nextToken)) {
          break;
        } else if (JsonToken.FIELD_NAME.equals(nextToken)) {
          String fieldName = jsonParser.getCurrentName();
          if (fieldName.equals(RESULT_FIELD)) // Field with list of items
            break;
          readToken(jsonParser, null);
          ret.put(fieldName, jsonParser.readValueAs(JsonNode.class));
        }
        else {
          throw new ApiException("Unexpected token " + nextToken + " while looking for FIELD_NAME or END_OBJECT", this);
        }
      }
      return ret;
    } catch (IllegalStateException | IOException e) {
      throw new ApiException("Error calling " + _apiMethodName, e, this);
    }
  }

  @Override
  public void close() {
    if (_producer != null)
      _producer.cancel(true);
  }

  private enum RequestPreliminaryOutcome {SUCCESS, RETRY, FAILURE}
}