/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.StreamedSequence;
import com.reportix.cellstore.model.IStream;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Object stream Api Call class.
 * @param <T> Type of the items
 * @param <U> Type of the metadata
 */
public class ApiCallObjectStream<T, U> extends ApiCallStreamed<T, U> {
  private IStream<T, U> _result;

  /**
   * Api call object stream constructor
   * @param apiClient the api client
   * @param apiRequest the api request
   * @param apiMethodName the api method name
   * @param returnType the return type
   */
  public ApiCallObjectStream(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type returnType) {
    super(apiClient, apiRequest, apiMethodName, ((ParameterizedType)returnType).getActualTypeArguments()[0], ((ParameterizedType)returnType).getActualTypeArguments()[1]);
  }

  /**
   * Gets the api call result
   * @return The api call result
   */
  public IStream<T, U> getResult() {
    return _result;
  }

  /**
   * Executes the api call
   * @throws ApiException api exception
   */
  public void execute() throws ApiException {
    super.execute();
    _result = new StreamedSequence<T, U>(_iterator, _metadata);
  }
}