/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.JSON;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;

/**
 * Streamed Api iterator class.
 */
public class StreamedApiIterator<T> implements Iterator<T> {
  private final String ERROR_FIELD = "__error__";
  private final String STATUS_FIELD = "status";
  private final ApiCallStreamed<T, ?> _apiCall;
  private final BlockingQueue<ObjectNode> _results;
  private final Type _returnType;

  private ObjectNode _current;
  private boolean _atEnd;

  /**
   * Streamed api constructor
   * @param apiCall the api call
   * @param results the results queue
   * @param returnType the return type
   */
  public StreamedApiIterator(ApiCallStreamed<T, ?> apiCall, BlockingQueue<ObjectNode> results, Type returnType) {
    _apiCall = apiCall;
    _results = results;
    _returnType = returnType;
    _current = null;
    _atEnd = false;
  }

  /**
   * Returns whether there is another item in the sequence or not
   * @return whether there is another item in the sequence or not
   */
  @Override
  public boolean hasNext() {
    if (_atEnd)
      return false;
    if (_current != null)
      return true;
    try {
      _current = _results.take();
    } catch (InterruptedException e) {
      return false;
    }
    if (ApiCallStreamed.POISON.equals(_current)) {
      _atEnd = true;
      return false;
    }
    return true;
  }

  /**
   * Returns the next item in the sequence
   * @return the next item in the sequence
   * @deprecated This method throws RuntimeException in case of error. Use nextChecked() which throws ApiException.
   */
  @Override
  @Deprecated
  public T next() {
    try {
      return nextChecked();
    } catch (ApiException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Returns the next item in the sequence
   * @return the next item in the sequence
   * @throws ApiException an api exception
   */
  public T nextChecked() throws ApiException {

    if (!hasNext())
      throw new NoSuchElementException();

    T ret;

    if (!_current.has(ERROR_FIELD)) {
      ret = JSON.parse(_current, _returnType);
      _current = null;
      return ret;
    }

    if (_current.get(ERROR_FIELD).isBoolean()) {
      if (_current.get(ERROR_FIELD).asBoolean()) {
        if (_current.get(STATUS_FIELD).isNumber()) {
          throw new ApiException(_current.toString(), _apiCall, _current.get(STATUS_FIELD).asInt());
        } else {
          throw new ApiException("An error object with an invalid " + STATUS_FIELD + " field has been produced: " + _current.toString(), _apiCall);
        }
      } else {
        ret = JSON.parse(_current, _returnType);
        _current = null;
        return ret;
      }
    } else {
      throw new ApiException("An object with an invalid " + ERROR_FIELD + " field has been produced: " + _current.toString(), _apiCall);
    }
  }
}