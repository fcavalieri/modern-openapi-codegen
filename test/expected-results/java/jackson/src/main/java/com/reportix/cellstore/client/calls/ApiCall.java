/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.reportix.cellstore.client.*;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.ApiResponse;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;

/**
 * Base Api Call class.
 */
public abstract class ApiCall {
  protected ApiResponse _apiResponse;
  protected ApiRequest _apiRequest;
  protected ApiClient _apiClient;
  protected String _apiMethodName;
  protected Type _itemType;
  protected JSON _json;

  /**
   * Gets the api response
   * @return The api response
   */
  public ApiResponse getApiResponse() {
    return _apiResponse;
  }

  /**
   * Gets the api request
   * @return The api request
   */
  public ApiRequest getApiRequest() {
    return _apiRequest;
  }

  /**
   * Gets the api client
   * @return The api client
   */
  public ApiClient getApiClient() {
    return _apiClient;
  }

  /**
   * Api call base class
   */
  protected ApiCall(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type itemType) {
    _apiClient = apiClient;
    _apiRequest = apiRequest;
    _apiMethodName = apiMethodName;
    _itemType = itemType;
    _json = new JSON();
  }

  public void checkResponseStatus(ApiResponse response) throws ApiException {
    boolean allRetryable = UnitTestConfiguration.isAllRequestsHaveRetryableErrors(_apiClient.getConfiguration());

    int status = response.code();
    if (status >= 400 || allRetryable) {
      try {
        throw new ApiException("Error calling " + _apiMethodName + ": " + response.body().string(), this, status);
      } catch (IOException e) {
        throw new ApiException("Error calling " + _apiMethodName, this, status);
      }
    }
  }

  protected int delayMs(int currentRetry)
  {
    return (currentRetry + 1) * 10  * 1000;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Request:" + "\n");
    sb.append(_apiRequest.toString() + "\n");
    sb.append("-------------------------" + "\n");
    sb.append("Response:" + "\n");
    if (_apiResponse != null) {
      sb.append(_apiResponse + "\n");
    }

    if (_apiClient != null && _apiClient.getConfiguration() != null) {
      sb.append("===Configuration===" + "\n");
      sb.append(_apiClient.getConfiguration().toString() + "\n");
    }

    return sb.toString();
  }

  public abstract void execute() throws ApiException;
}
