package com.reportix.cellstore.client;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import com.google.common.reflect.TypeToken;
import java.lang.reflect.Type;
import java.io.IOException;
import java.text.DateFormat;

/**
 * JSON-Jackson utility class.
 */
public class JSON {
  private ObjectMapper mapper;
  public static Type JSON_OBJECT_TYPE = new TypeToken<ObjectNode>() {}.getType();
  public static ObjectNode POISON = JsonNodeFactory.instance.objectNode().put("__poison__", true);

  public JSON() {
    mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
    mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
    mapper.registerModule(new JavaTimeModule());
  }

  /**
   * Serialize the given Java object into JSON string.
   *
   * @param obj Object
   * @return String representation of the JSON
   */
  public String serialize(Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Deserialize the given JSON string to Java object.
   *
   * @param <T>        Type
   * @param body       The JSON string
   * @param returnType The type to deserialize into
   * @return The deserialized Java object
   */
  @SuppressWarnings("unchecked")
  public <T> T deserialize(String body, Type returnType) {
    try {
      if (body == null || body.isEmpty())
        return null;
      return (T) mapper.readValue(body, mapper.constructType(returnType));
    } catch (IOException e) {
      // Fallback processing when failed to parse JSON form response body:
      // return the response body string directly for the String return type;
      if (returnType.equals(String.class))
        return (T) body;
      else
        throw new RuntimeException(e);
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> T parse(ObjectNode rawObject, Type returnType) throws ApiException {
    if (returnType.equals(ObjectNode.class))
      return (T) rawObject;
    else
      throw new ApiException("Unsupported return type " + returnType.getTypeName());
  }

  public ObjectMapper getMapper() {
    return mapper;
  }
}