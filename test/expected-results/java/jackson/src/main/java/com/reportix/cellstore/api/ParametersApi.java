/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.Configuration;
import com.reportix.cellstore.client.ProgressRequestBody;
import com.reportix.cellstore.client.ProgressResponseBody;

import com.reportix.cellstore.client.authentication.*;
import com.reportix.cellstore.client.calls.*;
import com.reportix.cellstore.model.*;
import com.reportix.cellstore.client.requests.*;
import com.reportix.cellstore.client.responses.*;

import com.google.common.reflect.TypeToken;

import java.io.IOException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Api methods for ParametersApi.
 */
public class ParametersApi extends ApiBase {

  /**
   * ParametersApi constructor.
   * @param basePath the base path
   * @throws ApiException api exception
   */
  public ParametersApi(String basePath) throws ApiException {
    super(basePath, null);
  }

  /**
   * ParametersApi constructor.
   * @param basePath the base path
   * @param configuration the configuration
   * @throws ApiException api exception
   */
  public ParametersApi(String basePath, Configuration configuration) throws ApiException {
    super(basePath, configuration);
  }

  /**
   * Build call for getPathParameters
   * @param id1 Description (required)
   * @param id2 Description (optional)
   * @param id3 Description (optional)
   * @param id4 Description (optional)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getPathParametersBuildRequest(Integer id1, String id2, Boolean id3, BigDecimal id4, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'id1' is set
    if (id1 == null) {
      throw new ApiException("Missing the required parameter 'id1' when calling ParametersApi->getPathParameters");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/path-parameters/{id1}/path/{id2}/{id3}/path/{id4}", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (id1 != null) __ApiRequest.addPathParameter("id1", id1); // path parameter
    if (id2 != null) __ApiRequest.addPathParameter("id2", id2); // path parameter
    if (id3 != null) __ApiRequest.addPathParameter("id3", id3); // path parameter
    if (id4 != null) __ApiRequest.addPathParameter("id4", id4); // path parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param id1 Description (required)
   * @param id2 Description (optional)
   * @param id3 Description (optional)
   * @param id4 Description (optional)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode getPathParameters(Integer id1, String id2, Boolean id3, BigDecimal id4, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = getPathParametersBuildRequest(id1, id2, id3, id4, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "getPathParameters", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getQueryParameters
   * @param booleanMandatoryWithoutDefault Description (required)
   * @param integerMandatoryWithoutDefault Description (required)
   * @param numberMandatoryWithoutDefault Description (required)
   * @param stringMandatoryWithoutDefault Description (required)
   * @param booleanArrayMandatoryWithoutDefault Description (required)
   * @param integerArrayMandatoryWithoutDefault Description (required)
   * @param numberArrayMandatoryWithoutDefault Description (required)
   * @param stringArrayMandatoryWithoutDefault Description (required)
   * @param patternParameterArrayMandatory Description (required)
   * @param patternParameterSingleMandatory Description (required)
   * @param overriddenTypeMandatory Description (required)
   * @param fail Description (optional, defaults to false)
   * @param booleanOptionalWithDefault Description (optional, defaults to true)
   * @param booleanOptionalWithoutDefault Description (optional)
   * @param integerOptionalWithDefault Description (optional, defaults to 2)
   * @param integerOptionalWithoutDefault Description (optional)
   * @param numberOptionalWithDefault Description (optional, defaults to 2)
   * @param numberOptionalWithoutDefault Description (optional)
   * @param stringOptionalWithDefault Description (optional, defaults to def)
   * @param stringOptionalWithoutDefault Description (optional)
   * @param booleanArrayOptionalWithDefault Description (optional)
   * @param booleanArrayOptionalWithoutDefault Description (optional)
   * @param integerArrayOptionalWithDefault Description (optional)
   * @param integerArrayOptionalWithoutDefault Description (optional)
   * @param numberArrayOptionalWithDefault Description (optional)
   * @param numberArrayOptionalWithoutDefault Description (optional)
   * @param stringArrayOptionalWithDefault Description (optional)
   * @param stringArrayOptionalWithoutDefault Description (optional)
   * @param patternParameterArrayOptional Description (optional)
   * @param patternParameterSingleOptional Description (optional)
   * @param overriddenTypeOptional Description (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getQueryParametersBuildRequest(Boolean booleanMandatoryWithoutDefault, Integer integerMandatoryWithoutDefault, BigDecimal numberMandatoryWithoutDefault, String stringMandatoryWithoutDefault, List<Boolean> booleanArrayMandatoryWithoutDefault, List<Integer> integerArrayMandatoryWithoutDefault, List<BigDecimal> numberArrayMandatoryWithoutDefault, List<String> stringArrayMandatoryWithoutDefault, Map<String, List<String>> patternParameterArrayMandatory, Map<String, String> patternParameterSingleMandatory, int overriddenTypeMandatory, Boolean fail, Boolean booleanOptionalWithDefault, Boolean booleanOptionalWithoutDefault, Integer integerOptionalWithDefault, Integer integerOptionalWithoutDefault, BigDecimal numberOptionalWithDefault, BigDecimal numberOptionalWithoutDefault, String stringOptionalWithDefault, String stringOptionalWithoutDefault, List<Boolean> booleanArrayOptionalWithDefault, List<Boolean> booleanArrayOptionalWithoutDefault, List<Integer> integerArrayOptionalWithDefault, List<Integer> integerArrayOptionalWithoutDefault, List<BigDecimal> numberArrayOptionalWithDefault, List<BigDecimal> numberArrayOptionalWithoutDefault, List<String> stringArrayOptionalWithDefault, List<String> stringArrayOptionalWithoutDefault, Map<String, List<String>> patternParameterArrayOptional, Map<String, String> patternParameterSingleOptional, Integer overriddenTypeOptional, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'booleanMandatoryWithoutDefault' is set
    if (booleanMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'booleanMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'integerMandatoryWithoutDefault' is set
    if (integerMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'integerMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'numberMandatoryWithoutDefault' is set
    if (numberMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'numberMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'stringMandatoryWithoutDefault' is set
    if (stringMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'stringMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'booleanArrayMandatoryWithoutDefault' is set
    if (booleanArrayMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'booleanArrayMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'integerArrayMandatoryWithoutDefault' is set
    if (integerArrayMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'integerArrayMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'numberArrayMandatoryWithoutDefault' is set
    if (numberArrayMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'numberArrayMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'stringArrayMandatoryWithoutDefault' is set
    if (stringArrayMandatoryWithoutDefault == null) {
      throw new ApiException("Missing the required parameter 'stringArrayMandatoryWithoutDefault' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'patternParameterArrayMandatory' is set
    if (patternParameterArrayMandatory == null) {
      throw new ApiException("Missing the required parameter 'patternParameterArrayMandatory' when calling ParametersApi->getQueryParameters");
    }
    // verify the required parameter 'patternParameterSingleMandatory' is set
    if (patternParameterSingleMandatory == null) {
      throw new ApiException("Missing the required parameter 'patternParameterSingleMandatory' when calling ParametersApi->getQueryParameters");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/query-parameters", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("boolean-hardcoded", "true"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("integer-hardcoded", "3"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("number-hardcoded", "987654321.987654321"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("string-hardcoded", "value"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (booleanMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("boolean-mandatory-without-default", booleanMandatoryWithoutDefault); // query parameter
    if (booleanOptionalWithDefault != null) __ApiRequest.addQueryParameter("boolean-optional-with-default", booleanOptionalWithDefault); // query parameter
    if (booleanOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("boolean-optional-without-default", booleanOptionalWithoutDefault); // query parameter
    if (integerMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("integer-mandatory-without-default", integerMandatoryWithoutDefault); // query parameter
    if (integerOptionalWithDefault != null) __ApiRequest.addQueryParameter("integer-optional-with-default", integerOptionalWithDefault); // query parameter
    if (integerOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("integer-optional-without-default", integerOptionalWithoutDefault); // query parameter
    if (numberMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("number-mandatory-without-default", numberMandatoryWithoutDefault); // query parameter
    if (numberOptionalWithDefault != null) __ApiRequest.addQueryParameter("number-optional-with-default", numberOptionalWithDefault); // query parameter
    if (numberOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("number-optional-without-default", numberOptionalWithoutDefault); // query parameter
    if (stringMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("string-mandatory-without-default", stringMandatoryWithoutDefault); // query parameter
    if (stringOptionalWithDefault != null) __ApiRequest.addQueryParameter("string-optional-with-default", stringOptionalWithDefault); // query parameter
    if (stringOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("string-optional-without-default", stringOptionalWithoutDefault); // query parameter
    if (booleanArrayMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("boolean-array-mandatory-without-default", booleanArrayMandatoryWithoutDefault); // query parameter
    if (booleanArrayOptionalWithDefault != null) __ApiRequest.addQueryParameter("boolean-array-optional-with-default", booleanArrayOptionalWithDefault); // query parameter
    if (booleanArrayOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("boolean-array-optional-without-default", booleanArrayOptionalWithoutDefault); // query parameter
    if (integerArrayMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("integer-array-mandatory-without-default", integerArrayMandatoryWithoutDefault); // query parameter
    if (integerArrayOptionalWithDefault != null) __ApiRequest.addQueryParameter("integer-array-optional-with-default", integerArrayOptionalWithDefault); // query parameter
    if (integerArrayOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("integer-array-optional-without-default", integerArrayOptionalWithoutDefault); // query parameter
    if (numberArrayMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("number-array-mandatory-without-default", numberArrayMandatoryWithoutDefault); // query parameter
    if (numberArrayOptionalWithDefault != null) __ApiRequest.addQueryParameter("number-array-optional-with-default", numberArrayOptionalWithDefault); // query parameter
    if (numberArrayOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("number-array-optional-without-default", numberArrayOptionalWithoutDefault); // query parameter
    if (stringArrayMandatoryWithoutDefault != null) __ApiRequest.addQueryParameter("string-array-mandatory-without-default", stringArrayMandatoryWithoutDefault); // query parameter
    if (stringArrayOptionalWithDefault != null) __ApiRequest.addQueryParameter("string-array-optional-with-default", stringArrayOptionalWithDefault); // query parameter
    if (stringArrayOptionalWithoutDefault != null) __ApiRequest.addQueryParameter("string-array-optional-without-default", stringArrayOptionalWithoutDefault); // query parameter
    if (overriddenTypeOptional != null) __ApiRequest.addQueryParameter("overridden-type-optional", overriddenTypeOptional); // query parameter
    __ApiRequest.addQueryParameter("overridden-type-mandatory", overriddenTypeMandatory); // query parameter
    if (patternParameterArrayOptional != null) __ApiRequest.addPatternQueryParameter(patternParameterArrayOptional, "^([^!:]+:[^:]+)$"); // pattern query parameter
    if (patternParameterSingleOptional != null) __ApiRequest.addPatternQueryParameter(patternParameterSingleOptional, "^([^:]+:[^:]+)$"); // pattern query parameter
    if (patternParameterArrayMandatory != null) __ApiRequest.addPatternQueryParameter(patternParameterArrayMandatory, "^([^!:]+:[^:]+)$"); // pattern query parameter
    if (patternParameterSingleMandatory != null) __ApiRequest.addPatternQueryParameter(patternParameterSingleMandatory, "^([^:]+:[^:]+)$"); // pattern query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param booleanMandatoryWithoutDefault Description (required)
   * @param integerMandatoryWithoutDefault Description (required)
   * @param numberMandatoryWithoutDefault Description (required)
   * @param stringMandatoryWithoutDefault Description (required)
   * @param booleanArrayMandatoryWithoutDefault Description (required)
   * @param integerArrayMandatoryWithoutDefault Description (required)
   * @param numberArrayMandatoryWithoutDefault Description (required)
   * @param stringArrayMandatoryWithoutDefault Description (required)
   * @param patternParameterArrayMandatory Description (required)
   * @param patternParameterSingleMandatory Description (required)
   * @param overriddenTypeMandatory Description (required)
   * @param fail Description (optional, defaults to false)
   * @param booleanOptionalWithDefault Description (optional, defaults to true)
   * @param booleanOptionalWithoutDefault Description (optional)
   * @param integerOptionalWithDefault Description (optional, defaults to 2)
   * @param integerOptionalWithoutDefault Description (optional)
   * @param numberOptionalWithDefault Description (optional, defaults to 2)
   * @param numberOptionalWithoutDefault Description (optional)
   * @param stringOptionalWithDefault Description (optional, defaults to def)
   * @param stringOptionalWithoutDefault Description (optional)
   * @param booleanArrayOptionalWithDefault Description (optional)
   * @param booleanArrayOptionalWithoutDefault Description (optional)
   * @param integerArrayOptionalWithDefault Description (optional)
   * @param integerArrayOptionalWithoutDefault Description (optional)
   * @param numberArrayOptionalWithDefault Description (optional)
   * @param numberArrayOptionalWithoutDefault Description (optional)
   * @param stringArrayOptionalWithDefault Description (optional)
   * @param stringArrayOptionalWithoutDefault Description (optional)
   * @param patternParameterArrayOptional Description (optional)
   * @param patternParameterSingleOptional Description (optional)
   * @param overriddenTypeOptional Description (optional)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode getQueryParameters(Boolean booleanMandatoryWithoutDefault, Integer integerMandatoryWithoutDefault, BigDecimal numberMandatoryWithoutDefault, String stringMandatoryWithoutDefault, List<Boolean> booleanArrayMandatoryWithoutDefault, List<Integer> integerArrayMandatoryWithoutDefault, List<BigDecimal> numberArrayMandatoryWithoutDefault, List<String> stringArrayMandatoryWithoutDefault, Map<String, List<String>> patternParameterArrayMandatory, Map<String, String> patternParameterSingleMandatory, int overriddenTypeMandatory, Boolean fail, Boolean booleanOptionalWithDefault, Boolean booleanOptionalWithoutDefault, Integer integerOptionalWithDefault, Integer integerOptionalWithoutDefault, BigDecimal numberOptionalWithDefault, BigDecimal numberOptionalWithoutDefault, String stringOptionalWithDefault, String stringOptionalWithoutDefault, List<Boolean> booleanArrayOptionalWithDefault, List<Boolean> booleanArrayOptionalWithoutDefault, List<Integer> integerArrayOptionalWithDefault, List<Integer> integerArrayOptionalWithoutDefault, List<BigDecimal> numberArrayOptionalWithDefault, List<BigDecimal> numberArrayOptionalWithoutDefault, List<String> stringArrayOptionalWithDefault, List<String> stringArrayOptionalWithoutDefault, Map<String, List<String>> patternParameterArrayOptional, Map<String, String> patternParameterSingleOptional, Integer overriddenTypeOptional) throws ApiException
  {
    ApiRequest __ApiRequest = getQueryParametersBuildRequest(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "getQueryParameters", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postBinaryBody
   * @param bodyName Description (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postBinaryBodyBuildRequest(byte[] bodyName, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'bodyName' is set
    if (bodyName == null) {
      throw new ApiException("Missing the required parameter 'bodyName' when calling ParametersApi->postBinaryBody");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/binary-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (bodyName != null) __ApiRequest.addBodyParameter("application/zip", bodyName); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param bodyName Description (required)
   * @param fail Description (optional, defaults to false)
   * @return byte[]
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public byte[] postBinaryBody(byte[] bodyName, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postBinaryBodyBuildRequest(bodyName, fail, null, null);
    Type __ReturnType = new TypeToken<byte[]>(){}.getType();
    ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(_apiClient, __ApiRequest, "postBinaryBody", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postBinaryBodyOptional
   * @param bodyName Description (optional)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postBinaryBodyOptionalBuildRequest(byte[] bodyName, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/binary-body-optional", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (bodyName != null) __ApiRequest.addBodyParameter("application/zip", bodyName); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param bodyName Description (optional)
   * @param fail Description (optional, defaults to false)
   * @return byte[]
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public byte[] postBinaryBodyOptional(byte[] bodyName, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postBinaryBodyOptionalBuildRequest(bodyName, fail, null, null);
    Type __ReturnType = new TypeToken<byte[]>(){}.getType();
    ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(_apiClient, __ApiRequest, "postBinaryBodyOptional", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postExcludedBinaryBody
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postExcludedBinaryBodyBuildRequest(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/excluded-binary-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String postExcludedBinaryBody() throws ApiException
  {
    ApiRequest __ApiRequest = postExcludedBinaryBodyBuildRequest(null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "postExcludedBinaryBody", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postObjectBody
   * @param archive An archive item (JSON). (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postObjectBodyBuildRequest(ObjectNode archive, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'archive' is set
    if (archive == null) {
      throw new ApiException("Missing the required parameter 'archive' when calling ParametersApi->postObjectBody");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/object-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (archive != null) __ApiRequest.addBodyParameter("application/json", archive); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param archive An archive item (JSON). (required)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode postObjectBody(ObjectNode archive, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postObjectBodyBuildRequest(archive, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "postObjectBody", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postObjectBodyOptional
   * @param archive An archive item (JSON). (optional)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postObjectBodyOptionalBuildRequest(ObjectNode archive, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/object-body-optional", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (archive != null) __ApiRequest.addBodyParameter("application/json", archive); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param archive An archive item (JSON). (optional)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode postObjectBodyOptional(ObjectNode archive, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postObjectBodyOptionalBuildRequest(archive, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "postObjectBodyOptional", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postOverriddenBinaryBody
   * @param archive An archive item (XML). (required)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postOverriddenBinaryBodyBuildRequest(byte[] archive, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'archive' is set
    if (archive == null) {
      throw new ApiException("Missing the required parameter 'archive' when calling ParametersApi->postOverriddenBinaryBody");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/overridden-binary-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (archive != null) __ApiRequest.addBodyParameter("application/xml", archive); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param archive An archive item (XML). (required)
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String postOverriddenBinaryBody(byte[] archive) throws ApiException
  {
    ApiRequest __ApiRequest = postOverriddenBinaryBodyBuildRequest(archive, null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "postOverriddenBinaryBody", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postStringBody
   * @param archive An archive item (XML). (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postStringBodyBuildRequest(String archive, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'archive' is set
    if (archive == null) {
      throw new ApiException("Missing the required parameter 'archive' when calling ParametersApi->postStringBody");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/string-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (archive != null) __ApiRequest.addBodyParameter("application/xml", archive); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param archive An archive item (XML). (required)
   * @param fail Description (optional, defaults to false)
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String postStringBody(String archive, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postStringBodyBuildRequest(archive, fail, null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "postStringBody", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postStringBodyOptional
   * @param archive An archive item (XML). (optional)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postStringBodyOptionalBuildRequest(String archive, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/string-body-optional", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (archive != null) __ApiRequest.addBodyParameter("application/xml", archive); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param archive An archive item (XML). (optional)
   * @param fail Description (optional, defaults to false)
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String postStringBodyOptional(String archive, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postStringBodyOptionalBuildRequest(archive, fail, null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "postStringBodyOptional", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postVariantBodyZipArchive
   * @param template A Zip Archive (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postVariantBodyZipArchiveBuildRequest(byte[] template, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'template' is set
    if (template == null) {
      throw new ApiException("Missing the required parameter 'template' when calling ParametersApi->postVariantBodyZipArchive");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/variant-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (template != null) __ApiRequest.addBodyParameter("application/zip", template); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param template A Zip Archive (required)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode postVariantBodyZipArchive(byte[] template, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postVariantBodyZipArchiveBuildRequest(template, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "postVariantBodyZipArchive", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postVariantBodyXHTML
   * @param template A XHTML file (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postVariantBodyXHTMLBuildRequest(String template, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'template' is set
    if (template == null) {
      throw new ApiException("Missing the required parameter 'template' when calling ParametersApi->postVariantBodyXHTML");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/variant-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (template != null) __ApiRequest.addBodyParameter("application/xhtml+xml", template); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param template A XHTML file (required)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode postVariantBodyXHTML(String template, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postVariantBodyXHTMLBuildRequest(template, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "postVariantBodyXHTML", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postVariantBodyHTML
   * @param template A HTML file (required)
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postVariantBodyHTMLBuildRequest(String template, Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'template' is set
    if (template == null) {
      throw new ApiException("Missing the required parameter 'template' when calling ParametersApi->postVariantBodyHTML");
    }

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/variant-body", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (template != null) __ApiRequest.addBodyParameter("text/html", template); // body parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param template A HTML file (required)
   * @param fail Description (optional, defaults to false)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ObjectNode postVariantBodyHTML(String template, Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postVariantBodyHTMLBuildRequest(template, fail, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "postVariantBodyHTML", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
}
