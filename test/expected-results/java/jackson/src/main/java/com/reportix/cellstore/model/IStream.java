/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.model;

/**
* Represents a sequence of items of type T that can be iterated only once.
* The type of the metadata is U.
* @param <T> The type of the objects.
* @param <U> The type of the metadata.
*/
public interface IStream<T, U> extends Iterable<T> {
  U getMetadata();
}