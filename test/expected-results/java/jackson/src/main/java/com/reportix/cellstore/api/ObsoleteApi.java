/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.Configuration;
import com.reportix.cellstore.client.ProgressRequestBody;
import com.reportix.cellstore.client.ProgressResponseBody;

import com.reportix.cellstore.client.authentication.*;
import com.reportix.cellstore.client.calls.*;
import com.reportix.cellstore.model.*;
import com.reportix.cellstore.client.requests.*;
import com.reportix.cellstore.client.responses.*;

import com.google.common.reflect.TypeToken;

import java.io.IOException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Api methods for ObsoleteApi.
 */
public class ObsoleteApi extends ApiBase {

  /**
   * ObsoleteApi constructor.
   * @param basePath the base path
   * @throws ApiException api exception
   */
  public ObsoleteApi(String basePath) throws ApiException {
    super(basePath, null);
  }

  /**
   * ObsoleteApi constructor.
   * @param basePath the base path
   * @param configuration the configuration
   * @throws ApiException api exception
   */
  public ObsoleteApi(String basePath, Configuration configuration) throws ApiException {
    super(basePath, configuration);
  }

  /**
   * Build call for getObsoleteOperation
   * @param limit Limits the amount of documents in the result of the query.  (optional, defaults to 50)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object
   * @deprecated This operation is obsolete
   */
  @Deprecated
  private ApiRequest getObsoleteOperationBuildRequest(Integer limit, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/obsolete-operation", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (limit != null) __ApiRequest.addQueryParameter("limit", limit); // query parameter
    return __ApiRequest;
  }

  /**
   * Obsolete summary
   * Obsolete description
   * @param limit Limits the amount of documents in the result of the query.  (optional, defaults to 50)
   * @return ObjectNode
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
   * @deprecated This operation is obsolete
   */
  @Deprecated
  public ObjectNode getObsoleteOperation(Integer limit) throws ApiException
  {
    ApiRequest __ApiRequest = getObsoleteOperationBuildRequest(limit, null, null);
    Type __ReturnType = new TypeToken<ObjectNode>(){}.getType();
    ApiCallSingleton<ObjectNode> __ApiCall = new ApiCallSingleton<ObjectNode>(_apiClient, __ApiRequest, "getObsoleteOperation", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
}
