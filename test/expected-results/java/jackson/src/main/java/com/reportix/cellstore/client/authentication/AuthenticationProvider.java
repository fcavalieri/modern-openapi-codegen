/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.authentication;

import com.reportix.cellstore.client.requests.ApiRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import lombok.Data;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * Authentication provider base class.
 */
@Data
public abstract class AuthenticationProvider implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * Transforms the request adding authentication information
   * @param httpClient The HTTP client
   * @param apiRequest The Api request
   */
  public abstract void addAuthentication(OkHttpClient httpClient, ApiRequest apiRequest);

  /**
   * Transforms the response processing authentication information
   * @param httpClient The HTTP client
   * @param apiRequest The Api request
   * @param response The Api response
   */
  public abstract void processResponse(OkHttpClient httpClient, ApiRequest apiRequest, Response response);

  /**
   * Returns the set of parameters to not serialize in diagnostic messages
   * @return the set of parameters to not serialize in diagnostic messages
   */
  public abstract Set<String> getSensitiveParameters();
}
