/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import okio.*;

import java.io.IOException;

/**
 * Progress request body class.
 */
public class ProgressRequestBody extends RequestBody {

  private final RequestBody _requestBody;
  private final ProgressRequestListener _progressListener;

  public ProgressRequestBody(RequestBody requestBody, ProgressRequestListener progressListener) {
    _requestBody = requestBody;
    _progressListener = progressListener;
  }

  @Override
  public MediaType contentType() {
    return _requestBody.contentType();
  }

  @Override
  public long contentLength() throws IOException {
    return _requestBody.contentLength();
  }

  @Override
  public void writeTo(BufferedSink sink) throws IOException {
    BufferedSink bufferedSink = Okio.buffer(sink(sink));
    _requestBody.writeTo(bufferedSink);
    bufferedSink.flush();
  }

  private Sink sink(Sink sink) {
    return new ForwardingSink(sink) {

      long bytesWritten = 0L;
      long contentLength = 0L;

      @Override
      public void write(Buffer source, long byteCount) throws IOException {
        super.write(source, byteCount);
        if (contentLength == 0) {
          contentLength = contentLength();
        }

        bytesWritten += byteCount;
        _progressListener.onRequestProgress(bytesWritten, contentLength, bytesWritten == contentLength);
      }
    };
  }

 /**
  * Progress request listener interface.
  */
  public interface ProgressRequestListener {
    void onRequestProgress(long bytesWritten, long contentLength, boolean done);
  }
}
