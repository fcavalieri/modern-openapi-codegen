/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.fasterxml.jackson.databind.node.ObjectNode;

import com.google.common.reflect.TypeToken;
import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.JSON;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.ApiResponse;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Singleton Api Call class.
 */
public class ApiCallSingleton<T> extends ApiCall {
  private T _result;

  /**
   * Api call singleton constructor
   * @param apiClient the api client
   * @param apiRequest the api request
   * @param apiMethodName the api method name
   * @param returnType the return type
   */
  public ApiCallSingleton(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type returnType) {
    super(apiClient, apiRequest, apiMethodName, returnType);
  }

  /**
   * Gets the api call result
   * @return The api call result
   */
  public T getResult() {
    return _result;
  }

  /**
   * Executes the api call
   * @throws ApiException api exception
   */
  public void execute() throws ApiException {
    LocalDateTime requestTime = LocalDateTime.now();
    Response response = _apiClient.execute(_apiRequest, _apiMethodName);
    _apiResponse = new ApiResponse(response, requestTime);

    checkResponseStatus(_apiResponse);
    processResponse(_apiResponse);
  }

  @SuppressWarnings({"UnstableApiUsage", "unchecked"})
  private void processResponse(ApiResponse response) throws ApiException {
    try {
      if (_itemType.equals(new TypeToken<byte[]>() {}.getType()))
        _result = (T) response.body().bytes();
      else if (_itemType.equals(new TypeToken<String>() {}.getType()))
        _result = (T) response.body().string();
      else if (_itemType.equals(JSON.JSON_OBJECT_TYPE)) {
        _result = new JSON().deserialize(response.body().string(), _itemType);
      } else
        throw new ApiException("Unsupported deserialization type " + _itemType.getTypeName(), this);
    } catch (IOException e) {
      throw new ApiException("Error calling " + _apiMethodName, e, this);
    }
  }
}