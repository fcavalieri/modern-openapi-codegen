/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.authentication;

import com.reportix.cellstore.client.requests.ApiRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.HashSet;
import java.util.Set;

/**
* Non authenticated authentication provider class.
*/
@Data
@EqualsAndHashCode(callSuper = true)
public class Unauthenticated extends AuthenticationProvider {
  private static final long serialVersionUID = 1L;

  @Override
  public void addAuthentication(OkHttpClient httpClient, ApiRequest apiRequest) {
  }

  @Override
  public void processResponse(OkHttpClient httpClient, ApiRequest apiRequest, Response response) {
  }

  @Override
  public Set<String> getSensitiveParameters() { return new HashSet<>();}
}
