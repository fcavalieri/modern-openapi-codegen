/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.MaterializedSequence;
import com.reportix.cellstore.model.ISequence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Object stream Api Call class.
 * @param <T> Type of the items
 * @param <U> Type of the metadata
 */
public class ApiCallObjectList<T, U> extends ApiCallStreamed<T, U> {
  private ISequence<T, U> _result;

  public ApiCallObjectList(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type returnType) {
    super(apiClient, apiRequest, apiMethodName, ((ParameterizedType)returnType).getActualTypeArguments()[0], ((ParameterizedType)returnType).getActualTypeArguments()[1]);
  }

  /**
   * Gets the api call result
   * @return The api call result
   */
  public ISequence<T, U> getResult() {
    return _result;
  }

  /**
   * Executes the api call
   * @throws ApiException api exception
   */
  public void execute() throws ApiException {
    super.execute();
    _result = new MaterializedSequence<T, U>(_iterator, _metadata);
  }
}