# cellstore-client-gson

This is a titl*
- API version: 1.0.0-TESTS

This is a descriptio*

  For more information, please visit [Contact url](Contact url)

## Requirements

Building the API client library requires:
1. Java 1.8+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>com.reportix</groupId>
  <artifactId>cellstore-client-gson</artifactId>
  <version>1.2.3</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.reportix:cellstore-client-gson:1.2.3"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/cellstore-client-gson-1.2.3.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java
```

## Documentation for API Endpoints

All URIs are relative to *{server}/prefix*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ObsoleteApi* | [**getObsoleteOperation**](docs/ObsoleteApi.md#getObsoleteOperation) | **GET** /obsolete-operation | Obsolete summary
*ParametersApi* | [**getPathParameters**](docs/ParametersApi.md#getPathParameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
*ParametersApi* | [**getQueryParameters**](docs/ParametersApi.md#getQueryParameters) | **GET** /query-parameters | Summary
*ParametersApi* | [**postBinaryBody**](docs/ParametersApi.md#postBinaryBody) | **POST** /binary-body | Summary
*ParametersApi* | [**postBinaryBodyOptional**](docs/ParametersApi.md#postBinaryBodyOptional) | **POST** /binary-body-optional | Summary
*ParametersApi* | [**postExcludedBinaryBody**](docs/ParametersApi.md#postExcludedBinaryBody) | **POST** /excluded-binary-body | Summary
*ParametersApi* | [**postObjectBody**](docs/ParametersApi.md#postObjectBody) | **POST** /object-body | Summary
*ParametersApi* | [**postObjectBodyOptional**](docs/ParametersApi.md#postObjectBodyOptional) | **POST** /object-body-optional | Summary
*ParametersApi* | [**postOverriddenBinaryBody**](docs/ParametersApi.md#postOverriddenBinaryBody) | **POST** /overridden-binary-body | Summary
*ParametersApi* | [**postStringBody**](docs/ParametersApi.md#postStringBody) | **POST** /string-body | Summary
*ParametersApi* | [**postStringBodyOptional**](docs/ParametersApi.md#postStringBodyOptional) | **POST** /string-body-optional | Summary
*ParametersApi* | [**postVariantBodyZipArchive**](docs/ParametersApi.md#postVariantBodyZipArchive) | **POST** /variant-body | Summary
*ParametersApi* | [**postVariantBodyXHTML**](docs/ParametersApi.md#postVariantBodyXHTML) | **POST** /variant-body | Summary
*ParametersApi* | [**postVariantBodyHTML**](docs/ParametersApi.md#postVariantBodyHTML) | **POST** /variant-body | Summary
*ResponsesApi* | [**emptyResponse**](docs/ResponsesApi.md#emptyResponse) | **GET** /empty-response | Summary
*ResponsesApi* | [**getBinaryResponse**](docs/ResponsesApi.md#getBinaryResponse) | **GET** /binary-response | Summary
*ResponsesApi* | [**getJsonResponse**](docs/ResponsesApi.md#getJsonResponse) | **GET** /json-response | Summary
*ResponsesApi* | [**getXMLResponse**](docs/ResponsesApi.md#getXMLResponse) | **GET** /xml-response | Summary
*ResponsesApi* | [**listJsonResponse**](docs/ResponsesApi.md#listJsonResponse) | **GET** /list-json-response | Summary
*ResponsesApi* | [**postJsonResponse**](docs/ResponsesApi.md#postJsonResponse) | **POST** /json-response | Summary
*ResponsesApi* | [**streamJsonResponse**](docs/ResponsesApi.md#streamJsonResponse) | **GET** /stream-json-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsList**](docs/ResponsesApi.md#getArbitraryResponseAsList) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsStream**](docs/ResponsesApi.md#getArbitraryResponseAsStream) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsXML**](docs/ResponsesApi.md#getArbitraryResponseAsXML) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsBinary**](docs/ResponsesApi.md#getArbitraryResponseAsBinary) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsBinaryObsolete**](docs/ResponsesApi.md#getArbitraryResponseAsBinaryObsolete) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**getArbitraryResponseAsJson**](docs/ResponsesApi.md#getArbitraryResponseAsJson) | **GET** /arbitrary-response | Summary

## Documentation for Models


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author


