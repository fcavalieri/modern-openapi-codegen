# ResponsesApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**emptyResponse**](ResponsesApi.md#emptyResponse) | **GET** /empty-response | Summary
[**getBinaryResponse**](ResponsesApi.md#getBinaryResponse) | **GET** /binary-response | Summary
[**getJsonResponse**](ResponsesApi.md#getJsonResponse) | **GET** /json-response | Summary
[**getXMLResponse**](ResponsesApi.md#getXMLResponse) | **GET** /xml-response | Summary
[**listJsonResponse**](ResponsesApi.md#listJsonResponse) | **GET** /list-json-response | Summary
[**postJsonResponse**](ResponsesApi.md#postJsonResponse) | **POST** /json-response | Summary
[**streamJsonResponse**](ResponsesApi.md#streamJsonResponse) | **GET** /stream-json-response | Summary
[**getArbitraryResponseAsList**](ResponsesApi.md#getArbitraryResponseAsList) | **GET** /arbitrary-response | Summary
[**getArbitraryResponseAsStream**](ResponsesApi.md#getArbitraryResponseAsStream) | **GET** /arbitrary-response | Summary
[**getArbitraryResponseAsXML**](ResponsesApi.md#getArbitraryResponseAsXML) | **GET** /arbitrary-response | Summary
[**getArbitraryResponseAsBinary**](ResponsesApi.md#getArbitraryResponseAsBinary) | **GET** /arbitrary-response | Summary
[**getArbitraryResponseAsBinaryObsolete**](ResponsesApi.md#getArbitraryResponseAsBinaryObsolete) | **GET** /arbitrary-response | Summary
[**getArbitraryResponseAsJson**](ResponsesApi.md#getArbitraryResponseAsJson) | **GET** /arbitrary-response | Summary

<a name="emptyResponse"></a>
# **emptyResponse**
> emptyResponse(fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
try {
    apiInstance.emptyResponse(fail);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#emptyResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

null (empty response body)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBinaryResponse"></a>
# **getBinaryResponse**
> byte[] getBinaryResponse(fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
try {
    byte[] result = apiInstance.getBinaryResponse(fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getBinaryResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

[**byte[]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getJsonResponse"></a>
# **getJsonResponse**
> JsonObject getJsonResponse(fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
try {
    JsonObject result = apiInstance.getJsonResponse(fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getJsonResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**JsonObject**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getXMLResponse"></a>
# **getXMLResponse**
> String getXMLResponse(fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
try {
    String result = apiInstance.getXMLResponse(fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getXMLResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**String**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml

<a name="listJsonResponse"></a>
# **listJsonResponse**
> ISequence&lt;JsonObject, JsonObject&gt; listJsonResponse(fail, failAt, sleep, amount)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
try {
    ISequence<JsonObject, JsonObject> result = apiInstance.listJsonResponse(fail, failAt, sleep, amount);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#listJsonResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]

### Return type

**ISequence&lt;JsonObject, JsonObject&gt;**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postJsonResponse"></a>
# **postJsonResponse**
> JsonObject postJsonResponse(fail)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
try {
    JsonObject result = apiInstance.postJsonResponse(fail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#postJsonResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]

### Return type

**JsonObject**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="streamJsonResponse"></a>
# **streamJsonResponse**
> IStream&lt;JsonObject, JsonObject&gt; streamJsonResponse(fail, failAt, sleep, amount)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
try {
    IStream<JsonObject, JsonObject> result = apiInstance.streamJsonResponse(fail, failAt, sleep, amount);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#streamJsonResponse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]

### Return type

**IStream&lt;JsonObject, JsonObject&gt;**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getArbitraryResponseAsList"></a>
# **getArbitraryResponseAsList**
> ISequence&lt;JsonObject, JsonObject&gt; getArbitraryResponseAsList(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedC = ...; // String | Excluded c (A single excluded parameter should be present per variant)
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
try {
    ISequence<JsonObject, JsonObject> result = apiInstance.getArbitraryResponseAsList(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) |
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedC** | **String**| Excluded c (A single excluded parameter should be present per variant) | [optional]
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) | [optional]
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) | [optional]

### Return type

[**ISequence&lt;JsonObject, JsonObject&gt;**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArbitraryResponseAsStream"></a>
# **getArbitraryResponseAsStream**
> IStream&lt;JsonObject, JsonObject&gt; getArbitraryResponseAsStream(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedA = ...; // String | Excluded a (A single excluded parameter should be present per variant)
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
try {
    IStream<JsonObject, JsonObject> result = apiInstance.getArbitraryResponseAsStream(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) |
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedA** | **String**| Excluded a (A single excluded parameter should be present per variant) | [optional]
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) | [optional]
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) | [optional]

### Return type

[**IStream&lt;JsonObject, JsonObject&gt;**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArbitraryResponseAsXML"></a>
# **getArbitraryResponseAsXML**
> String getArbitraryResponseAsXML(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedC = ...; // String | Excluded c (A single excluded parameter should be present per variant)
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
try {
    String result = apiInstance.getArbitraryResponseAsXML(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsXML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) |
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedC** | **String**| Excluded c (A single excluded parameter should be present per variant) | [optional]
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) | [optional]
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) | [optional]

### Return type

[**String**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArbitraryResponseAsBinary"></a>
# **getArbitraryResponseAsBinary**
> byte[] getArbitraryResponseAsBinary(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedA = ...; // String | Excluded a (A single excluded parameter should be present per variant)
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
try {
    byte[] result = apiInstance.getArbitraryResponseAsBinary(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsBinary");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) |
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedA** | **String**| Excluded a (A single excluded parameter should be present per variant) | [optional]
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) | [optional]
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) | [optional]

### Return type

[**byte[]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArbitraryResponseAsBinaryObsolete"></a>
# **getArbitraryResponseAsBinaryObsolete**
> byte[] getArbitraryResponseAsBinaryObsolete(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD)

Summary

Description

This variant is obsolete

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedC = ...; // String | Excluded c (A single excluded parameter should be present per variant)
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
try {
    byte[] result = apiInstance.getArbitraryResponseAsBinaryObsolete(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsBinaryObsolete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) |
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedC** | **String**| Excluded c (A single excluded parameter should be present per variant) | [optional]
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) | [optional]
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) | [optional]

### Return type

[**byte[]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArbitraryResponseAsJson"></a>
# **getArbitraryResponseAsJson**
> JsonObject getArbitraryResponseAsJson(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC)

Summary

Description

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ResponsesApi;

ResponsesApi apiInstance = new ResponsesApi();
String requiredA = ...; // String | Required a (Two required parameter should be optional per variant)
String requiredD = ...; // String | Required d (Two required parameter should be optional per variant)
Boolean fail = ...; // Boolean | Description
Integer failAt = ...; // Integer | Description
Integer sleep = ...; // Integer | Description
Integer amount = ...; // Integer | Description
String excludedA = ...; // String | Excluded a (A single excluded parameter should be present per variant)
String requiredB = ...; // String | Required b (Two required parameter should be optional per variant)
String requiredC = ...; // String | Required c (Two required parameter should be optional per variant)
try {
    JsonObject result = apiInstance.getArbitraryResponseAsJson(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ResponsesApi#getArbitraryResponseAsJson");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **String**| Required a (Two required parameter should be optional per variant) |
 **requiredD** | **String**| Required d (Two required parameter should be optional per variant) |
 **fail** | **Boolean**| Description | [optional] [default to false]
 **failAt** | **Integer**| Description | [optional] [default to -1]
 **sleep** | **Integer**| Description | [optional] [default to 0]
 **amount** | **Integer**| Description | [optional] [default to 1000]
 **excludedA** | **String**| Excluded a (A single excluded parameter should be present per variant) | [optional]
 **requiredB** | **String**| Required b (Two required parameter should be optional per variant) | [optional]
 **requiredC** | **String**| Required c (Two required parameter should be optional per variant) | [optional]

### Return type

[**JsonObject**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

