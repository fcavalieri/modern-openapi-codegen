# ObsoleteApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getObsoleteOperation**](ObsoleteApi.md#getObsoleteOperation) | **GET** /obsolete-operation | Obsolete summary

<a name="getObsoleteOperation"></a>
# **getObsoleteOperation**
> JsonObject getObsoleteOperation(limit)

Obsolete summary

Obsolete description

This operation is obsolete

### Example
```java
// Import classes:
//import com.reportix.cellstore.client.ApiClient;
//import com.reportix.cellstore.client.ApiException;
//import com.reportix.cellstore.client.Configuration;
//import com.reportix.cellstore.api.ObsoleteApi;

ObsoleteApi apiInstance = new ObsoleteApi();
Integer limit = ...; // Integer | Limits the amount of documents in the result of the query. 
try {
    JsonObject result = apiInstance.getObsoleteOperation(limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ObsoleteApi#getObsoleteOperation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| Limits the amount of documents in the result of the query.  | [optional] [default to 50]

### Return type

**JsonObject**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

