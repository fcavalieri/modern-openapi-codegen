/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.model;

import java.util.Collection;

/**
* Represents a sequence of items of type T.
* The type of the metadata is U.
* @param <T> The type of the objects.
* @param <U> The type of the metadata.
*/
public interface ISequence<T, U> extends Collection<T> {
  U getMetadata();
}
