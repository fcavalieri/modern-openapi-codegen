/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.api;

import com.google.gson.JsonObject;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.Configuration;
import com.reportix.cellstore.client.ProgressRequestBody;
import com.reportix.cellstore.client.ProgressResponseBody;

import com.reportix.cellstore.client.authentication.*;
import com.reportix.cellstore.client.calls.*;
import com.reportix.cellstore.model.*;
import com.reportix.cellstore.client.requests.*;
import com.reportix.cellstore.client.responses.*;

import com.google.common.reflect.TypeToken;

import java.io.IOException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Api methods for ResponsesApi.
 */
public class ResponsesApi extends ApiBase {

  /**
   * ResponsesApi constructor.
   * @param basePath the base path
   * @throws ApiException api exception
   */
  public ResponsesApi(String basePath) throws ApiException {
    super(basePath, null);
  }

  /**
   * ResponsesApi constructor.
   * @param basePath the base path
   * @param configuration the configuration
   * @throws ApiException api exception
   */
  public ResponsesApi(String basePath, Configuration configuration) throws ApiException {
    super(basePath, configuration);
  }

  /**
   * Build call for emptyResponse
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest emptyResponseBuildRequest(Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/empty-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public void emptyResponse(Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = emptyResponseBuildRequest(fail, null, null);
    Type __ReturnType = new TypeToken<Void>(){}.getType();
    ApiCallSingleton<Void> __ApiCall = new ApiCallSingleton<Void>(_apiClient, __ApiRequest, "emptyResponse", __ReturnType);
    __ApiCall.execute();
    
  }
  /**
   * Build call for getBinaryResponse
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getBinaryResponseBuildRequest(Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/binary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @return byte[]
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public byte[] getBinaryResponse(Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = getBinaryResponseBuildRequest(fail, null, null);
    Type __ReturnType = new TypeToken<byte[]>(){}.getType();
    ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(_apiClient, __ApiRequest, "getBinaryResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getJsonResponse
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getJsonResponseBuildRequest(Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/json-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @return JsonObject
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public JsonObject getJsonResponse(Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = getJsonResponseBuildRequest(fail, null, null);
    Type __ReturnType = new TypeToken<JsonObject>(){}.getType();
    ApiCallSingleton<JsonObject> __ApiCall = new ApiCallSingleton<JsonObject>(_apiClient, __ApiRequest, "getJsonResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getXMLResponse
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getXMLResponseBuildRequest(Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/xml-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String getXMLResponse(Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = getXMLResponseBuildRequest(fail, null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "getXMLResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for listJsonResponse
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest listJsonResponseBuildRequest(Boolean fail, Integer failAt, Integer sleep, Integer amount, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/list-json-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @return ISequence&lt;JsonObject, JsonObject&gt;
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ISequence<JsonObject, JsonObject> listJsonResponse(Boolean fail, Integer failAt, Integer sleep, Integer amount) throws ApiException
  {
    ApiRequest __ApiRequest = listJsonResponseBuildRequest(fail, failAt, sleep, amount, null, null);
    Type __ReturnType = new TypeToken<ISequence<JsonObject, JsonObject>>(){}.getType();
    ApiCallObjectList<JsonObject, JsonObject> __ApiCall = new ApiCallObjectList<JsonObject, JsonObject>(_apiClient, __ApiRequest, "listJsonResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for postJsonResponse
   * @param fail Description (optional, defaults to false)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest postJsonResponseBuildRequest(Boolean fail, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("POST", _basePath, "/json-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @return JsonObject
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public JsonObject postJsonResponse(Boolean fail) throws ApiException
  {
    ApiRequest __ApiRequest = postJsonResponseBuildRequest(fail, null, null);
    Type __ReturnType = new TypeToken<JsonObject>(){}.getType();
    ApiCallSingleton<JsonObject> __ApiCall = new ApiCallSingleton<JsonObject>(_apiClient, __ApiRequest, "postJsonResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for streamJsonResponse
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest streamJsonResponseBuildRequest(Boolean fail, Integer failAt, Integer sleep, Integer amount, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/stream-json-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @return IStream&lt;JsonObject, JsonObject&gt;
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public IStream<JsonObject, JsonObject> streamJsonResponse(Boolean fail, Integer failAt, Integer sleep, Integer amount) throws ApiException
  {
    ApiRequest __ApiRequest = streamJsonResponseBuildRequest(fail, failAt, sleep, amount, null, null);
    Type __ReturnType = new TypeToken<IStream<JsonObject, JsonObject>>(){}.getType();
    ApiCallObjectStream<JsonObject, JsonObject> __ApiCall = new ApiCallObjectStream<JsonObject, JsonObject>(_apiClient, __ApiRequest, "streamJsonResponse", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsList
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getArbitraryResponseAsListBuildRequest(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredB' is set
    if (requiredB == null) {
      throw new ApiException("Missing the required parameter 'requiredB' when calling ResponsesApi->getArbitraryResponseAsList");
    }
    // verify the required parameter 'requiredC' is set
    if (requiredC == null) {
      throw new ApiException("Missing the required parameter 'requiredC' when calling ResponsesApi->getArbitraryResponseAsList");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "json-list"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedC != null) __ApiRequest.addQueryParameter("excluded-c", excludedC); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @return ISequence&lt;JsonObject, JsonObject&gt;
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public ISequence<JsonObject, JsonObject> getArbitraryResponseAsList(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsListBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD, null, null);
    Type __ReturnType = new TypeToken<ISequence<JsonObject, JsonObject>>(){}.getType();
    ApiCallObjectList<JsonObject, JsonObject> __ApiCall = new ApiCallObjectList<JsonObject, JsonObject>(_apiClient, __ApiRequest, "getArbitraryResponseAsList", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsStream
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getArbitraryResponseAsStreamBuildRequest(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredA' is set
    if (requiredA == null) {
      throw new ApiException("Missing the required parameter 'requiredA' when calling ResponsesApi->getArbitraryResponseAsStream");
    }
    // verify the required parameter 'requiredD' is set
    if (requiredD == null) {
      throw new ApiException("Missing the required parameter 'requiredD' when calling ResponsesApi->getArbitraryResponseAsStream");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "json-stream"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedA != null) __ApiRequest.addQueryParameter("excluded-a", excludedA); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @return IStream&lt;JsonObject, JsonObject&gt;
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public IStream<JsonObject, JsonObject> getArbitraryResponseAsStream(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsStreamBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC, null, null);
    Type __ReturnType = new TypeToken<IStream<JsonObject, JsonObject>>(){}.getType();
    ApiCallObjectStream<JsonObject, JsonObject> __ApiCall = new ApiCallObjectStream<JsonObject, JsonObject>(_apiClient, __ApiRequest, "getArbitraryResponseAsStream", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsXML
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getArbitraryResponseAsXMLBuildRequest(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredB' is set
    if (requiredB == null) {
      throw new ApiException("Missing the required parameter 'requiredB' when calling ResponsesApi->getArbitraryResponseAsXML");
    }
    // verify the required parameter 'requiredC' is set
    if (requiredC == null) {
      throw new ApiException("Missing the required parameter 'requiredC' when calling ResponsesApi->getArbitraryResponseAsXML");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "xml"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "xml-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedC != null) __ApiRequest.addQueryParameter("excluded-c", excludedC); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @return String
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public String getArbitraryResponseAsXML(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsXMLBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD, null, null);
    Type __ReturnType = new TypeToken<String>(){}.getType();
    ApiCallSingleton<String> __ApiCall = new ApiCallSingleton<String>(_apiClient, __ApiRequest, "getArbitraryResponseAsXML", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsBinary
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getArbitraryResponseAsBinaryBuildRequest(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredA' is set
    if (requiredA == null) {
      throw new ApiException("Missing the required parameter 'requiredA' when calling ResponsesApi->getArbitraryResponseAsBinary");
    }
    // verify the required parameter 'requiredD' is set
    if (requiredD == null) {
      throw new ApiException("Missing the required parameter 'requiredD' when calling ResponsesApi->getArbitraryResponseAsBinary");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "binary"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "binary-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedA != null) __ApiRequest.addQueryParameter("excluded-a", excludedA); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @return byte[]
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public byte[] getArbitraryResponseAsBinary(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsBinaryBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC, null, null);
    Type __ReturnType = new TypeToken<byte[]>(){}.getType();
    ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(_apiClient, __ApiRequest, "getArbitraryResponseAsBinary", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsBinaryObsolete
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object
   * @deprecated This variant is obsolete
   */
  @Deprecated
  private ApiRequest getArbitraryResponseAsBinaryObsoleteBuildRequest(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredB' is set
    if (requiredB == null) {
      throw new ApiException("Missing the required parameter 'requiredB' when calling ResponsesApi->getArbitraryResponseAsBinaryObsolete");
    }
    // verify the required parameter 'requiredC' is set
    if (requiredC == null) {
      throw new ApiException("Missing the required parameter 'requiredC' when calling ResponsesApi->getArbitraryResponseAsBinaryObsolete");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "binary"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "binary-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedC != null) __ApiRequest.addQueryParameter("excluded-c", excludedC); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredB Required b (Two required parameter should be optional per variant) (required)
   * @param requiredC Required c (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedC Excluded c (A single excluded parameter should be present per variant) (optional)
   * @param requiredA Required a (Two required parameter should be optional per variant) (optional)
   * @param requiredD Required d (Two required parameter should be optional per variant) (optional)
   * @return byte[]
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
   * @deprecated This variant is obsolete
   */
  @Deprecated
  public byte[] getArbitraryResponseAsBinaryObsolete(String requiredB, String requiredC, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedC, String requiredA, String requiredD) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsBinaryObsoleteBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD, null, null);
    Type __ReturnType = new TypeToken<byte[]>(){}.getType();
    ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(_apiClient, __ApiRequest, "getArbitraryResponseAsBinaryObsolete", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
  /**
   * Build call for getArbitraryResponseAsJson
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @param progressListener Progress listener
   * @param progressRequestListener Progress request listener
   * @return Call to execute
   * @throws ApiException If fail to serialize the request body object

   */
  
  private ApiRequest getArbitraryResponseAsJsonBuildRequest(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException
  {
    // verify the required parameter 'requiredA' is set
    if (requiredA == null) {
      throw new ApiException("Missing the required parameter 'requiredA' when calling ResponsesApi->getArbitraryResponseAsJson");
    }
    // verify the required parameter 'requiredD' is set
    if (requiredD == null) {
      throw new ApiException("Missing the required parameter 'requiredD' when calling ResponsesApi->getArbitraryResponseAsJson");
    }

    ApiRequest __ApiRequest = new ApiRequest("GET", _basePath, "/arbitrary-response", _apiClient.getConfiguration().getAuthenticationProvider(), progressListener, progressRequestListener);
    __ApiRequest.addQueryParameter("format", "json"); // hardcoded query parameter
    __ApiRequest.addQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
    if (fail != null) __ApiRequest.addQueryParameter("fail", fail); // query parameter
    if (failAt != null) __ApiRequest.addQueryParameter("fail-at", failAt); // query parameter
    if (sleep != null) __ApiRequest.addQueryParameter("sleep", sleep); // query parameter
    if (amount != null) __ApiRequest.addQueryParameter("amount", amount); // query parameter
    if (excludedA != null) __ApiRequest.addQueryParameter("excluded-a", excludedA); // query parameter
    if (requiredA != null) __ApiRequest.addQueryParameter("required-a", requiredA); // query parameter
    if (requiredB != null) __ApiRequest.addQueryParameter("required-b", requiredB); // query parameter
    if (requiredC != null) __ApiRequest.addQueryParameter("required-c", requiredC); // query parameter
    if (requiredD != null) __ApiRequest.addQueryParameter("required-d", requiredD); // query parameter
    return __ApiRequest;
  }

  /**
   * Summary
   * Description
   * @param requiredA Required a (Two required parameter should be optional per variant) (required)
   * @param requiredD Required d (Two required parameter should be optional per variant) (required)
   * @param fail Description (optional, defaults to false)
   * @param failAt Description (optional, defaults to -1)
   * @param sleep Description (optional, defaults to 0)
   * @param amount Description (optional, defaults to 1000)
   * @param excludedA Excluded a (A single excluded parameter should be present per variant) (optional)
   * @param requiredB Required b (Two required parameter should be optional per variant) (optional)
   * @param requiredC Required c (Two required parameter should be optional per variant) (optional)
   * @return JsonObject
   * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

   */
  
  public JsonObject getArbitraryResponseAsJson(String requiredA, String requiredD, Boolean fail, Integer failAt, Integer sleep, Integer amount, String excludedA, String requiredB, String requiredC) throws ApiException
  {
    ApiRequest __ApiRequest = getArbitraryResponseAsJsonBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC, null, null);
    Type __ReturnType = new TypeToken<JsonObject>(){}.getType();
    ApiCallSingleton<JsonObject> __ApiCall = new ApiCallSingleton<JsonObject>(_apiClient, __ApiRequest, "getArbitraryResponseAsJson", __ReturnType);
    __ApiCall.execute();
    return __ApiCall.getResult();
  }
}
