/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client;

import com.reportix.cellstore.client.calls.ApiCall;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * API exception class.
 */
public class ApiException extends Exception {
  private final LocalDateTime _errorTime;
  private int _errorCode;
  private ApiCall _apiCall;
  private String _diagnosticInformation;

  /**
   * API exception constructor.
   * @param message the message
   */
  public ApiException(String message) {
    super(message);
    _errorTime = LocalDateTime.now();
  }

  /**
   * API exception constructor.
   * @param message the message
   * @param apiCall the api call
   */
  public ApiException(String message, ApiCall apiCall) {
    super(message);
    _errorTime = LocalDateTime.now();
    _apiCall = apiCall;
  }

  /**
   * API exception constructor.
   * @param message the message
   * @param apiCall the api call
   * @param errorCode the error code
   */
  public ApiException(String message, ApiCall apiCall, int errorCode) {
    super(message);
    _errorCode = errorCode;
    _errorTime = LocalDateTime.now();
    _apiCall = apiCall;
  }

  /**
   * API exception constructor.
   * @param message the message
   * @param innerThrowable the inner throwable
   */
  public ApiException(String message, Throwable innerThrowable) {
    super(message, innerThrowable);
    _errorTime = LocalDateTime.now();
  }

  /**
   * API exception constructor.
   * @param message the message
   * @param innerThrowable the inner throwable
   * @param apiCall the api call
   */
  public ApiException(String message, Throwable innerThrowable, ApiCall apiCall) {
    super(message, innerThrowable);
    if (apiCall != null && apiCall.getApiResponse() != null)
      _errorCode = apiCall.getApiResponse().code();
    _errorTime = LocalDateTime.now();
    _apiCall = apiCall;
  }

  /**
   * Returns the error code
   * @return the error code
   */
  public int getErrorCode() {
    return _errorCode;
  }

  /**
   * Returns a string representation
   * @return a string representation
   */
  public String toString() {
    if (_apiCall != null && _apiCall.getApiClient() != null && _apiCall.getApiClient().getConfiguration() != null && !_apiCall.getApiClient().getConfiguration().isVerboseExceptions())
      return super.toString();

    StringBuilder sb = new StringBuilder();
    sb.append(super.toString() + "\n");
    sb.append("===System===\n");
    sb.append(new DiagnosticInfo() + "\n");
    if (_apiCall != null) {
      sb.append("===API Call===\n");
      sb.append(_apiCall + "\n");
    }
    sb.append("Error Time: " + _errorTime.format(DateTimeFormatter.ofPattern(Configuration.DATE_FORMAT)));
    return sb.toString();
  }


}
