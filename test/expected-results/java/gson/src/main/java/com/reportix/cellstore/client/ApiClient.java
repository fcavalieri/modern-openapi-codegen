/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client;

import com.reportix.cellstore.client.requests.ApiRequest;
import com.squareup.okhttp.*;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import com.squareup.okhttp.logging.HttpLoggingInterceptor.Level;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * API client class.
 */
public class ApiClient {

  private Configuration _configuration;

  /**
   * API client class constructor.
   */
  public ApiClient(Configuration configuration) {
    setConfiguration(configuration);
  }

  /**
   * Returns the configuration.
   * @return the configuration.
   */
  public Configuration getConfiguration() {
    return _configuration;
  }

  /**
   * Sets the configuration.
   * @param configuration the configuration.
   */
  public void setConfiguration(Configuration configuration) {
    if (configuration == null)
      _configuration = new Configuration();
    else {
      _configuration = configuration;
    }
  }

  private OkHttpClient initRestClient() throws ApiException {
    OkHttpClient httpClient = new OkHttpClient();
    httpClient.setConnectTimeout(30 * 1000, TimeUnit.MILLISECONDS);
    httpClient.setReadTimeout(_configuration.getTimeoutMs(), TimeUnit.MILLISECONDS);
    httpClient.setWriteTimeout(_configuration.getTimeoutMs(), TimeUnit.MILLISECONDS);

    if (!_configuration.isVerifySSL()) {
      TrustManager trustAll = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
          return null;
        }
      };
      TrustManager[] trustManagers = new TrustManager[]{trustAll};
      HostnameVerifier hostnameVerifier = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };


      try {
        SSLContext sslContext = sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagers, new SecureRandom());
        httpClient.setSslSocketFactory(sslContext.getSocketFactory());
        httpClient.setHostnameVerifier(hostnameVerifier);
      } catch (NoSuchAlgorithmException | KeyManagementException e) {
        throw new ApiException("Cannot initialize rest client", e);
      }
    }
    return httpClient;
  }

  /**
   * Executes an api request
   * @param apiRequest the api request
   * @param apiMethodName the api method name
   * @return api response
   * @throws ApiException api exception
   */
  public Response execute(ApiRequest apiRequest, String apiMethodName) throws ApiException {
    try {
      OkHttpClient httpClient = initRestClient();
      if (_configuration.getAuthenticationProvider() != null)
        _configuration.getAuthenticationProvider().addAuthentication(httpClient, apiRequest);
      Request request = apiRequest.getRequest();
      Call call = httpClient.newCall(request);

      Response response = call.execute();
      if (_configuration.getAuthenticationProvider() != null)
        _configuration.getAuthenticationProvider().processResponse(httpClient, apiRequest, response);
      return response;
    } catch (IOException e) {
      throw new ApiException("Error calling " + apiMethodName, e);
    }
  }
}