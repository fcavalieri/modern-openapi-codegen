/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.responses;

import com.reportix.cellstore.client.Configuration;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * Api response class.
 */
public class ApiResponse {
  protected Response _response;
  protected LocalDateTime _requestTime;
  protected LocalDateTime _responseTime;

  public ApiResponse(Response response, LocalDateTime requestTime) {
    _response = response;
    _requestTime = requestTime;
    _responseTime = LocalDateTime.now();
  }

  /// <summary>
  /// Returns a string representation of the Response
  /// </summary>
  /// <returns>The string representation of the response</returns>
  public String toString() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Configuration.DATE_FORMAT);
    StringBuilder sb = new StringBuilder();
    sb.append("RequestTime: " + formatter.format(_requestTime) + "\n");
    sb.append("ResponseTime: " + formatter.format(_responseTime) + "\n");
    if (_response != null) {
      sb.append("Status: " + _response.code() + "\n");
      if (_response.headers() != null) {
        for (Map.Entry<String, List<String>> header : _response.headers().toMultimap().entrySet()) {
          for (String headerValue : header.getValue()) {
            sb.append(header.getKey() + ": " + headerValue + "\n");
          }
        }
      }
    }
    return sb.toString();
  }

  public int code() {
    return _response.code();
  }

  public ResponseBody body() {
    return _response.body();
  }
}
