/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.ResponseBody;
import okio.*;

import java.io.IOException;

/**
 * Progress response body class.
 */
public class ProgressResponseBody extends ResponseBody {

  private final ResponseBody _responseBody;
  private final ProgressListener _progressListener;
  private BufferedSource _bufferedSource;

  public ProgressResponseBody(ResponseBody responseBody, ProgressListener progressListener) {
    _responseBody = responseBody;
    _progressListener = progressListener;
  }

  @Override
  public MediaType contentType() {
    return _responseBody.contentType();
  }

  @Override
  public long contentLength() throws IOException {
    return _responseBody.contentLength();
  }

  @Override
  public BufferedSource source() throws IOException {
    if (_bufferedSource == null) {
      _bufferedSource = Okio.buffer(source(_responseBody.source()));
    }
    return _bufferedSource;
  }

  private Source source(Source source) {
    return new ForwardingSource(source) {
      long totalBytesRead = 0L;

      @Override
      public long read(Buffer sink, long byteCount) throws IOException {
        long bytesRead = super.read(sink, byteCount);
        // read() returns the number of bytes read, or -1 if this source is exhausted.
        totalBytesRead += bytesRead != -1 ? bytesRead : 0;
        _progressListener.update(totalBytesRead, _responseBody.contentLength(), bytesRead == -1);
        return bytesRead;
      }
    };
  }

 /**
  * Progress response listener interface.
  */
  public interface ProgressListener {
    void update(long bytesRead, long contentLength, boolean done);
  }
}

