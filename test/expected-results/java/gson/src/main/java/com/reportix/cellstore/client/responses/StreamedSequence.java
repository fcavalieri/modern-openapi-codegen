/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.responses;

import com.reportix.cellstore.client.calls.StreamedApiIterator;
import com.reportix.cellstore.model.IStream;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

/**
 * Streamed sequence class.
 * @param <T> Item type
 * @param <U> Metadata type
 */
public class StreamedSequence<T, U> implements IStream<T, U> {
  private final Iterator<T> _iterator;
  private final U _metadata;

  private boolean iteratorReturned = false;

  public StreamedSequence(StreamedApiIterator<T> iterator, U metadata) {
    _iterator = iterator;
    _metadata = metadata;
  }

  @Override
  public Iterator<T> iterator() {
    if (!iteratorReturned) {
      iteratorReturned = true;
      return _iterator;
    }
    throw new RuntimeException("It is forbidden to retrieve an iterator multiple times for the same request");
  }

  public U getMetadata() {
    return _metadata;
  }

  @Override
  public void forEach(Consumer<? super T> action) {
    if (!iteratorReturned) {
      iteratorReturned = true;
      _iterator.forEachRemaining(action);
    }
    throw new RuntimeException("It is forbidden to retrieve an iterator multiple times for the same request");
  }

  @Override
  public Spliterator<T> spliterator() {
    if (!iteratorReturned) {
      iteratorReturned = true;
      return Spliterators.spliteratorUnknownSize(_iterator, Spliterator.ORDERED);
    }
    throw new RuntimeException("It is forbidden to retrieve an iterator multiple times for the same request");
  }
}