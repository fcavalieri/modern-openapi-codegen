/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.api;

import com.google.gson.JsonObject;

import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.Configuration;
import com.reportix.cellstore.client.ProgressRequestBody;
import com.reportix.cellstore.client.ProgressResponseBody;

import com.reportix.cellstore.client.authentication.*;
import com.reportix.cellstore.client.calls.*;
import com.reportix.cellstore.model.*;
import com.reportix.cellstore.client.requests.*;
import com.reportix.cellstore.client.responses.*;

import com.google.common.reflect.TypeToken;

import java.io.IOException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Api methods for ExcludedApi.
 */
public class ExcludedApi extends ApiBase {

  /**
   * ExcludedApi constructor.
   * @param basePath the base path
   * @throws ApiException api exception
   */
  public ExcludedApi(String basePath) throws ApiException {
    super(basePath, null);
  }

  /**
   * ExcludedApi constructor.
   * @param basePath the base path
   * @param configuration the configuration
   * @throws ApiException api exception
   */
  public ExcludedApi(String basePath, Configuration configuration) throws ApiException {
    super(basePath, configuration);
  }

}
