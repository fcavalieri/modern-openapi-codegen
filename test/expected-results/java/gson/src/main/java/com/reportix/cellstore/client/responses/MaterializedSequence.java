/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.responses;

import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.calls. StreamedApiIterator;
import com.reportix.cellstore.model.ISequence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Materialized sequence class.
 * @param <T> Item type
 * @param <U> Metadata type
 */
public class MaterializedSequence<T, U> implements ISequence<T, U> {
  private final ArrayList<T> _list;
  private final U _metadata;

  public MaterializedSequence(StreamedApiIterator<T> iterator, U metadata) throws ApiException {
    _list = new ArrayList<T>();
    while (iterator.hasNext())
      _list.add(iterator.nextChecked());
    _metadata = metadata;
  }

  public U getMetadata() {
    return _metadata;
  }

  @Override
  public int size() {
    return _list.size();
  }

  @Override
  public boolean isEmpty() {
    return _list.isEmpty();
  }

  @Override
  public boolean contains(Object o) {
    return _list.contains(o);
  }

  @Override
  public Iterator<T> iterator() {
    return _list.iterator();
  }

  @Override
  public void forEach(Consumer<? super T> action) {
    _list.forEach(action);
  }

  @Override
  public Object[] toArray() {
    return _list.toArray();
  }

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return _list.toArray(a);
  }

  @Override
  public boolean add(T t) {
    return _list.add(t);
  }

  @Override
  public boolean remove(Object o) {
    return _list.remove(o);
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return _list.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    return _list.addAll(c);
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return _list.removeAll(c);
  }

  @Override
  public boolean removeIf(Predicate<? super T> filter) {
    return _list.removeIf(filter);
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return _list.retainAll(c);
  }

  @Override
  public void clear() {
    _list.clear();
  }

  @Override
  public Spliterator<T> spliterator() {
    return _list.spliterator();
  }

  @Override
  public Stream<T> stream() {
    return _list.stream();
  }

  @Override
  public Stream<T> parallelStream() {
    return _list.parallelStream();
  }
}