/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.api;

import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.Configuration;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Api base class.
 */
public abstract class ApiBase {

  /**
   * Api base path
   */
  protected String _basePath;
  /**
   * Api client
   */
  protected ApiClient _apiClient;

  /**
   * Api base class constructor
   * @param basePath the base path
   * @throws ApiException api exception
   */
  public ApiBase(String basePath) throws ApiException {
    this(basePath, null);
  }

  /**
   * Api base class constructor
   * @param basePath The base path
   * @param configuration The configuration
   * @throws ApiException api exception
   */
  public ApiBase(String basePath, Configuration configuration) throws ApiException {
    setBasePath(basePath);
    _apiClient = configuration == null ? new ApiClient(new Configuration()) : new ApiClient(configuration);
  }

  private void setBasePath(String basePath) throws ApiException {
    if (basePath == null || basePath.isEmpty())
      throw new ApiException("basePath cannot be empty");

    try {
      URI basePathUri = new URI(basePath);
      if (!basePathUri.isAbsolute())
        throw new ApiException(basePath + ": invalid basePath. Must be absolute.");

      if (!"http".equals(basePathUri.getScheme()) && !"https".equals(basePathUri.getScheme()))
        throw new ApiException(basePath + ": invalid basePath. Scheme must be http or https.");

      if (basePathUri.getUserInfo() != null)
        throw new ApiException(basePath + ": invalid basePath. Cannot contain credentials in the Uri.");

      if (basePathUri.getQuery() != null)
        throw new ApiException(basePath + ": invalid basePath. Cannot contain query in the Uri.");

      if (basePathUri.getFragment() != null)
        throw new ApiException(basePath + ": invalid basePath. Cannot contain fragment in the Uri.");

      if (basePathUri.getPath() == null || basePathUri.getPath().isEmpty() || basePathUri.getPath().split("/").length != 2)
        throw new ApiException(basePath + ": invalid basePath. It must contain 2 segments.");
    } catch (URISyntaxException e) {
      throw new ApiException("Invalid basePath URI.", e);
    }

    _basePath = basePath;
  }
}