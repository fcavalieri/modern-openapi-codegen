/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client;

import com.reportix.cellstore.client.authentication.AuthenticationProvider;
import com.reportix.cellstore.client.authentication.Unauthenticated;

import lombok.Data;
import java.io.Serializable;

/**
 * Configuration class.
 */
@Data
public class Configuration implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * Date format
   */
  public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

  private final String userAgent = "Swagger-Codegen//java";
  private int timeoutMs = 60 * 60 * 1000;
  private boolean verboseExceptions = true;
  private boolean verifySSL = true;
  private AuthenticationProvider authenticationProvider = new Unauthenticated();
}
