/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.requests;

import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.JSON;
import com.reportix.cellstore.client.ProgressRequestBody;
import com.reportix.cellstore.client.ProgressResponseBody;
import com.reportix.cellstore.client.authentication.AuthenticationProvider;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.internal.http.HttpMethod;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Api request class.
 */
public class ApiRequest {
  private final String _baseUrl;
  private final String _method;
  private final String _path;
  private final AuthenticationProvider _authenticationProvider;
  private final ProgressResponseBody.ProgressListener _progressListener;
  private final ProgressRequestBody.ProgressRequestListener _progressRequestListener;
  private final HashMap<String, String> _pathParameters;
  private final HashMap<String, String> _headerParameters;
  private final HashMap<String, List<String>> _queryParameters;
  private RequestBody _requestBody;

  /**
   * @param method the HTTP method
   * @param baseUrl the base URL
   * @param path the path
   * @param authenticationProvider the authentication provider
   * @param progressListener a response progress listener
   * @param progressRequestListener a request progress listener
   * @throws ApiException an api exception
   */
  public ApiRequest(String method, String baseUrl, String path, AuthenticationProvider authenticationProvider, ProgressResponseBody.ProgressListener progressListener, ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
    _baseUrl = baseUrl;
    _method = method;
    _path = path;

    _authenticationProvider = authenticationProvider;
    _progressListener = progressListener;
    _progressRequestListener = progressRequestListener;

    _pathParameters = new HashMap<>();
    _headerParameters = new HashMap<>();
    _queryParameters = new HashMap<>();
  }

  /**
   * Adds a path parameter
   * @param name the name
   * @param value the value
   */
  public void addPathParameter(String name, Object value) {
    if (value == null)
      return;
    if (value instanceof String) {
      _pathParameters.put(name, value.toString());
    } else if (value instanceof Boolean) {
      _pathParameters.put(name, (boolean) value ? "true" : "false");
    } else {
      _pathParameters.put(name, value.toString());
    }
  }

  /**
   * Adds a query parameter
   * @param name the name
   * @param value the value
   */
  public void addQueryParameter(String name, Object value) {
    if (value == null)
      return;
    if (!_queryParameters.containsKey(name))
      _queryParameters.put(name, new ArrayList<>());
    List<String> parameterValues = _queryParameters.get(name);
    if (value instanceof String) {
      parameterValues.add((String) value);
    } else if (value instanceof Boolean) {
      parameterValues.add((boolean) value ? "true" : "false");
    } else if (value instanceof List) {
      for (Object innerValue : (List) value) {
        addQueryParameter(name, innerValue);
      }
    } else {
      parameterValues.add(value.toString());
    }
  }

  /**
   * Adds a pattern parameter
   * @param parameters a pattern parameter
   * @param pattern the pattern
   * @param <T> the value type of the pattern
   * @throws ApiException an api exception
   */
  public <T> void addPatternQueryParameter(Map<String, T> parameters, String pattern) throws ApiException {
    if (parameters == null)
      return;
    for (Map.Entry<String, T> entry : parameters.entrySet()) {
      if (entry.getKey().matches(pattern)) {
        addQueryParameter(entry.getKey(), entry.getValue());
      } else {
        throw new ApiException("Invalid parameter " + entry.getKey() + ", parameter name must match the regular expression \"" + pattern + "\"");
      }
    }
  }

  /**
   * Adds an header parameter
   * @param name the name
   * @param value the value
   */
  public void addHeaderParameter(String name, String value) {
    if (value == null)
      return;
    _headerParameters.put(name, value);
  }

  /**
   * Adds a body to the request
   * @param contentType the content type
   * @param value the body content
   */
  public void addBodyParameter(String contentType, Object value) {
    if (value == null)
      return;

    if (value instanceof byte[]) {
      _requestBody = RequestBody.create(MediaType.parse(contentType), (byte[]) value);
    } else if (value instanceof String) {
      _requestBody = RequestBody.create(MediaType.parse(contentType), (String) value);
    } else {
      _requestBody = RequestBody.create(MediaType.parse(contentType), new JSON().serialize(value));
    }
  }

  private String singleParameterToString(Object obj) {
    if (obj instanceof Boolean)
      return (boolean) obj ? "true" : "false";
    else if (obj != null)
      return obj.toString();
    else
      return null;
  }

  private List<String> serializeQueryParameter(boolean value) {
    ArrayList<String> ret = new ArrayList<>();
    ret.add(value ? "true" : "false");
    return ret;
  }

  private List<String> serializeQueryParameter(int value) {
    ArrayList<String> ret = new ArrayList<>();
    ret.add(Integer.toString(value));
    return ret;
  }

  private List<String> SerializeQueryParameter(String value) {
    ArrayList<String> ret = new ArrayList<>();
    ret.add(value);
    return ret;
  }

  private List<String> serializeQueryParameter(Object obj) {
    if (obj instanceof List) {
      List<String> ret = new ArrayList<String>();
      for (Object item : (List) obj) {
        ret.add(singleParameterToString(item));
      }
      return ret;
    } else {
      ArrayList<String> ret = new ArrayList<>();
      ret.add(singleParameterToString(obj));
      return ret;
    }

  }

  /**
   * Returns an OkHttp request
   * @return an OkHttp request
   */
  public Request getRequest() {
    final String url = buildUrl(false);
    final Request.Builder reqBuilder = new Request.Builder().url(url);

    for (Map.Entry<String, String> param : _headerParameters.entrySet()) {
      reqBuilder.header(param.getKey(), escapeString(param.getValue()));
    }

    RequestBody reqBody;
    if (!HttpMethod.permitsRequestBody(_method)) {
      //We cannot send a body
      reqBody = null;
    } else if (_requestBody == null && "DELETE".equals(_method)) {
      //We can avoid send a body
      reqBody = null;
    } else if (_requestBody == null) {
      //We need to send a body for OkHttp limitations
      reqBody = RequestBody.create(null, "");
    } else if (_progressRequestListener != null) {
      //We send the body encapsulated
      reqBody = new ProgressRequestBody(_requestBody, _progressRequestListener);
    } else {
      //We send the body as is
      reqBody = _requestBody;
    }
    reqBuilder.method(_method, reqBody);

    return reqBuilder.build();
  }

  /**
   * Returns the string representation
   * @return the string representation
   */
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(_method + " " + buildUrl(true) + "\n");

    for (Map.Entry<String, String> param : _headerParameters.entrySet()) {
      sb.append(param.getKey() + ": " + escapeString(param.getValue()));
    }
    return sb.toString();
  }

  /**
   * Builds the url for the request
   * @param omitSensitiveParameters whether to omitSensitiveParameters
   * @return the url
   */
  public String buildUrl(boolean omitSensitiveParameters) {
    String replacedPath = _path;
    if (_pathParameters != null && !_pathParameters.isEmpty()) {
      for (Map.Entry<String, String> parameter : _pathParameters.entrySet()) {
        replacedPath = replacedPath.replace("{" + parameter.getKey() + "}", escapeString(parameter.getValue()));
      }
    }
    replacedPath = replacedPath.replaceAll("/\\{[^}]+\\}", "");

    final StringBuilder url = new StringBuilder();
    url.append(_baseUrl).append(replacedPath);

    if (_queryParameters != null && !_queryParameters.isEmpty()) {
      List<String> escapedParameters = new ArrayList<String>();

      for (Map.Entry<String, List<String>> parameter : _queryParameters.entrySet()) {
        for (String value : parameter.getValue()) {
          if (omitSensitiveParameters && _authenticationProvider != null && _authenticationProvider.getSensitiveParameters().contains(parameter.getKey()))
            escapedParameters.add(escapeString(parameter.getKey()) + "=***");
          else
            escapedParameters.add(escapeString(parameter.getKey()) + "=" + escapeString(value));
        }
      }
      url.append("?");
      url.append(String.join("&", escapedParameters));
    }

    return url.toString();
  }

  private String escapeString(String str) {
    try {
      return URLEncoder.encode(str, "utf8").replaceAll("\\+", "%20");
    } catch (UnsupportedEncodingException e) {
      return str;
    }
  }
}