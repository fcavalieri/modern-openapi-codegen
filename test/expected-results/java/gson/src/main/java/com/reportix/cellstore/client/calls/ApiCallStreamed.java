/*
 * This is a titl*
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 *
 */

package com.reportix.cellstore.client.calls;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.reportix.cellstore.client.ApiCallback;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.client.JSON;
import com.reportix.cellstore.client.requests.ApiRequest;
import com.reportix.cellstore.client.responses.ApiResponse;
import com.squareup.okhttp.Response;
import io.gsonfire.builders.JsonObjectBuilder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.concurrent.*;

/**
 * Streamed Api Call class.
 * @param <T> Type of the items
 * @param <U> Type of the metadata
  */
public class ApiCallStreamed<T, U> extends ApiCall implements AutoCloseable {
  public static JsonObject POISON = new JsonObjectBuilder().set("__poison__", true).build();
  private final int CACHED_ITEMS = 1000;
  private final String RESULT_FIELD = "results";

  private final ExecutorService _executor = Executors.newSingleThreadExecutor();
  private BlockingQueue<JsonObject> _resultObjects;
  private Future<?> _producer;

  /**
   * The response metadata
   */
  protected U _metadata;

  /**
   * The result iterator
   */
  protected StreamedApiIterator<T> _iterator;
  private ApiException _exception;
  private Type _metadataType;

  private RequestPreliminaryOutcome _requestOutcome;
  private int _currentRetry;

  /// <summary>
  /// The semaphore is locked at request start.
  /// The request is made in another thread and the main thread waits acquiring the semaphore.
  /// Once the request produces a response, it is consumed enough to determine if it is an error or read its metadata.
  /// The response data (requestOutcome/exception/metadata) is then populated.
  /// The semaphore is then released no matter what by the secondary thread.
  /// The main thread processes the request outcome and returns to the caller.
  /// If the request did not produce an error, it is consumed in the background.
  /// Note: no attempt is made to release the semaphore exactly once. It might be released more than once to simplify code.
  /// </summary>
  private Semaphore semaphore;

  public ApiCallStreamed(ApiClient apiClient, ApiRequest apiRequest, String apiMethodName, Type returnType, Type metadataType) {
    super(apiClient, apiRequest, apiMethodName, returnType);

    _metadataType = metadataType;
    _resultObjects = new LinkedBlockingDeque<JsonObject>(CACHED_ITEMS);
    _iterator = new StreamedApiIterator<T>(this, _resultObjects, returnType);
  }

  /**
   * Gets the api call metadata
   * @return The api call metadata
   */
  public U getMetadata() {
    return _metadata;
  }

  private void startRequest() {
    try {
      LocalDateTime requestTime = LocalDateTime.now();
      Response response = _apiClient.execute(_apiRequest, _apiMethodName);
      _apiResponse = new ApiResponse(response, requestTime);
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      semaphore.release();
      return;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      semaphore.release();
      throw t;
    }

    //There are no retries left or the response has no retryable errors (either success or not retryable)
    try {
      checkResponseStatus(_apiResponse);
      //The response is successful
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      semaphore.release();
      return;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      semaphore.release();
      throw t;
    }

    _producer = _executor.submit(() -> {
      //A non-error response was produced. The caller is still waiting to be unlocked
      try (JsonReader jsonReader = new JsonReader(_apiResponse.body().charStream())) {
        //A non-error response was produced. The caller is still waiting to be unlocked
        //ProcessMetadata will extract the metadata and no-matter-what release the semaphore
        boolean hasData = processMetadata(jsonReader);

        // Control is returned to the caller of Execute() here, because processMetadata has released the semaphore.
        // The rest is running in the background. Errors are put in the stream
        //ProcessData will process the data items and no-matter-what add the POISON at the end
        if (hasData) {
          processData(jsonReader);
        } else {
          _resultObjects.put(POISON);
        }
      } catch (InterruptedException e) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, e, this);
        throw new RuntimeException(e);
      } catch (IOException e) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, e, this);
        return;
      } catch (Throwable t) {
        _requestOutcome = RequestPreliminaryOutcome.FAILURE;
        _exception = new ApiException("Error calling " + _apiMethodName, t, this);
        throw t;
      } finally {
        semaphore.release();
      }
    });
  }

  private boolean processMetadata(JsonReader jsonReader) {
    try {
      _metadata = JSON.parse(readMetadata(jsonReader), _metadataType);
      JsonToken nextToken = jsonReader.peek();
      if (JsonToken.END_DOCUMENT.equals(nextToken)) {
        //Case 1: there is no result field
        _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
        return false;
      } else if (JsonToken.NULL.equals(nextToken)) {
        //Case 2: there is a null result field
        jsonReader.nextNull();
        jsonReader.endObject(); // End of response object
        _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
        return false;
      }
      //Case 3: there is a non-empty result field
      else if (JsonToken.BEGIN_ARRAY.equals(nextToken)) {
        jsonReader.beginArray();
        _requestOutcome = RequestPreliminaryOutcome.SUCCESS;
        return true;
      } else {
        throw new ApiException(
                "Unexpected token " + nextToken + ", expecting " +
                        JsonToken.BEGIN_ARRAY + " or " + JsonToken.NULL + " or " + JsonToken.END_DOCUMENT, this);
      }
    } catch (ApiException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = e;
      return false;
    } catch (IOException e) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      return false;
    } catch (Throwable t) {
      _requestOutcome = RequestPreliminaryOutcome.FAILURE;
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      throw t;
    } finally {
      semaphore.release();
    }
  }

  private void processData(JsonReader jsonReader) throws InterruptedException {

    try {
        while (true) {
          JsonToken nextItemToken = jsonReader.peek();
          if (JsonToken.BEGIN_OBJECT.equals(nextItemToken)) {
            JsonObject obj = _json.getGson().fromJson(jsonReader, JsonObject.class);
            _resultObjects.put(obj); //Blocking call
          } else if (JsonToken.END_ARRAY.equals(nextItemToken)) {
            break;
          }
          else {
            throw new ApiException(
                    "Unexpected token " + nextItemToken + ", expecting " +
                            JsonToken.END_ARRAY + " or " + JsonToken.BEGIN_OBJECT, this);
          }
        }
        jsonReader.endArray();
        jsonReader.endObject(); // End of response object
        if (!JsonToken.END_DOCUMENT.equals(jsonReader.peek()))
          throw new ApiException("Unexpected content at the end of response", this);
    } catch (InterruptedException e) {
      _resultObjects.put(throwableToJsonError(e));
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      throw e;
    } catch (Exception e) {
      //Other exceptions are thrown in case of communication errors
      _exception = new ApiException("Error calling " + _apiMethodName, e, this);
      _resultObjects.put(throwableToJsonError(e));
    } catch (Throwable t) {
      //Other exceptions are thrown in case of communication errors
      _exception = new ApiException("Error calling " + _apiMethodName, t, this);
      _resultObjects.put(throwableToJsonError(t));
      throw t;
    }

    finally {
      //Signal any consumer that the stream has been fully produced
      _resultObjects.put(POISON);
    }
  }

  private JsonObject throwableToJsonError(Throwable t) {
    JsonObject error = new JsonObject();
    error.addProperty("__error__", true);
    error.addProperty("status", 500);
    error.addProperty("type", "Internal Error");
    error.addProperty("code", "STREAMING_ERROR");
    error.addProperty("message", t.toString());
    return error;
  }

  /**
   * Executes the api call
   * @throws ApiException api exception
   */
  public void execute() throws ApiException {
    for (; ; ) {
      semaphore = new Semaphore(0);
      startRequest();

      try {
        semaphore.acquire(); // Waits for the metadata/error to be produced
        switch (_requestOutcome)
        {
          case SUCCESS:
            return;
          case RETRY:
            break;
          case FAILURE:
            if (_producer != null) {
              _producer.cancel(true);
            }
            throw _exception;
          default:
            throw new ApiException("Unexpected request outcome: " + _requestOutcome, this);
        }

      } catch (InterruptedException e) {
        throw new ApiException("Error calling " + _apiMethodName, e, this);
      }

      return;
    }
  }

  private JsonObject readMetadata(JsonReader jsonReader) throws ApiException {
    try {
      JsonObject ret = new JsonObject();
      jsonReader.beginObject();
      while (true) {
        JsonToken nextToken = jsonReader.peek();
        if (JsonToken.END_OBJECT.equals(nextToken)) {
          jsonReader.endObject();
          break;
        }

        String fieldName = jsonReader.nextName();
        if (fieldName.equals(RESULT_FIELD)) {// Field with list of items
          break;
        }
        ret.add(fieldName, _json.getGson().fromJson(jsonReader, JsonElement.class));
      }
      return ret;
    } catch (IllegalStateException | IOException e) {
      throw new ApiException("Error calling " + _apiMethodName, e, this);
    }
  }

  @Override
  public void close() {
    if (_producer != null)
      _producer.cancel(true);
  }

  private enum RequestPreliminaryOutcome {SUCCESS, RETRY, FAILURE}
}