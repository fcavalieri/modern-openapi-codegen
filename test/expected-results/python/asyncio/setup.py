"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""

from setuptools import setup, find_packages  # noqa: H301

NAME = "cellstore"
VERSION = "1.2.3"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["certifi >= 2020.12.5", "urllib3 >= 1.26.4", "ijson >= 3.1.4", "psutil >= 5.8.0"]
REQUIRES.append("aiohttp >= 3.7.4.post0")
REQUIRES.append("aiodns >= 2.0.0")

setup(
    name=NAME,
    version=VERSION,
    description="This is a titl*",
    author_email="",
    url="https://www.reportix.com",
    keywords=["Swagger", "This is a titl*"],
    install_requires=REQUIRES,
    packages=find_packages(),
    include_package_data=True,
    long_description="""\
    This is a descriptio*  # noqa: E501
    """
)
