"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
from abc import ABC
from urllib.parse import urlparse

from cellstore.configuration import Configuration
from cellstore.client.api_client import ApiClient
from cellstore.client.api_exception import ApiException


class ApiBase(ABC):
    def __init__(self, base_path: str, configuration: Configuration = None):
        self.base_path = base_path
        self.__validate_base_path()
        self.api_client = ApiClient(configuration) if configuration is not None else ApiClient(Configuration())

    def __validate_base_path(self) -> None:
        if self.base_path is None or self.base_path == "":
            raise ApiException("Invalid base path, cannot be empty or null")

        try:
            url = urlparse(self.base_path)
            if not bool(url.netloc):
                raise ApiException(f"{self.base_path}: invalid base path. Must be absolute.")

            if url.scheme not in ("http", "https"):
                raise ApiException(f"{self.base_path}: invalid base path. Scheme must be http or https.")

            if url.username is not None or url.password is not None:
                raise ApiException(f"{self.base_path}: invalid base path. Cannot contain credentials in the Uri.")

            if url.query is not None and url.query != "":
                raise ApiException(f"{self.base_path}: invalid base path. Cannot contain query in the Uri.")

            if url.fragment is not None and url.fragment != "":
                raise ApiException(f"{self.base_path}: invalid base path. Cannot contain fragment in the Uri.")

            if url.path is None or url.path == "" or len(url.path.split("/")) != 2:
                raise ApiException(f"{self.base_path}: invalid base path. It must contain 2 segments.")
        except ValueError as e:
            raise ApiException(f"{self.base_path}: invalid base path", inner_exception=e)
