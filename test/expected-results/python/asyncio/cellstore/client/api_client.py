"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
import datetime
import ssl

import aiohttp
import certifi

from cellstore.client.api_request import ApiRequest
from cellstore.client.api_response import ApiResponse
from cellstore.configuration import Configuration


class ApiClient:
    def __init__(self, configuration=None):
        if configuration is None:
            configuration = Configuration()
        self.configuration = configuration

    def __init_rest_client(self) -> aiohttp.ClientSession:
        ssl_context = ssl.create_default_context(cafile=certifi.where())

        connector = aiohttp.TCPConnector(
            ssl=ssl_context,
            verify_ssl=self.configuration.verify_ssl
        )

        client_session = aiohttp.ClientSession(
            connector=connector,
            timeout=aiohttp.ClientTimeout(sock_read=self.configuration.timeout_ms)
        )

        return client_session

    async def execute(self, api_request: ApiRequest, api_method_name: str) -> ApiResponse:
        client_session = self.__init_rest_client()
        if self.configuration.authentication_provider is not None:
            self.configuration.authentication_provider.add_authentication(client_session, api_request)

        request_time = datetime.datetime.now()
        response = await client_session.request(method=api_request.method,
                                                url=api_request.compute_url(),
                                                headers=api_request.header_parameters,
                                                params=api_request.query_parameters,
                                                data=api_request.request_body)
        api_response = ApiResponse(response, request_time)
        try:
            if self.configuration.authentication_provider is not None:
                self.configuration.authentication_provider.process_response(client_session, api_request, api_response)
            return api_response
        except Exception as e:
            api_response.close()
            raise e
