"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
from abc import ABC, abstractmethod

from urllib3 import PoolManager, HTTPResponse

from cellstore.client.api_request import ApiRequest
from cellstore.client.api_response import ApiResponse


class AuthenticationProvider(ABC):

    @abstractmethod
    def add_authentication(self, pool_manager: PoolManager, api_request: ApiRequest):
        pass

    @abstractmethod
    def process_response(self, pool_manager: PoolManager, api_request: ApiRequest, api_response: ApiResponse):
        pass


class Unauthenticated(AuthenticationProvider):

    def process_response(self, pool_manager: PoolManager, api_request: ApiRequest, api_response: ApiResponse):
        pass

    def add_authentication(self, pool_manager: PoolManager, api_request: ApiRequest):
        pass

    def __str__(self):
        return "Unauthenticated"
