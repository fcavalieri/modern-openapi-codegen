"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
import sys
import traceback
from typing import Any
import psutil
import os
import platform


class ApiException(Exception):
    def __init__(self, message: str, inner_exception: Exception = None, api_call: Any = None, status: int = None):
        self.message = message
        self.inner_exception = inner_exception
        self.api_call = api_call
        self.status = status
        super().__init__(message)

    def system_info(self) -> str:
        pid = os.getpid()
        py = psutil.Process(pid)

        diagnostics = f"Running Python library version 1.2.3, built for the API specification 1.0.0-TESTS as a {'64bit' if sys.maxsize > 2**32 else '32bit'} process " + \
                      f"on {platform.python_implementation()} {platform.python_version()} in a {platform.architecture()[0]} {platform.platform(True, False)} OS.\n" + \
                      f"System has {int(psutil.virtual_memory().total /1024 /1024)} MB, of which {int(psutil.virtual_memory().available /1024 /1024)} MB are currently free. " + \
                      f"Application is using {int(py.memory_info().rss /1024/1024)} MB."
        return diagnostics

    def __str__(self) -> str:
        if self.api_call is not None and self.api_call.api_client is not None and self.api_call.api_client.configuration is not None and not self.api_call.api_client.configuration.verbose_exceptions:
            return f'{self.message}'

        return f"{self.message}\n" + \
               (traceback.format_exc() + "\n" if self.inner_exception is not None else "") +\
               "===System===\n" + \
               self.system_info() + "\n" + \
               ("===Api Call===\n" + str(self.api_call) if self.api_call is not None else "")
