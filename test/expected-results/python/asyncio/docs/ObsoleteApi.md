# cellstore.ObsoleteApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_obsolete_operation**](ObsoleteApi.md#get_obsolete_operation) | **GET** /obsolete-operation | Obsolete summary

# **get_obsolete_operation**
> dict[str, Any] get_obsolete_operation(limit=limit)

Obsolete summary

Obsolete description

This operation is obsolete

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ObsoleteApi()
limit = ... # Optional[int] | Limits the amount of documents in the result of the query.  (optional) (default to 50)

try:
    # Obsolete summary
    api_response = api_instance.get_obsolete_operation(limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ObsoleteApi->get_obsolete_operation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Optional[int]**| Limits the amount of documents in the result of the query.  | [optional] [default to 50]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

