# cellstore.ParametersApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_path_parameters**](ParametersApi.md#get_path_parameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
[**get_query_parameters**](ParametersApi.md#get_query_parameters) | **GET** /query-parameters | Summary
[**post_binary_body**](ParametersApi.md#post_binary_body) | **POST** /binary-body | Summary
[**post_binary_body_optional**](ParametersApi.md#post_binary_body_optional) | **POST** /binary-body-optional | Summary
[**post_excluded_binary_body**](ParametersApi.md#post_excluded_binary_body) | **POST** /excluded-binary-body | Summary
[**post_object_body**](ParametersApi.md#post_object_body) | **POST** /object-body | Summary
[**post_object_body_optional**](ParametersApi.md#post_object_body_optional) | **POST** /object-body-optional | Summary
[**post_overridden_binary_body**](ParametersApi.md#post_overridden_binary_body) | **POST** /overridden-binary-body | Summary
[**post_string_body**](ParametersApi.md#post_string_body) | **POST** /string-body | Summary
[**post_string_body_optional**](ParametersApi.md#post_string_body_optional) | **POST** /string-body-optional | Summary
[**post_variant_body_zip_archive**](ParametersApi.md#post_variant_body_zip_archive) | **POST** /variant-body | Summary
[**post_variant_body_xhtml**](ParametersApi.md#post_variant_body_xhtml) | **POST** /variant-body | Summary
[**post_variant_body_html**](ParametersApi.md#post_variant_body_html) | **POST** /variant-body | Summary

# **get_path_parameters**
> dict[str, Any] get_path_parameters(id1, id2=id2, id3=id3, id4=id4, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
id1 = ... # int | Description
id2 = ... # Optional[str] | Description (optional)
id3 = ... # Optional[bool] | Description (optional)
id4 = ... # Optional[Union[float, Decimal]] | Description (optional)
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.get_path_parameters(id1, id2=id2, id3=id3, id4=id4, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->get_path_parameters: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id1** | **int**| Description | 
 **id2** | **Optional[str]**| Description | [optional] 
 **id3** | **Optional[bool]**| Description | [optional] 
 **id4** | **Optional[Union[float, Decimal]]**| Description | [optional] 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_query_parameters**
> dict[str, Any] get_query_parameters(boolean_mandatory_without_default, integer_mandatory_without_default, number_mandatory_without_default, string_mandatory_without_default, boolean_array_mandatory_without_default, integer_array_mandatory_without_default, number_array_mandatory_without_default, string_array_mandatory_without_default, pattern_parameter_array_mandatory, pattern_parameter_single_mandatory, overridden_type_mandatory, fail=fail, boolean_optional_with_default=boolean_optional_with_default, boolean_optional_without_default=boolean_optional_without_default, integer_optional_with_default=integer_optional_with_default, integer_optional_without_default=integer_optional_without_default, number_optional_with_default=number_optional_with_default, number_optional_without_default=number_optional_without_default, string_optional_with_default=string_optional_with_default, string_optional_without_default=string_optional_without_default, boolean_array_optional_with_default=boolean_array_optional_with_default, boolean_array_optional_without_default=boolean_array_optional_without_default, integer_array_optional_with_default=integer_array_optional_with_default, integer_array_optional_without_default=integer_array_optional_without_default, number_array_optional_with_default=number_array_optional_with_default, number_array_optional_without_default=number_array_optional_without_default, string_array_optional_with_default=string_array_optional_with_default, string_array_optional_without_default=string_array_optional_without_default, pattern_parameter_array_optional=pattern_parameter_array_optional, pattern_parameter_single_optional=pattern_parameter_single_optional, overridden_type_optional=overridden_type_optional)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
boolean_mandatory_without_default = ... # bool | Description
integer_mandatory_without_default = ... # int | Description
number_mandatory_without_default = ... # Union[float, Decimal] | Description
string_mandatory_without_default = ... # str | Description
boolean_array_mandatory_without_default = ... # list[bool] | Description
integer_array_mandatory_without_default = ... # list[int] | Description
number_array_mandatory_without_default = ... # list[Union[float, Decimal]] | Description
string_array_mandatory_without_default = ... # list[str] | Description
pattern_parameter_array_mandatory = ... # dict[str, list[str]] | Description
pattern_parameter_single_mandatory = ... # dict[str, str] | Description
overridden_type_mandatory = ... # int | Description
fail = ... # Optional[bool] | Description (optional) (default to false)
boolean_optional_with_default = ... # Optional[bool] | Description (optional) (default to true)
boolean_optional_without_default = ... # Optional[bool] | Description (optional)
integer_optional_with_default = ... # Optional[int] | Description (optional) (default to 2)
integer_optional_without_default = ... # Optional[int] | Description (optional)
number_optional_with_default = ... # Optional[Union[float, Decimal]] | Description (optional) (default to 2)
number_optional_without_default = ... # Optional[Union[float, Decimal]] | Description (optional)
string_optional_with_default = ... # Optional[str] | Description (optional) (default to def)
string_optional_without_default = ... # Optional[str] | Description (optional)
boolean_array_optional_with_default = ... # Optional[list[bool]] | Description (optional)
boolean_array_optional_without_default = ... # Optional[list[bool]] | Description (optional)
integer_array_optional_with_default = ... # Optional[list[int]] | Description (optional)
integer_array_optional_without_default = ... # Optional[list[int]] | Description (optional)
number_array_optional_with_default = ... # Optional[list[Union[float, Decimal]]] | Description (optional)
number_array_optional_without_default = ... # Optional[list[Union[float, Decimal]]] | Description (optional)
string_array_optional_with_default = ... # Optional[list[str]] | Description (optional)
string_array_optional_without_default = ... # Optional[list[str]] | Description (optional)
pattern_parameter_array_optional = ... # Optional[dict[str, list[str]]] | Description (optional)
pattern_parameter_single_optional = ... # Optional[dict[str, str]] | Description (optional)
overridden_type_optional = ... # Optional[int] | Description (optional)

try:
    # Summary
    api_response = api_instance.get_query_parameters(boolean_mandatory_without_default, integer_mandatory_without_default, number_mandatory_without_default, string_mandatory_without_default, boolean_array_mandatory_without_default, integer_array_mandatory_without_default, number_array_mandatory_without_default, string_array_mandatory_without_default, pattern_parameter_array_mandatory, pattern_parameter_single_mandatory, overridden_type_mandatory, fail=fail, boolean_optional_with_default=boolean_optional_with_default, boolean_optional_without_default=boolean_optional_without_default, integer_optional_with_default=integer_optional_with_default, integer_optional_without_default=integer_optional_without_default, number_optional_with_default=number_optional_with_default, number_optional_without_default=number_optional_without_default, string_optional_with_default=string_optional_with_default, string_optional_without_default=string_optional_without_default, boolean_array_optional_with_default=boolean_array_optional_with_default, boolean_array_optional_without_default=boolean_array_optional_without_default, integer_array_optional_with_default=integer_array_optional_with_default, integer_array_optional_without_default=integer_array_optional_without_default, number_array_optional_with_default=number_array_optional_with_default, number_array_optional_without_default=number_array_optional_without_default, string_array_optional_with_default=string_array_optional_with_default, string_array_optional_without_default=string_array_optional_without_default, pattern_parameter_array_optional=pattern_parameter_array_optional, pattern_parameter_single_optional=pattern_parameter_single_optional, overridden_type_optional=overridden_type_optional)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->get_query_parameters: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boolean_mandatory_without_default** | **bool**| Description | 
 **integer_mandatory_without_default** | **int**| Description | 
 **number_mandatory_without_default** | **Union[float, Decimal]**| Description | 
 **string_mandatory_without_default** | **str**| Description | 
 **boolean_array_mandatory_without_default** | [**list[bool]**](bool.md)| Description | 
 **integer_array_mandatory_without_default** | [**list[int]**](int.md)| Description | 
 **number_array_mandatory_without_default** | [**list[Union[float, Decimal]]**](Union[float, Decimal].md)| Description | 
 **string_array_mandatory_without_default** | [**list[str]**](str.md)| Description | 
 **pattern_parameter_array_mandatory** | [**dict[str, list[str]]**](str.md)| Description | 
 **pattern_parameter_single_mandatory** | **dict[str, str]**| Description | 
 **overridden_type_mandatory** | **int**| Description | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **boolean_optional_with_default** | **Optional[bool]**| Description | [optional] [default to true]
 **boolean_optional_without_default** | **Optional[bool]**| Description | [optional] 
 **integer_optional_with_default** | **Optional[int]**| Description | [optional] [default to 2]
 **integer_optional_without_default** | **Optional[int]**| Description | [optional] 
 **number_optional_with_default** | **Optional[Union[float, Decimal]]**| Description | [optional] [default to 2]
 **number_optional_without_default** | **Optional[Union[float, Decimal]]**| Description | [optional] 
 **string_optional_with_default** | **Optional[str]**| Description | [optional] [default to def]
 **string_optional_without_default** | **Optional[str]**| Description | [optional] 
 **boolean_array_optional_with_default** | [**Optional[list[bool]]**](bool.md)| Description | [optional] 
 **boolean_array_optional_without_default** | [**Optional[list[bool]]**](bool.md)| Description | [optional] 
 **integer_array_optional_with_default** | [**Optional[list[int]]**](int.md)| Description | [optional] 
 **integer_array_optional_without_default** | [**Optional[list[int]]**](int.md)| Description | [optional] 
 **number_array_optional_with_default** | [**Optional[list[Union[float, Decimal]]]**](Union[float, Decimal].md)| Description | [optional] 
 **number_array_optional_without_default** | [**Optional[list[Union[float, Decimal]]]**](Union[float, Decimal].md)| Description | [optional] 
 **string_array_optional_with_default** | [**Optional[list[str]]**](str.md)| Description | [optional] 
 **string_array_optional_without_default** | [**Optional[list[str]]**](str.md)| Description | [optional] 
 **pattern_parameter_array_optional** | [**Optional[dict[str, list[str]]]**](str.md)| Description | [optional] 
 **pattern_parameter_single_optional** | **Optional[dict[str, str]]**| Description | [optional] 
 **overridden_type_optional** | **Optional[int]**| Description | [optional] 

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_binary_body**
> bytes post_binary_body(body_name, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
body_name = ... # bytes | Description
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_binary_body(body_name, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_binary_body: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body_name** | [**bytes**](object.md)| Description | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

[**bytes**](.md)

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_binary_body_optional**
> bytes post_binary_body_optional(body_name=body_name, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
body_name = ... # Optional[bytes] | Description (optional)
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_binary_body_optional(body_name=body_name, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_binary_body_optional: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body_name** | [**Optional[bytes]**](object.md)| Description | [optional] 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

[**bytes**](.md)

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_excluded_binary_body**
> str post_excluded_binary_body()

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()

try:
    # Summary
    api_response = api_instance.post_excluded_binary_body()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_excluded_binary_body: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_object_body**
> dict[str, Any] post_object_body(archive, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
archive = ... # dict | An archive item (JSON).
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_object_body(archive, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_object_body: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**dict**](object.md)| An archive item (JSON). | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_object_body_optional**
> dict[str, Any] post_object_body_optional(archive=archive, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
archive = ... # Optional[dict] | An archive item (JSON). (optional)
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_object_body_optional(archive=archive, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_object_body_optional: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**Optional[dict]**](object.md)| An archive item (JSON). | [optional] 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_overridden_binary_body**
> str post_overridden_binary_body(archive)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
archive = ... # bytes | An archive item (XML).

try:
    # Summary
    api_response = api_instance.post_overridden_binary_body(archive)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_overridden_binary_body: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**bytes**](object.md)| An archive item (XML). | 

### Return type

**str**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_string_body**
> str post_string_body(archive, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
archive = ... # str | An archive item (XML).
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_string_body(archive, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_string_body: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**str**](object.md)| An archive item (XML). | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**str**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_string_body_optional**
> str post_string_body_optional(archive=archive, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
archive = ... # Optional[str] | An archive item (XML). (optional)
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_string_body_optional(archive=archive, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_string_body_optional: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | [**Optional[str]**](object.md)| An archive item (XML). | [optional] 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**str**

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_variant_body_zip_archive**
> dict[str, Any] post_variant_body_zip_archive(template, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
template = ... # bytes | A Zip Archive
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_variant_body_zip_archive(template, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_variant_body_zip_archive: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **bytes**| A Zip Archive | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: application/zip
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_variant_body_xhtml**
> dict[str, Any] post_variant_body_xhtml(template, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
template = ... # str | A XHTML file
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_variant_body_xhtml(template, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_variant_body_xhtml: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **str**| A XHTML file | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: application/xhtml+xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_variant_body_html**
> dict[str, Any] post_variant_body_html(template, fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ParametersApi()
template = ... # str | A HTML file
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_variant_body_html(template, fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ParametersApi->post_variant_body_html: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **str**| A HTML file | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: text/html
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

