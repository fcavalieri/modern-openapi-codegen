# cellstore.ResponsesApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**empty_response**](ResponsesApi.md#empty_response) | **GET** /empty-response | Summary
[**get_binary_response**](ResponsesApi.md#get_binary_response) | **GET** /binary-response | Summary
[**get_json_response**](ResponsesApi.md#get_json_response) | **GET** /json-response | Summary
[**get_xml_response**](ResponsesApi.md#get_xml_response) | **GET** /xml-response | Summary
[**list_json_response**](ResponsesApi.md#list_json_response) | **GET** /list-json-response | Summary
[**post_json_response**](ResponsesApi.md#post_json_response) | **POST** /json-response | Summary
[**stream_json_response**](ResponsesApi.md#stream_json_response) | **GET** /stream-json-response | Summary
[**get_arbitrary_response_as_list**](ResponsesApi.md#get_arbitrary_response_as_list) | **GET** /arbitrary-response | Summary
[**get_arbitrary_response_as_stream**](ResponsesApi.md#get_arbitrary_response_as_stream) | **GET** /arbitrary-response | Summary
[**get_arbitrary_response_as_xml**](ResponsesApi.md#get_arbitrary_response_as_xml) | **GET** /arbitrary-response | Summary
[**get_arbitrary_response_as_binary**](ResponsesApi.md#get_arbitrary_response_as_binary) | **GET** /arbitrary-response | Summary
[**get_arbitrary_response_as_binary_obsolete**](ResponsesApi.md#get_arbitrary_response_as_binary_obsolete) | **GET** /arbitrary-response | Summary
[**get_arbitrary_response_as_json**](ResponsesApi.md#get_arbitrary_response_as_json) | **GET** /arbitrary-response | Summary

# **empty_response**
> None empty_response(fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.empty_response(fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->empty_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

[**None**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_binary_response**
> bytes get_binary_response(fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.get_binary_response(fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_binary_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

[**bytes**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_json_response**
> dict[str, Any] get_json_response(fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.get_json_response(fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_json_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_xml_response**
> str get_xml_response(fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.get_xml_response(fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_xml_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**str**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_json_response**
> ISequence[dict[str, Any]] list_json_response(fail=fail, fail_at=fail_at, sleep=sleep, amount=amount)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)

try:
    # Summary
    api_response = api_instance.list_json_response(fail=fail, fail_at=fail_at, sleep=sleep, amount=amount)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->list_json_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]

### Return type

**ISequence[dict[str, Any]]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_json_response**
> dict[str, Any] post_json_response(fail=fail)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)

try:
    # Summary
    api_response = api_instance.post_json_response(fail=fail)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->post_json_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]

### Return type

**dict[str, Any]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stream_json_response**
> IStreamedSequence[dict[str, Any]] stream_json_response(fail=fail, fail_at=fail_at, sleep=sleep, amount=amount)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)

try:
    # Summary
    api_response = api_instance.stream_json_response(fail=fail, fail_at=fail_at, sleep=sleep, amount=amount)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->stream_json_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]

### Return type

**IStreamedSequence[dict[str, Any]]**

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_list**
> ISequence[dict[str, Any]] get_arbitrary_response_as_list(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant)
required_c = ... # str | Required c (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_c = ... # Optional[str] | Excluded c (A single excluded parameter should be present per variant) (optional)
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant) (optional)
required_d = ... # str | Required d (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_list(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_c** | **Optional[str]**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | [optional] 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

[**ISequence[dict[str, Any]]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_stream**
> IStreamedSequence[dict[str, Any]] get_arbitrary_response_as_stream(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant)
required_d = ... # str | Required d (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_a = ... # Optional[str] | Excluded a (A single excluded parameter should be present per variant) (optional)
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant) (optional)
required_c = ... # str | Required c (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_stream(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_stream: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_a** | **Optional[str]**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | [optional] 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

[**IStreamedSequence[dict[str, Any]]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_xml**
> str get_arbitrary_response_as_xml(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant)
required_c = ... # str | Required c (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_c = ... # Optional[str] | Excluded c (A single excluded parameter should be present per variant) (optional)
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant) (optional)
required_d = ... # str | Required d (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_xml(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_xml: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_c** | **Optional[str]**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | [optional] 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

[**str**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_binary**
> bytes get_arbitrary_response_as_binary(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant)
required_d = ... # str | Required d (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_a = ... # Optional[str] | Excluded a (A single excluded parameter should be present per variant) (optional)
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant) (optional)
required_c = ... # str | Required c (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_binary(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_binary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_a** | **Optional[str]**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | [optional] 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

[**bytes**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_binary_obsolete**
> bytes get_arbitrary_response_as_binary_obsolete(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)

Summary

Description

This variant is obsolete

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant)
required_c = ... # str | Required c (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_c = ... # Optional[str] | Excluded c (A single excluded parameter should be present per variant) (optional)
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant) (optional)
required_d = ... # str | Required d (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_binary_obsolete(required_b, required_c, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_c=excluded_c, required_a=required_a, required_d=required_d)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_binary_obsolete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_c** | **Optional[str]**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | [optional] 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

[**bytes**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_arbitrary_response_as_json**
> dict[str, Any] get_arbitrary_response_as_json(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)

Summary

Description

### Example
```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cellstore.ResponsesApi()
required_a = ... # Optional[str] | Required a (Two required parameter should be optional per variant)
required_d = ... # str | Required d (Two required parameter should be optional per variant)
fail = ... # Optional[bool] | Description (optional) (default to false)
fail_at = ... # Optional[int] | Description (optional) (default to -1)
sleep = ... # Optional[int] | Description (optional) (default to 0)
amount = ... # Optional[int] | Description (optional) (default to 1000)
excluded_a = ... # Optional[str] | Excluded a (A single excluded parameter should be present per variant) (optional)
required_b = ... # Optional[str] | Required b (Two required parameter should be optional per variant) (optional)
required_c = ... # str | Required c (Two required parameter should be optional per variant) (optional)

try:
    # Summary
    api_response = api_instance.get_arbitrary_response_as_json(required_a, required_d, fail=fail, fail_at=fail_at, sleep=sleep, amount=amount, excluded_a=excluded_a, required_b=required_b, required_c=required_c)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResponsesApi->get_arbitrary_response_as_json: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **required_a** | **Optional[str]**| Required a (Two required parameter should be optional per variant) | 
 **required_d** | **str**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **Optional[bool]**| Description | [optional] [default to false]
 **fail_at** | **Optional[int]**| Description | [optional] [default to -1]
 **sleep** | **Optional[int]**| Description | [optional] [default to 0]
 **amount** | **Optional[int]**| Description | [optional] [default to 1000]
 **excluded_a** | **Optional[str]**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **required_b** | **Optional[str]**| Required b (Two required parameter should be optional per variant) | [optional] 
 **required_c** | **str**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

[**dict[str, Any]**](.md)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

