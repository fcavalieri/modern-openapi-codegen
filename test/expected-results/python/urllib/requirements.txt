certifi >= 2020.12.5
setuptools >= 56.0.0
urllib3 >= 1.26.4
ijson >= 3.1.4
psutil >= 5.8.0