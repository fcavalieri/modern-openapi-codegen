"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
import datetime
import json
from abc import ABC, abstractmethod
from decimal import Decimal
from io import BytesIO
from typing import TypeVar, Generic

import ijson

from cellstore.client.api_client import ApiClient
from cellstore.client.api_exception import ApiException
from cellstore.client.api_request import ApiRequest
from cellstore.client.api_response import ApiResponse
from cellstore.models.sequences import ISequence, IStreamedSequence, MaterializedSequence, StreamedSequence

T = TypeVar('T')


class ApiCall(ABC):
    def __init__(self, api_client: ApiClient, api_request: ApiRequest, api_method_name: str, item_type: str):
        self.api_client = api_client
        self.api_request = api_request
        self.api_response = None
        self.api_method_name = api_method_name
        self.item_type = item_type

    def check_response_status(self, response: ApiResponse):
        if response.code() >= 400:
            raise ApiException(f"Error calling {self.api_method_name}: {response.full_body().decode('utf-8')}", api_call=self, status=response.code())

    @abstractmethod
    def execute(self) -> None:
        pass

    def __str__(self) -> None:
        return f"Request:\n{str(self.api_request)}\n-----------------\nResponse:\n{str(self.api_response)}\nConfiguration:\n{str(self.api_client.configuration)}"


class ApiCallSingleton(ApiCall, Generic[T]):
    def __init__(self, api_client: ApiClient, api_request: ApiRequest, api_method_name: str, item_type: str):
        super().__init__(api_client, api_request, api_method_name, item_type)
        self.__result = None

    def get_result(self) -> T:
        return self.__result

    def execute(self) -> None:
        self.api_response = self.api_client.execute(self.api_request, self.api_method_name)
        self.check_response_status(self.api_response)
        self.process_response(self.api_response)

    def process_response(self, api_response: ApiResponse):
        if self.item_type == "bytes":
            self.__result = api_response.full_body()
        elif self.item_type == "str":
            self.__result = api_response.full_body().decode('utf-8')
        elif self.item_type == "dict":
            self.__result = json.loads(api_response.full_body().decode('utf-8') or 'null')
        else:
            raise ApiException(f"Unsupported deserialization type {self.item_type}", api_call=self)


class ApiCallStreamed(ApiCall, Generic[T], ABC):
    RESULT_FIELD = "results"

    def __init__(self, api_client: ApiClient, api_request: ApiRequest, api_method_name: str, item_type: str):
        super().__init__(api_client, api_request, api_method_name, item_type)
        self.api_response = None
        self.generator = None
        self.metadata = None

    def execute(self) -> None:
        self.api_response = self.api_client.execute(self.api_request, self.api_method_name, stream=True)
        try:
            self.check_response_status(self.api_response)
            self.process_response(self.api_response)
        except Exception as e:
            self.api_response.close()
            raise e

    def floaten(self, parse_events):
        for prefix, event, value in parse_events:
            if event == 'number' and isinstance(value, Decimal):
                value = float(value)
            yield prefix, event, value

    def process_response(self, api_response: ApiResponse):
        parse_events = self.floaten(ijson.parse(api_response.response()))
        _, event, _ = parse_events.__next__()
        if event != "start_map":
            raise Exception(event)
        self.metadata = {}
        for prefix, event, value in parse_events:
            if event == "map_key" and value != self.RESULT_FIELD:
                self.metadata[value] = ijson.items(parse_events, value).__next__()
            elif event == "map_key" and value == self.RESULT_FIELD:
                parse_events.__next__()
                break
            elif event == "end_map":
                break
            else:
                raise Exception(event)

        self.generator = self.process_items(ijson.items(parse_events, self.RESULT_FIELD + ".item"))

    def process_items(self, raw_items):
        for raw_item in raw_items:
            if "__error__" not in raw_item:
                yield raw_item
            else:
                if isinstance(raw_item["__error__"], bool):
                    if raw_item["__error__"]:
                        if "status" in raw_item:
                            if isinstance(raw_item["status"], int):
                                raise ApiException(str(raw_item), status=raw_item["status"], api_call=self)
                            else:
                                raise ApiException("An error object with an invalid status field has been produced: " + str(raw_item))
                    else:
                        yield raw_item
                else:
                    raise("An object with an invalid __error__ field has been produced: " + raw_item)


class ApiCallObjectList(ApiCallStreamed, Generic[T], ABC):
    def __init__(self, api_client: ApiClient, api_request: ApiRequest, api_method_name: str, item_type: str):
        super().__init__(api_client, api_request, api_method_name, item_type)
        self.__result = None

    def get_result(self) -> ISequence[T]:
        return self.__result

    def execute(self) -> None:
        super().execute()
        try:
            self.__result = MaterializedSequence(self.generator, self.metadata)
        finally:
            self.api_response.close()


class ApiCallObjectStream(ApiCallStreamed, Generic[T], ABC):
    def __init__(self, api_client: ApiClient, api_request: ApiRequest, api_method_name: str, item_type: str):
        super().__init__(api_client, api_request, api_method_name, item_type)
        self.__result = None

    def get_result(self) -> IStreamedSequence[T]:
        return self.__result

    def execute(self) -> None:
        super().execute()
        self.__result = StreamedSequence(self.api_response, self.generator, self.metadata)
