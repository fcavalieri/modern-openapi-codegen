"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""
from datetime import datetime
from urllib3 import HTTPResponse


class ApiResponse:

    def __init__(self, response: HTTPResponse, request_time: datetime, stream: bool):
        self.__response = response
        self.__request_time = request_time
        self.__response_time = datetime.now()
        self.__stream = stream

    def response(self) -> HTTPResponse:
        return self.__response

    def code(self) -> int:
        return self.__response.status

    def full_body(self) -> bytes:
        return self.__response.data

    def close(self) -> None:
        if self.__stream:
            try:
                self.__response.release_conn()
            except Exception:
                pass

    def __str__(self):
        return f"Request Time: {self.__request_time}\n" + \
               f"Response Time: {self.__response_time}\n" + \
               f"Status: {self.__response.status}\n" + \
               "\n".join([f"{name}:{value}" for (name, value) in self.__response.headers.items()])
