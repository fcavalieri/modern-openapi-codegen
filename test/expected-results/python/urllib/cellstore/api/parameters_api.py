"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""

import re

import warnings
from typing import Optional, Any, Union
from decimal import Decimal
from cellstore.client.api_client import ApiClient
from cellstore.configuration import Configuration
from cellstore.api.api_base import ApiBase
from cellstore.client.api_call import ApiCallSingleton, ApiCallObjectList, ApiCallObjectStream
from cellstore.client.api_exception import ApiException
from cellstore.client.api_request import ApiRequest
from cellstore.models.sequences import IStreamedSequence, ISequence


class ParametersApi(ApiBase):
    def __init__(self, base_path: str, configuration: Configuration = None):
        super().__init__(base_path, configuration)

    def __get_path_parameters_build_request(self, id1: int, id2: Optional[str], id3: Optional[bool], id4: Optional[Union[float, Decimal]], fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'id1' is set
        if id1 is None:
            raise ApiException("Missing the required parameter `id1` when calling `ParametersApi->get_path_parameters`")

        local_api_request = ApiRequest("GET", self.base_path, "/path-parameters/{id1}/path/{id2}/{id3}/path/{id4}")
        if id1 is not None:
            local_api_request.add_path_parameter("id1", id1)  # path parameter
        if id2 is not None:
            local_api_request.add_path_parameter("id2", id2)  # path parameter
        if id3 is not None:
            local_api_request.add_path_parameter("id3", id3)  # path parameter
        if id4 is not None:
            local_api_request.add_path_parameter("id4", id4)  # path parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def get_path_parameters(self, id1: int, id2: Optional[str] = None, id3: Optional[bool] = None, id4: Optional[Union[float, Decimal]] = None, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param int id1: Description (required)
        :param Optional[str] id2: Description(optional)
        :param Optional[bool] id3: Description(optional)
        :param Optional[Union[float, Decimal]] id4: Description(optional)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__get_path_parameters_build_request(id1, id2, id3, id4, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_path_parameters", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_query_parameters_build_request(self, boolean_mandatory_without_default: bool, integer_mandatory_without_default: int, number_mandatory_without_default: Union[float, Decimal], string_mandatory_without_default: str, boolean_array_mandatory_without_default: list[bool], integer_array_mandatory_without_default: list[int], number_array_mandatory_without_default: list[Union[float, Decimal]], string_array_mandatory_without_default: list[str], pattern_parameter_array_mandatory: dict[str, list[str]], pattern_parameter_single_mandatory: dict[str, str], overridden_type_mandatory: int, fail: Optional[bool], boolean_optional_with_default: Optional[bool], boolean_optional_without_default: Optional[bool], integer_optional_with_default: Optional[int], integer_optional_without_default: Optional[int], number_optional_with_default: Optional[Union[float, Decimal]], number_optional_without_default: Optional[Union[float, Decimal]], string_optional_with_default: Optional[str], string_optional_without_default: Optional[str], boolean_array_optional_with_default: Optional[list[bool]], boolean_array_optional_without_default: Optional[list[bool]], integer_array_optional_with_default: Optional[list[int]], integer_array_optional_without_default: Optional[list[int]], number_array_optional_with_default: Optional[list[Union[float, Decimal]]], number_array_optional_without_default: Optional[list[Union[float, Decimal]]], string_array_optional_with_default: Optional[list[str]], string_array_optional_without_default: Optional[list[str]], pattern_parameter_array_optional: Optional[dict[str, list[str]]], pattern_parameter_single_optional: Optional[dict[str, str]], overridden_type_optional: Optional[int]) -> ApiRequest:
        # verify the required parameter 'boolean_mandatory_without_default' is set
        if boolean_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `boolean_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'integer_mandatory_without_default' is set
        if integer_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `integer_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'number_mandatory_without_default' is set
        if number_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `number_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'string_mandatory_without_default' is set
        if string_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `string_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'boolean_array_mandatory_without_default' is set
        if boolean_array_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `boolean_array_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'integer_array_mandatory_without_default' is set
        if integer_array_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `integer_array_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'number_array_mandatory_without_default' is set
        if number_array_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `number_array_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'string_array_mandatory_without_default' is set
        if string_array_mandatory_without_default is None:
            raise ApiException("Missing the required parameter `string_array_mandatory_without_default` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'pattern_parameter_array_mandatory' is set
        if pattern_parameter_array_mandatory is None:
            raise ApiException("Missing the required parameter `pattern_parameter_array_mandatory` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'pattern_parameter_single_mandatory' is set
        if pattern_parameter_single_mandatory is None:
            raise ApiException("Missing the required parameter `pattern_parameter_single_mandatory` when calling `ParametersApi->get_query_parameters`")
        # verify the required parameter 'overridden_type_mandatory' is set
        if overridden_type_mandatory is None:
            raise ApiException("Missing the required parameter `overridden_type_mandatory` when calling `ParametersApi->get_query_parameters`")

        local_api_request = ApiRequest("GET", self.base_path, "/query-parameters")
        local_api_request.add_query_parameter("boolean-hardcoded", "true")  # hardcoded query parameter
        local_api_request.add_query_parameter("integer-hardcoded", "3")  # hardcoded query parameter
        local_api_request.add_query_parameter("number-hardcoded", "987654321.987654321")  # hardcoded query parameter
        local_api_request.add_query_parameter("string-hardcoded", "value")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if boolean_mandatory_without_default is not None:
            local_api_request.add_query_parameter("boolean-mandatory-without-default", boolean_mandatory_without_default)  # query parameter
        if boolean_optional_with_default is not None:
            local_api_request.add_query_parameter("boolean-optional-with-default", boolean_optional_with_default)  # query parameter
        if boolean_optional_without_default is not None:
            local_api_request.add_query_parameter("boolean-optional-without-default", boolean_optional_without_default)  # query parameter
        if integer_mandatory_without_default is not None:
            local_api_request.add_query_parameter("integer-mandatory-without-default", integer_mandatory_without_default)  # query parameter
        if integer_optional_with_default is not None:
            local_api_request.add_query_parameter("integer-optional-with-default", integer_optional_with_default)  # query parameter
        if integer_optional_without_default is not None:
            local_api_request.add_query_parameter("integer-optional-without-default", integer_optional_without_default)  # query parameter
        if number_mandatory_without_default is not None:
            local_api_request.add_query_parameter("number-mandatory-without-default", number_mandatory_without_default)  # query parameter
        if number_optional_with_default is not None:
            local_api_request.add_query_parameter("number-optional-with-default", number_optional_with_default)  # query parameter
        if number_optional_without_default is not None:
            local_api_request.add_query_parameter("number-optional-without-default", number_optional_without_default)  # query parameter
        if string_mandatory_without_default is not None:
            local_api_request.add_query_parameter("string-mandatory-without-default", string_mandatory_without_default)  # query parameter
        if string_optional_with_default is not None:
            local_api_request.add_query_parameter("string-optional-with-default", string_optional_with_default)  # query parameter
        if string_optional_without_default is not None:
            local_api_request.add_query_parameter("string-optional-without-default", string_optional_without_default)  # query parameter
        if boolean_array_mandatory_without_default is not None:
            local_api_request.add_query_parameter("boolean-array-mandatory-without-default", boolean_array_mandatory_without_default)  # query parameter
        if boolean_array_optional_with_default is not None:
            local_api_request.add_query_parameter("boolean-array-optional-with-default", boolean_array_optional_with_default)  # query parameter
        if boolean_array_optional_without_default is not None:
            local_api_request.add_query_parameter("boolean-array-optional-without-default", boolean_array_optional_without_default)  # query parameter
        if integer_array_mandatory_without_default is not None:
            local_api_request.add_query_parameter("integer-array-mandatory-without-default", integer_array_mandatory_without_default)  # query parameter
        if integer_array_optional_with_default is not None:
            local_api_request.add_query_parameter("integer-array-optional-with-default", integer_array_optional_with_default)  # query parameter
        if integer_array_optional_without_default is not None:
            local_api_request.add_query_parameter("integer-array-optional-without-default", integer_array_optional_without_default)  # query parameter
        if number_array_mandatory_without_default is not None:
            local_api_request.add_query_parameter("number-array-mandatory-without-default", number_array_mandatory_without_default)  # query parameter
        if number_array_optional_with_default is not None:
            local_api_request.add_query_parameter("number-array-optional-with-default", number_array_optional_with_default)  # query parameter
        if number_array_optional_without_default is not None:
            local_api_request.add_query_parameter("number-array-optional-without-default", number_array_optional_without_default)  # query parameter
        if string_array_mandatory_without_default is not None:
            local_api_request.add_query_parameter("string-array-mandatory-without-default", string_array_mandatory_without_default)  # query parameter
        if string_array_optional_with_default is not None:
            local_api_request.add_query_parameter("string-array-optional-with-default", string_array_optional_with_default)  # query parameter
        if string_array_optional_without_default is not None:
            local_api_request.add_query_parameter("string-array-optional-without-default", string_array_optional_without_default)  # query parameter
        if overridden_type_optional is not None:
            local_api_request.add_query_parameter("overridden-type-optional", overridden_type_optional)  # query parameter
        if overridden_type_mandatory is not None:
            local_api_request.add_query_parameter("overridden-type-mandatory", overridden_type_mandatory)  # query parameter
        if pattern_parameter_array_optional is not None:
            local_api_request.add_pattern_query_parameter(pattern_parameter_array_optional, "^([^!:]+:[^:]+)$")  # pattern query parameter
        if pattern_parameter_single_optional is not None:
            local_api_request.add_pattern_query_parameter(pattern_parameter_single_optional, "^([^:]+:[^:]+)$")  # pattern query parameter
        if pattern_parameter_array_mandatory is not None:
            local_api_request.add_pattern_query_parameter(pattern_parameter_array_mandatory, "^([^!:]+:[^:]+)$")  # pattern query parameter
        if pattern_parameter_single_mandatory is not None:
            local_api_request.add_pattern_query_parameter(pattern_parameter_single_mandatory, "^([^:]+:[^:]+)$")  # pattern query parameter
        return local_api_request

    def get_query_parameters(self, boolean_mandatory_without_default: bool, integer_mandatory_without_default: int, number_mandatory_without_default: Union[float, Decimal], string_mandatory_without_default: str, boolean_array_mandatory_without_default: list[bool], integer_array_mandatory_without_default: list[int], number_array_mandatory_without_default: list[Union[float, Decimal]], string_array_mandatory_without_default: list[str], pattern_parameter_array_mandatory: dict[str, list[str]], pattern_parameter_single_mandatory: dict[str, str], overridden_type_mandatory: int, fail: Optional[bool] = None, boolean_optional_with_default: Optional[bool] = None, boolean_optional_without_default: Optional[bool] = None, integer_optional_with_default: Optional[int] = None, integer_optional_without_default: Optional[int] = None, number_optional_with_default: Optional[Union[float, Decimal]] = None, number_optional_without_default: Optional[Union[float, Decimal]] = None, string_optional_with_default: Optional[str] = None, string_optional_without_default: Optional[str] = None, boolean_array_optional_with_default: Optional[list[bool]] = None, boolean_array_optional_without_default: Optional[list[bool]] = None, integer_array_optional_with_default: Optional[list[int]] = None, integer_array_optional_without_default: Optional[list[int]] = None, number_array_optional_with_default: Optional[list[Union[float, Decimal]]] = None, number_array_optional_without_default: Optional[list[Union[float, Decimal]]] = None, string_array_optional_with_default: Optional[list[str]] = None, string_array_optional_without_default: Optional[list[str]] = None, pattern_parameter_array_optional: Optional[dict[str, list[str]]] = None, pattern_parameter_single_optional: Optional[dict[str, str]] = None, overridden_type_optional: Optional[int] = None) -> dict[str, Any]:
        """Summary

        Description

        :param bool boolean_mandatory_without_default: Description (required)
        :param int integer_mandatory_without_default: Description (required)
        :param Union[float, Decimal] number_mandatory_without_default: Description (required)
        :param str string_mandatory_without_default: Description (required)
        :param list[bool] boolean_array_mandatory_without_default: Description (required)
        :param list[int] integer_array_mandatory_without_default: Description (required)
        :param list[Union[float, Decimal]] number_array_mandatory_without_default: Description (required)
        :param list[str] string_array_mandatory_without_default: Description (required)
        :param dict[str, list[str]] pattern_parameter_array_mandatory: Description (required)
        :param dict[str, str] pattern_parameter_single_mandatory: Description (required)
        :param int overridden_type_mandatory: Description (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[bool] boolean_optional_with_default: Description(optional, defaults to true)
        :param Optional[bool] boolean_optional_without_default: Description(optional)
        :param Optional[int] integer_optional_with_default: Description(optional, defaults to 2)
        :param Optional[int] integer_optional_without_default: Description(optional)
        :param Optional[Union[float, Decimal]] number_optional_with_default: Description(optional, defaults to 2)
        :param Optional[Union[float, Decimal]] number_optional_without_default: Description(optional)
        :param Optional[str] string_optional_with_default: Description(optional, defaults to def)
        :param Optional[str] string_optional_without_default: Description(optional)
        :param Optional[list[bool]] boolean_array_optional_with_default: Description(optional)
        :param Optional[list[bool]] boolean_array_optional_without_default: Description(optional)
        :param Optional[list[int]] integer_array_optional_with_default: Description(optional)
        :param Optional[list[int]] integer_array_optional_without_default: Description(optional)
        :param Optional[list[Union[float, Decimal]]] number_array_optional_with_default: Description(optional)
        :param Optional[list[Union[float, Decimal]]] number_array_optional_without_default: Description(optional)
        :param Optional[list[str]] string_array_optional_with_default: Description(optional)
        :param Optional[list[str]] string_array_optional_without_default: Description(optional)
        :param Optional[dict[str, list[str]]] pattern_parameter_array_optional: Description(optional)
        :param Optional[dict[str, str]] pattern_parameter_single_optional: Description(optional)
        :param Optional[int] overridden_type_optional: Description(optional)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__get_query_parameters_build_request(boolean_mandatory_without_default, integer_mandatory_without_default, number_mandatory_without_default, string_mandatory_without_default, boolean_array_mandatory_without_default, integer_array_mandatory_without_default, number_array_mandatory_without_default, string_array_mandatory_without_default, pattern_parameter_array_mandatory, pattern_parameter_single_mandatory, overridden_type_mandatory, fail, boolean_optional_with_default, boolean_optional_without_default, integer_optional_with_default, integer_optional_without_default, number_optional_with_default, number_optional_without_default, string_optional_with_default, string_optional_without_default, boolean_array_optional_with_default, boolean_array_optional_without_default, integer_array_optional_with_default, integer_array_optional_without_default, number_array_optional_with_default, number_array_optional_without_default, string_array_optional_with_default, string_array_optional_without_default, pattern_parameter_array_optional, pattern_parameter_single_optional, overridden_type_optional)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_query_parameters", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_binary_body_build_request(self, body_name: bytes, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'body_name' is set
        if body_name is None:
            raise ApiException("Missing the required parameter `body_name` when calling `ParametersApi->post_binary_body`")

        local_api_request = ApiRequest("POST", self.base_path, "/binary-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/zip", body_name)  # body parameter
        return local_api_request

    def post_binary_body(self, body_name: bytes, fail: Optional[bool] = None) -> bytes:
        """Summary

        Description

        :param bytes body_name: Description (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: bytes
        """
        
        local_api_request = self.__post_binary_body_build_request(body_name, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_binary_body", "bytes")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_binary_body_optional_build_request(self, body_name: Optional[bytes], fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("POST", self.base_path, "/binary-body-optional")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/zip", body_name)  # body parameter
        return local_api_request

    def post_binary_body_optional(self, body_name: Optional[bytes] = None, fail: Optional[bool] = None) -> bytes:
        """Summary

        Description

        :param Optional[bytes] body_name: Description(optional)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: bytes
        """
        
        local_api_request = self.__post_binary_body_optional_build_request(body_name, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_binary_body_optional", "bytes")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_excluded_binary_body_build_request(self) -> ApiRequest:

        local_api_request = ApiRequest("POST", self.base_path, "/excluded-binary-body")
        return local_api_request

    def post_excluded_binary_body(self) -> str:
        """Summary

        Description

        :return: str
        """
        
        local_api_request = self.__post_excluded_binary_body_build_request()
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_excluded_binary_body", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_object_body_build_request(self, archive: dict, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'archive' is set
        if archive is None:
            raise ApiException("Missing the required parameter `archive` when calling `ParametersApi->post_object_body`")

        local_api_request = ApiRequest("POST", self.base_path, "/object-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/json", archive)  # body parameter
        return local_api_request

    def post_object_body(self, archive: dict, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param dict archive: An archive item (JSON). (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_object_body_build_request(archive, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_object_body", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_object_body_optional_build_request(self, archive: Optional[dict], fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("POST", self.base_path, "/object-body-optional")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/json", archive)  # body parameter
        return local_api_request

    def post_object_body_optional(self, archive: Optional[dict] = None, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param Optional[dict] archive: An archive item (JSON).(optional)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_object_body_optional_build_request(archive, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_object_body_optional", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_overridden_binary_body_build_request(self, archive: bytes) -> ApiRequest:
        # verify the required parameter 'archive' is set
        if archive is None:
            raise ApiException("Missing the required parameter `archive` when calling `ParametersApi->post_overridden_binary_body`")

        local_api_request = ApiRequest("POST", self.base_path, "/overridden-binary-body")
        local_api_request.add_body_parameter("application/xml", archive)  # body parameter
        return local_api_request

    def post_overridden_binary_body(self, archive: bytes) -> str:
        """Summary

        Description

        :param bytes archive: An archive item (XML). (required)
        :return: str
        """
        
        local_api_request = self.__post_overridden_binary_body_build_request(archive)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_overridden_binary_body", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_string_body_build_request(self, archive: str, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'archive' is set
        if archive is None:
            raise ApiException("Missing the required parameter `archive` when calling `ParametersApi->post_string_body`")

        local_api_request = ApiRequest("POST", self.base_path, "/string-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/xml", archive)  # body parameter
        return local_api_request

    def post_string_body(self, archive: str, fail: Optional[bool] = None) -> str:
        """Summary

        Description

        :param str archive: An archive item (XML). (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: str
        """
        
        local_api_request = self.__post_string_body_build_request(archive, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_string_body", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_string_body_optional_build_request(self, archive: Optional[str], fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("POST", self.base_path, "/string-body-optional")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/xml", archive)  # body parameter
        return local_api_request

    def post_string_body_optional(self, archive: Optional[str] = None, fail: Optional[bool] = None) -> str:
        """Summary

        Description

        :param Optional[str] archive: An archive item (XML).(optional)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: str
        """
        
        local_api_request = self.__post_string_body_optional_build_request(archive, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_string_body_optional", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_variant_body_zip_archive_build_request(self, template: bytes, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'template' is set
        if template is None:
            raise ApiException("Missing the required parameter `template` when calling `ParametersApi->post_variant_body_zip_archive`")

        local_api_request = ApiRequest("POST", self.base_path, "/variant-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/zip", template)  # body parameter
        return local_api_request

    def post_variant_body_zip_archive(self, template: bytes, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param bytes template: A Zip Archive (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_variant_body_zip_archive_build_request(template, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_variant_body_zip_archive", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_variant_body_xhtml_build_request(self, template: str, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'template' is set
        if template is None:
            raise ApiException("Missing the required parameter `template` when calling `ParametersApi->post_variant_body_xhtml`")

        local_api_request = ApiRequest("POST", self.base_path, "/variant-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("application/xhtml+xml", template)  # body parameter
        return local_api_request

    def post_variant_body_xhtml(self, template: str, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param str template: A XHTML file (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_variant_body_xhtml_build_request(template, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_variant_body_xhtml", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_variant_body_html_build_request(self, template: str, fail: Optional[bool]) -> ApiRequest:
        # verify the required parameter 'template' is set
        if template is None:
            raise ApiException("Missing the required parameter `template` when calling `ParametersApi->post_variant_body_html`")

        local_api_request = ApiRequest("POST", self.base_path, "/variant-body")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        local_api_request.add_body_parameter("text/html", template)  # body parameter
        return local_api_request

    def post_variant_body_html(self, template: str, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param str template: A HTML file (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_variant_body_html_build_request(template, fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_variant_body_html", "dict")
        local_api_call.execute()
        return local_api_call.get_result()
