"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""

import re

import warnings
from typing import Optional, Any, Union
from decimal import Decimal
from cellstore.client.api_client import ApiClient
from cellstore.configuration import Configuration
from cellstore.api.api_base import ApiBase
from cellstore.client.api_call import ApiCallSingleton, ApiCallObjectList, ApiCallObjectStream
from cellstore.client.api_exception import ApiException
from cellstore.client.api_request import ApiRequest
from cellstore.models.sequences import IStreamedSequence, ISequence


class ResponsesApi(ApiBase):
    def __init__(self, base_path: str, configuration: Configuration = None):
        super().__init__(base_path, configuration)

    def __empty_response_build_request(self, fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/empty-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def empty_response(self, fail: Optional[bool] = None) -> None:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: None
        """
        
        local_api_request = self.__empty_response_build_request(fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "empty_response", "None")
        local_api_call.execute()
        

    def __get_binary_response_build_request(self, fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/binary-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def get_binary_response(self, fail: Optional[bool] = None) -> bytes:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: bytes
        """
        
        local_api_request = self.__get_binary_response_build_request(fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_binary_response", "bytes")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_json_response_build_request(self, fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/json-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def get_json_response(self, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__get_json_response_build_request(fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_json_response", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_xml_response_build_request(self, fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/xml-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def get_xml_response(self, fail: Optional[bool] = None) -> str:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: str
        """
        
        local_api_request = self.__get_xml_response_build_request(fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_xml_response", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __list_json_response_build_request(self, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/list-json-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        return local_api_request

    def list_json_response(self, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None) -> ISequence[dict[str, Any]]:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :return: ISequence[dict[str, Any]]
        """
        
        local_api_request = self.__list_json_response_build_request(fail, fail_at, sleep, amount)
        local_api_call = ApiCallObjectList(self.api_client, local_api_request, "list_json_response", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __post_json_response_build_request(self, fail: Optional[bool]) -> ApiRequest:

        local_api_request = ApiRequest("POST", self.base_path, "/json-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        return local_api_request

    def post_json_response(self, fail: Optional[bool] = None) -> dict[str, Any]:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__post_json_response_build_request(fail)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "post_json_response", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __stream_json_response_build_request(self, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/stream-json-response")
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        return local_api_request

    def stream_json_response(self, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None) -> IStreamedSequence[dict[str, Any]]:
        """Summary

        Description

        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :return: IStreamedSequence[dict[str, Any]]
        """
        
        local_api_request = self.__stream_json_response_build_request(fail, fail_at, sleep, amount)
        local_api_call = ApiCallObjectStream(self.api_client, local_api_request, "stream_json_response", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_list_build_request(self, required_b: Optional[str], required_c: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_c: Optional[str], required_a: Optional[str], required_d: str) -> ApiRequest:
        # verify the required parameter 'required_b' is set
        if required_b is None:
            raise ApiException("Missing the required parameter `required_b` when calling `ResponsesApi->get_arbitrary_response_as_list`")
        # verify the required parameter 'required_c' is set
        if required_c is None:
            raise ApiException("Missing the required parameter `required_c` when calling `ResponsesApi->get_arbitrary_response_as_list`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "json-list")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "json-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_c is not None:
            local_api_request.add_query_parameter("excluded-c", excluded_c)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_list(self, required_b: Optional[str], required_c: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_c: Optional[str] = None, required_a: Optional[str] = None, required_d: str = None) -> ISequence[dict[str, Any]]:
        """Summary

        Description

        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant) (required)
        :param str required_c: Required c (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_c: Excluded c (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant)(optional)
        :param str required_d: Required d (Two required parameter should be optional per variant)(optional)
        :return: ISequence[dict[str, Any]]
        """
        
        local_api_request = self.__get_arbitrary_response_as_list_build_request(required_b, required_c, fail, fail_at, sleep, amount, excluded_c, required_a, required_d)
        local_api_call = ApiCallObjectList(self.api_client, local_api_request, "get_arbitrary_response_as_list", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_stream_build_request(self, required_a: Optional[str], required_d: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_a: Optional[str], required_b: Optional[str], required_c: str) -> ApiRequest:
        # verify the required parameter 'required_a' is set
        if required_a is None:
            raise ApiException("Missing the required parameter `required_a` when calling `ResponsesApi->get_arbitrary_response_as_stream`")
        # verify the required parameter 'required_d' is set
        if required_d is None:
            raise ApiException("Missing the required parameter `required_d` when calling `ResponsesApi->get_arbitrary_response_as_stream`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "json-stream")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "json-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_a is not None:
            local_api_request.add_query_parameter("excluded-a", excluded_a)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_stream(self, required_a: Optional[str], required_d: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_a: Optional[str] = None, required_b: Optional[str] = None, required_c: str = None) -> IStreamedSequence[dict[str, Any]]:
        """Summary

        Description

        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant) (required)
        :param str required_d: Required d (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_a: Excluded a (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant)(optional)
        :param str required_c: Required c (Two required parameter should be optional per variant)(optional)
        :return: IStreamedSequence[dict[str, Any]]
        """
        
        local_api_request = self.__get_arbitrary_response_as_stream_build_request(required_a, required_d, fail, fail_at, sleep, amount, excluded_a, required_b, required_c)
        local_api_call = ApiCallObjectStream(self.api_client, local_api_request, "get_arbitrary_response_as_stream", "dict")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_xml_build_request(self, required_b: Optional[str], required_c: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_c: Optional[str], required_a: Optional[str], required_d: str) -> ApiRequest:
        # verify the required parameter 'required_b' is set
        if required_b is None:
            raise ApiException("Missing the required parameter `required_b` when calling `ResponsesApi->get_arbitrary_response_as_xml`")
        # verify the required parameter 'required_c' is set
        if required_c is None:
            raise ApiException("Missing the required parameter `required_c` when calling `ResponsesApi->get_arbitrary_response_as_xml`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "xml")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "xml-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_c is not None:
            local_api_request.add_query_parameter("excluded-c", excluded_c)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_xml(self, required_b: Optional[str], required_c: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_c: Optional[str] = None, required_a: Optional[str] = None, required_d: str = None) -> str:
        """Summary

        Description

        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant) (required)
        :param str required_c: Required c (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_c: Excluded c (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant)(optional)
        :param str required_d: Required d (Two required parameter should be optional per variant)(optional)
        :return: str
        """
        
        local_api_request = self.__get_arbitrary_response_as_xml_build_request(required_b, required_c, fail, fail_at, sleep, amount, excluded_c, required_a, required_d)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_arbitrary_response_as_xml", "str")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_binary_build_request(self, required_a: Optional[str], required_d: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_a: Optional[str], required_b: Optional[str], required_c: str) -> ApiRequest:
        # verify the required parameter 'required_a' is set
        if required_a is None:
            raise ApiException("Missing the required parameter `required_a` when calling `ResponsesApi->get_arbitrary_response_as_binary`")
        # verify the required parameter 'required_d' is set
        if required_d is None:
            raise ApiException("Missing the required parameter `required_d` when calling `ResponsesApi->get_arbitrary_response_as_binary`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "binary")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "binary-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_a is not None:
            local_api_request.add_query_parameter("excluded-a", excluded_a)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_binary(self, required_a: Optional[str], required_d: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_a: Optional[str] = None, required_b: Optional[str] = None, required_c: str = None) -> bytes:
        """Summary

        Description

        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant) (required)
        :param str required_d: Required d (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_a: Excluded a (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant)(optional)
        :param str required_c: Required c (Two required parameter should be optional per variant)(optional)
        :return: bytes
        """
        
        local_api_request = self.__get_arbitrary_response_as_binary_build_request(required_a, required_d, fail, fail_at, sleep, amount, excluded_a, required_b, required_c)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_arbitrary_response_as_binary", "bytes")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_binary_obsolete_build_request(self, required_b: Optional[str], required_c: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_c: Optional[str], required_a: Optional[str], required_d: str) -> ApiRequest:
        # verify the required parameter 'required_b' is set
        if required_b is None:
            raise ApiException("Missing the required parameter `required_b` when calling `ResponsesApi->get_arbitrary_response_as_binary_obsolete`")
        # verify the required parameter 'required_c' is set
        if required_c is None:
            raise ApiException("Missing the required parameter `required_c` when calling `ResponsesApi->get_arbitrary_response_as_binary_obsolete`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "binary")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "binary-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_c is not None:
            local_api_request.add_query_parameter("excluded-c", excluded_c)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_binary_obsolete(self, required_b: Optional[str], required_c: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_c: Optional[str] = None, required_a: Optional[str] = None, required_d: str = None) -> bytes:
        """Summary

        This variant is obsolete
        Description

        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant) (required)
        :param str required_c: Required c (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_c: Excluded c (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant)(optional)
        :param str required_d: Required d (Two required parameter should be optional per variant)(optional)
        :return: bytes
        """
        warnings.warn("This variant is obsolete", DeprecationWarning)
        local_api_request = self.__get_arbitrary_response_as_binary_obsolete_build_request(required_b, required_c, fail, fail_at, sleep, amount, excluded_c, required_a, required_d)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_arbitrary_response_as_binary_obsolete", "bytes")
        local_api_call.execute()
        return local_api_call.get_result()

    def __get_arbitrary_response_as_json_build_request(self, required_a: Optional[str], required_d: str, fail: Optional[bool], fail_at: Optional[int], sleep: Optional[int], amount: Optional[int], excluded_a: Optional[str], required_b: Optional[str], required_c: str) -> ApiRequest:
        # verify the required parameter 'required_a' is set
        if required_a is None:
            raise ApiException("Missing the required parameter `required_a` when calling `ResponsesApi->get_arbitrary_response_as_json`")
        # verify the required parameter 'required_d' is set
        if required_d is None:
            raise ApiException("Missing the required parameter `required_d` when calling `ResponsesApi->get_arbitrary_response_as_json`")

        local_api_request = ApiRequest("GET", self.base_path, "/arbitrary-response")
        local_api_request.add_query_parameter("format", "json")  # hardcoded query parameter
        local_api_request.add_query_parameter("format-copy", "json-copy")  # hardcoded query parameter
        if fail is not None:
            local_api_request.add_query_parameter("fail", fail)  # query parameter
        if fail_at is not None:
            local_api_request.add_query_parameter("fail-at", fail_at)  # query parameter
        if sleep is not None:
            local_api_request.add_query_parameter("sleep", sleep)  # query parameter
        if amount is not None:
            local_api_request.add_query_parameter("amount", amount)  # query parameter
        if excluded_a is not None:
            local_api_request.add_query_parameter("excluded-a", excluded_a)  # query parameter
        if required_a is not None:
            local_api_request.add_query_parameter("required-a", required_a)  # query parameter
        if required_b is not None:
            local_api_request.add_query_parameter("required-b", required_b)  # query parameter
        if required_c is not None:
            local_api_request.add_query_parameter("required-c", required_c)  # query parameter
        if required_d is not None:
            local_api_request.add_query_parameter("required-d", required_d)  # query parameter
        return local_api_request

    def get_arbitrary_response_as_json(self, required_a: Optional[str], required_d: str, fail: Optional[bool] = None, fail_at: Optional[int] = None, sleep: Optional[int] = None, amount: Optional[int] = None, excluded_a: Optional[str] = None, required_b: Optional[str] = None, required_c: str = None) -> dict[str, Any]:
        """Summary

        Description

        :param Optional[str] required_a: Required a (Two required parameter should be optional per variant) (required)
        :param str required_d: Required d (Two required parameter should be optional per variant) (required)
        :param Optional[bool] fail: Description(optional, defaults to false)
        :param Optional[int] fail_at: Description(optional, defaults to -1)
        :param Optional[int] sleep: Description(optional, defaults to 0)
        :param Optional[int] amount: Description(optional, defaults to 1000)
        :param Optional[str] excluded_a: Excluded a (A single excluded parameter should be present per variant)(optional)
        :param Optional[str] required_b: Required b (Two required parameter should be optional per variant)(optional)
        :param str required_c: Required c (Two required parameter should be optional per variant)(optional)
        :return: dict[str, Any]
        """
        
        local_api_request = self.__get_arbitrary_response_as_json_build_request(required_a, required_d, fail, fail_at, sleep, amount, excluded_a, required_b, required_c)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_arbitrary_response_as_json", "dict")
        local_api_call.execute()
        return local_api_call.get_result()
