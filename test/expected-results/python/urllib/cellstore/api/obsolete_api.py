"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""

import re

import warnings
from typing import Optional, Any, Union
from decimal import Decimal
from cellstore.client.api_client import ApiClient
from cellstore.configuration import Configuration
from cellstore.api.api_base import ApiBase
from cellstore.client.api_call import ApiCallSingleton, ApiCallObjectList, ApiCallObjectStream
from cellstore.client.api_exception import ApiException
from cellstore.client.api_request import ApiRequest
from cellstore.models.sequences import IStreamedSequence, ISequence


class ObsoleteApi(ApiBase):
    def __init__(self, base_path: str, configuration: Configuration = None):
        super().__init__(base_path, configuration)

    def __get_obsolete_operation_build_request(self, limit: Optional[int]) -> ApiRequest:

        local_api_request = ApiRequest("GET", self.base_path, "/obsolete-operation")
        if limit is not None:
            local_api_request.add_query_parameter("limit", limit)  # query parameter
        return local_api_request

    def get_obsolete_operation(self, limit: Optional[int] = None) -> dict[str, Any]:
        """Obsolete summary

        This operation is obsolete
        Obsolete description

        :param Optional[int] limit: Limits the amount of documents in the result of the query. (optional, defaults to 50)
        :return: dict[str, Any]
        """
        warnings.warn("This operation is obsolete", DeprecationWarning)
        local_api_request = self.__get_obsolete_operation_build_request(limit)
        local_api_call = ApiCallSingleton(self.api_client, local_api_request, "get_obsolete_operation", "dict")
        local_api_call.execute()
        return local_api_call.get_result()
