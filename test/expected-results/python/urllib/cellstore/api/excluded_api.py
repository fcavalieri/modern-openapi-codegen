"""
    This is a titl*

    This is a descriptio*  # noqa: E501

    OpenAPI spec version: 1.0.0-TESTS
    
"""

import re

import warnings
from typing import Optional, Any, Union
from decimal import Decimal
from cellstore.client.api_client import ApiClient
from cellstore.configuration import Configuration
from cellstore.api.api_base import ApiBase
from cellstore.client.api_call import ApiCallSingleton, ApiCallObjectList, ApiCallObjectStream
from cellstore.client.api_exception import ApiException
from cellstore.client.api_request import ApiRequest
from cellstore.models.sequences import IStreamedSequence, ISequence


class ExcludedApi(ApiBase):
    def __init__(self, base_path: str, configuration: Configuration = None):
        super().__init__(base_path, configuration)
