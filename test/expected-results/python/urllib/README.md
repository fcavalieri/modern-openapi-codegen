# cellstore
This is a descriptio*

This Python package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0-TESTS
- Package version: 1.2.3
- Build package: com.fcavalieri.openapi.codegen.python.PythonGenerator
For more information, please visit [Contact url](Contact url)

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import cellstore 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import cellstore
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import cellstore
from cellstore.rest import ApiException
from pprint import pprint
```

## Documentation for API Endpoints

All URIs are relative to *{server}/prefix*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ObsoleteApi* | [**get_obsolete_operation**](docs/ObsoleteApi.md#get_obsolete_operation) | **GET** /obsolete-operation | Obsolete summary
*ParametersApi* | [**get_path_parameters**](docs/ParametersApi.md#get_path_parameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
*ParametersApi* | [**get_query_parameters**](docs/ParametersApi.md#get_query_parameters) | **GET** /query-parameters | Summary
*ParametersApi* | [**post_binary_body**](docs/ParametersApi.md#post_binary_body) | **POST** /binary-body | Summary
*ParametersApi* | [**post_binary_body_optional**](docs/ParametersApi.md#post_binary_body_optional) | **POST** /binary-body-optional | Summary
*ParametersApi* | [**post_excluded_binary_body**](docs/ParametersApi.md#post_excluded_binary_body) | **POST** /excluded-binary-body | Summary
*ParametersApi* | [**post_object_body**](docs/ParametersApi.md#post_object_body) | **POST** /object-body | Summary
*ParametersApi* | [**post_object_body_optional**](docs/ParametersApi.md#post_object_body_optional) | **POST** /object-body-optional | Summary
*ParametersApi* | [**post_overridden_binary_body**](docs/ParametersApi.md#post_overridden_binary_body) | **POST** /overridden-binary-body | Summary
*ParametersApi* | [**post_string_body**](docs/ParametersApi.md#post_string_body) | **POST** /string-body | Summary
*ParametersApi* | [**post_string_body_optional**](docs/ParametersApi.md#post_string_body_optional) | **POST** /string-body-optional | Summary
*ParametersApi* | [**post_variant_body_zip_archive**](docs/ParametersApi.md#postvariantbodyziparchive) | **POST** /variant-body | Summary
*ParametersApi* | [**post_variant_body_xhtml**](docs/ParametersApi.md#postvariantbodyxhtml) | **POST** /variant-body | Summary
*ParametersApi* | [**post_variant_body_html**](docs/ParametersApi.md#postvariantbodyhtml) | **POST** /variant-body | Summary
*ResponsesApi* | [**empty_response**](docs/ResponsesApi.md#empty_response) | **GET** /empty-response | Summary
*ResponsesApi* | [**get_binary_response**](docs/ResponsesApi.md#get_binary_response) | **GET** /binary-response | Summary
*ResponsesApi* | [**get_json_response**](docs/ResponsesApi.md#get_json_response) | **GET** /json-response | Summary
*ResponsesApi* | [**get_xml_response**](docs/ResponsesApi.md#get_xml_response) | **GET** /xml-response | Summary
*ResponsesApi* | [**list_json_response**](docs/ResponsesApi.md#list_json_response) | **GET** /list-json-response | Summary
*ResponsesApi* | [**post_json_response**](docs/ResponsesApi.md#post_json_response) | **POST** /json-response | Summary
*ResponsesApi* | [**stream_json_response**](docs/ResponsesApi.md#stream_json_response) | **GET** /stream-json-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_list**](docs/ResponsesApi.md#getarbitraryresponseaslist) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_stream**](docs/ResponsesApi.md#getarbitraryresponseasstream) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_xml**](docs/ResponsesApi.md#getarbitraryresponseasxml) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_binary**](docs/ResponsesApi.md#getarbitraryresponseasbinary) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_binary_obsolete**](docs/ResponsesApi.md#getarbitraryresponseasbinaryobsolete) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**get_arbitrary_response_as_json**](docs/ResponsesApi.md#getarbitraryresponseasjson) | **GET** /arbitrary-response | Summary

## Documentation For Models


## Author


