# CellStore - the C# library for the This is a titl*

This is a descriptio*

This C# SDK was built for

- API version: 1.0.0-TESTS
- SDK version: 1.2.3
    For more information, please visit [Contact url](Contact url)

<a name="frameworks-supported"></a>
## Frameworks supported
- .NET Core >= 2.0

<a name="dependencies"></a>
## Dependencies
- RestSharp 106.11.7
- Newtonsoft.Json 12.0.3
- System.Diagnostic.Process 4.3.0

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *{server}/prefix*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ObsoleteApi* | [**GetObsoleteOperation**](docs/ObsoleteApi.md#getobsoleteoperation) | **GET** /obsolete-operation | Obsolete summary
*ParametersApi* | [**GetPathParameters**](docs/ParametersApi.md#getpathparameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
*ParametersApi* | [**GetQueryParameters**](docs/ParametersApi.md#getqueryparameters) | **GET** /query-parameters | Summary
*ParametersApi* | [**PostBinaryBody**](docs/ParametersApi.md#postbinarybody) | **POST** /binary-body | Summary
*ParametersApi* | [**PostBinaryBodyOptional**](docs/ParametersApi.md#postbinarybodyoptional) | **POST** /binary-body-optional | Summary
*ParametersApi* | [**PostExcludedBinaryBody**](docs/ParametersApi.md#postexcludedbinarybody) | **POST** /excluded-binary-body | Summary
*ParametersApi* | [**PostObjectBody**](docs/ParametersApi.md#postobjectbody) | **POST** /object-body | Summary
*ParametersApi* | [**PostObjectBodyOptional**](docs/ParametersApi.md#postobjectbodyoptional) | **POST** /object-body-optional | Summary
*ParametersApi* | [**PostOverriddenBinaryBody**](docs/ParametersApi.md#postoverriddenbinarybody) | **POST** /overridden-binary-body | Summary
*ParametersApi* | [**PostStringBody**](docs/ParametersApi.md#poststringbody) | **POST** /string-body | Summary
*ParametersApi* | [**PostStringBodyOptional**](docs/ParametersApi.md#poststringbodyoptional) | **POST** /string-body-optional | Summary
*ParametersApi* | [**PostVariantBodyZipArchive**](docs/ParametersApi.md#postvariantbodyziparchive) | **POST** /variant-body | Summary
*ParametersApi* | [**PostVariantBodyXHTML**](docs/ParametersApi.md#postvariantbodyxhtml) | **POST** /variant-body | Summary
*ParametersApi* | [**PostVariantBodyHTML**](docs/ParametersApi.md#postvariantbodyhtml) | **POST** /variant-body | Summary
*ResponsesApi* | [**EmptyResponse**](docs/ResponsesApi.md#emptyresponse) | **GET** /empty-response | Summary
*ResponsesApi* | [**GetBinaryResponse**](docs/ResponsesApi.md#getbinaryresponse) | **GET** /binary-response | Summary
*ResponsesApi* | [**GetJsonResponse**](docs/ResponsesApi.md#getjsonresponse) | **GET** /json-response | Summary
*ResponsesApi* | [**GetXMLResponse**](docs/ResponsesApi.md#getxmlresponse) | **GET** /xml-response | Summary
*ResponsesApi* | [**ListJsonResponse**](docs/ResponsesApi.md#listjsonresponse) | **GET** /list-json-response | Summary
*ResponsesApi* | [**PostJsonResponse**](docs/ResponsesApi.md#postjsonresponse) | **POST** /json-response | Summary
*ResponsesApi* | [**StreamJsonResponse**](docs/ResponsesApi.md#streamjsonresponse) | **GET** /stream-json-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsList**](docs/ResponsesApi.md#getarbitraryresponseaslist) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsStream**](docs/ResponsesApi.md#getarbitraryresponseasstream) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsXML**](docs/ResponsesApi.md#getarbitraryresponseasxml) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsBinary**](docs/ResponsesApi.md#getarbitraryresponseasbinary) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsBinaryObsolete**](docs/ResponsesApi.md#getarbitraryresponseasbinaryobsolete) | **GET** /arbitrary-response | Summary
*ResponsesApi* | [**GetArbitraryResponseAsJson**](docs/ResponsesApi.md#getarbitraryresponseasjson) | **GET** /arbitrary-response | Summary
