/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;
using CellStore.Client;
using CellStore.Client.Calls;
using CellStore.Client.Requests;
using CellStore.Model;

namespace CellStore.Api
{
  /// <summary>
  /// Represents a collection of functions to interact with the API endpoints
  /// </summary>
  public class ParametersApi: ApiBase
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ParametersApi"/> class.
    /// </summary>
    /// <returns></returns>
    public ParametersApi(string basePath, Configuration configuration = null) : base(basePath, configuration)
    {
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="id1">Description</param>
    /// <param name="id2">Description (optional)</param>
    /// <param name="id3">Description (optional)</param>
    /// <param name="id4">Description (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest GetPathParametersBuildRequest(int id1, string id2 = null, bool? id3 = null, decimal? id4 = null, bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/path-parameters/{id1}/path/{id2}/{id3}/path/{id4}");
      __ApiRequest.AddPathParameter("id1", id1); // path parameter
      if (id2 != null) __ApiRequest.AddPathParameter("id2", id2); // path parameter
      if (id3 != null) __ApiRequest.AddPathParameter("id3", id3); // path parameter
      if (id4 != null) __ApiRequest.AddPathParameter("id4", id4); // path parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="id1">Description</param>
    /// <param name="id2">Description (optional)</param>
    /// <param name="id3">Description (optional)</param>
    /// <param name="id4">Description (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject GetPathParameters(int id1, string id2 = null, bool? id3 = null, decimal? id4 = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = GetPathParametersBuildRequest(id1, id2, id3, id4, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetPathParameters");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="id1">Description</param>
    /// <param name="id2">Description (optional)</param>
    /// <param name="id3">Description (optional)</param>
    /// <param name="id4">Description (optional)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> GetPathParametersAsync (int id1, string id2 = null, bool? id3 = null, decimal? id4 = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = GetPathParametersBuildRequest(id1, id2, id3, id4, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetPathParameters");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="booleanMandatoryWithoutDefault">Description</param>
    /// <param name="integerMandatoryWithoutDefault">Description</param>
    /// <param name="numberMandatoryWithoutDefault">Description</param>
    /// <param name="stringMandatoryWithoutDefault">Description</param>
    /// <param name="booleanArrayMandatoryWithoutDefault">Description</param>
    /// <param name="integerArrayMandatoryWithoutDefault">Description</param>
    /// <param name="numberArrayMandatoryWithoutDefault">Description</param>
    /// <param name="stringArrayMandatoryWithoutDefault">Description</param>
    /// <param name="patternParameterArrayMandatory">Description</param>
    /// <param name="patternParameterSingleMandatory">Description</param>
    /// <param name="overriddenTypeMandatory">Description</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="booleanOptionalWithDefault">Description (optional, defaults to true)</param>
    /// <param name="booleanOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerOptionalWithDefault">Description (optional, defaults to 2)</param>
    /// <param name="integerOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberOptionalWithDefault">Description (optional, defaults to 2)</param>
    /// <param name="numberOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringOptionalWithDefault">Description (optional, defaults to def)</param>
    /// <param name="stringOptionalWithoutDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="patternParameterArrayOptional">Description (optional)</param>
    /// <param name="patternParameterSingleOptional">Description (optional)</param>
    /// <param name="overriddenTypeOptional">Description (optional)</param>
    /// <returns>JObject</returns>
    private ApiRequest GetQueryParametersBuildRequest(bool booleanMandatoryWithoutDefault, int integerMandatoryWithoutDefault, decimal numberMandatoryWithoutDefault, string stringMandatoryWithoutDefault, IEnumerable<bool> booleanArrayMandatoryWithoutDefault, IEnumerable<int> integerArrayMandatoryWithoutDefault, IEnumerable<decimal> numberArrayMandatoryWithoutDefault, IEnumerable<string> stringArrayMandatoryWithoutDefault, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory, IReadOnlyDictionary<string, string> patternParameterSingleMandatory, int overriddenTypeMandatory, bool? fail = null, bool? booleanOptionalWithDefault = null, bool? booleanOptionalWithoutDefault = null, int? integerOptionalWithDefault = null, int? integerOptionalWithoutDefault = null, decimal? numberOptionalWithDefault = null, decimal? numberOptionalWithoutDefault = null, string stringOptionalWithDefault = null, string stringOptionalWithoutDefault = null, IEnumerable<bool> booleanArrayOptionalWithDefault = null, IEnumerable<bool> booleanArrayOptionalWithoutDefault = null, IEnumerable<int> integerArrayOptionalWithDefault = null, IEnumerable<int> integerArrayOptionalWithoutDefault = null, IEnumerable<decimal> numberArrayOptionalWithDefault = null, IEnumerable<decimal> numberArrayOptionalWithoutDefault = null, IEnumerable<string> stringArrayOptionalWithDefault = null, IEnumerable<string> stringArrayOptionalWithoutDefault = null, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null, IReadOnlyDictionary<string, string> patternParameterSingleOptional = null, int? overriddenTypeOptional = null)
    {
      // verify the required parameter 'stringMandatoryWithoutDefault' is set
      if (stringMandatoryWithoutDefault == null)
        throw new ApiException("Missing required parameter 'stringMandatoryWithoutDefault' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'booleanArrayMandatoryWithoutDefault' is set
      if (booleanArrayMandatoryWithoutDefault == null)
        throw new ApiException("Missing required parameter 'booleanArrayMandatoryWithoutDefault' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'integerArrayMandatoryWithoutDefault' is set
      if (integerArrayMandatoryWithoutDefault == null)
        throw new ApiException("Missing required parameter 'integerArrayMandatoryWithoutDefault' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'numberArrayMandatoryWithoutDefault' is set
      if (numberArrayMandatoryWithoutDefault == null)
        throw new ApiException("Missing required parameter 'numberArrayMandatoryWithoutDefault' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'stringArrayMandatoryWithoutDefault' is set
      if (stringArrayMandatoryWithoutDefault == null)
        throw new ApiException("Missing required parameter 'stringArrayMandatoryWithoutDefault' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'patternParameterArrayMandatory' is set
      if (patternParameterArrayMandatory == null)
        throw new ApiException("Missing required parameter 'patternParameterArrayMandatory' when calling ParametersApi->GetQueryParameters");
      // verify the required parameter 'patternParameterSingleMandatory' is set
      if (patternParameterSingleMandatory == null)
        throw new ApiException("Missing required parameter 'patternParameterSingleMandatory' when calling ParametersApi->GetQueryParameters");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/query-parameters");
      __ApiRequest.AddQueryParameter("boolean-hardcoded", "true"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("integer-hardcoded", "3"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("number-hardcoded", "987654321.987654321"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("string-hardcoded", "value"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.AddQueryParameter("boolean-mandatory-without-default", booleanMandatoryWithoutDefault); // query parameter
      if (booleanOptionalWithDefault != null) __ApiRequest.AddQueryParameter("boolean-optional-with-default", booleanOptionalWithDefault.Value); // query parameter
      if (booleanOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("boolean-optional-without-default", booleanOptionalWithoutDefault.Value); // query parameter
      __ApiRequest.AddQueryParameter("integer-mandatory-without-default", integerMandatoryWithoutDefault); // query parameter
      if (integerOptionalWithDefault != null) __ApiRequest.AddQueryParameter("integer-optional-with-default", integerOptionalWithDefault.Value); // query parameter
      if (integerOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("integer-optional-without-default", integerOptionalWithoutDefault.Value); // query parameter
      __ApiRequest.AddQueryParameter("number-mandatory-without-default", numberMandatoryWithoutDefault); // query parameter
      if (numberOptionalWithDefault != null) __ApiRequest.AddQueryParameter("number-optional-with-default", numberOptionalWithDefault.Value); // query parameter
      if (numberOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("number-optional-without-default", numberOptionalWithoutDefault.Value); // query parameter
      if (stringMandatoryWithoutDefault != null) __ApiRequest.AddQueryParameter("string-mandatory-without-default", stringMandatoryWithoutDefault); // query parameter
      if (stringOptionalWithDefault != null) __ApiRequest.AddQueryParameter("string-optional-with-default", stringOptionalWithDefault); // query parameter
      if (stringOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("string-optional-without-default", stringOptionalWithoutDefault); // query parameter
      if (booleanArrayMandatoryWithoutDefault != null) __ApiRequest.AddQueryParameter("boolean-array-mandatory-without-default", booleanArrayMandatoryWithoutDefault); // query parameter
      if (booleanArrayOptionalWithDefault != null) __ApiRequest.AddQueryParameter("boolean-array-optional-with-default", booleanArrayOptionalWithDefault); // query parameter
      if (booleanArrayOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("boolean-array-optional-without-default", booleanArrayOptionalWithoutDefault); // query parameter
      if (integerArrayMandatoryWithoutDefault != null) __ApiRequest.AddQueryParameter("integer-array-mandatory-without-default", integerArrayMandatoryWithoutDefault); // query parameter
      if (integerArrayOptionalWithDefault != null) __ApiRequest.AddQueryParameter("integer-array-optional-with-default", integerArrayOptionalWithDefault); // query parameter
      if (integerArrayOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("integer-array-optional-without-default", integerArrayOptionalWithoutDefault); // query parameter
      if (numberArrayMandatoryWithoutDefault != null) __ApiRequest.AddQueryParameter("number-array-mandatory-without-default", numberArrayMandatoryWithoutDefault); // query parameter
      if (numberArrayOptionalWithDefault != null) __ApiRequest.AddQueryParameter("number-array-optional-with-default", numberArrayOptionalWithDefault); // query parameter
      if (numberArrayOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("number-array-optional-without-default", numberArrayOptionalWithoutDefault); // query parameter
      if (stringArrayMandatoryWithoutDefault != null) __ApiRequest.AddQueryParameter("string-array-mandatory-without-default", stringArrayMandatoryWithoutDefault); // query parameter
      if (stringArrayOptionalWithDefault != null) __ApiRequest.AddQueryParameter("string-array-optional-with-default", stringArrayOptionalWithDefault); // query parameter
      if (stringArrayOptionalWithoutDefault != null) __ApiRequest.AddQueryParameter("string-array-optional-without-default", stringArrayOptionalWithoutDefault); // query parameter
      if (overriddenTypeOptional != null) __ApiRequest.AddQueryParameter("overridden-type-optional", overriddenTypeOptional.Value); // query parameter
      __ApiRequest.AddQueryParameter("overridden-type-mandatory", overriddenTypeMandatory); // query parameter
      if (patternParameterArrayOptional != null) __ApiRequest.AddPatternQueryParameter(patternParameterArrayOptional, "^([^!:]+:[^:]+)$"); // pattern query parameter
      if (patternParameterSingleOptional != null) __ApiRequest.AddPatternQueryParameter(patternParameterSingleOptional, "^([^:]+:[^:]+)$"); // pattern query parameter
      if (patternParameterArrayMandatory != null) __ApiRequest.AddPatternQueryParameter(patternParameterArrayMandatory, "^([^!:]+:[^:]+)$"); // pattern query parameter
      if (patternParameterSingleMandatory != null) __ApiRequest.AddPatternQueryParameter(patternParameterSingleMandatory, "^([^:]+:[^:]+)$"); // pattern query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="booleanMandatoryWithoutDefault">Description</param>
    /// <param name="integerMandatoryWithoutDefault">Description</param>
    /// <param name="numberMandatoryWithoutDefault">Description</param>
    /// <param name="stringMandatoryWithoutDefault">Description</param>
    /// <param name="booleanArrayMandatoryWithoutDefault">Description</param>
    /// <param name="integerArrayMandatoryWithoutDefault">Description</param>
    /// <param name="numberArrayMandatoryWithoutDefault">Description</param>
    /// <param name="stringArrayMandatoryWithoutDefault">Description</param>
    /// <param name="patternParameterArrayMandatory">Description</param>
    /// <param name="patternParameterSingleMandatory">Description</param>
    /// <param name="overriddenTypeMandatory">Description</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="booleanOptionalWithDefault">Description (optional, defaults to true)</param>
    /// <param name="booleanOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerOptionalWithDefault">Description (optional, defaults to 2)</param>
    /// <param name="integerOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberOptionalWithDefault">Description (optional, defaults to 2)</param>
    /// <param name="numberOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringOptionalWithDefault">Description (optional, defaults to def)</param>
    /// <param name="stringOptionalWithoutDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="patternParameterArrayOptional">Description (optional)</param>
    /// <param name="patternParameterSingleOptional">Description (optional)</param>
    /// <param name="overriddenTypeOptional">Description (optional)</param>
    /// <returns>JObject</returns>
    
    public JObject GetQueryParameters(bool booleanMandatoryWithoutDefault, int integerMandatoryWithoutDefault, decimal numberMandatoryWithoutDefault, string stringMandatoryWithoutDefault, IEnumerable<bool> booleanArrayMandatoryWithoutDefault, IEnumerable<int> integerArrayMandatoryWithoutDefault, IEnumerable<decimal> numberArrayMandatoryWithoutDefault, IEnumerable<string> stringArrayMandatoryWithoutDefault, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory, IReadOnlyDictionary<string, string> patternParameterSingleMandatory, int overriddenTypeMandatory, bool? fail = null, bool? booleanOptionalWithDefault = null, bool? booleanOptionalWithoutDefault = null, int? integerOptionalWithDefault = null, int? integerOptionalWithoutDefault = null, decimal? numberOptionalWithDefault = null, decimal? numberOptionalWithoutDefault = null, string stringOptionalWithDefault = null, string stringOptionalWithoutDefault = null, IEnumerable<bool> booleanArrayOptionalWithDefault = null, IEnumerable<bool> booleanArrayOptionalWithoutDefault = null, IEnumerable<int> integerArrayOptionalWithDefault = null, IEnumerable<int> integerArrayOptionalWithoutDefault = null, IEnumerable<decimal> numberArrayOptionalWithDefault = null, IEnumerable<decimal> numberArrayOptionalWithoutDefault = null, IEnumerable<string> stringArrayOptionalWithDefault = null, IEnumerable<string> stringArrayOptionalWithoutDefault = null, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null, IReadOnlyDictionary<string, string> patternParameterSingleOptional = null, int? overriddenTypeOptional = null)
    {
      ApiRequest __ApiRequest = GetQueryParametersBuildRequest(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetQueryParameters");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="booleanMandatoryWithoutDefault">Description</param>
    /// <param name="integerMandatoryWithoutDefault">Description</param>
    /// <param name="numberMandatoryWithoutDefault">Description</param>
    /// <param name="stringMandatoryWithoutDefault">Description</param>
    /// <param name="booleanArrayMandatoryWithoutDefault">Description</param>
    /// <param name="integerArrayMandatoryWithoutDefault">Description</param>
    /// <param name="numberArrayMandatoryWithoutDefault">Description</param>
    /// <param name="stringArrayMandatoryWithoutDefault">Description</param>
    /// <param name="patternParameterArrayMandatory">Description</param>
    /// <param name="patternParameterSingleMandatory">Description</param>
    /// <param name="overriddenTypeMandatory">Description</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="booleanOptionalWithDefault">Description (optional, default to true)</param>
    /// <param name="booleanOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerOptionalWithDefault">Description (optional, default to 2)</param>
    /// <param name="integerOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberOptionalWithDefault">Description (optional, default to 2)</param>
    /// <param name="numberOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringOptionalWithDefault">Description (optional, default to def)</param>
    /// <param name="stringOptionalWithoutDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="booleanArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="integerArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="numberArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithDefault">Description (optional)</param>
    /// <param name="stringArrayOptionalWithoutDefault">Description (optional)</param>
    /// <param name="patternParameterArrayOptional">Description (optional)</param>
    /// <param name="patternParameterSingleOptional">Description (optional)</param>
    /// <param name="overriddenTypeOptional">Description (optional)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> GetQueryParametersAsync (bool booleanMandatoryWithoutDefault, int integerMandatoryWithoutDefault, decimal numberMandatoryWithoutDefault, string stringMandatoryWithoutDefault, IEnumerable<bool> booleanArrayMandatoryWithoutDefault, IEnumerable<int> integerArrayMandatoryWithoutDefault, IEnumerable<decimal> numberArrayMandatoryWithoutDefault, IEnumerable<string> stringArrayMandatoryWithoutDefault, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory, IReadOnlyDictionary<string, string> patternParameterSingleMandatory, int overriddenTypeMandatory, bool? fail = null, bool? booleanOptionalWithDefault = null, bool? booleanOptionalWithoutDefault = null, int? integerOptionalWithDefault = null, int? integerOptionalWithoutDefault = null, decimal? numberOptionalWithDefault = null, decimal? numberOptionalWithoutDefault = null, string stringOptionalWithDefault = null, string stringOptionalWithoutDefault = null, IEnumerable<bool> booleanArrayOptionalWithDefault = null, IEnumerable<bool> booleanArrayOptionalWithoutDefault = null, IEnumerable<int> integerArrayOptionalWithDefault = null, IEnumerable<int> integerArrayOptionalWithoutDefault = null, IEnumerable<decimal> numberArrayOptionalWithDefault = null, IEnumerable<decimal> numberArrayOptionalWithoutDefault = null, IEnumerable<string> stringArrayOptionalWithDefault = null, IEnumerable<string> stringArrayOptionalWithoutDefault = null, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null, IReadOnlyDictionary<string, string> patternParameterSingleOptional = null, int? overriddenTypeOptional = null)
    {
      ApiRequest __ApiRequest = GetQueryParametersBuildRequest(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetQueryParameters");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    private ApiRequest PostBinaryBodyBuildRequest(byte[] bodyName, bool? fail = null)
    {
      // verify the required parameter 'bodyName' is set
      if (bodyName == null)
        throw new ApiException("Missing required parameter 'bodyName' when calling ParametersApi->PostBinaryBody");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/binary-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (bodyName != null) __ApiRequest.AddBodyParameter("application/zip", bodyName); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    
    public byte[] PostBinaryBody(byte[] bodyName, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostBinaryBodyBuildRequest(bodyName, fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "PostBinaryBody");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of byte[]</returns>
    
    public async System.Threading.Tasks.Task<byte[]> PostBinaryBodyAsync (byte[] bodyName, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostBinaryBodyBuildRequest(bodyName, fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "PostBinaryBody");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    private ApiRequest PostBinaryBodyOptionalBuildRequest(byte[] bodyName = null, bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/binary-body-optional");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (bodyName != null) __ApiRequest.AddBodyParameter("application/zip", bodyName); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    
    public byte[] PostBinaryBodyOptional(byte[] bodyName = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostBinaryBodyOptionalBuildRequest(bodyName, fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "PostBinaryBodyOptional");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="bodyName">Description (optional)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of byte[]</returns>
    
    public async System.Threading.Tasks.Task<byte[]> PostBinaryBodyOptionalAsync (byte[] bodyName = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostBinaryBodyOptionalBuildRequest(bodyName, fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "PostBinaryBodyOptional");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>string</returns>
    private ApiRequest PostExcludedBinaryBodyBuildRequest()
    {

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/excluded-binary-body");
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>string</returns>
    
    public string PostExcludedBinaryBody()
    {
      ApiRequest __ApiRequest = PostExcludedBinaryBodyBuildRequest();
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostExcludedBinaryBody");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> PostExcludedBinaryBodyAsync ()
    {
      ApiRequest __ApiRequest = PostExcludedBinaryBodyBuildRequest();
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostExcludedBinaryBody");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON).</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostObjectBodyBuildRequest(JObject archive, bool? fail = null)
    {
      // verify the required parameter 'archive' is set
      if (archive == null)
        throw new ApiException("Missing required parameter 'archive' when calling ParametersApi->PostObjectBody");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/object-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (archive != null) __ApiRequest.AddBodyParameter("application/json", archive); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON).</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostObjectBody(JObject archive, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostObjectBodyBuildRequest(archive, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostObjectBody");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON).</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostObjectBodyAsync (JObject archive, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostObjectBodyBuildRequest(archive, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostObjectBody");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON). (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostObjectBodyOptionalBuildRequest(JObject archive = null, bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/object-body-optional");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (archive != null) __ApiRequest.AddBodyParameter("application/json", archive); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON). (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostObjectBodyOptional(JObject archive = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostObjectBodyOptionalBuildRequest(archive, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostObjectBodyOptional");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (JSON). (optional)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostObjectBodyOptionalAsync (JObject archive = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostObjectBodyOptionalBuildRequest(archive, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostObjectBodyOptional");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <returns>string</returns>
    private ApiRequest PostOverriddenBinaryBodyBuildRequest(byte[] archive)
    {
      // verify the required parameter 'archive' is set
      if (archive == null)
        throw new ApiException("Missing required parameter 'archive' when calling ParametersApi->PostOverriddenBinaryBody");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/overridden-binary-body");
      if (archive != null) __ApiRequest.AddBodyParameter("application/xml", archive); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <returns>string</returns>
    
    public string PostOverriddenBinaryBody(byte[] archive)
    {
      ApiRequest __ApiRequest = PostOverriddenBinaryBodyBuildRequest(archive);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostOverriddenBinaryBody");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> PostOverriddenBinaryBodyAsync (byte[] archive)
    {
      ApiRequest __ApiRequest = PostOverriddenBinaryBodyBuildRequest(archive);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostOverriddenBinaryBody");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    private ApiRequest PostStringBodyBuildRequest(string archive, bool? fail = null)
    {
      // verify the required parameter 'archive' is set
      if (archive == null)
        throw new ApiException("Missing required parameter 'archive' when calling ParametersApi->PostStringBody");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/string-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (archive != null) __ApiRequest.AddBodyParameter("application/xml", archive); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    
    public string PostStringBody(string archive, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostStringBodyBuildRequest(archive, fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostStringBody");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML).</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> PostStringBodyAsync (string archive, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostStringBodyBuildRequest(archive, fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostStringBody");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML). (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    private ApiRequest PostStringBodyOptionalBuildRequest(string archive = null, bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/string-body-optional");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (archive != null) __ApiRequest.AddBodyParameter("application/xml", archive); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML). (optional)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    
    public string PostStringBodyOptional(string archive = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostStringBodyOptionalBuildRequest(archive, fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostStringBodyOptional");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="archive">An archive item (XML). (optional)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> PostStringBodyOptionalAsync (string archive = null, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostStringBodyOptionalBuildRequest(archive, fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "PostStringBodyOptional");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A Zip Archive</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostVariantBodyZipArchiveBuildRequest(byte[] template, bool? fail = null)
    {
      // verify the required parameter 'template' is set
      if (template == null)
        throw new ApiException("Missing required parameter 'template' when calling ParametersApi->PostVariantBodyZipArchive");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/variant-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (template != null) __ApiRequest.AddBodyParameter("application/zip", template); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A Zip Archive</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostVariantBodyZipArchive(byte[] template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyZipArchiveBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyZipArchive");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A Zip Archive</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostVariantBodyZipArchiveAsync (byte[] template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyZipArchiveBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyZipArchive");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A XHTML file</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostVariantBodyXHTMLBuildRequest(string template, bool? fail = null)
    {
      // verify the required parameter 'template' is set
      if (template == null)
        throw new ApiException("Missing required parameter 'template' when calling ParametersApi->PostVariantBodyXHTML");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/variant-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (template != null) __ApiRequest.AddBodyParameter("application/xhtml+xml", template); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A XHTML file</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostVariantBodyXHTML(string template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyXHTMLBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyXHTML");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A XHTML file</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostVariantBodyXHTMLAsync (string template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyXHTMLBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyXHTML");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A HTML file</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostVariantBodyHTMLBuildRequest(string template, bool? fail = null)
    {
      // verify the required parameter 'template' is set
      if (template == null)
        throw new ApiException("Missing required parameter 'template' when calling ParametersApi->PostVariantBodyHTML");

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/variant-body");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (template != null) __ApiRequest.AddBodyParameter("text/html", template); // body parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A HTML file</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostVariantBodyHTML(string template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyHTMLBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyHTML");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="template">A HTML file</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostVariantBodyHTMLAsync (string template, bool? fail = null)
    {
      ApiRequest __ApiRequest = PostVariantBodyHTMLBuildRequest(template, fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostVariantBodyHTML");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }
  }
}

