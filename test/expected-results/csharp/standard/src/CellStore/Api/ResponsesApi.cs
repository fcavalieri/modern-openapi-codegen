/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;
using CellStore.Client;
using CellStore.Client.Calls;
using CellStore.Client.Requests;
using CellStore.Model;

namespace CellStore.Api
{
  /// <summary>
  /// Represents a collection of functions to interact with the API endpoints
  /// </summary>
  public class ResponsesApi: ApiBase
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ResponsesApi"/> class.
    /// </summary>
    /// <returns></returns>
    public ResponsesApi(string basePath, Configuration configuration = null) : base(basePath, configuration)
    {
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns></returns>
    private ApiRequest EmptyResponseBuildRequest(bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/empty-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns></returns>
    
    public void EmptyResponse(bool? fail = null)
    {
      ApiRequest __ApiRequest = EmptyResponseBuildRequest(fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "EmptyResponse");
      __ApiCall.Execute();
      
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of void</returns>
    
    public async System.Threading.Tasks.Task EmptyResponseAsync (bool? fail = null)
    {
      ApiRequest __ApiRequest = EmptyResponseBuildRequest(fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "EmptyResponse");
      await __ApiCall.ExecuteAsync();
      
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    private ApiRequest GetBinaryResponseBuildRequest(bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/binary-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>byte[]</returns>
    
    public byte[] GetBinaryResponse(bool? fail = null)
    {
      ApiRequest __ApiRequest = GetBinaryResponseBuildRequest(fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetBinaryResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of byte[]</returns>
    
    public async System.Threading.Tasks.Task<byte[]> GetBinaryResponseAsync (bool? fail = null)
    {
      ApiRequest __ApiRequest = GetBinaryResponseBuildRequest(fail);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetBinaryResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest GetJsonResponseBuildRequest(bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/json-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject GetJsonResponse(bool? fail = null)
    {
      ApiRequest __ApiRequest = GetJsonResponseBuildRequest(fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetJsonResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> GetJsonResponseAsync (bool? fail = null)
    {
      ApiRequest __ApiRequest = GetJsonResponseBuildRequest(fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetJsonResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    private ApiRequest GetXMLResponseBuildRequest(bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/xml-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>string</returns>
    
    public string GetXMLResponse(bool? fail = null)
    {
      ApiRequest __ApiRequest = GetXMLResponseBuildRequest(fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "GetXMLResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> GetXMLResponseAsync (bool? fail = null)
    {
      ApiRequest __ApiRequest = GetXMLResponseBuildRequest(fail);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "GetXMLResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <returns>ISequence&lt;JObject&gt;</returns>
    private ApiRequest ListJsonResponseBuildRequest(bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/list-json-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <returns>ISequence&lt;JObject&gt;</returns>
    
    public ISequence<JObject> ListJsonResponse(bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {
      ApiRequest __ApiRequest = ListJsonResponseBuildRequest(fail, failAt, sleep, amount);
      ApiCallObjectList<JObject> __ApiCall = new ApiCallObjectList<JObject>(ApiClient, __ApiRequest, "ListJsonResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <returns>Task of ISequence&lt;JObject&gt;</returns>
    
    public async System.Threading.Tasks.Task<ISequence<JObject>> ListJsonResponseAsync (bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {
      ApiRequest __ApiRequest = ListJsonResponseBuildRequest(fail, failAt, sleep, amount);
      ApiCallObjectList<JObject> __ApiCall = new ApiCallObjectList<JObject>(ApiClient, __ApiRequest, "ListJsonResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    private ApiRequest PostJsonResponseBuildRequest(bool? fail = null)
    {

      var __ApiRequest = new ApiRequest(Method.POST, BasePath, "/json-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <returns>JObject</returns>
    
    public JObject PostJsonResponse(bool? fail = null)
    {
      ApiRequest __ApiRequest = PostJsonResponseBuildRequest(fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostJsonResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> PostJsonResponseAsync (bool? fail = null)
    {
      ApiRequest __ApiRequest = PostJsonResponseBuildRequest(fail);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "PostJsonResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <returns>IStream&lt;JObject&gt;</returns>
    private ApiRequest StreamJsonResponseBuildRequest(bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/stream-json-response");
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <returns>IStream&lt;JObject&gt;</returns>
    
    public IStream<JObject> StreamJsonResponse(bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {
      ApiRequest __ApiRequest = StreamJsonResponseBuildRequest(fail, failAt, sleep, amount);
      ApiCallObjectStream<JObject> __ApiCall = new ApiCallObjectStream<JObject>(ApiClient, __ApiRequest, "StreamJsonResponse");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <returns>Task of IStream&lt;JObject&gt;</returns>
    
    public async System.Threading.Tasks.Task<IStream<JObject>> StreamJsonResponseAsync (bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)
    {
      ApiRequest __ApiRequest = StreamJsonResponseBuildRequest(fail, failAt, sleep, amount);
      ApiCallObjectStream<JObject> __ApiCall = new ApiCallObjectStream<JObject>(ApiClient, __ApiRequest, "StreamJsonResponse");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>ISequence&lt;JObject&gt;</returns>
    private ApiRequest GetArbitraryResponseAsListBuildRequest(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      // verify the required parameter 'requiredB' is set
      if (requiredB == null)
        throw new ApiException("Missing required parameter 'requiredB' when calling ResponsesApi->GetArbitraryResponseAsList");
      // verify the required parameter 'requiredC' is set
      if (requiredC == null)
        throw new ApiException("Missing required parameter 'requiredC' when calling ResponsesApi->GetArbitraryResponseAsList");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "json-list"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedC != null) __ApiRequest.AddQueryParameter("excluded-c", excludedC); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>ISequence&lt;JObject&gt;</returns>
    
    public ISequence<JObject> GetArbitraryResponseAsList(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsListBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallObjectList<JObject> __ApiCall = new ApiCallObjectList<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsList");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of ISequence&lt;JObject&gt;</returns>
    
    public async System.Threading.Tasks.Task<ISequence<JObject>> GetArbitraryResponseAsListAsync (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsListBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallObjectList<JObject> __ApiCall = new ApiCallObjectList<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsList");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>IStream&lt;JObject&gt;</returns>
    private ApiRequest GetArbitraryResponseAsStreamBuildRequest(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      // verify the required parameter 'requiredA' is set
      if (requiredA == null)
        throw new ApiException("Missing required parameter 'requiredA' when calling ResponsesApi->GetArbitraryResponseAsStream");
      // verify the required parameter 'requiredD' is set
      if (requiredD == null)
        throw new ApiException("Missing required parameter 'requiredD' when calling ResponsesApi->GetArbitraryResponseAsStream");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "json-stream"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedA != null) __ApiRequest.AddQueryParameter("excluded-a", excludedA); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>IStream&lt;JObject&gt;</returns>
    
    public IStream<JObject> GetArbitraryResponseAsStream(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsStreamBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallObjectStream<JObject> __ApiCall = new ApiCallObjectStream<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsStream");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of IStream&lt;JObject&gt;</returns>
    
    public async System.Threading.Tasks.Task<IStream<JObject>> GetArbitraryResponseAsStreamAsync (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsStreamBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallObjectStream<JObject> __ApiCall = new ApiCallObjectStream<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsStream");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>string</returns>
    private ApiRequest GetArbitraryResponseAsXMLBuildRequest(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      // verify the required parameter 'requiredB' is set
      if (requiredB == null)
        throw new ApiException("Missing required parameter 'requiredB' when calling ResponsesApi->GetArbitraryResponseAsXML");
      // verify the required parameter 'requiredC' is set
      if (requiredC == null)
        throw new ApiException("Missing required parameter 'requiredC' when calling ResponsesApi->GetArbitraryResponseAsXML");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "xml"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "xml-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedC != null) __ApiRequest.AddQueryParameter("excluded-c", excludedC); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>string</returns>
    
    public string GetArbitraryResponseAsXML(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsXMLBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "GetArbitraryResponseAsXML");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of string</returns>
    
    public async System.Threading.Tasks.Task<string> GetArbitraryResponseAsXMLAsync (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsXMLBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallSingleton<string> __ApiCall = new ApiCallSingleton<string>(ApiClient, __ApiRequest, "GetArbitraryResponseAsXML");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>byte[]</returns>
    private ApiRequest GetArbitraryResponseAsBinaryBuildRequest(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      // verify the required parameter 'requiredA' is set
      if (requiredA == null)
        throw new ApiException("Missing required parameter 'requiredA' when calling ResponsesApi->GetArbitraryResponseAsBinary");
      // verify the required parameter 'requiredD' is set
      if (requiredD == null)
        throw new ApiException("Missing required parameter 'requiredD' when calling ResponsesApi->GetArbitraryResponseAsBinary");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "binary"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "binary-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedA != null) __ApiRequest.AddQueryParameter("excluded-a", excludedA); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>byte[]</returns>
    
    public byte[] GetArbitraryResponseAsBinary(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsBinaryBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetArbitraryResponseAsBinary");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of byte[]</returns>
    
    public async System.Threading.Tasks.Task<byte[]> GetArbitraryResponseAsBinaryAsync (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsBinaryBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetArbitraryResponseAsBinary");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>byte[]</returns>
    private ApiRequest GetArbitraryResponseAsBinaryObsoleteBuildRequest(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      // verify the required parameter 'requiredB' is set
      if (requiredB == null)
        throw new ApiException("Missing required parameter 'requiredB' when calling ResponsesApi->GetArbitraryResponseAsBinaryObsolete");
      // verify the required parameter 'requiredC' is set
      if (requiredC == null)
        throw new ApiException("Missing required parameter 'requiredC' when calling ResponsesApi->GetArbitraryResponseAsBinaryObsolete");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "binary"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "binary-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedC != null) __ApiRequest.AddQueryParameter("excluded-c", excludedC); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>byte[]</returns>
    [Obsolete("This variant is obsolete")]
    public byte[] GetArbitraryResponseAsBinaryObsolete(string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsBinaryObsoleteBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetArbitraryResponseAsBinaryObsolete");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedC">Excluded c (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of byte[]</returns>
    [Obsolete("This variant is obsolete")]
    public async System.Threading.Tasks.Task<byte[]> GetArbitraryResponseAsBinaryObsoleteAsync (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsBinaryObsoleteBuildRequest(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
      ApiCallSingleton<byte[]> __ApiCall = new ApiCallSingleton<byte[]>(ApiClient, __ApiRequest, "GetArbitraryResponseAsBinaryObsolete");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>JObject</returns>
    private ApiRequest GetArbitraryResponseAsJsonBuildRequest(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      // verify the required parameter 'requiredA' is set
      if (requiredA == null)
        throw new ApiException("Missing required parameter 'requiredA' when calling ResponsesApi->GetArbitraryResponseAsJson");
      // verify the required parameter 'requiredD' is set
      if (requiredD == null)
        throw new ApiException("Missing required parameter 'requiredD' when calling ResponsesApi->GetArbitraryResponseAsJson");

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/arbitrary-response");
      __ApiRequest.AddQueryParameter("format", "json"); // hardcoded query parameter
      __ApiRequest.AddQueryParameter("format-copy", "json-copy"); // hardcoded query parameter
      if (fail != null) __ApiRequest.AddQueryParameter("fail", fail.Value); // query parameter
      if (failAt != null) __ApiRequest.AddQueryParameter("fail-at", failAt.Value); // query parameter
      if (sleep != null) __ApiRequest.AddQueryParameter("sleep", sleep.Value); // query parameter
      if (amount != null) __ApiRequest.AddQueryParameter("amount", amount.Value); // query parameter
      if (excludedA != null) __ApiRequest.AddQueryParameter("excluded-a", excludedA); // query parameter
      if (requiredA != null) __ApiRequest.AddQueryParameter("required-a", requiredA); // query parameter
      if (requiredB != null) __ApiRequest.AddQueryParameter("required-b", requiredB); // query parameter
      if (requiredC != null) __ApiRequest.AddQueryParameter("required-c", requiredC); // query parameter
      if (requiredD != null) __ApiRequest.AddQueryParameter("required-d", requiredD); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, defaults to false)</param>
    /// <param name="failAt">Description (optional, defaults to -1)</param>
    /// <param name="sleep">Description (optional, defaults to 0)</param>
    /// <param name="amount">Description (optional, defaults to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>JObject</returns>
    
    public JObject GetArbitraryResponseAsJson(string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsJsonBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsJson");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Summary Description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="requiredA">Required a (Two required parameter should be optional per variant)</param>
    /// <param name="requiredD">Required d (Two required parameter should be optional per variant)</param>
    /// <param name="fail">Description (optional, default to false)</param>
    /// <param name="failAt">Description (optional, default to -1)</param>
    /// <param name="sleep">Description (optional, default to 0)</param>
    /// <param name="amount">Description (optional, default to 1000)</param>
    /// <param name="excludedA">Excluded a (A single excluded parameter should be present per variant) (optional)</param>
    /// <param name="requiredB">Required b (Two required parameter should be optional per variant) (optional)</param>
    /// <param name="requiredC">Required c (Two required parameter should be optional per variant) (optional)</param>
    /// <returns>Task of JObject</returns>
    
    public async System.Threading.Tasks.Task<JObject> GetArbitraryResponseAsJsonAsync (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)
    {
      ApiRequest __ApiRequest = GetArbitraryResponseAsJsonBuildRequest(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetArbitraryResponseAsJson");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }
  }
}

