/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;
using CellStore.Client;
using CellStore.Client.Calls;
using CellStore.Client.Requests;
using CellStore.Model;

namespace CellStore.Api
{
  /// <summary>
  /// Represents a collection of functions to interact with the API endpoints
  /// </summary>
  public class ObsoleteApi: ApiBase
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ObsoleteApi"/> class.
    /// </summary>
    /// <returns></returns>
    public ObsoleteApi(string basePath, Configuration configuration = null) : base(basePath, configuration)
    {
    }

    /// <summary>
    /// Obsolete summary Obsolete description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="limit">Limits the amount of documents in the result of the query.  (optional, defaults to 50)</param>
    /// <returns>JObject</returns>
    private ApiRequest GetObsoleteOperationBuildRequest(int? limit = null)
    {

      var __ApiRequest = new ApiRequest(Method.GET, BasePath, "/obsolete-operation");
      if (limit != null) __ApiRequest.AddQueryParameter("limit", limit.Value); // query parameter
      __ApiRequest.Assemble();
      return __ApiRequest;
    }

    /// <summary>
    /// Obsolete summary Obsolete description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="limit">Limits the amount of documents in the result of the query.  (optional, defaults to 50)</param>
    /// <returns>JObject</returns>
    [Obsolete("This operation is obsolete")]
    public JObject GetObsoleteOperation(int? limit = null)
    {
      ApiRequest __ApiRequest = GetObsoleteOperationBuildRequest(limit);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetObsoleteOperation");
      __ApiCall.Execute();
      return __ApiCall.Result;
    }

    /// <summary>
    /// Obsolete summary Obsolete description
    /// </summary>
    /// <exception cref="CellStore.Client.ApiException">Thrown when fails to make API call</exception>
    /// <param name="limit">Limits the amount of documents in the result of the query.  (optional, default to 50)</param>
    /// <returns>Task of JObject</returns>
    [Obsolete("This operation is obsolete")]
    public async System.Threading.Tasks.Task<JObject> GetObsoleteOperationAsync (int? limit = null)
    {
      ApiRequest __ApiRequest = GetObsoleteOperationBuildRequest(limit);
      ApiCallSingleton<JObject> __ApiCall = new ApiCallSingleton<JObject>(ApiClient, __ApiRequest, "GetObsoleteOperation");
      await __ApiCall.ExecuteAsync();
      return __ApiCall.Result;
    }
  }
}

