/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;
using CellStore.Client;
using CellStore.Client.Calls;
using CellStore.Client.Requests;
using CellStore.Model;

namespace CellStore.Api
{
  /// <summary>
  /// Represents a collection of functions to interact with the API endpoints
  /// </summary>
  public class ExcludedApi: ApiBase
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ExcludedApi"/> class.
    /// </summary>
    /// <returns></returns>
    public ExcludedApi(string basePath, Configuration configuration = null) : base(basePath, configuration)
    {
    }
  }
}

