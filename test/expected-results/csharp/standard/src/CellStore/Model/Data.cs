/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using CellStore.Client;
using Newtonsoft.Json.Linq;

namespace CellStore.Model
{
  public static class Data
  {
    internal static T Parse<T>(JObject rawObject)
    {
      if (typeof(T) == typeof(JObject))
        return (T) (object) rawObject;
      throw new ApiException("Unsupported return type " + typeof(T).Name);
    }
  }
}