/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CellStore.Model
{
  public interface IStream<out T> : IEnumerable<T>
  {
    JObject Metadata { get; }
  }
}