/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System;
using System.Threading.Tasks;
using CellStore.Client.Requests;
using RestSharp;

namespace CellStore.Client
{
  /// <summary>
  ///   API client is mainly responsible for making the HTTP call to the API backend.
  /// </summary>
  public class ApiClient
  {
    private Configuration _configuration;

    /// <summary>
    ///   Initializes a new instance of the <see cref="ApiClient" /> class.
    /// </summary>
    /// <param name="configuration">An instance of Configuration.</param>
    public ApiClient(Configuration configuration)
    {
      Configuration = configuration ?? new Configuration();
    }

    /// <summary>
    ///   Gets or sets an instance of the Configuration.
    /// </summary>
    /// <value>An instance of the Configuration.</value>
    public Configuration Configuration
    {
      get => _configuration;
      private set
      {
        _configuration = value;
        if (_configuration is UnitTestConfiguration unitTestConfiguration)
          UnitTestConfiguration = unitTestConfiguration;
        else
          UnitTestConfiguration = new UnitTestConfiguration();
      }
    }

    /// <summary>
    ///   Gets or sets an instance of the Configuration.
    /// </summary>
    /// <value>An instance of the Configuration.</value>
    internal UnitTestConfiguration UnitTestConfiguration { get; set; }

    /// <summary>
    ///   Gets or sets the RestClient.
    /// </summary>
    /// <value>An instance of the RestClient</value>
    public RestClient RestClient { get; set; }

    private RestClient InitRestClient(ApiRequest apiRequest)
    {
      var restClient = new RestClient(apiRequest.BaseUrl);
      restClient.Timeout = (int) Configuration.TimeoutMs;
      restClient.UserAgent = Configuration.UserAgent;
      restClient.ClearHandlers();
      if (Configuration.Pipelining)
        restClient.AddDefaultHeader("Connection", "Keep-Alive");
      else
        restClient.AddDefaultHeader("Connection", "Close");
      return restClient;
    }

    internal IRestResponse Execute(ApiRequest apiRequest)
    {
      var restClient = InitRestClient(apiRequest);
      if (Configuration.AuthenticationProvider != null)
        Configuration.AuthenticationProvider.AddAuthentication(restClient, apiRequest);
      var response = restClient.Execute(apiRequest.GetRequest());
      if (Configuration.AuthenticationProvider != null)
        Configuration.AuthenticationProvider.ProcessResponse(restClient, apiRequest, response);
      return response;
    }

    internal Task<IRestResponse> ExecuteAsync(ApiRequest apiRequest)
    {
      var restClient = InitRestClient(apiRequest);
      if (Configuration.AuthenticationProvider != null)
        Configuration.AuthenticationProvider.AddAuthentication(restClient, apiRequest);
      var response = restClient.ExecuteAsync(apiRequest.GetRequest());
      response.Wait();
      if (Configuration.AuthenticationProvider != null)
        Configuration.AuthenticationProvider.ProcessResponse(restClient, apiRequest, response.Result);
      return response;
    }
  }
}