/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System.Threading.Tasks;
using CellStore.Client.Requests;
using CellStore.Client.Responses;
using CellStore.Model;

namespace CellStore.Client.Calls
{
  /// <summary>
  ///   API Request
  /// </summary>
  public class ApiCallObjectStream<T> : ApiCallStreamed<T>
  {
    public ApiCallObjectStream(ApiClient apiClient, ApiRequest apiRequest, string apiMethodName) : base(apiClient,
      apiRequest, apiMethodName)
    {
    }

    public IStream<T> Result { get; private set; }

    internal override void Execute()
    {
      base.Execute();
      Result = new StreamedSequence<T>(enumerator, metadata);
    }

    internal override async Task ExecuteAsync()
    {
      await base.ExecuteAsync();
      Result = new StreamedSequence<T>(enumerator, metadata);
    }
  }
}