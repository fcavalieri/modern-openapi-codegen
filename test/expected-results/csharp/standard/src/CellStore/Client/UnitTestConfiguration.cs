/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

namespace CellStore.Client
{
  /// <summary>
  ///   Represents a set of configuration settings
  /// </summary>
  public class UnitTestConfiguration : Configuration
  {
    public bool AllRequestsHaveRetryableErrors { get; set; }

    public bool NextRequestHasRetryableError { get; set; }

    public bool NextRequestHasReceiveError { get; set; }
  }
}