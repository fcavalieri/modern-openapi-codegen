/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System.Collections;
using System.Collections.Generic;
using CellStore.Model;
using Newtonsoft.Json.Linq;

namespace CellStore.Client.Responses
{
  public class MaterializedSequence<T> : ISequence<T>
  {
    private readonly List<T> _list;

    public MaterializedSequence(IEnumerator<T> enumerator, JObject metadata)
    {
      _list = new List<T>();
      while (enumerator.MoveNext())
        _list.Add(enumerator.Current);
      Metadata = metadata;
    }

    public IEnumerator<T> GetEnumerator()
    {
      return _list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return ((IEnumerable) _list).GetEnumerator();
    }

    public int Count => _list.Count;

    public T this[int index] => _list[index];

    public JObject Metadata { get; }
  }
}