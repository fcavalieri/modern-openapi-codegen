/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System;
using System.Xml.Serialization;
using CellStore.Client.Requests;
using RestSharp;

namespace CellStore.Client.Authentication
{
  [XmlInclude(typeof(Unauthenticated))]
  [Serializable()]
  public abstract class AuthenticationProvider
  {
    public abstract void AddAuthentication(RestClient restClient, ApiRequest apiRequest);

    public abstract void ProcessResponse(RestClient restClient, ApiRequest apiRequest, IRestResponse response);
  }
}
