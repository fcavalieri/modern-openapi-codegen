/* 
 * This is a titl*
 *
 * This is a descriptio*
 *
 * OpenAPI spec version: 1.0.0-TESTS
 * 
 */

using System;
using CellStore.Client.Requests;
using RestSharp;

namespace CellStore.Client.Authentication
{
  [Serializable()]
  public class Unauthenticated : AuthenticationProvider
  {
    public override void AddAuthentication(RestClient restClient, ApiRequest apiRequest)
    {
    }

    public override void ProcessResponse(RestClient restClient, ApiRequest apiRequest, IRestResponse response)
    {
    }

    public override int GetHashCode() => 0;

    public override bool Equals(object obj)
    {
        return !(obj as Unauthenticated is null);
    }

    public override string ToString() {
      return "No Authentication";
    }
  }
}
