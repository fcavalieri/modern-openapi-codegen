# CellStore.Api.ResponsesApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EmptyResponse**](ResponsesApi.md#emptyresponse) | **GET** /empty-response | Summary
[**GetBinaryResponse**](ResponsesApi.md#getbinaryresponse) | **GET** /binary-response | Summary
[**GetJsonResponse**](ResponsesApi.md#getjsonresponse) | **GET** /json-response | Summary
[**GetXMLResponse**](ResponsesApi.md#getxmlresponse) | **GET** /xml-response | Summary
[**ListJsonResponse**](ResponsesApi.md#listjsonresponse) | **GET** /list-json-response | Summary
[**PostJsonResponse**](ResponsesApi.md#postjsonresponse) | **POST** /json-response | Summary
[**StreamJsonResponse**](ResponsesApi.md#streamjsonresponse) | **GET** /stream-json-response | Summary
[**GetArbitraryResponseAsList**](ResponsesApi.md#getarbitraryresponseaslist) | **GET** /arbitrary-response | Summary
[**GetArbitraryResponseAsStream**](ResponsesApi.md#getarbitraryresponseasstream) | **GET** /arbitrary-response | Summary
[**GetArbitraryResponseAsXML**](ResponsesApi.md#getarbitraryresponseasxml) | **GET** /arbitrary-response | Summary
[**GetArbitraryResponseAsBinary**](ResponsesApi.md#getarbitraryresponseasbinary) | **GET** /arbitrary-response | Summary
[**GetArbitraryResponseAsBinaryObsolete**](ResponsesApi.md#getarbitraryresponseasbinaryobsolete) | **GET** /arbitrary-response | Summary
[**GetArbitraryResponseAsJson**](ResponsesApi.md#getarbitraryresponseasjson) | **GET** /arbitrary-response | Summary

<a name="emptyresponse"></a>
# **EmptyResponse**
> void EmptyResponse (bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class EmptyResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                apiInstance.EmptyResponse(fail);            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.EmptyResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

void

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getbinaryresponse"></a>
# **GetBinaryResponse**
> byte[] GetBinaryResponse (bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetBinaryResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                byte[] result = apiInstance.GetBinaryResponse(fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetBinaryResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**byte[]**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getjsonresponse"></a>
# **GetJsonResponse**
> JObject GetJsonResponse (bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetJsonResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.GetJsonResponse(fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetJsonResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getxmlresponse"></a>
# **GetXMLResponse**
> string GetXMLResponse (bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetXMLResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                string result = apiInstance.GetXMLResponse(fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetXMLResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="listjsonresponse"></a>
# **ListJsonResponse**
> ISequence&lt;JObject&gt; ListJsonResponse (bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class ListJsonResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)

            try
            {
                // Summary
                ISequence<JObject> result = apiInstance.ListJsonResponse(fail, failAt, sleep, amount);
                foreach (var item in result)
                    Debug.WriteLine(item);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.ListJsonResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]

### Return type

**ISequence<JObject>**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postjsonresponse"></a>
# **PostJsonResponse**
> JObject PostJsonResponse (bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostJsonResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostJsonResponse(fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.PostJsonResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="streamjsonresponse"></a>
# **StreamJsonResponse**
> IStream&lt;JObject&gt; StreamJsonResponse (bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class StreamJsonResponseExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)

            try
            {
                // Summary
                IStream<JObject> result = apiInstance.StreamJsonResponse(fail, failAt, sleep, amount);
                foreach (var item in result)
                    Debug.WriteLine(item);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.StreamJsonResponse: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]

### Return type

**IStream<JObject>**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseaslist"></a>
# **GetArbitraryResponseAsList**
> ISequence&lt;JObject&gt; GetArbitraryResponseAsList (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsListExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant)
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedC = ...;   // Excluded c (A single excluded parameter should be present per variant) (optional) 
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant) (optional) 
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                ISequence<JObject> result = apiInstance.GetArbitraryResponseAsList(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
                foreach (var item in result)
                    Debug.WriteLine(item);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsList: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedC** | **string**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | [optional] 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

**ISequence<JObject>**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseasstream"></a>
# **GetArbitraryResponseAsStream**
> IStream&lt;JObject&gt; GetArbitraryResponseAsStream (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsStreamExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant)
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedA = ...;   // Excluded a (A single excluded parameter should be present per variant) (optional) 
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant) (optional) 
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                IStream<JObject> result = apiInstance.GetArbitraryResponseAsStream(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
                foreach (var item in result)
                    Debug.WriteLine(item);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsStream: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedA** | **string**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | [optional] 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

**IStream<JObject>**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseasxml"></a>
# **GetArbitraryResponseAsXML**
> string GetArbitraryResponseAsXML (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsXMLExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant)
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedC = ...;   // Excluded c (A single excluded parameter should be present per variant) (optional) 
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant) (optional) 
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                string result = apiInstance.GetArbitraryResponseAsXML(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsXML: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedC** | **string**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | [optional] 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseasbinary"></a>
# **GetArbitraryResponseAsBinary**
> byte[] GetArbitraryResponseAsBinary (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsBinaryExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant)
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedA = ...;   // Excluded a (A single excluded parameter should be present per variant) (optional) 
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant) (optional) 
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                byte[] result = apiInstance.GetArbitraryResponseAsBinary(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsBinary: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedA** | **string**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | [optional] 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

**byte[]**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseasbinaryobsolete"></a>
# **GetArbitraryResponseAsBinaryObsolete**
> byte[] GetArbitraryResponseAsBinaryObsolete (string requiredB, string requiredC, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedC = null, string requiredA = null, string requiredD = null)

Summary

Description

This variant is obsolete

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsBinaryObsoleteExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant)
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedC = ...;   // Excluded c (A single excluded parameter should be present per variant) (optional) 
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant) (optional) 
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                byte[] result = apiInstance.GetArbitraryResponseAsBinaryObsolete(requiredB, requiredC, fail, failAt, sleep, amount, excludedC, requiredA, requiredD);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsBinaryObsolete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedC** | **string**| Excluded c (A single excluded parameter should be present per variant) | [optional] 
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | [optional] 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | [optional] 

### Return type

**byte[]**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getarbitraryresponseasjson"></a>
# **GetArbitraryResponseAsJson**
> JObject GetArbitraryResponseAsJson (string requiredA, string requiredD, bool? fail = null, int? failAt = null, int? sleep = null, int? amount = null, string excludedA = null, string requiredB = null, string requiredC = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetArbitraryResponseAsJsonExample
    {
        public void main()
        {
            var apiInstance = new ResponsesApi("http://localhost:8888/prefix");
            string requiredA = ...;   // Required a (Two required parameter should be optional per variant)
            string requiredD = ...;   // Required d (Two required parameter should be optional per variant)
            bool? fail = ...;   // Description (optional)  (default to false)
            int? failAt = ...;   // Description (optional)  (default to -1)
            int? sleep = ...;   // Description (optional)  (default to 0)
            int? amount = ...;   // Description (optional)  (default to 1000)
            string excludedA = ...;   // Excluded a (A single excluded parameter should be present per variant) (optional) 
            string requiredB = ...;   // Required b (Two required parameter should be optional per variant) (optional) 
            string requiredC = ...;   // Required c (Two required parameter should be optional per variant) (optional) 

            try
            {
                // Summary
                JObject result = apiInstance.GetArbitraryResponseAsJson(requiredA, requiredD, fail, failAt, sleep, amount, excludedA, requiredB, requiredC);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResponsesApi.GetArbitraryResponseAsJson: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requiredA** | **string**| Required a (Two required parameter should be optional per variant) | 
 **requiredD** | **string**| Required d (Two required parameter should be optional per variant) | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **failAt** | **int?**| Description | [optional] [default to -1]
 **sleep** | **int?**| Description | [optional] [default to 0]
 **amount** | **int?**| Description | [optional] [default to 1000]
 **excludedA** | **string**| Excluded a (A single excluded parameter should be present per variant) | [optional] 
 **requiredB** | **string**| Required b (Two required parameter should be optional per variant) | [optional] 
 **requiredC** | **string**| Required c (Two required parameter should be optional per variant) | [optional] 

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
