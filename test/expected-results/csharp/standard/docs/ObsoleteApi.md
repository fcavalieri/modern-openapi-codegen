# CellStore.Api.ObsoleteApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetObsoleteOperation**](ObsoleteApi.md#getobsoleteoperation) | **GET** /obsolete-operation | Obsolete summary

<a name="getobsoleteoperation"></a>
# **GetObsoleteOperation**
> JObject GetObsoleteOperation (int? limit = null)

Obsolete summary

Obsolete description

This operation is obsolete

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetObsoleteOperationExample
    {
        public void main()
        {
            var apiInstance = new ObsoleteApi("http://localhost:8888/prefix");
            int? limit = ...;   // Limits the amount of documents in the result of the query.  (optional)  (default to 50)

            try
            {
                // Obsolete summary
                JObject result = apiInstance.GetObsoleteOperation(limit);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ObsoleteApi.GetObsoleteOperation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**| Limits the amount of documents in the result of the query.  | [optional] [default to 50]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
