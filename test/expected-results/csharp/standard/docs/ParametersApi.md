# CellStore.Api.ParametersApi

All URIs are relative to *{server}/prefix*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetPathParameters**](ParametersApi.md#getpathparameters) | **GET** /path-parameters/{id1}/path/{id2}/{id3}/path/{id4} | Summary
[**GetQueryParameters**](ParametersApi.md#getqueryparameters) | **GET** /query-parameters | Summary
[**PostBinaryBody**](ParametersApi.md#postbinarybody) | **POST** /binary-body | Summary
[**PostBinaryBodyOptional**](ParametersApi.md#postbinarybodyoptional) | **POST** /binary-body-optional | Summary
[**PostExcludedBinaryBody**](ParametersApi.md#postexcludedbinarybody) | **POST** /excluded-binary-body | Summary
[**PostObjectBody**](ParametersApi.md#postobjectbody) | **POST** /object-body | Summary
[**PostObjectBodyOptional**](ParametersApi.md#postobjectbodyoptional) | **POST** /object-body-optional | Summary
[**PostOverriddenBinaryBody**](ParametersApi.md#postoverriddenbinarybody) | **POST** /overridden-binary-body | Summary
[**PostStringBody**](ParametersApi.md#poststringbody) | **POST** /string-body | Summary
[**PostStringBodyOptional**](ParametersApi.md#poststringbodyoptional) | **POST** /string-body-optional | Summary
[**PostVariantBodyZipArchive**](ParametersApi.md#postvariantbodyziparchive) | **POST** /variant-body | Summary
[**PostVariantBodyXHTML**](ParametersApi.md#postvariantbodyxhtml) | **POST** /variant-body | Summary
[**PostVariantBodyHTML**](ParametersApi.md#postvariantbodyhtml) | **POST** /variant-body | Summary

<a name="getpathparameters"></a>
# **GetPathParameters**
> JObject GetPathParameters (int id1, string id2 = null, bool? id3 = null, decimal? id4 = null, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetPathParametersExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            int id1 = ...;   // Description
            string id2 = ...;   // Description (optional) 
            bool? id3 = ...;   // Description (optional) 
            decimal? id4 = ...;   // Description (optional) 
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.GetPathParameters(id1, id2, id3, id4, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.GetPathParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id1** | **int**| Description | 
 **id2** | **string**| Description | [optional] 
 **id3** | **bool?**| Description | [optional] 
 **id4** | **decimal?**| Description | [optional] 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="getqueryparameters"></a>
# **GetQueryParameters**
> JObject GetQueryParameters (bool booleanMandatoryWithoutDefault, int integerMandatoryWithoutDefault, decimal numberMandatoryWithoutDefault, string stringMandatoryWithoutDefault, IEnumerable<bool> booleanArrayMandatoryWithoutDefault, IEnumerable<int> integerArrayMandatoryWithoutDefault, IEnumerable<decimal> numberArrayMandatoryWithoutDefault, IEnumerable<string> stringArrayMandatoryWithoutDefault, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory, IReadOnlyDictionary<string, string> patternParameterSingleMandatory, int overriddenTypeMandatory, bool? fail = null, bool? booleanOptionalWithDefault = null, bool? booleanOptionalWithoutDefault = null, int? integerOptionalWithDefault = null, int? integerOptionalWithoutDefault = null, decimal? numberOptionalWithDefault = null, decimal? numberOptionalWithoutDefault = null, string stringOptionalWithDefault = null, string stringOptionalWithoutDefault = null, IEnumerable<bool> booleanArrayOptionalWithDefault = null, IEnumerable<bool> booleanArrayOptionalWithoutDefault = null, IEnumerable<int> integerArrayOptionalWithDefault = null, IEnumerable<int> integerArrayOptionalWithoutDefault = null, IEnumerable<decimal> numberArrayOptionalWithDefault = null, IEnumerable<decimal> numberArrayOptionalWithoutDefault = null, IEnumerable<string> stringArrayOptionalWithDefault = null, IEnumerable<string> stringArrayOptionalWithoutDefault = null, IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null, IReadOnlyDictionary<string, string> patternParameterSingleOptional = null, int? overriddenTypeOptional = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class GetQueryParametersExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            bool booleanMandatoryWithoutDefault = ...;   // Description
            int integerMandatoryWithoutDefault = ...;   // Description
            decimal numberMandatoryWithoutDefault = ...;   // Description
            string stringMandatoryWithoutDefault = ...;   // Description
            IEnumerable<bool> booleanArrayMandatoryWithoutDefault = ...;   // Description
            IEnumerable<int> integerArrayMandatoryWithoutDefault = ...;   // Description
            IEnumerable<decimal> numberArrayMandatoryWithoutDefault = ...;   // Description
            IEnumerable<string> stringArrayMandatoryWithoutDefault = ...;   // Description
            IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory = ...;   // Description
            IReadOnlyDictionary<string, string> patternParameterSingleMandatory = ...;   // Description
            int overriddenTypeMandatory = ...;   // Description
            bool? fail = ...;   // Description (optional)  (default to false)
            bool? booleanOptionalWithDefault = ...;   // Description (optional)  (default to true)
            bool? booleanOptionalWithoutDefault = ...;   // Description (optional) 
            int? integerOptionalWithDefault = ...;   // Description (optional)  (default to 2)
            int? integerOptionalWithoutDefault = ...;   // Description (optional) 
            decimal? numberOptionalWithDefault = ...;   // Description (optional)  (default to 2)
            decimal? numberOptionalWithoutDefault = ...;   // Description (optional) 
            string stringOptionalWithDefault = ...;   // Description (optional)  (default to def)
            string stringOptionalWithoutDefault = ...;   // Description (optional) 
            IEnumerable<bool> booleanArrayOptionalWithDefault = ...;   // Description (optional) 
            IEnumerable<bool> booleanArrayOptionalWithoutDefault = ...;   // Description (optional) 
            IEnumerable<int> integerArrayOptionalWithDefault = ...;   // Description (optional) 
            IEnumerable<int> integerArrayOptionalWithoutDefault = ...;   // Description (optional) 
            IEnumerable<decimal> numberArrayOptionalWithDefault = ...;   // Description (optional) 
            IEnumerable<decimal> numberArrayOptionalWithoutDefault = ...;   // Description (optional) 
            IEnumerable<string> stringArrayOptionalWithDefault = ...;   // Description (optional) 
            IEnumerable<string> stringArrayOptionalWithoutDefault = ...;   // Description (optional) 
            IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = ...;   // Description (optional) 
            IReadOnlyDictionary<string, string> patternParameterSingleOptional = ...;   // Description (optional) 
            int? overriddenTypeOptional = ...;   // Description (optional) 

            try
            {
                // Summary
                JObject result = apiInstance.GetQueryParameters(booleanMandatoryWithoutDefault, integerMandatoryWithoutDefault, numberMandatoryWithoutDefault, stringMandatoryWithoutDefault, booleanArrayMandatoryWithoutDefault, integerArrayMandatoryWithoutDefault, numberArrayMandatoryWithoutDefault, stringArrayMandatoryWithoutDefault, patternParameterArrayMandatory, patternParameterSingleMandatory, overriddenTypeMandatory, fail, booleanOptionalWithDefault, booleanOptionalWithoutDefault, integerOptionalWithDefault, integerOptionalWithoutDefault, numberOptionalWithDefault, numberOptionalWithoutDefault, stringOptionalWithDefault, stringOptionalWithoutDefault, booleanArrayOptionalWithDefault, booleanArrayOptionalWithoutDefault, integerArrayOptionalWithDefault, integerArrayOptionalWithoutDefault, numberArrayOptionalWithDefault, numberArrayOptionalWithoutDefault, stringArrayOptionalWithDefault, stringArrayOptionalWithoutDefault, patternParameterArrayOptional, patternParameterSingleOptional, overriddenTypeOptional);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.GetQueryParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **booleanMandatoryWithoutDefault** | **bool**| Description | 
 **integerMandatoryWithoutDefault** | **int**| Description | 
 **numberMandatoryWithoutDefault** | **decimal**| Description | 
 **stringMandatoryWithoutDefault** | **string**| Description | 
 **booleanArrayMandatoryWithoutDefault** | **IEnumerable&lt;bool&gt;**| Description | 
 **integerArrayMandatoryWithoutDefault** | **IEnumerable&lt;int&gt;**| Description | 
 **numberArrayMandatoryWithoutDefault** | **IEnumerable&lt;decimal&gt;**| Description | 
 **stringArrayMandatoryWithoutDefault** | **IEnumerable&lt;string&gt;**| Description | 
 **patternParameterArrayMandatory** | **IReadOnlyDictionary&lt;string, IEnumerable&lt;string&gt;&gt;**| Description | 
 **patternParameterSingleMandatory** | **IReadOnlyDictionary&lt;string, string&gt;**| Description | 
 **overriddenTypeMandatory** | **int**| Description | 
 **fail** | **bool?**| Description | [optional] [default to false]
 **booleanOptionalWithDefault** | **bool?**| Description | [optional] [default to true]
 **booleanOptionalWithoutDefault** | **bool?**| Description | [optional] 
 **integerOptionalWithDefault** | **int?**| Description | [optional] [default to 2]
 **integerOptionalWithoutDefault** | **int?**| Description | [optional] 
 **numberOptionalWithDefault** | **decimal?**| Description | [optional] [default to 2]
 **numberOptionalWithoutDefault** | **decimal?**| Description | [optional] 
 **stringOptionalWithDefault** | **string**| Description | [optional] [default to def]
 **stringOptionalWithoutDefault** | **string**| Description | [optional] 
 **booleanArrayOptionalWithDefault** | **IEnumerable&lt;bool&gt;**| Description | [optional] 
 **booleanArrayOptionalWithoutDefault** | **IEnumerable&lt;bool&gt;**| Description | [optional] 
 **integerArrayOptionalWithDefault** | **IEnumerable&lt;int&gt;**| Description | [optional] 
 **integerArrayOptionalWithoutDefault** | **IEnumerable&lt;int&gt;**| Description | [optional] 
 **numberArrayOptionalWithDefault** | **IEnumerable&lt;decimal&gt;**| Description | [optional] 
 **numberArrayOptionalWithoutDefault** | **IEnumerable&lt;decimal&gt;**| Description | [optional] 
 **stringArrayOptionalWithDefault** | **IEnumerable&lt;string&gt;**| Description | [optional] 
 **stringArrayOptionalWithoutDefault** | **IEnumerable&lt;string&gt;**| Description | [optional] 
 **patternParameterArrayOptional** | **IReadOnlyDictionary&lt;string, IEnumerable&lt;string&gt;&gt;**| Description | [optional] 
 **patternParameterSingleOptional** | **IReadOnlyDictionary&lt;string, string&gt;**| Description | [optional] 
 **overriddenTypeOptional** | **int?**| Description | [optional] 

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postbinarybody"></a>
# **PostBinaryBody**
> byte[] PostBinaryBody (byte[] bodyName, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostBinaryBodyExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            byte[] bodyName = ...;   // Description
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                byte[] result = apiInstance.PostBinaryBody(bodyName, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostBinaryBody: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bodyName** | **byte[]**| Description | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**byte[]**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postbinarybodyoptional"></a>
# **PostBinaryBodyOptional**
> byte[] PostBinaryBodyOptional (byte[] bodyName = null, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostBinaryBodyOptionalExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            byte[] bodyName = ...;   // Description (optional) 
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                byte[] result = apiInstance.PostBinaryBodyOptional(bodyName, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostBinaryBodyOptional: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bodyName** | **byte[]**| Description | [optional] 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**byte[]**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postexcludedbinarybody"></a>
# **PostExcludedBinaryBody**
> string PostExcludedBinaryBody ()

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostExcludedBinaryBodyExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");

            try
            {
                // Summary
                string result = apiInstance.PostExcludedBinaryBody();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostExcludedBinaryBody: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postobjectbody"></a>
# **PostObjectBody**
> JObject PostObjectBody (JObject archive, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostObjectBodyExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            JObject archive = ...;   // An archive item (JSON).
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostObjectBody(archive, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostObjectBody: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | **JObject**| An archive item (JSON). | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postobjectbodyoptional"></a>
# **PostObjectBodyOptional**
> JObject PostObjectBodyOptional (JObject archive = null, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostObjectBodyOptionalExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            JObject archive = ...;   // An archive item (JSON). (optional) 
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostObjectBodyOptional(archive, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostObjectBodyOptional: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | **JObject**| An archive item (JSON). | [optional] 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postoverriddenbinarybody"></a>
# **PostOverriddenBinaryBody**
> string PostOverriddenBinaryBody (byte[] archive)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostOverriddenBinaryBodyExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            byte[] archive = ...;   // An archive item (XML).

            try
            {
                // Summary
                string result = apiInstance.PostOverriddenBinaryBody(archive);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostOverriddenBinaryBody: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | **byte[]**| An archive item (XML). | 

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="poststringbody"></a>
# **PostStringBody**
> string PostStringBody (string archive, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostStringBodyExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            string archive = ...;   // An archive item (XML).
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                string result = apiInstance.PostStringBody(archive, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostStringBody: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | **string**| An archive item (XML). | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="poststringbodyoptional"></a>
# **PostStringBodyOptional**
> string PostStringBodyOptional (string archive = null, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostStringBodyOptionalExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            string archive = ...;   // An archive item (XML). (optional) 
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                string result = apiInstance.PostStringBodyOptional(archive, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostStringBodyOptional: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archive** | **string**| An archive item (XML). | [optional] 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**string**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postvariantbodyziparchive"></a>
# **PostVariantBodyZipArchive**
> JObject PostVariantBodyZipArchive (byte[] template, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostVariantBodyZipArchiveExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            byte[] template = ...;   // A Zip Archive
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostVariantBodyZipArchive(template, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostVariantBodyZipArchive: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **byte[]**| A Zip Archive | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postvariantbodyxhtml"></a>
# **PostVariantBodyXHTML**
> JObject PostVariantBodyXHTML (string template, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostVariantBodyXHTMLExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            string template = ...;   // A XHTML file
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostVariantBodyXHTML(template, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostVariantBodyXHTML: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **string**| A XHTML file | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
<a name="postvariantbodyhtml"></a>
# **PostVariantBodyHTML**
> JObject PostVariantBodyHTML (string template, bool? fail = null)

Summary

Description

### Example
```csharp
using System;
using System.Diagnostics;
using CellStore.Api;
using CellStore.Client;
using CellStore.Model;

namespace Example
{
    public class PostVariantBodyHTMLExample
    {
        public void main()
        {
            var apiInstance = new ParametersApi("http://localhost:8888/prefix");
            string template = ...;   // A HTML file
            bool? fail = ...;   // Description (optional)  (default to false)

            try
            {
                // Summary
                JObject result = apiInstance.PostVariantBodyHTML(template, fail);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.PostVariantBodyHTML: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **string**| A HTML file | 
 **fail** | **bool?**| Description | [optional] [default to false]

### Return type

**JObject**

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
