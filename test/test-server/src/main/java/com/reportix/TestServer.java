package com.reportix;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoReactiveAutoConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class, MongoReactiveAutoConfiguration.class})
@RestController
public class TestServer {
	private static String RESULT_FIELD = "results";
	public static void main(String[] args) {
		SpringApplication.run(TestServer.class, args);
	}

	public org.bson.Document generateJsonDocument(int id) {
		return new org.bson.Document()
				.append("id_field", id)
				.append("string_field", "string")
				.append("int_field", 1)
				.append("bool_field", true)
				.append("float_field", 3.14)
				.append("null_field", null)
				.append("array_field", Arrays.asList("string", 1, true))
				.append("object_field", new org.bson.Document("utf8_string_field", "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。"));
	}

	private String generateJsonListResponse(int amount) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		streamJsonListResponse(amount, 0, -1, baos);
		return new String(baos.toByteArray(), Charset.forName("UTF-8"));
	}

	private void streamJsonListResponse(int amount, int sleep, int failAt, OutputStream bodyStream) throws IOException {
		JsonFactory jfactory = new JsonFactory();
		JsonGenerator jGenerator = jfactory.createGenerator(bodyStream, JsonEncoding.UTF8);

		jGenerator.writeStartObject();

		jGenerator.writeNumberField("id_field", -2);
		jGenerator.writeStringField("string_field", "string");
		jGenerator.writeNumberField("int_field", 1);
		jGenerator.writeBooleanField("bool_field", true);
		jGenerator.writeNumberField("float_field", 3.14);
		jGenerator.writeNullField("null_field");
		jGenerator.writeArrayFieldStart("array_field");
		jGenerator.writeString("string");
		jGenerator.writeNumber(1);
		jGenerator.writeBoolean(true);
		jGenerator.writeEndArray();
		jGenerator.writeObjectFieldStart("object_field");
		jGenerator.writeStringField("utf8_string_field", "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。");
		jGenerator.writeEndObject();
		if (amount == -2)
		{}
		else if (amount == -1)
			jGenerator.writeNullField(RESULT_FIELD);
		else if (amount == 0) {
			jGenerator.writeArrayFieldStart(RESULT_FIELD);
			jGenerator.writeEndArray();
		} else {
			jGenerator.writeArrayFieldStart(RESULT_FIELD);
			for	(int i = 0; i< amount; ++i)
			{
				if (failAt == i) {
					jGenerator.writeRawValue(generateError("Failing as requested. Streaming error.").toJson());
					break;
				}
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				jGenerator.writeRawValue(generateJsonDocument(i).toJson());
			}
			jGenerator.writeEndArray();
		}
		jGenerator.writeEndObject();
		jGenerator.close();
		bodyStream.close();
	}

	public String generateXMLDocument() {
		return "<?xml version = \"1.0\" encoding = \"utf-8\"?>\n" +
				"<!-- planes.xml - A document that lists ads for used airplanes -->\n" +
				"<planes_for_sale>\n" +
				"   <ad>\n" +
				"      <year>1977</year>\n" +
				"      <make>&c;</make>\n" +
				"      <model>Skyhawk</model>\n" +
				"   </ad>\n" +
				"</planes_for_sale>";
	}

	public byte[] generateBinary() throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("Test String");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream out = new ZipOutputStream(baos);
		ZipEntry e = new ZipEntry("mytext.txt");
		out.putNextEntry(e);
		byte[] data = sb.toString().getBytes();
		out.write(data, 0, data.length);
		out.closeEntry();
		out.close();
		return baos.toByteArray();
	}

	public org.bson.Document generateError(String message) {
		return new org.bson.Document()
				.append("__error__", true)
				.append("status", 500)
				.append("type", "Internal Error")
				.append("code", "CELLSTORE_EXCEPTION")
				.append("message", message)
				.append("stackTrace", Arrays.asList("com.reportix.xbrl.server.rest.gen.endpoints.ArchivesGetRequest.<init>(ArchivesGetRequest.java:138)",
						"com.reportix.xbrl.server.rest.controller.ArchivesController.query(ArchivesController.java:327)",
						"jdk.internal.reflect.GeneratedMethodAccessor37.invoke(Unknown Source)"))
				.append("system", new org.bson.Document().append("release", "0.9.6-SNAPSHOT").append("releaseDate", "2021-01-19").append("releaseRevision", "3b684965a6a851744c8790adf5618cb2c9de9299"))
				.append(RESULT_FIELD, null);
	}

	private <T> HttpEntity<T> httpEntity(T body, String contentType) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", contentType);
		return new HttpEntity<T>(body, responseHeaders);
	}

	@GetMapping("/test/json-response")
	public HttpEntity<String> getJsonResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");
		return httpEntity(generateJsonDocument(0).toJson(), "application/json;charset=UTF-8");
	}

	@PostMapping("/test/json-response")
	public HttpEntity<String> postJsonResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");
		return httpEntity(generateJsonDocument(0).toJson(), "application/json;charset=UTF-8");
	}

	@GetMapping("/test/xml-response")
	public HttpEntity<String> xmlResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(generateXMLDocument(), "application/xml;charset=UTF-8");
	}

	@GetMapping("/test/binary-response")
	public @ResponseBody HttpEntity<byte[]> binaryResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(generateBinary(), "application/x-binary");
	}

	@GetMapping(value = {"/test/list-json-response",  "/test/stream-json-response"})
	public void listJsonResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail,
								   @RequestParam(value = "format", defaultValue = "json") String format,
								   @RequestParam(value = "amount", defaultValue = "1000") int amount,
								   @RequestParam(value = "sleep", defaultValue = "0") int sleep,
								   @RequestParam(value = "fail-at", defaultValue = "-1") int failAt,
								   HttpServletResponse servletResponse
	) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		final OutputStream bodyStream = servletResponse.getOutputStream();
		servletResponse.addHeader("Content-Type", "application/json");

		streamJsonListResponse(amount, sleep, failAt, bodyStream);
	}



	@GetMapping("/test/arbitrary-response")
	public void arbitraryResponse(@RequestParam(value = "fail", defaultValue = "false") boolean fail,
								  @RequestParam(value = "format", defaultValue = "json") String format,
								  @RequestParam(value = "amount", defaultValue = "1000") int amount,
								  @RequestParam(value = "sleep", defaultValue = "0") int sleep,
								  @RequestParam(value = "fail-at", defaultValue = "-1") int failAt,
								  HttpServletResponse servletResponse) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		final OutputStream bodyStream = servletResponse.getOutputStream();
		if (format.equals("json")) {
			servletResponse.addHeader("Content-Type", "application/json;charset=UTF-8");

			String json = generateJsonDocument(0).toJson();
			byte[] buffer = json.getBytes(StandardCharsets.UTF_8);
			bodyStream.write(buffer, 0, buffer.length);
			bodyStream.close();
		} else if (format.equals("json-list")) {
			String jsonList = generateJsonListResponse(amount);
			
			byte[] buffer = jsonList.getBytes(StandardCharsets.UTF_8);
			bodyStream.write(buffer, 0, buffer.length);
			bodyStream.close();
		}
		else if (format.equals("json-stream")) {
			servletResponse.addHeader("Content-Type", "application/json;charset=UTF-8");

			streamJsonListResponse(amount, sleep, failAt, bodyStream);
		}
		else if (format.equals("xml")) {
			servletResponse.addHeader("Content-Type", "application/xml");

			String xml = generateXMLDocument();
			byte[] buffer = xml.getBytes(StandardCharsets.UTF_8);
			bodyStream.write(buffer, 0, buffer.length);
			bodyStream.close();
		} else if (format.equals("binary")) {
			servletResponse.addHeader("Content-Type", "application/x-binary");

			byte [] buffer = generateBinary();
			bodyStream.write(buffer, 0, buffer.length);
			bodyStream.close();
		} else {
			throw new Exception("Unsupported format");
		}
	}

	@PostMapping("/test/binary-body")
	public HttpEntity<byte[]> binaryBody(
			@RequestBody byte[] body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail,
			HttpServletResponse servletResponse) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "application/x-binary");
	}

	@PostMapping("/test/binary-body-optional")
	public HttpEntity<byte[]> binaryBodyOptional(
			@RequestBody(required = false) byte[] body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "application/x-binary");
	}

	@PostMapping("/test/object-body")
	public HttpEntity<JsonNode> objectBody(
			@RequestBody JsonNode body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "application/json;charset=UTF-8");
	}

	@PostMapping("/test/object-body-optional")
	public HttpEntity<JsonNode> objectBodyOptional(
			@RequestBody(required = false) JsonNode body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "application/json;charset=UTF-8");
	}

	@PostMapping("/test/string-body")
	public HttpEntity<String> stringBody(
			@RequestBody String body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "text/plain;charset=UTF-8");
	}

	@PostMapping("/test/string-body-optional")
	public HttpEntity<String> stringBodyOptional(
			@RequestBody(required = false) String body,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail) throws Exception {
		if (fail)
			throw new Exception("Failing as requested");

		return httpEntity(body, "text/plain;charset=UTF-8");
	}

	@GetMapping("/test/query-parameters")
	public HttpEntity<String> queryParameters(@RequestParam MultiValueMap<String, String> params,
			@RequestParam(value = "fail", defaultValue = "false") boolean fail)
	{
		if (fail)
			throw new RuntimeException("Failing as requested");

		org.bson.Document ret = new org.bson.Document();
		for (String key: params.keySet())
		{
			ret.append(key, params.get(key));
		}
		return httpEntity(ret.toJson(), "application/json;charset=UTF-8");
	}

	@GetMapping(value = { "/test/path-parameters/{id1}/path/path", "/test/path-parameters/{id1}/path/{id2}/path", "/test/path-parameters/{id1}/path/{id2}/{id3}/path", "/test/path-parameters/{id1}/path/{id2}/{id3}/path/{id4}" })
	public HttpEntity<String> pathParameters(@PathVariable Map<String, String> params,
																					 @RequestParam(value = "fail", defaultValue = "false") boolean fail)
	{
		if (fail)
			throw new RuntimeException("Failing as requested");

		org.bson.Document ret = new org.bson.Document();
		for (String key: params.keySet())
		{
			ret.append(key, params.get(key));
		}
		return httpEntity(ret.toJson(), "application/json;charset=UTF-8");
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleException(Exception ex)
	{
		return generateError(ex.getMessage()).toJson();
	}
}
