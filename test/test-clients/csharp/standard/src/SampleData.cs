using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using CellStore.Client;
using CellStore.Model;
using Newtonsoft.Json.Linq;

namespace test_client
{
  public static class SampleData
  {
    public static void VerifyValidBinary(byte[] binaryResponse)
    {
      using (var zippedStream = new MemoryStream(binaryResponse))
      {
        using (var archive = new ZipArchive(zippedStream))
        {
          var entry = archive.Entries.FirstOrDefault();

          if (entry != null)
            using (var unzippedEntryStream = entry.Open())
            {
              using (var ms = new MemoryStream())
              {
                unzippedEntryStream.CopyTo(ms);
                var unzippedArray = ms.ToArray();

                var str = Encoding.Default.GetString(unzippedArray);
                if (str != "Test String")
                  throw new IOException("Cannot verify binary (1)");
              }
            }
          else
            throw new IOException("Cannot verify binary (2)");
        }
      }
    }

    public static JObject SampleJsonResponse(int id)
    {
      return new JObject(
        new JProperty("id_field", id),
        new JProperty("string_field", "string"),
        new JProperty("int_field", 1),
        new JProperty("bool_field", true),
        new JProperty("float_field", 3.14),
        new JProperty("null_field", null),
        new JProperty("array_field", new JArray("string", 1, true)),
        new JProperty("object_field",
          new JObject(new JProperty("utf8_string_field",
            "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。"))));
    }

    public static string SampleXMLResponse()
    {
      return "<?xml version = \"1.0\" encoding = \"utf-8\"?>\n" +
             "<!-- planes.xml - A document that lists ads for used airplanes -->\n" +
             "<planes_for_sale>\n" +
             "   <ad>\n" +
             "      <year>1977</year>\n" +
             "      <make>&c;</make>\n" +
             "      <model>Skyhawk</model>\n" +
             "   </ad>\n" +
             "</planes_for_sale>";
    }

    public static byte[] SampleBinaryBody()
    {
      var ret = new byte[1024];
      var rnd = new Random();
      rnd.NextBytes(ret);
      return ret;
    }

    public static string SampleString()
    {
      return "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。";
    }

    public static void VerifyJsonResponse(JObject jsonResponse)
    {
      var expected = SampleJsonResponse(0);
      if (!jsonResponse.ToString().Equals(expected.ToString()))
        throw new IOException("Unexpected Json");
    }

    public static void VerifyXMLResponse(string xmlResponse)
    {
      var expected = SampleXMLResponse();
      if (!xmlResponse.Equals(expected))
        throw new IOException("Unexpected Xml");
    }

    public static void VerifyException(AggregateException aggregateException, string endpoint)
    {
      if (aggregateException.InnerException is ApiException apiException)
        VerifyException(apiException, endpoint);
    }

    public static void VerifyException(ApiException apiException, string endpoint)
    {
      if (!apiException.Message.Contains("Failing as requested"))
        throw new IOException("Expecting request to fail due to explicit request");

      if (apiException.ErrorCode != 500)
        throw new IOException("Expecting error to contain http response code");
      var message = apiException.ToString();
      if (!message.Contains("CELLSTORE_EXCEPTION"))
        throw new IOException("Expecting error to contain raw response");
      if (!message.Contains("com.reportix.xbrl.server.rest.gen.endpoints"))
        throw new IOException("Expecting error to contain stack trace");
      if (!message.Contains("1.0.0-TESTS"))
        throw new IOException("Expecting error to contain library information");
      if (!message.Contains("System has"))
        throw new IOException("Expecting error to contain system memory information");
      if (!message.Contains("Application is using"))
        throw new IOException("Expecting error to contain application memory information");
      if ((endpoint != null) & !message.Contains(endpoint))
        throw new IOException("Expecting error to contain call information");
      if (!message.Contains("RequestTime"))
        throw new IOException("Expecting error to contain call request time");
      if (!message.Contains("ResponseTime"))
        throw new IOException("Expecting error to contain call response time");
      if (!message.Contains("RetryCount"))
        throw new IOException("Expecting error to contain configuration");
      if (!message.Contains("AuthenticationProvider"))
        throw new IOException("Expecting error to contain configuration");
    }

    public static void VerifyJsonResponse(JObject actualResponse, JObject expectedResponse)
    {
      if (actualResponse == null && expectedResponse == null)
        return;
      if (!actualResponse.ToString().Equals(expectedResponse.ToString()))
        throw new IOException("Expected != Actual");
    }

    public static void VerifyBinaryResponse(byte[] actualResponse, byte[] expectedResponse)
    {
      if (!actualResponse.SequenceEqual(expectedResponse))
        throw new IOException("Expected != Actual");
    }

    public static void VerifyStringResponse(string actualResponse, string expectedResponse)
    {
      if (!actualResponse.SequenceEqual(expectedResponse))
        throw new IOException("Expected != Actual");
    }

    public static void VerifyJsonListResponse(ISequence<JObject> jsonListResponse, int amount)
    {
      if (Math.Max(amount, 0) != jsonListResponse.Count)
        throw new IOException("Unexpected count");

      VerifyJsonList(jsonListResponse.ToList(), jsonListResponse.Metadata);
    }

    public static void VerifyJsonStreamResponse(IStream<JObject> jsonStreamResponse, int amount)
    {
      var objectList = jsonStreamResponse.ToList();
      if (Math.Max(amount, 0) != objectList.Count)
        throw new IOException("Unexpected count");

      VerifyJsonList(objectList.ToList(), jsonStreamResponse.Metadata);
    }

    public static void VerifyFailedJsonStreamResponse(IStream<JObject> jsonStreamResponse, int minimum, int maximum,
      string endpoint)
    {
      VerifyJsonResponse(jsonStreamResponse.Metadata, SampleJsonResponse(-2));
      var en = jsonStreamResponse.GetEnumerator();
      for (var i = 0; i < minimum; ++i)
      {
        if (!en.MoveNext())
          throw new Exception("Cannot get item " + i);
        VerifyJsonResponse(en.Current, SampleJsonResponse(i));
      }

      for (var i = minimum; i <= maximum; ++i)
        try
        {
          if (!en.MoveNext())
            throw new Exception("Cannot get item " + i);
          VerifyJsonResponse(en.Current, SampleJsonResponse(i));
        }
        catch (ApiException e)
        {
          VerifyException(e, endpoint);
          return;
        }
    }

    private static void VerifyJsonList(List<JObject> toList, JObject metadata)
    {
      VerifyJsonResponse(metadata, SampleJsonResponse(-2));
      for (var i = 0; i < toList.Count; ++i) VerifyJsonResponse(toList[i], SampleJsonResponse(i));
    }
  }
}