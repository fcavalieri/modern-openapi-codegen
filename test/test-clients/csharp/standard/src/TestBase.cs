using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CellStore.Api;
using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using NUnit.Framework;

namespace test_client.Tests
{
  public class TestBase
  {
    protected ParametersApi parametersApi;
    protected ResponsesApi responsesApi;
    protected TestcontainersContainer testContainer;
    protected string testEndpoint;

    [OneTimeSetUp]
    public void SetUp()
    {
      SetupContainer().Wait();

      testEndpoint = "http://" + testContainer.IpAddress + ":8888/test";
      responsesApi = new ResponsesApi(testEndpoint);
      parametersApi = new ParametersApi(testEndpoint);
    }

    [OneTimeTearDown]
    public void TearDown()
    {
      testContainer.StopAsync().Wait();
    }

    private async Task SetupContainer()
    {
      var parent = FindBaseFolder(Directory.GetCurrentDirectory());
      var jarPath = parent + "/test/test-server/target/TestServer.jar";

      var testcontainersBuilder = new TestcontainersBuilder<TestcontainersContainer>()
        .WithImage("openjdk:15")
        .WithPortBinding(8888, true)
        .WithBindMount(jarPath, "/TestServer.jar")
        .WithCommand("java", "-jar", "/TestServer.jar")
        .WithWaitStrategy(Wait.ForUnixContainer().UntilPortIsAvailable(8888));

      testContainer = testcontainersBuilder.Build();
      await testContainer.StartAsync();
    }

    private string FindBaseFolder(string currentDirectory)
    {
      if (Directory.GetDirectories(currentDirectory).Select(d => Path.GetFileName(d)).Contains(".git"))
        return currentDirectory;
      return FindBaseFolder(Directory.GetParent(currentDirectory).FullName);
    }
  }
}