using System;
using System.IO;
using CellStore.Api;
using CellStore.Client;
using NUnit.Framework;

namespace test_client.Tests
{
  [TestFixture]
  public class Operations : TestBase
  {
    /// <summary>
    ///   Basic functionality (Run, Error response (before streaming started))
    /// </summary>
    /// <exception cref="IOException"></exception>
    [Test]
    public void TestDefinedBinaryResponse()
    {
      var binaryResponse = responsesApi.GetBinaryResponse();
      SampleData.VerifyValidBinary(binaryResponse);
      var binaryResponseAsync = responsesApi.GetBinaryResponseAsync().Result;
      SampleData.VerifyValidBinary(binaryResponseAsync);
      try
      {
        var _ = responsesApi.GetBinaryResponse(true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetBinaryResponseAsync(true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestDefinedObjectResponse()
    {
      var jsonResponse = responsesApi.GetJsonResponse();
      SampleData.VerifyJsonResponse(jsonResponse);
      var jsonResponseAsync = responsesApi.GetJsonResponseAsync().Result;
      SampleData.VerifyJsonResponse(jsonResponseAsync);
      try
      {
        var _ = responsesApi.GetJsonResponse(true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetJsonResponseAsync(true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestDefinedXMLResponse()
    {
      var xmlResponse = responsesApi.GetXMLResponse();
      SampleData.VerifyXMLResponse(xmlResponse);
      var xmlResponseAsync = responsesApi.GetXMLResponseAsync().Result;
      SampleData.VerifyXMLResponse(xmlResponseAsync);
      try
      {
        var _ = responsesApi.GetXMLResponse(true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetXMLResponseAsync(true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestDefinedObjectListResponse()
    {
      var jsonListResponseMinusTwo = responsesApi.ListJsonResponse(amount: -2);
      SampleData.VerifyJsonListResponse(jsonListResponseMinusTwo, -2);
      var jsonListResponseMinusOne = responsesApi.ListJsonResponse(amount: -1);
      SampleData.VerifyJsonListResponse(jsonListResponseMinusOne, -1);
      var jsonListResponseZero = responsesApi.ListJsonResponse(amount: 0);
      SampleData.VerifyJsonListResponse(jsonListResponseZero, 0);
      
      
      var jsonListResponse = responsesApi.ListJsonResponse(amount: 100);
      SampleData.VerifyJsonListResponse(jsonListResponse, 100);
      var jsonListResponseAsync = responsesApi.ListJsonResponseAsync(amount: 100).Result;
      SampleData.VerifyJsonListResponse(jsonListResponseAsync, 100);

      try
      {
        var _ = responsesApi.ListJsonResponse(true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.ListJsonResponseAsync(true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestDefinedObjectStreamResponse()
    {
      var jsonStreamResponseMinusTwo = responsesApi.StreamJsonResponse(amount: -2);
      SampleData.VerifyJsonStreamResponse(jsonStreamResponseMinusTwo, -2);
      var jsonStreamResponseMinusOne = responsesApi.StreamJsonResponse(amount: -1);
      SampleData.VerifyJsonStreamResponse(jsonStreamResponseMinusOne, -1);
      var jsonStreamResponseZero = responsesApi.StreamJsonResponse(amount: 0);
      SampleData.VerifyJsonStreamResponse(jsonStreamResponseZero, 0);
    
      var jsonStreamResponse = responsesApi.StreamJsonResponse(amount: 100);
      SampleData.VerifyJsonStreamResponse(jsonStreamResponse, 100);
      var jsonStreamResponseAsync = responsesApi.StreamJsonResponseAsync(amount: 100).Result;
      SampleData.VerifyJsonStreamResponse(jsonStreamResponseAsync, 100);
      try
      {
        var _ = responsesApi.StreamJsonResponse(true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      var partialStream = responsesApi.StreamJsonResponse(failAt: 5000, amount: 10000, sleep: 1);
      SampleData.VerifyFailedJsonStreamResponse(partialStream, 3000, 5000, testEndpoint);

      try
      {
        var _ = responsesApi.StreamJsonResponseAsync(true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      var partialStreamAsync = responsesApi.StreamJsonResponseAsync(failAt: 5000, amount: 10000, sleep: 1).Result;
      SampleData.VerifyFailedJsonStreamResponse(partialStreamAsync, 3000, 5000, testEndpoint);
    }

    /// <summary>
    ///   Basic functionality (Run, Error response (before streaming started)) for methods that have multiple return values.
    /// </summary>
    /// <exception cref="IOException"></exception>
    [Test]
    public void TestArbitraryBinaryResponse()
    {
      var binaryResponse = responsesApi.GetArbitraryResponseAsBinary("", "");
      SampleData.VerifyValidBinary(binaryResponse);
      var binaryResponseAsync =
        responsesApi.GetArbitraryResponseAsBinaryAsync("", "").Result;
      SampleData.VerifyValidBinary(binaryResponseAsync);
      try
      {
        responsesApi.GetArbitraryResponseAsBinary("", "", true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetArbitraryResponseAsBinaryAsync("", "", true)
          .Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestArbitraryJsonResponse()
    {
      var jsonResponse = responsesApi.GetArbitraryResponseAsJson("", "");
      SampleData.VerifyJsonResponse(jsonResponse);
      var jsonResponseAsync =
        responsesApi.GetArbitraryResponseAsJsonAsync("", "").Result;
      SampleData.VerifyJsonResponse(jsonResponseAsync);
      try
      {
        var _ = responsesApi.GetArbitraryResponseAsJson("", "", true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetArbitraryResponseAsJsonAsync("", "", true)
          .Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestArbitraryXMLResponse()
    {
      var xml = responsesApi.GetArbitraryResponseAsXML("", "");
      SampleData.VerifyXMLResponse(xml);
      var xmlAsync = responsesApi.GetArbitraryResponseAsXMLAsync("", "").Result;
      SampleData.VerifyXMLResponse(xmlAsync);
      try
      {
        responsesApi.GetArbitraryResponseAsXML("", "", true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi.GetArbitraryResponseAsXMLAsync("", "", true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestArbitraryObjectListResponse()
    {
      var jsonListResponse =
        responsesApi.GetArbitraryResponseAsList("", "", amount: 100);
      SampleData.VerifyJsonListResponse(jsonListResponse, 100);
      var jsonListResponseAsync = responsesApi
        .GetArbitraryResponseAsListAsync("", "", amount: 100).Result;
      SampleData.VerifyJsonListResponse(jsonListResponseAsync, 100);
      try
      {
        var _ =
          responsesApi.GetArbitraryResponseAsList("", "", amount: 100, fail: true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      try
      {
        var _ = responsesApi
          .GetArbitraryResponseAsListAsync("", "", amount: 100, fail: true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }
    }

    [Test]
    public void TestArbitraryObjectStreamResponse()
    {
      var jsonStreamResponse = responsesApi.GetArbitraryResponseAsStream("", "", amount: 100);
      SampleData.VerifyJsonStreamResponse(jsonStreamResponse, 100);
      var jsonStreamResponseAsync = responsesApi.GetArbitraryResponseAsStreamAsync("", "", amount: 100).Result;
      SampleData.VerifyJsonStreamResponse(jsonStreamResponseAsync, 100);
      try
      {
        var _ = responsesApi.GetArbitraryResponseAsStream("", "", true);
        throw new IOException("Expecting request to fail");
      }
      catch (ApiException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      var partialStream = responsesApi.GetArbitraryResponseAsStream("", "", failAt: 5000, amount: 10000, sleep: 1);
      SampleData.VerifyFailedJsonStreamResponse(partialStream, 3000, 5000, testEndpoint);

      try
      {
        var _ = responsesApi.GetArbitraryResponseAsStreamAsync("", "", true).Result;
        throw new IOException("Expecting request to fail");
      }
      catch (AggregateException e)
      {
        SampleData.VerifyException(e, testEndpoint);
      }

      var partialStreamAsync = responsesApi
        .GetArbitraryResponseAsStreamAsync("", "", failAt: 5000, amount: 10000, sleep: 1).Result;
      SampleData.VerifyFailedJsonStreamResponse(partialStreamAsync, 3000, 5000, testEndpoint);
      
      var watch = new System.Diagnostics.Stopwatch();
      watch.Start();
      var streamedResponse = responsesApi.GetArbitraryResponseAsStream("", "", amount: 10000, sleep: 1);
      var _meta = streamedResponse.Metadata;
      var _item = streamedResponse.GetEnumerator().MoveNext();
      watch.Stop();
      if (watch.ElapsedMilliseconds > 1000)
        throw new IOException("The time to get the first item and the metadata suggest no streaming occurred.");
    }

    [Test]
    public void TestAutomaticRetries()
    {
      var configuration = new UnitTestConfiguration();
      var api = new ResponsesApi(testEndpoint, configuration);

      //No retries. The request fails at first try
      configuration.RetryCount = 0;
      configuration.NextRequestHasReceiveError = true;
      try
      {
        api.GetJsonResponse();
        throw new Exception("Expecting request to fail");
      }
      catch (ApiException e)
      {
        if (e.ErrorCode != 417)
          throw new Exception("Expecting request to fail with status 417");
      }

      //No retries. The request fails at first try
      configuration.RetryCount = 0;
      configuration.NextRequestHasRetryableError = true;
      try
      {
        api.GetJsonResponse();
        throw new Exception("Expecting request to fail");
      }
      catch (ApiException e)
      {
        if (e.ErrorCode != 417)
          throw new Exception("Expecting request to fail with status 417");
      }

      //One retry. The request fails at first try. Retry succeeds.
      configuration.RetryCount = 1;
      configuration.NextRequestHasReceiveError = true;
      api.GetJsonResponse();

      //One retry. The request fails at first try. Retry succeeds.
      configuration.RetryCount = 1;
      configuration.NextRequestHasRetryableError = true;
      api.GetJsonResponse();

      //One retry. The request fails at first try. Retry is not performed because request is not idempotent.
      configuration.RetryCount = 1;
      configuration.NextRequestHasReceiveError = true;
      try
      {
        api.PostJsonResponse();
        throw new Exception("Expecting request to fail");
      }
      catch (ApiException e)
      {
        if (e.ErrorCode != 417)
          throw new Exception("Expecting request to fail with status 417");
      }

      //One retry. The request fails at first try. Retry succeeds.
      configuration.RetryCount = 1;
      configuration.NextRequestHasRetryableError = true;
      api.PostJsonResponse();

      //One retry. The request fails at first and second try.
      configuration.RetryCount = 1;
      configuration.AllRequestsHaveRetryableErrors = true;
      try
      {
        api.GetJsonResponse();
        throw new Exception("Expecting request to fail");
      }
      catch (ApiException e)
      {
        if (e.ErrorCode != 417)
          throw new Exception("Expecting request to fail with status 417");
      }
    }
  }
}