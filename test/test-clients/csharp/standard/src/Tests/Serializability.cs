using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using CellStore.Client;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace test_client.Tests
{
  [TestFixture]
  public class Serializability : TestBase
  {
    [Test]
    public void TestSerializability()
    {
      Configuration configuration = new Configuration();
      configuration.TimeoutMs = 123;
      configuration.VerboseExceptions = true;
      configuration.Pipelining = false;
      XmlSerializer xmlSerializer = new XmlSerializer(typeof(Configuration));
      StringWriter stringWriter = new StringWriter();
      xmlSerializer.Serialize(stringWriter, configuration);
      String serialized = stringWriter.ToString();
      StringReader stringReader = new StringReader(serialized);
      Configuration configuration2 = (Configuration)xmlSerializer.Deserialize(stringReader);
      Assert.AreEqual(configuration, configuration2);
    }
  }
}