using System;
using System.Collections.Generic;
using System.Linq;
using CellStore.Client;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace test_client.Tests
{
  [TestFixture]
  public class Parameters : TestBase
  {
    /// <summary>
    ///   Test that any kind of parameter is serialized, sent, received and echoed back correctly
    /// </summary>
    [Test]
    public void TestParametersMin()
    {
      //Least amount of parameters
      var parametersResponseMin = parametersApi.GetQueryParameters(
        //bool booleanMandatoryWithoutDefault
        true,
        //int integerMandatoryWithoutDefault
        1,
        //decimal numberMandatoryWithoutDefault
        1234567890.123456789m,
        //string stringMandatoryWithoutDefault
        "str",
        //IEnumerable<bool> booleanArrayMandatoryWithoutDefault
        new[] {true, false},
        //IEnumerable<int> integerArrayMandatoryWithoutDefault
        new List<int> {1, 2},
        //IEnumerable<decimal> numberArrayMandatoryWithoutDefault
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayMandatoryWithoutDefault
        new List<string> {"a", "b"}.AsEnumerable(),
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory
        new Dictionary<string, IEnumerable<string>>
          {{"a:b", new[] {"value_a_b", "value_a_b_2"}}, {"c:d", new[] {"value_c_d", "value_c_d_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleMandatory
        new Dictionary<string, string> {{"e:f", "value_e_f"}, {"g:h", "value_g_h"}},
        //int overriddenTypeMandatory
        1);

      var parametersResponseMinExpected =
        new JObject(
          new JProperty("boolean-hardcoded", new JArray("true")),
          new JProperty("integer-hardcoded", new JArray("3")),
          new JProperty("number-hardcoded", new JArray("987654321.987654321")),
          new JProperty("string-hardcoded", new JArray("value")),
          new JProperty("boolean-mandatory-without-default", new JArray("true")),
          new JProperty("integer-mandatory-without-default", new JArray("1")),
          new JProperty("number-mandatory-without-default", new JArray("1234567890.123456789")),
          new JProperty("string-mandatory-without-default", new JArray("str")),
          new JProperty("boolean-array-mandatory-without-default", new JArray("true", "false")),
          new JProperty("integer-array-mandatory-without-default", new JArray("1", "2")),
          new JProperty("number-array-mandatory-without-default", new JArray("1234567890.123456789", "12345678901234567890.123456789")),
          new JProperty("string-array-mandatory-without-default", new JArray("a", "b")),
          new JProperty("overridden-type-mandatory", new JArray("1")),
          new JProperty("a:b", new JArray("value_a_b", "value_a_b_2")),
          new JProperty("c:d", new JArray("value_c_d", "value_c_d_2")),
          new JProperty("e:f", new JArray("value_e_f")),
          new JProperty("g:h", new JArray("value_g_h"))
        );

      SampleData.VerifyJsonResponse(parametersResponseMin, parametersResponseMinExpected);

      var parametersResponseMinAsync = parametersApi.GetQueryParametersAsync(
        //bool booleanMandatoryWithoutDefault
        true,
        //int integerMandatoryWithoutDefault
        1,
        //decimal numberMandatoryWithoutDefault
        1234567890.123456789m,
        //string stringMandatoryWithoutDefault
        "str",
        //IEnumerable<bool> booleanArrayMandatoryWithoutDefault
        new[] {true, false},
        //IEnumerable<int> integerArrayMandatoryWithoutDefault
        new List<int> {1, 2},
        //IEnumerable<decimal> numberArrayMandatoryWithoutDefault
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayMandatoryWithoutDefault
        new List<string> {"a", "b"}.AsEnumerable(),
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory
        new Dictionary<string, IEnumerable<string>>
          {{"a:b", new[] {"value_a_b", "value_a_b_2"}}, {"c:d", new[] {"value_c_d", "value_c_d_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleMandatory
        new Dictionary<string, string> {{"e:f", "value_e_f"}, {"g:h", "value_g_h"}},
        //int overriddenTypeMandatory
        1).Result;


      SampleData.VerifyJsonResponse(parametersResponseMinAsync, parametersResponseMinExpected);
    }

    [Test]
    public void TestParametersMax()
    {
      //Maximum amount of parameters
      var parametersResponseMax = parametersApi.GetQueryParameters(
        //bool booleanMandatoryWithoutDefault
        true,
        //int integerMandatoryWithoutDefault
        1,
        //decimal numberMandatoryWithoutDefault
        1234567890.123456789m,
        //string stringMandatoryWithoutDefault
        "str",
        //IEnumerable<bool> booleanArrayMandatoryWithoutDefault
        new[] {true, false},
        //IEnumerable<int> integerArrayMandatoryWithoutDefault
        new List<int> {1, 2},
        //IEnumerable<decimal> numberArrayMandatoryWithoutDefault
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayMandatoryWithoutDefault
        new List<string> {"a", "b"}.AsEnumerable(),
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory
        new Dictionary<string, IEnumerable<string>>
          {{"a:b", new[] {"value_a_b", "value_a_b_2"}}, {"c:d", new[] {"value_c_d", "value_c_d_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleMandatory
        new Dictionary<string, string> {{"e:f", "value_e_f"}, {"g:h", "value_g_h"}},
        //int overriddenTypeMandatory
        1,
        //bool? fail = null
        false,
        //bool? booleanOptionalWithDefault = null
        true,
        //bool? booleanOptionalWithoutDefault = null
        true,
        //int? integerOptionalWithDefault = null
        1,
        //int? integerOptionalWithoutDefault = null
        1,
        //decimal? numberOptionalWithDefault = null
        1234567890m,
        //decimal? numberOptionalWithoutDefault = null
        12345678901234567890m,
        //string stringOptionalWithDefault = null
        "str",
        //string stringOptionalWithoutDefault = null
        "str",
        //IEnumerable<bool> booleanArrayOptionalWithDefault = null
        new[] {true, false},
        //IEnumerable<bool> booleanArrayOptionalWithoutDefault = null
        new[] {true, false},
        //IEnumerable<int> integerArrayOptionalWithDefault = null
        new[] {1, 2},
        //IEnumerable<int> integerArrayOptionalWithoutDefault = null
        new[] {1, 2},
        //IEnumerable<decimal> numberArrayOptionalWithDefault = null
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<decimal> numberArrayOptionalWithoutDefault = null
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayOptionalWithDefault = null
        new[] {"a", "b"},
        //IEnumerable<string> stringArrayOptionalWithoutDefault = null
        new[] {"a", "b"},
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null
        new Dictionary<string, IEnumerable<string>>
          {{"i:j", new[] {"value_i_j", "value_i_j_2"}}, {"k:l", new[] {"value_k_l", "value_k_l_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleOptional = null
        new Dictionary<string, string> {{"m:n", "value_m_n"}, {"o:p", "value_o_p"}},
        //int? overriddenTypeOptional = null
        1);

      var parametersResponseMaxExpected =
        new JObject(
          new JProperty("boolean-hardcoded", new JArray("true")),
          new JProperty("integer-hardcoded", new JArray("3")),
          new JProperty("number-hardcoded", new JArray("987654321.987654321")),
          new JProperty("string-hardcoded", new JArray("value")),
          new JProperty("fail", new JArray("false")),
          new JProperty("boolean-mandatory-without-default", new JArray("true")),
          new JProperty("boolean-optional-with-default", new JArray("true")),
          new JProperty("boolean-optional-without-default", new JArray("true")),
          new JProperty("integer-mandatory-without-default", new JArray("1")),
          new JProperty("integer-optional-with-default", new JArray("1")),
          new JProperty("integer-optional-without-default", new JArray("1")),
          new JProperty("number-mandatory-without-default", new JArray("1234567890.123456789")),
          new JProperty("number-optional-with-default", new JArray("1234567890")),
          new JProperty("number-optional-without-default", new JArray("12345678901234567890")),
          new JProperty("string-mandatory-without-default", new JArray("str")),
          new JProperty("string-optional-with-default", new JArray("str")),
          new JProperty("string-optional-without-default", new JArray("str")),
          new JProperty("boolean-array-mandatory-without-default", new JArray("true", "false")),
          new JProperty("boolean-array-optional-with-default", new JArray("true", "false")),
          new JProperty("boolean-array-optional-without-default", new JArray("true", "false")),
          new JProperty("integer-array-mandatory-without-default", new JArray("1", "2")),
          new JProperty("integer-array-optional-with-default", new JArray("1", "2")),
          new JProperty("integer-array-optional-without-default", new JArray("1", "2")),
          new JProperty("number-array-mandatory-without-default", new JArray("1234567890.123456789", "12345678901234567890.123456789")),
          new JProperty("number-array-optional-with-default", new JArray("1234567890.123456789", "12345678901234567890.123456789")),
          new JProperty("number-array-optional-without-default", new JArray("1234567890.123456789", "12345678901234567890.123456789")),
          new JProperty("string-array-mandatory-without-default", new JArray("a", "b")),
          new JProperty("string-array-optional-with-default", new JArray("a", "b")),
          new JProperty("string-array-optional-without-default", new JArray("a", "b")),
          new JProperty("overridden-type-optional", new JArray("1")),
          new JProperty("overridden-type-mandatory", new JArray("1")),
          new JProperty("i:j", new JArray("value_i_j", "value_i_j_2")),
          new JProperty("k:l", new JArray("value_k_l", "value_k_l_2")),
          new JProperty("m:n", new JArray("value_m_n")),
          new JProperty("o:p", new JArray("value_o_p")),
          new JProperty("a:b", new JArray("value_a_b", "value_a_b_2")),
          new JProperty("c:d", new JArray("value_c_d", "value_c_d_2")),
          new JProperty("e:f", new JArray("value_e_f")),
          new JProperty("g:h", new JArray("value_g_h"))
        );

      SampleData.VerifyJsonResponse(parametersResponseMax, parametersResponseMaxExpected);

      //Maximum amount of parameters
      var parametersResponseMaxAsync = parametersApi.GetQueryParametersAsync(
        //bool booleanMandatoryWithoutDefault
        true,
        //int integerMandatoryWithoutDefault
        1,
        //decimal numberMandatoryWithoutDefault
        1234567890.123456789m,
        //string stringMandatoryWithoutDefault
        "str",
        //IEnumerable<bool> booleanArrayMandatoryWithoutDefault
        new[] {true, false},
        //IEnumerable<int> integerArrayMandatoryWithoutDefault
        new List<int> {1, 2},
        //IEnumerable<decimal> numberArrayMandatoryWithoutDefault
        new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayMandatoryWithoutDefault
        new List<string> {"a", "b"}.AsEnumerable(),
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory
        new Dictionary<string, IEnumerable<string>>
          {{"a:b", new[] {"value_a_b", "value_a_b_2"}}, {"c:d", new[] {"value_c_d", "value_c_d_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleMandatory
        new Dictionary<string, string> {{"e:f", "value_e_f"}, {"g:h", "value_g_h"}},
        //int overriddenTypeMandatory
        1,
        //bool? fail = null
        false,
        //bool? booleanOptionalWithDefault = null
        true,
        //bool? booleanOptionalWithoutDefault = null
        true,
        //int? integerOptionalWithDefault = null
        1,
        //int? integerOptionalWithoutDefault = null
        1,
        //decimal? numberOptionalWithDefault = null
        1234567890m,
        //decimal? numberOptionalWithoutDefault = null
        12345678901234567890m,
        //string stringOptionalWithDefault = null
        "str",
        //string stringOptionalWithoutDefault = null
        "str",
        //IEnumerable<bool> booleanArrayOptionalWithDefault = null
        new[] {true, false},
        //IEnumerable<bool> booleanArrayOptionalWithoutDefault = null
        new[] {true, false},
        //IEnumerable<int> integerArrayOptionalWithDefault = null
        new[] {1, 2},
        //IEnumerable<int> integerArrayOptionalWithoutDefault = null
        new[] {1, 2},
        //IEnumerable<decimal> numberArrayOptionalWithDefault = null
        new[] {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<decimal> numberArrayOptionalWithoutDefault = null
        new[] {1234567890.123456789m, 12345678901234567890.123456789m},
        //IEnumerable<string> stringArrayOptionalWithDefault = null
        new[] {"a", "b"},
        //IEnumerable<string> stringArrayOptionalWithoutDefault = null
        new[] {"a", "b"},
        //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayOptional = null
        new Dictionary<string, IEnumerable<string>>
          {{"i:j", new[] {"value_i_j", "value_i_j_2"}}, {"k:l", new[] {"value_k_l", "value_k_l_2"}}},
        //IReadOnlyDictionary<string, string> patternParameterSingleOptional = null
        new Dictionary<string, string> {{"m:n", "value_m_n"}, {"o:p", "value_o_p"}},
        //int? overriddenTypeOptional = null
        1).Result;

      SampleData.VerifyJsonResponse(parametersResponseMaxAsync, parametersResponseMaxExpected);
    }

    [Test]
    public void TestPatternParameters()
    {
      //Invalid
      try
      {
        parametersApi.GetQueryParameters(
          //bool booleanMandatoryWithoutDefault
          true,
          //int integerMandatoryWithoutDefault
          1,
          //decimal numberMandatoryWithoutDefault
          1234567890.123456789m,
          //string stringMandatoryWithoutDefault
          "str",
          //IEnumerable<bool> booleanArrayMandatoryWithoutDefault
          new[] {true, false},
          //IEnumerable<int> integerArrayMandatoryWithoutDefault
          new List<int> {1, 2},
          //IEnumerable<decimal> numberArrayMandatoryWithoutDefault
          new List<decimal> {1234567890.123456789m, 12345678901234567890.123456789m},
          //IEnumerable<string> stringArrayMandatoryWithoutDefault
          new List<string> {"a", "b"}.AsEnumerable(),
          //IReadOnlyDictionary<string, IEnumerable<string>> patternParameterArrayMandatory
          new Dictionary<string, IEnumerable<string>>
            {{"ab", new[] {"value_a_b", "value_a_b_2"}}, {"c:d", new[] {"value_c_d", "value_c_d_2"}}},
          //IReadOnlyDictionary<string, string> patternParameterSingleMandatory
          new Dictionary<string, string> {{"e:f", "value_e_f"}, {"g:h", "value_g_h"}},
          //int overriddenTypeMandatory
          1);
        throw new Exception("Expecting pattern parameter validation to fail");
      }
      catch (ApiException e)
      {
        if (!e.Message.Contains("match the regular expression"))
          throw new Exception("Expecting pattern parameter validation to fail, but another error occurred");
      }
    }

    /// <summary>
    ///   Test that bodies are received correctly
    /// </summary>
    [Test]
    public void TestBinaryBodies()
    {
      var testBinaryBody = SampleData.SampleBinaryBody();

      var binaryBody = parametersApi.PostBinaryBody(testBinaryBody);
      SampleData.VerifyBinaryResponse(binaryBody, testBinaryBody);

      var binaryBodyAsync = parametersApi.PostBinaryBodyAsync(testBinaryBody).Result;
      SampleData.VerifyBinaryResponse(binaryBodyAsync, testBinaryBody);

      var binaryBodyOptMin = parametersApi.PostBinaryBodyOptional();
      SampleData.VerifyBinaryResponse(binaryBodyOptMin, new byte[0]);
      var binaryBodyOptMinAsync = parametersApi.PostBinaryBodyOptionalAsync().Result;
      SampleData.VerifyBinaryResponse(binaryBodyOptMinAsync, new byte[0]);

      var binaryBodyOptMax = parametersApi.PostBinaryBodyOptional(testBinaryBody);
      SampleData.VerifyBinaryResponse(binaryBodyOptMax, testBinaryBody);
      var binaryBodyOptMaxAsync = parametersApi.PostBinaryBodyOptionalAsync(testBinaryBody).Result;
      SampleData.VerifyBinaryResponse(binaryBodyOptMaxAsync, testBinaryBody);
    }

    [Test]
    public void TestObjectBodies()
    {
      var testObjectBody = SampleData.SampleJsonResponse(0);

      var objectBody = parametersApi.PostObjectBody(testObjectBody);
      SampleData.VerifyJsonResponse(objectBody, testObjectBody);
      var objectBodyAsync = parametersApi.PostObjectBodyAsync(testObjectBody).Result;
      SampleData.VerifyJsonResponse(objectBodyAsync, testObjectBody);

      var objectBodyOptMin = parametersApi.PostObjectBodyOptional();
      SampleData.VerifyJsonResponse(objectBodyOptMin, null);
      var objectBodyOptMinAsync = parametersApi.PostObjectBodyOptionalAsync().Result;
      SampleData.VerifyJsonResponse(objectBodyOptMinAsync, null);

      var objectBodyOptMax = parametersApi.PostObjectBodyOptional(testObjectBody);
      SampleData.VerifyJsonResponse(objectBodyOptMax, testObjectBody);
      var objectBodyOptMaxAsync = parametersApi.PostObjectBodyOptionalAsync(testObjectBody).Result;
      SampleData.VerifyJsonResponse(objectBodyOptMaxAsync, testObjectBody);
    }

    [Test]
    public void TestStringBodies()
    {
      var testStringBody = SampleData.SampleString();

      var stringBody = parametersApi.PostStringBody(testStringBody);
      SampleData.VerifyStringResponse(stringBody, testStringBody);
      var stringBodyAsync = parametersApi.PostStringBodyAsync(testStringBody).Result;
      SampleData.VerifyStringResponse(stringBodyAsync, testStringBody);

      var stringBodyOptMin = parametersApi.PostStringBodyOptional();
      SampleData.VerifyStringResponse(stringBodyOptMin, "");
      var stringBodyOptMinAsync = parametersApi.PostStringBodyOptionalAsync().Result;
      SampleData.VerifyStringResponse(stringBodyOptMinAsync, "");

      var stringBodyOptMax = parametersApi.PostStringBodyOptional(testStringBody);
      SampleData.VerifyStringResponse(stringBodyOptMax, testStringBody);
      var stringBodyOptMaxAsync = parametersApi.PostStringBodyOptionalAsync(testStringBody).Result;
      SampleData.VerifyStringResponse(stringBodyOptMaxAsync, testStringBody);
    }
    
    [Test]
    public void TestPathParameters()
    {
      //Least amount of parameters
      var parametersResponse1 = parametersApi.GetPathParameters(
        //int id1
        1);

      var parametersResponse1Expected =
        new JObject(
          new JProperty("id1", "1")
        );

      SampleData.VerifyJsonResponse(parametersResponse1, parametersResponse1Expected);
      
      //Least amount of parameters
      var parametersResponse2 = parametersApi.GetPathParameters(
        //int id1
        1,
        //string id2
        "two");

      var parametersResponse2Expected =
        new JObject(
          new JProperty("id1", "1"),
          new JProperty("id2", "two")
        );

      SampleData.VerifyJsonResponse(parametersResponse2, parametersResponse2Expected);
      
      //Least amount of parameters
      var parametersResponse3 = parametersApi.GetPathParameters(
        //int id1
        1,
        //string id2
        "two",
        //bool id3
        true);

      var parametersResponse3Expected =
        new JObject(
          new JProperty("id1", "1"),
          new JProperty("id2", "two"),
          new JProperty("id3", "true")
        );

      SampleData.VerifyJsonResponse(parametersResponse3, parametersResponse3Expected);
      
      //Least amount of parameters
      var parametersResponse4 = parametersApi.GetPathParameters(
        //int id1
        1,
        //string id2
        "two",
        //bool id3
        true,
        //decimal id4
        4
      );

      var parametersResponse4Expected =
        new JObject(
          new JProperty("id1", "1"),
          new JProperty("id2", "two"),
          new JProperty("id3", "true"),
          new JProperty("id4", "4")
        );

      SampleData.VerifyJsonResponse(parametersResponse4, parametersResponse4Expected);

      var parametersResponse1Async = parametersApi.GetPathParametersAsync(
        //int id1
        1).Result;


      SampleData.VerifyJsonResponse(parametersResponse1Async, parametersResponse1Expected);
      
      var parametersResponse2Async = parametersApi.GetPathParametersAsync(
        //int id1
        1,
        //string id2
        "two").Result;


      SampleData.VerifyJsonResponse(parametersResponse2Async, parametersResponse2Expected);
      
      var parametersResponse3Async = parametersApi.GetPathParametersAsync(
        //int id1
        1,
        //string id2
        "two",
        //bool id3
        true).Result;


      SampleData.VerifyJsonResponse(parametersResponse3Async, parametersResponse3Expected);
      
      var parametersResponse4Async = parametersApi.GetPathParametersAsync(
        //int id1
        1,
        //string id2
        "two",
        //bool id3
        true,
        //decimal id4
        4).Result;


      SampleData.VerifyJsonResponse(parametersResponse4Async, parametersResponse4Expected);
    }
  }
}