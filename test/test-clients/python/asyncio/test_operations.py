import time

from cellstore.client.api_exception import ApiException
from test_base import TestBase
from sample_data import SampleData
import pytest


class TestOperations(TestBase):

    # Basic functionality (Run, Error response (before streaming started))
    @pytest.mark.asyncio
    async def test_defined_binary_response(self):
        binary_response = await self.responses_api.get_binary_response()
        SampleData.verify_valid_binary(binary_response)
        try:
            _ = await self.responses_api.get_binary_response(fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_defined_json_response(self):
        json_response = await self.responses_api.get_json_response()
        SampleData.verify_json_response(json_response)
        try:
            _ = await self.responses_api.get_json_response(fail=True)
            raise Exception("Expecting request to fail")
        except ApiException as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_defined_xml_response(self):
        xml_response = await self.responses_api.get_xml_response()
        SampleData.verify_xml_response(xml_response)
        try:
            _ = await self.responses_api.get_xml_response(fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_defined_object_list_response(self):
        json_list_response_minus_two = await self.responses_api.list_json_response(amount=-2)
        SampleData.verify_json_list_response(json_list_response_minus_two, -2)
        json_list_response_minus_one = await self.responses_api.list_json_response(amount=-1)
        SampleData.verify_json_list_response(json_list_response_minus_one, -1)
        json_list_response_zero = await self.responses_api.list_json_response(amount=0)
        SampleData.verify_json_list_response(json_list_response_zero, 0)

        json_list_response = await self.responses_api.list_json_response(amount=100)
        SampleData.verify_json_list_response(json_list_response, 100)
        try:
            _ = await self.responses_api.list_json_response(fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_defined_object_stream_response(self):
        async with await self.responses_api.stream_json_response(amount=-2) as json_stream_response_minus_two:
            await SampleData.verify_json_stream_response(json_stream_response_minus_two, -2)
        async with await self.responses_api.stream_json_response(amount=-1) as json_stream_response_minus_one:
            await SampleData.verify_json_stream_response(json_stream_response_minus_one, -1)
        async with await self.responses_api.stream_json_response(amount=0) as json_stream_response_zero:
            await SampleData.verify_json_stream_response(json_stream_response_zero, 0)

        async with await self.responses_api.stream_json_response(amount=100) as json_stream_response:
            await SampleData.verify_json_stream_response(json_stream_response, 100)
        try:
            async with await self.responses_api.stream_json_response(fail=True) as _:
                raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

        async with await self.responses_api.stream_json_response(fail_at=5000, amount=10000, sleep=1) as partial_stream:
            await SampleData.verify_failed_json_stream_response(partial_stream, 3000, 5000, self.test_endpoint)
        try:
            async with await self.responses_api.stream_json_response(fail=True) as _:
                raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)


    # Basic functionality (Run, Error response (before streaming started)) for methods that have multiple return values.
    @pytest.mark.asyncio
    async def test_arbitrary_binary_response(self):
        binary_response = await self.responses_api.get_arbitrary_response_as_binary(required_a="", required_d="")
        SampleData.verify_valid_binary(binary_response)
        try:
            _ = await self.responses_api.get_arbitrary_response_as_binary(required_a="", required_d="", fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_arbitrary_json_response(self):
        json_response = await self.responses_api.get_arbitrary_response_as_json(required_a="", required_d="")
        SampleData.verify_json_response(json_response)
        try:
            _ = await self.responses_api.get_arbitrary_response_as_json(required_a="", required_d="", fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_arbitrary_xml_response(self):
        xml_response = await self.responses_api.get_arbitrary_response_as_xml(required_b="", required_c="")
        SampleData.verify_xml_response(xml_response)
        try:
            _ = await self.responses_api.get_arbitrary_response_as_xml(required_b="", required_c="", fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_arbitrary_object_list_response(self):
        json_list_response = await self.responses_api.get_arbitrary_response_as_list(required_b="", required_c="", amount=100)
        SampleData.verify_json_list_response(json_list_response, 100)
        try:
            _ = await self.responses_api.get_arbitrary_response_as_list(required_b="", required_c="", fail=True)
            raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_arbitrary_object_stream_response(self):
        async with await self.responses_api.get_arbitrary_response_as_stream(required_a="", required_d="", amount=100) as json_stream_response:
            await SampleData.verify_json_stream_response(json_stream_response, 100)
        try:
            async with await self.responses_api.get_arbitrary_response_as_stream(required_a="", required_d="", fail=True) as _:
                raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

        async with await self.responses_api.get_arbitrary_response_as_stream(required_a="", required_d="", fail_at=5000, amount=10000, sleep=1) as partial_stream:
            await SampleData.verify_failed_json_stream_response(partial_stream, 3000, 5000, self.test_endpoint)
        try:
            async with await self.responses_api.get_arbitrary_response_as_stream(required_a="", required_d="", fail=True) as _:
                raise Exception("Expecting request to fail")
        except Exception as e:
            SampleData.verify_exception(e, self.test_endpoint)

    @pytest.mark.asyncio
    async def test_streaming(self):
        start_time = time.time()
        async with await self.responses_api.get_arbitrary_response_as_stream(required_a="", required_d="", amount=10000, sleep=1) as streamed_response:
            _metadata = streamed_response.get_metadata()
            # async for item in streamed_response:
            #     print(item)
            _item = await streamed_response.__aiter__().__anext__()
            end_time = time.time()
            if (end_time - start_time) * 1000 > 1000:
                raise Exception("The time to get the first item and the metadata suggest no streaming occurred.")
