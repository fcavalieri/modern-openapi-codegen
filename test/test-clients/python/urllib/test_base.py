import os

import requests
import time
from testcontainers.core.container import DockerContainer
from testcontainers.core.waiting_utils import wait_for

from cellstore.api.parameters_api import ParametersApi
from cellstore.api.responses_api import ResponsesApi


class TestBase:
    @classmethod
    def setup_class(cls):
        parent = cls.find_base_folder(os.path.abspath(os.getcwd()))
        jar_path = parent + "/test/test-server/target/TestServer.jar"

        cls.test_container = DockerContainer(image="openjdk:15")\
            .with_exposed_ports(8888)\
            .with_volume_mapping(host=jar_path, container="/TestServer.jar", mode="ro")\
            .with_command("java -jar /TestServer.jar")
        cls.test_container.start()
        cls.test_endpoint = "http://" + cls.test_container.get_container_host_ip() + ":" + cls.test_container.get_exposed_port(8888) + "/test"

        def connect():
            try:
                requests.get(cls.test_endpoint)
            except Exception as e:
                raise ConnectionResetError(e)

        wait_for(connect)
        print(cls.test_endpoint)
        cls.responses_api = ResponsesApi(cls.test_endpoint)
        cls.parameters_api = ParametersApi(cls.test_endpoint)

    @classmethod
    def teardown_class(cls):
        cls.test_container.stop()

    @classmethod
    def find_base_folder(cls, folder):
        if ".git" in [os.path.basename(sub_folder) for sub_folder in os.listdir(folder) if os.path.isdir(folder + "/" + sub_folder)]:
            return folder
        else:
            return cls.find_base_folder(os.path.dirname(folder))
