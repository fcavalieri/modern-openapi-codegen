import io
import json
import os
import zipfile

from cellstore.client.api_exception import ApiException


class SampleData:

    @staticmethod
    def verify_valid_binary(binary_response: bytes):
        z = zipfile.ZipFile(io.BytesIO(binary_response))
        if "Test String" != z.read("mytext.txt").decode("utf-8"):
            raise Exception("Cannot verify binary (1)")

    @staticmethod
    def sample_json_response(id_value):
        return {
            "id_field": id_value,
            "string_field": "string",
            "int_field": 1,
            "bool_field": True,
            "float_field": 3.14,
            "null_field": None,
            "array_field": ["string", 1, True],
            "object_field": {
                "utf8_string_field": "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。"}
        }

    @staticmethod
    def sample_xml_response():
        return "<?xml version = \"1.0\" encoding = \"utf-8\"?>\n" + \
               "<!-- planes.xml - A document that lists ads for used airplanes -->\n" + \
               "<planes_for_sale>\n" + \
               "   <ad>\n" + \
               "      <year>1977</year>\n" + \
               "      <make>&c;</make>\n" + \
               "      <model>Skyhawk</model>\n" + \
               "   </ad>\n" + \
               "</planes_for_sale>"

    @staticmethod
    def sample_binary_body():
        return bytearray(os.urandom(1024))

    @staticmethod
    def sample_string():
        return "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。"

    @staticmethod
    def verify_json_response(json_response):
        expected = SampleData.sample_json_response(0)
        if json_response != expected:
            raise Exception(f"Unexpected JSON: {expected} != {json_response}")

    @staticmethod
    def verify_xml_response(xml_response):
        expected = SampleData.sample_xml_response()
        if xml_response != expected:
            raise Exception("Unexpected XML")

    @staticmethod
    def verify_exception(api_exception, endpoint):
        if "Failing as requested" not in str(api_exception):
            raise Exception("Expecting request to fail due to explicit request")

        if api_exception.status != 500:
            raise Exception("Expecting error to contain http response code")
        message = str(api_exception)

        if "CELLSTORE_EXCEPTION" not in message:
            raise Exception("Expecting error to contain raw response")
        if "com.reportix.xbrl.server.rest.gen.endpoints" not in message:
            raise Exception("Expecting error to contain stack trace")
        if "1.0.0-TESTS" not in message:
            raise Exception("Expecting error to contain library information")
        if "System has" not in message:
            raise Exception("Expecting error to contain system memory information")
        if "Application is using" not in message:
            raise Exception("Expecting error to contain application memory information")
        if endpoint is not None and endpoint not in message:
            raise Exception("Expecting error to contain call information")
        if "Request Time" not in message:
            raise Exception("Expecting error to contain call request time")
        if "Response Time" not in message:
            raise Exception("Expecting error to contain call response time")
        if "Authentication Provider" not in message:
            raise Exception("Expecting error to contain configuration")

    @staticmethod
    def verify_json_response_2(actual_response, expected_response):
        if actual_response is None and expected_response is None:
            return
        if actual_response != expected_response:
            print(
                f"Actual:\n{json.dumps(actual_response, indent=2)}\n\nExpected:\n{json.dumps(expected_response, indent=2)}")
            raise Exception("Expected != Actual")

    @staticmethod
    def verify_binary_response(actual_response, expected_response):
        if actual_response != expected_response:
            raise Exception("Expected != Actual")

    @staticmethod
    def verify_string_response(actual_response, expected_response):
        if actual_response != expected_response:
            raise Exception("Expected != Actual")

    @staticmethod
    def verify_json_list_response(actual_response, amount):
        if max(amount, 0) != len(actual_response):
            raise Exception("Unexpected count")
        SampleData.verify_json_list(actual_response, actual_response.get_metadata())

    @staticmethod
    def verify_json_stream_response(actual_response, amount):
        object_list = [*actual_response]
        if max(amount, 0) != len(object_list):
            raise Exception("Unexpected count")
        SampleData.verify_json_list(object_list, actual_response.get_metadata())

    @staticmethod
    def verify_failed_json_stream_response(json_stream_response, minimum, maximum, endpoint):
        SampleData.verify_json_response_2(json_stream_response.get_metadata(), SampleData.sample_json_response(-2))

        iter = json_stream_response.iter()

        for i in range(0, minimum):
            item = next(iter)
            SampleData.verify_json_response_2(item, SampleData.sample_json_response(i))
        try:
            for i in range(minimum, maximum + 1):
                item = next(iter)
                SampleData.verify_json_response_2(item, SampleData.sample_json_response(i))
        except ApiException as e:
            SampleData.verify_exception(e, endpoint)

    @staticmethod
    def verify_json_list(object_list, metadata):
        SampleData.verify_json_response_2(metadata, SampleData.sample_json_response(-2))
        for i in range(0, len(object_list)):
            SampleData.verify_json_response_2(object_list[i], SampleData.sample_json_response(i))
