from http.client import HTTPResponse

from cellstore.client.api_exception import ApiException
from test_base import TestBase
from sample_data import SampleData
from decimal import Decimal

class TestOperations(TestBase):
    HTTPResponse
    # Test that any kind of parameter is serialized, sent, received and echoed back correctly
    def test_parameters_min(self):
        # Least amount of parameters
        parameters_response_min = self.parameters_api.get_query_parameters(
            boolean_mandatory_without_default=True,
            integer_mandatory_without_default=1,
            number_mandatory_without_default=Decimal("1234567890.123456789"),
            string_mandatory_without_default="str",
            boolean_array_mandatory_without_default=[True, False],
            integer_array_mandatory_without_default=[1, 2],
            number_array_mandatory_without_default=[Decimal("1234567890.123456789"), Decimal("12345678901234567890.123456789")],
            string_array_mandatory_without_default=["a", "b"],
            pattern_parameter_array_mandatory={"a:b": ("value_a_b", "value_a_b_2"),
                                               "c:d": ("value_c_d", "value_c_d_2")},
            pattern_parameter_single_mandatory={"e:f": "value_e_f", "g:h": "value_g_h"},
            overridden_type_mandatory=1)

        parameters_response_min_expected = {
            "boolean-hardcoded": ["true"],
            "integer-hardcoded": ["3"],
            "number-hardcoded": ["987654321.987654321"],
            "string-hardcoded": ["value"],
            "boolean-mandatory-without-default": ["true"],
            "integer-mandatory-without-default": ["1"],
            "number-mandatory-without-default": ["1234567890.123456789"],
            "string-mandatory-without-default": ["str"],
            "boolean-array-mandatory-without-default": ["true", "false"],
            "integer-array-mandatory-without-default": ["1", "2"],
            "number-array-mandatory-without-default": ["1234567890.123456789", "12345678901234567890.123456789"],
            "string-array-mandatory-without-default": ["a", "b"],
            "overridden-type-mandatory": ["1"],
            "a:b": ["value_a_b", "value_a_b_2"],
            "c:d": ["value_c_d", "value_c_d_2"],
            "e:f": ["value_e_f"],
            "g:h": ["value_g_h"]
        }

        SampleData.verify_json_response_2(parameters_response_min, parameters_response_min_expected)

    # Test that any kind of parameter is serialized, sent, received and echoed back correctly
    def test_parameters_min_float(self):
        # Least amount of parameters (using floats)

        parameters_response_min_float = self.parameters_api.get_query_parameters(
            boolean_mandatory_without_default=True,
            integer_mandatory_without_default=1,
            number_mandatory_without_default=1234567890.123456789,
            string_mandatory_without_default="str",
            boolean_array_mandatory_without_default=[True, False],
            integer_array_mandatory_without_default=[1, 2],
            number_array_mandatory_without_default=[1234567890.123456789,
                                                    12345678901234567890.123456789],
            string_array_mandatory_without_default=["a", "b"],
            pattern_parameter_array_mandatory={"a:b": ("value_a_b", "value_a_b_2"),
                                               "c:d": ("value_c_d", "value_c_d_2")},
            pattern_parameter_single_mandatory={"e:f": "value_e_f", "g:h": "value_g_h"},
            overridden_type_mandatory=1)

        parameters_response_min_float_expected = {
            "boolean-hardcoded": ["true"],
            "integer-hardcoded": ["3"],
            "number-hardcoded": ["987654321.987654321"],
            "string-hardcoded": ["value"],
            "boolean-mandatory-without-default": ["true"],
            "integer-mandatory-without-default": ["1"],
            "number-mandatory-without-default": ["1234567890.1234567"], # Lossy due to float
            "string-mandatory-without-default": ["str"],
            "boolean-array-mandatory-without-default": ["true", "false"],
            "integer-array-mandatory-without-default": ["1", "2"],
            "number-array-mandatory-without-default": ["1234567890.1234567", "12345678901234567000"],  # Lossy due to float
            "string-array-mandatory-without-default": ["a", "b"],
            "overridden-type-mandatory": ["1"],
            "a:b": ["value_a_b", "value_a_b_2"],
            "c:d": ["value_c_d", "value_c_d_2"],
            "e:f": ["value_e_f"],
            "g:h": ["value_g_h"]
        }

        SampleData.verify_json_response_2(parameters_response_min_float, parameters_response_min_float_expected)

    def test_parameters_max(self):
        # Maximum amount of parameters
        parameters_response_max = self.parameters_api.get_query_parameters(
            boolean_mandatory_without_default=True,
            integer_mandatory_without_default=1,
            number_mandatory_without_default=Decimal("1234567890.123456789"),
            string_mandatory_without_default="str",
            boolean_array_mandatory_without_default=[True, False],
            integer_array_mandatory_without_default=[1, 2],
            number_array_mandatory_without_default=[Decimal("1234567890.123456789"), Decimal("12345678901234567890.123456789")],
            string_array_mandatory_without_default=["a", "b"],
            pattern_parameter_array_mandatory={"a:b": ("value_a_b", "value_a_b_2"),
                                               "c:d": ("value_c_d", "value_c_d_2")},
            pattern_parameter_single_mandatory={"e:f": "value_e_f", "g:h": "value_g_h"},
            overridden_type_mandatory=1,
            fail=False,
            boolean_optional_with_default=True,
            boolean_optional_without_default=True,
            integer_optional_with_default=1,
            integer_optional_without_default=1,
            number_optional_with_default=Decimal("1234567890"),
            number_optional_without_default=Decimal("12345678901234567890"),
            string_optional_with_default="str",
            string_optional_without_default="str",
            boolean_array_optional_with_default=[True, False],
            boolean_array_optional_without_default=[True, False],
            integer_array_optional_with_default=[1, 2],
            integer_array_optional_without_default=[1, 2],
            number_array_optional_with_default=[Decimal("1234567890.123456789"), Decimal("12345678901234567890.123456789")],
            number_array_optional_without_default=[Decimal("1234567890.123456789"), Decimal("12345678901234567890.123456789")],
            string_array_optional_with_default=["a", "b"],
            string_array_optional_without_default=["a", "b"],
            pattern_parameter_array_optional={"i:j": ["value_i_j", "value_i_j_2"], "k:l": ["value_k_l", "value_k_l_2"]},
            pattern_parameter_single_optional={"m:n": "value_m_n", "o:p": "value_o_p"},
            overridden_type_optional=1
        )

        parameters_response_max_expected = {
            "boolean-hardcoded": ["true"],
            "integer-hardcoded": ["3"],
            "number-hardcoded": ["987654321.987654321"],
            "string-hardcoded": ["value"],
            "boolean-mandatory-without-default": ["true"],
            "integer-mandatory-without-default": ["1"],
            "number-mandatory-without-default": ["1234567890.123456789"],
            "string-mandatory-without-default": ["str"],
            "boolean-array-mandatory-without-default": ["true", "false"],
            "integer-array-mandatory-without-default": ["1", "2"],
            "number-array-mandatory-without-default": ["1234567890.123456789", "12345678901234567890.123456789"],
            "string-array-mandatory-without-default": ["a", "b"],
            "overridden-type-mandatory": ["1"],
            "a:b": ["value_a_b", "value_a_b_2"],
            "c:d": ["value_c_d", "value_c_d_2"],
            "e:f": ["value_e_f"],
            "g:h": ["value_g_h"],
            "fail": ["false"],
            "boolean-optional-with-default": ["true"],
            "boolean-optional-without-default": ["true"],
            "integer-optional-with-default": ["1"],
            "integer-optional-without-default": ["1"],
            "number-optional-with-default": ["1234567890"],
            "number-optional-without-default": ["12345678901234567890"],
            "string-optional-with-default": ["str"],
            "string-optional-without-default": ["str"],
            "boolean-array-optional-with-default": ["true", "false"],
            "boolean-array-optional-without-default": ["true", "false"],
            "integer-array-optional-with-default": ["1", "2"],
            "integer-array-optional-without-default": ["1", "2"],
            "number-array-optional-with-default": ["1234567890.123456789", "12345678901234567890.123456789"],
            "number-array-optional-without-default": ["1234567890.123456789", "12345678901234567890.123456789"],
            "string-array-optional-with-default": ["a", "b"],
            "string-array-optional-without-default": ["a", "b"],
            "i:j": ["value_i_j", "value_i_j_2"],
            "k:l": ["value_k_l", "value_k_l_2"],
            "m:n": ["value_m_n"],
            "o:p": ["value_o_p"],
            "overridden-type-optional": ["1"]
        }

        SampleData.verify_json_response_2(parameters_response_max, parameters_response_max_expected);

    def test_pattern_parameters(self):
        try:
            parameters_response_min = self.parameters_api.get_query_parameters(
                boolean_mandatory_without_default=True,
                integer_mandatory_without_default=1,
                number_mandatory_without_default=Decimal("1234567890.123456789"),
                string_mandatory_without_default="str",
                boolean_array_mandatory_without_default=[True, False],
                integer_array_mandatory_without_default=[1, 2],
                number_array_mandatory_without_default=[Decimal("1234567890.123456789"), Decimal("12345678901234567890.123456789")],
                string_array_mandatory_without_default=["a", "b"],
                pattern_parameter_array_mandatory={"ab": ["value_a_b", "value_a_b_2"],
                                                   "c:d": ["value_c_d", "value_c_d_2"]},
                pattern_parameter_single_mandatory={"e:f": "value_e_f",
                                                    "g:h": "value_g_h"},
                overridden_type_mandatory=1)
            raise Exception("Expecting pattern parameter validation to fail")
        except ApiException as e:
            if "match the regular expression" not in str(e):
                raise Exception("Expecting pattern parameter validation to fail, but another error occurred")

    def test_binary_bodies(self):
        test_binary_body = SampleData.sample_binary_body()

        binary_body = self.parameters_api.post_binary_body(body_name=test_binary_body)
        SampleData.verify_binary_response(binary_body, test_binary_body)

        binary_body_opt_min = self.parameters_api.post_binary_body_optional()
        SampleData.verify_binary_response(binary_body_opt_min, b'')

        binary_body_opt_max = self.parameters_api.post_binary_body_optional(body_name=test_binary_body)
        SampleData.verify_binary_response(binary_body_opt_max, test_binary_body)

    def test_object_bodies(self):
        test_object_body = SampleData.sample_json_response(0)

        object_body = self.parameters_api.post_object_body(archive=test_object_body)
        SampleData.verify_json_response_2(object_body, test_object_body)

        object_body_opt_min = self.parameters_api.post_object_body_optional()
        SampleData.verify_json_response_2(object_body_opt_min, None)

        object_body_opt_max = self.parameters_api.post_object_body_optional(archive=test_object_body)
        SampleData.verify_json_response_2(object_body_opt_max, test_object_body)

    def test_string_bodies(self):
        test_string_body = SampleData.sample_string()

        string_body = self.parameters_api.post_string_body(archive=test_string_body)
        SampleData.verify_string_response(string_body, test_string_body)

        string_body_opt_min = self.parameters_api.post_string_body_optional()
        SampleData.verify_string_response(string_body_opt_min, "")

        string_body_opt_max = self.parameters_api.post_string_body_optional(archive=test_string_body)
        SampleData.verify_string_response(string_body_opt_max, test_string_body)

    def test_path_parameters(self):
        parameters_response_1 = self.parameters_api.get_path_parameters(
            id1=1)

        parameters_response_1_expected = {
            "id1": "1"
        }

        SampleData.verify_json_response_2(parameters_response_1, parameters_response_1_expected)

        parameters_response_2 = self.parameters_api.get_path_parameters(
            id1=1,
            id2="two")

        parameters_response_2_expected = {
            "id1": "1",
            "id2": "two"
        }

        SampleData.verify_json_response_2(parameters_response_2, parameters_response_2_expected)

        parameters_response_3 = self.parameters_api.get_path_parameters(
            id1=1,
            id2="two",
            id3=True)

        parameters_response_3_expected = {
            "id1": "1",
            "id2": "two",
            "id3": "true"
        }

        SampleData.verify_json_response_2(parameters_response_3, parameters_response_3_expected)

        parameters_response_4 = self.parameters_api.get_path_parameters(
            id1=1,
            id2="two",
            id3=True,
            id4=Decimal("4"))

        parameters_response_4_expected = {
            "id1": "1",
            "id2": "two",
            "id3": "true",
            "id4": "4"
        }

        SampleData.verify_json_response_2(parameters_response_4, parameters_response_4_expected)
