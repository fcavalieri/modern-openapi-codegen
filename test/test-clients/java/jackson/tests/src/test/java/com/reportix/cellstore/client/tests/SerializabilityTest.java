package com.reportix.cellstore.client.tests;

import com.reportix.cellstore.client.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

public class SerializabilityTest extends TestBase {

    @Test
    public void TestSerialization() throws IOException, ClassNotFoundException {
        Configuration configuration = new Configuration();
        configuration.setTimeoutMs(123);
        configuration.setVerboseExceptions(true);
        configuration.setVerifySSL(false);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(configuration);
        objectOutputStream.flush();
        objectOutputStream.close();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        Configuration configuration2 = (Configuration) objectInputStream.readObject();
        objectInputStream.close();

        Assertions.assertEquals(configuration, configuration2);
    }
}
