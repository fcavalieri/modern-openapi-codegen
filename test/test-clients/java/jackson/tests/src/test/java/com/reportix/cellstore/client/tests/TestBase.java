package com.reportix.cellstore.client.tests;

import com.reportix.cellstore.api.ParametersApi;
import com.reportix.cellstore.api.ResponsesApi;
import com.reportix.cellstore.client.ApiClient;
import com.reportix.cellstore.client.ApiException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestBase {
    protected ResponsesApi responsesApi;
    protected ParametersApi parametersApi;
    protected GenericContainer testContainer;
    protected String testEndpoint;

    @BeforeAll
    public void setup() throws IOException, ApiException {
        setupContainer();

        testEndpoint = "http://" + testContainer.getHost() + ":" + testContainer.getMappedPort(8888) + "/test";
        responsesApi = new ResponsesApi(testEndpoint);
        parametersApi = new ParametersApi(testEndpoint);
    }

    private void setupContainer() throws IOException {
        var parent = findBaseFolder(Paths.get(".").toAbsolutePath());
        String jarPath = jarPath = parent + "/test/test-server/target/TestServer.jar";

        testContainer = new GenericContainer(DockerImageName.parse("openjdk:15"))
                .withExposedPorts(8888)
                .withFileSystemBind(jarPath, "/TestServer.jar", BindMode.READ_ONLY)
                .withCommand("java", "-jar", "/TestServer.jar")
                .waitingFor(Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(30)));
        testContainer.start();
    }


    @AfterAll
    public void teardown()
    {
        testContainer.stop();
    }

    private Path findBaseFolder(Path currentDirectory) throws IOException {
        if (Files.list(currentDirectory).filter(p -> Files.isDirectory(p)).map(p -> p.getFileName().toString()).anyMatch(p -> p.equals(".git")))
            return currentDirectory;
        else
            return findBaseFolder(currentDirectory.getParent());
    }
}
