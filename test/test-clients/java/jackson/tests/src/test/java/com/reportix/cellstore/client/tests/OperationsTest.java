package com.reportix.cellstore.client.tests;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.model.ISequence;
import com.reportix.cellstore.model.IStream;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class OperationsTest extends TestBase {

    /// <summary>
    /// Basic functionality (Run, Error response (before streaming started))
    /// </summary>
    /// <exception cref="IOException"></exception>
    @Test
    public void TestDefinedBinaryResponse() throws IOException, ApiException {
        byte[] binaryResponse = responsesApi.getBinaryResponse(null);
        SampleData.verifyValidBinary(binaryResponse);
        try
        {
            byte[] _var = responsesApi.getBinaryResponse(true);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestDefinedObjectResponse() throws IOException, ApiException {
        ObjectNode jsonResponse = responsesApi.getJsonResponse(null);
        SampleData.verifyJsonResponse(jsonResponse);
        try
        {
            ObjectNode _Var = responsesApi.getJsonResponse(true);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestDefinedXMLResponse() throws IOException, ApiException {
        String xmlResponse = responsesApi.getXMLResponse(null);
        SampleData.verifyXMLResponse(xmlResponse);
        try
        {
            String _Var = responsesApi.getXMLResponse(true);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestDefinedObjectListResponse() throws IOException, ApiException {
        ISequence<ObjectNode, ObjectNode> jsonListResponseMinusTwo = responsesApi.listJsonResponse(null, null, null, -2);
        SampleData.verifyJsonListResponse(jsonListResponseMinusTwo, -2);
        ISequence<ObjectNode, ObjectNode> jsonListResponseMinusOne = responsesApi.listJsonResponse(null, null, null, -1);
        SampleData.verifyJsonListResponse(jsonListResponseMinusOne, -1);
        ISequence<ObjectNode, ObjectNode> jsonListResponseZero = responsesApi.listJsonResponse(null, null, null, 0);
        SampleData.verifyJsonListResponse(jsonListResponseZero, 0);

        ISequence<ObjectNode, ObjectNode> jsonListResponse = responsesApi.listJsonResponse(null, null, null, 100);
        SampleData.verifyJsonListResponse(jsonListResponse, 100);
        try
        {
            ISequence<ObjectNode, ObjectNode> _Var = responsesApi.listJsonResponse(true, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestDefinedObjectStreamResponse() throws IOException, ApiException {
        IStream<ObjectNode, ObjectNode> jsonListResponseMinusTwo = responsesApi.streamJsonResponse(null, null, null, -2);
        SampleData.verifyJsonStreamResponse(jsonListResponseMinusTwo, -2);
        IStream<ObjectNode, ObjectNode> jsonListResponseMinusOne = responsesApi.streamJsonResponse(null, null, null, -1);
        SampleData.verifyJsonStreamResponse(jsonListResponseMinusOne, -1);
        IStream<ObjectNode, ObjectNode> jsonListResponseZero = responsesApi.streamJsonResponse(null, null, null, 0);
        SampleData.verifyJsonStreamResponse(jsonListResponseZero, 0);

        IStream<ObjectNode, ObjectNode> jsonStreamResponse = responsesApi.streamJsonResponse(null, null, null, 100);
        SampleData.verifyJsonStreamResponse(jsonStreamResponse, 100);
        try
        {
            IStream<ObjectNode, ObjectNode> _Var = responsesApi.streamJsonResponse(true, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }

        IStream<ObjectNode, ObjectNode> partialStream = responsesApi.streamJsonResponse(false, 5000, 1, 10000);
        SampleData.verifyFailedJsonStreamResponse(partialStream, 3000, 5000, testEndpoint);
    }

    /// <summary>
    /// Basic functionality (Run, Error response (before streaming started)) for methods that have multiple return values.
    /// </summary>
    /// <exception cref="IOException"></exception>
    @Test
    public void TestArbitraryBinaryResponse() throws IOException, ApiException {
        byte[] binaryResponse = responsesApi.getArbitraryResponseAsBinary("", "", null, null, null, null, null, null, null);
        SampleData.verifyValidBinary(binaryResponse);
        try
        {
            byte[] _Var = responsesApi.getArbitraryResponseAsBinary("", "", true, null, null, null, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestArbitraryJsonResponse() throws IOException, ApiException {
        ObjectNode jsonResponse = responsesApi.getArbitraryResponseAsJson("", "", null, null, null, null, null, null, null);
        SampleData.verifyJsonResponse(jsonResponse);
        try
        {
            ObjectNode _Var = responsesApi.getArbitraryResponseAsJson("", "", true, null, null, null, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestArbitraryXMLResponse() throws IOException, ApiException {
        String xml = responsesApi.getArbitraryResponseAsXML("", "", null, null, null, null, null, null, null);
        SampleData.verifyXMLResponse(xml);
        try
        {
            String _Var = responsesApi.getArbitraryResponseAsXML("", "", true, null, null, null, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestArbitraryObjectListResponse() throws ApiException, IOException {
        ISequence<ObjectNode, ObjectNode> jsonListResponse =
                responsesApi.getArbitraryResponseAsList("", "", null, null, null, 100, null, null, null);
        SampleData.verifyJsonListResponse(jsonListResponse, 100);
        try
        {
            ISequence<ObjectNode, ObjectNode> _Var = responsesApi.getArbitraryResponseAsList("", "", true, null, null, 100, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }
    }

    @Test
    public void TestArbitraryObjectStreamResponse() throws IOException, ApiException {
        IStream<ObjectNode, ObjectNode> jsonStreamResponse = responsesApi.getArbitraryResponseAsStream("", "", null, null, null, 100, null, null, null);
        SampleData.verifyJsonStreamResponse(jsonStreamResponse, 100);
        try
        {
            IStream<ObjectNode, ObjectNode> _Var = responsesApi.getArbitraryResponseAsStream("", "", true, null, null, null, null, null, null);
            throw new IOException("Expecting request to fail");
        }
        catch (ApiException e)
        {
            SampleData.verifyException(e, testEndpoint);
        }

        IStream<ObjectNode, ObjectNode> partialStream = responsesApi.getArbitraryResponseAsStream("", "", null, 5000, 1, 10000, null, null, null);
        SampleData.verifyFailedJsonStreamResponse(partialStream, 3000, 5000, testEndpoint);

        long start = System.currentTimeMillis();
        IStream<ObjectNode, ObjectNode> streamedResponse = responsesApi.getArbitraryResponseAsStream("", "", null, null, 1, 10000, null, null, null);
        ObjectNode _meta = streamedResponse.getMetadata();
        ObjectNode _item = streamedResponse.iterator().next();
        long end = System.currentTimeMillis();
        if (end - start > 1000)
            throw new IOException("The time to get the first item and the metadata suggest no streaming occurred.");
    }
}
