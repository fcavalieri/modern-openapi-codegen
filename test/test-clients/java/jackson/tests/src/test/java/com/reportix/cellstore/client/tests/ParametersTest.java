package com.reportix.cellstore.client.tests;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.reportix.cellstore.client.ApiException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ParametersTest extends TestBase {

    /// <summary>
    /// Test that any kind of parameter is serialized, sent, received and echoed back correctly
    /// </summary>
    @Test
    public void TestParametersMin() throws ApiException, IOException {
        //Least amount of parameters
        ObjectNode parametersResponseMin = parametersApi.getQueryParameters(
                //Boolean booleanMandatoryWithoutDefault
                true,
                //Integer integerMandatoryWithoutDefault
                1,
                //BigDecimal numberMandatoryWithoutDefault
                new BigDecimal("1234567890.123456789"),
                //String stringMandatoryWithoutDefault
                "str",
                //List<Boolean> booleanArrayMandatoryWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayMandatoryWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayMandatoryWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayMandatoryWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayMandatory
                Map.of("a:b", List.of("value_a_b", "value_a_b_2"),
                        "c:d", List.of("value_c_d", "value_c_d_2")),
                //Map<String, String> patternParameterSingleMandatory
                Map.of("e:f", "value_e_f",
                        "g:h", "value_g_h"),
                //int overriddenTypeMandatory
                1,
                //Boolean fail
                null,
                //Boolean booleanOptionalWithDefault
                null,
                //Boolean booleanOptionalWithoutDefault
                null,
                //Integer integerOptionalWithDefault
                null,
                //Integer integerOptionalWithoutDefault
                null,
                //BigDecimal numberOptionalWithDefault
                null,
                //BigDecimal numberOptionalWithoutDefault
                null,
                //String stringOptionalWithDefault
                null,
                //String stringOptionalWithoutDefault
                null,
                //List<Boolean> booleanArrayOptionalWithDefault
                null,
                //List<Boolean> booleanArrayOptionalWithoutDefault
                null,
                //List<Integer> integerArrayOptionalWithDefault
                null,
                //List<Integer> integerArrayOptionalWithoutDefault
                null,
                //List<BigDecimal> numberArrayOptionalWithDefault
                null,
                //List<BigDecimal> numberArrayOptionalWithoutDefault
                null,
                //List<String> stringArrayOptionalWithDefault
                null,
                //List<String> stringArrayOptionalWithoutDefault
                null,
                //Map<String, List<String>> patternParameterArrayOptional
                null,
                //Map<String, String> patternParameterSingleOptional
                null,
                //Integer overriddenTypeOptional
                null);

        ObjectNode parametersResponseMinExpected = JsonNodeFactory.instance.objectNode();
        parametersResponseMinExpected.putArray("boolean-hardcoded").add("true");
        parametersResponseMinExpected.putArray("integer-hardcoded").add("3");
        parametersResponseMinExpected.putArray("number-hardcoded").add("987654321.987654321");
        parametersResponseMinExpected.putArray("string-hardcoded").add("value");

        parametersResponseMinExpected.putArray("boolean-mandatory-without-default").add("true");
        parametersResponseMinExpected.putArray("integer-mandatory-without-default").add("1");
        parametersResponseMinExpected.putArray("number-mandatory-without-default").add("1234567890.123456789");
        parametersResponseMinExpected.putArray("string-mandatory-without-default").add("str");

        parametersResponseMinExpected.putArray("boolean-array-mandatory-without-default").add("true").add("false");
        parametersResponseMinExpected.putArray("integer-array-mandatory-without-default").add("1").add("2");
        parametersResponseMinExpected.putArray("number-array-mandatory-without-default").add("1234567890.123456789").add("12345678901234567890.123456789");
        parametersResponseMinExpected.putArray("string-array-mandatory-without-default").add("a").add("b");

        parametersResponseMinExpected.putArray("overridden-type-mandatory").add("1");
        parametersResponseMinExpected.putArray("a:b").add("value_a_b").add("value_a_b_2");
        parametersResponseMinExpected.putArray("c:d").add("value_c_d").add("value_c_d_2");
        parametersResponseMinExpected.putArray("e:f").add("value_e_f");
        parametersResponseMinExpected.putArray("g:h").add("value_g_h");

        SampleData.verifyJsonResponse(parametersResponseMin, parametersResponseMinExpected);
    }

    @Test
    public void TestParametersMax() throws IOException, ApiException {
        //Maximum amount of parameters
        ObjectNode parametersResponseMax = parametersApi.getQueryParameters(
                //Boolean booleanMandatoryWithoutDefault
                true,
                //Integer integerMandatoryWithoutDefault
                1,
                //BigDecimal numberMandatoryWithoutDefault
                new BigDecimal("1234567890.123456789"),
                //String stringMandatoryWithoutDefault
                "str",
                //List<Boolean> booleanArrayMandatoryWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayMandatoryWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayMandatoryWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayMandatoryWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayMandatory
                Map.of("a:b", List.of("value_a_b", "value_a_b_2"),
                        "c:d", List.of("value_c_d", "value_c_d_2")),
                //Map<String, String> patternParameterSingleMandatory
                Map.of("e:f", "value_e_f",
                        "g:h", "value_g_h"),
                //int overriddenTypeMandatory
                1,
                //Boolean fail
                false,
                //Boolean booleanOptionalWithDefault
                true,
                //Boolean booleanOptionalWithoutDefault
                true,
                //Integer integerOptionalWithDefault
                1,
                //Integer integerOptionalWithoutDefault
                1,
                //BigDecimal numberOptionalWithDefault
                new BigDecimal("1234567890"),
                //BigDecimal numberOptionalWithoutDefault
                new BigDecimal("12345678901234567890"),
                //String stringOptionalWithDefault
                "str",
                //String stringOptionalWithoutDefault
                "str",
                //List<Boolean> booleanArrayOptionalWithDefault
                List.of(true, false),
                //List<Boolean> booleanArrayOptionalWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayOptionalWithDefault
                List.of(1, 2),
                //List<Integer> integerArrayOptionalWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayOptionalWithDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<BigDecimal> numberArrayOptionalWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayOptionalWithDefault
                List.of("a", "b"),
                //List<String> stringArrayOptionalWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayOptional
                Map.of("i:j", List.of("value_i_j", "value_i_j_2"),
                        "k:l", List.of("value_k_l", "value_k_l_2")),
                //Map<String, String> patternParameterSingleOptional
                Map.of("m:n", "value_m_n",
                        "o:p", "value_o_p"),
                //Integer overriddenTypeOptional
                1);

        ObjectNode parametersResponseMaxExpected = JsonNodeFactory.instance.objectNode();
        parametersResponseMaxExpected.putArray("boolean-hardcoded").add("true");
        parametersResponseMaxExpected.putArray("integer-hardcoded").add("3");
        parametersResponseMaxExpected.putArray("number-hardcoded").add("987654321.987654321");
        parametersResponseMaxExpected.putArray("string-hardcoded").add("value");

        parametersResponseMaxExpected.putArray("fail").add("false");

        parametersResponseMaxExpected.putArray("boolean-mandatory-without-default").add("true");
        parametersResponseMaxExpected.putArray("boolean-optional-with-default").add("true");
        parametersResponseMaxExpected.putArray("boolean-optional-without-default").add("true");

        parametersResponseMaxExpected.putArray("integer-mandatory-without-default").add("1");
        parametersResponseMaxExpected.putArray("integer-optional-with-default").add("1");
        parametersResponseMaxExpected.putArray("integer-optional-without-default").add("1");

        parametersResponseMaxExpected.putArray("number-mandatory-without-default").add("1234567890.123456789");
        parametersResponseMaxExpected.putArray("number-optional-with-default").add("1234567890");
        parametersResponseMaxExpected.putArray("number-optional-without-default").add("12345678901234567890");

        parametersResponseMaxExpected.putArray("string-mandatory-without-default").add("str");
        parametersResponseMaxExpected.putArray("string-optional-with-default").add("str");
        parametersResponseMaxExpected.putArray("string-optional-without-default").add("str");

        parametersResponseMaxExpected.putArray("boolean-array-mandatory-without-default").add("true").add("false");
        parametersResponseMaxExpected.putArray("boolean-array-optional-with-default").add("true").add("false");
        parametersResponseMaxExpected.putArray("boolean-array-optional-without-default").add("true").add("false");

        parametersResponseMaxExpected.putArray("integer-array-mandatory-without-default").add("1").add("2");
        parametersResponseMaxExpected.putArray("integer-array-optional-with-default").add("1").add("2");
        parametersResponseMaxExpected.putArray("integer-array-optional-without-default").add("1").add("2");

        parametersResponseMaxExpected.putArray("number-array-mandatory-without-default").add("1234567890.123456789").add("12345678901234567890.123456789");
        parametersResponseMaxExpected.putArray("number-array-optional-with-default").add("1234567890.123456789").add("12345678901234567890.123456789");
        parametersResponseMaxExpected.putArray("number-array-optional-without-default").add("1234567890.123456789").add("12345678901234567890.123456789");

        parametersResponseMaxExpected.putArray("string-array-mandatory-without-default").add("a").add("b");
        parametersResponseMaxExpected.putArray("string-array-optional-with-default").add("a").add("b");
        parametersResponseMaxExpected.putArray("string-array-optional-without-default").add("a").add("b");

        parametersResponseMaxExpected.putArray("overridden-type-optional").add("1");
        parametersResponseMaxExpected.putArray("overridden-type-mandatory").add("1");

        parametersResponseMaxExpected.putArray("i:j").add("value_i_j").add("value_i_j_2");
        parametersResponseMaxExpected.putArray("k:l").add("value_k_l").add("value_k_l_2");
        parametersResponseMaxExpected.putArray("m:n").add("value_m_n");
        parametersResponseMaxExpected.putArray("o:p").add("value_o_p");

        parametersResponseMaxExpected.putArray("a:b").add("value_a_b").add("value_a_b_2");
        parametersResponseMaxExpected.putArray("c:d").add("value_c_d").add("value_c_d_2");
        parametersResponseMaxExpected.putArray("e:f").add("value_e_f");
        parametersResponseMaxExpected.putArray("g:h").add("value_g_h");

        SampleData.verifyJsonResponse(parametersResponseMax, parametersResponseMaxExpected);

        //Maximum amount of parameters
    }

    @Test
    public void TestPatternParameters() throws IOException {
        //Invalid
        try
        {
            parametersApi.getQueryParameters(
                    true,
                    //Integer integerMandatoryWithoutDefault
                    1,
                    //BigDecimal numberMandatoryWithoutDefault
                    new BigDecimal("1234567890.123456789"),
                    //String stringMandatoryWithoutDefault
                    "str",
                    //List<Boolean> booleanArrayMandatoryWithoutDefault
                    List.of(true, false),
                    //List<Integer> integerArrayMandatoryWithoutDefault
                    List.of(1, 2),
                    //List<BigDecimal> numberArrayMandatoryWithoutDefault
                    List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                    //List<String> stringArrayMandatoryWithoutDefault
                    List.of("a", "b"),
                    //Map<String, List<String>> patternParameterArrayMandatory
                    Map.of("ab", List.of("value_a_b", "value_a_b_2"),
                            "c:d", List.of("value_c_d", "value_c_d_2")),
                    //Map<String, String> patternParameterSingleMandatory
                    Map.of("e:f", "value_e_f",
                            "g:h", "value_g_h"),
                    //int overriddenTypeMandatory
                    1,
                    //Boolean fail
                    null,
                    //Boolean booleanOptionalWithDefault
                    null,
                    //Boolean booleanOptionalWithoutDefault
                    null,
                    //Integer integerOptionalWithDefault
                    null,
                    //Integer integerOptionalWithoutDefault
                    null,
                    //BigDecimal numberOptionalWithDefault
                    null,
                    //BigDecimal numberOptionalWithoutDefault
                    null,
                    //String stringOptionalWithDefault
                    null,
                    //String stringOptionalWithoutDefault
                    null,
                    //List<Boolean> booleanArrayOptionalWithDefault
                    null,
                    //List<Boolean> booleanArrayOptionalWithoutDefault
                    null,
                    //List<Integer> integerArrayOptionalWithDefault
                    null,
                    //List<Integer> integerArrayOptionalWithoutDefault
                    null,
                    //List<BigDecimal> numberArrayOptionalWithDefault
                    null,
                    //List<BigDecimal> numberArrayOptionalWithoutDefault
                    null,
                    //List<String> stringArrayOptionalWithDefault
                    null,
                    //List<String> stringArrayOptionalWithoutDefault
                    null,
                    //Map<String, List<String>> patternParameterArrayOptional
                    null,
                    //Map<String, String> patternParameterSingleOptional
                    null,
                    //Integer overriddenTypeOptional
                    null);
            throw new IOException("Expecting pattern parameter validation to fail");
        }
        catch (ApiException e)
        {
            if (!e.getMessage().contains("match the regular expression"))
                throw new IOException("Expecting pattern parameter validation to fail, but another error occurred");
        }
    }

    /// <summary>
    /// Test that bodies are received correctly
    /// </summary>
    @Test
    public void TestBinaryBodies() throws IOException, ApiException {
        byte[] testBinaryBody = SampleData.sampleBinaryBody();

        byte[] binaryBody = parametersApi.postBinaryBody(testBinaryBody, null);
        SampleData.verifyBinaryResponse(binaryBody, testBinaryBody);
        byte[] binaryBodyOptMin = parametersApi.postBinaryBodyOptional(null, null);
        SampleData.verifyBinaryResponse(binaryBodyOptMin, new byte[0]);
        byte[] binaryBodyOptMax = parametersApi.postBinaryBodyOptional(testBinaryBody, null);
        SampleData.verifyBinaryResponse(binaryBodyOptMax, testBinaryBody);
    }

    @Test
    public void TestObjectBodies() throws IOException, ApiException {
        ObjectNode testObjectBody = SampleData.sampleJsonResponse(0);

        ObjectNode objectBody = parametersApi.postObjectBody(testObjectBody, null);
        SampleData.verifyJsonResponse(objectBody, testObjectBody);

        ObjectNode objectBodyOptMin = parametersApi.postObjectBodyOptional(null, null);
        SampleData.verifyJsonResponse(objectBodyOptMin, null);

        ObjectNode objectBodyOptMax = parametersApi.postObjectBodyOptional(testObjectBody, null);
        SampleData.verifyJsonResponse(objectBodyOptMax, testObjectBody);
    }

    @Test
    public void TestStringBodies() throws IOException, ApiException {
        String testStringBody = SampleData.sampleString();

        String stringBody = parametersApi.postStringBody(testStringBody, null);
        SampleData.verifyStringResponse(stringBody, testStringBody);
        String stringBodyOptMin = parametersApi.postStringBodyOptional(null, null);
        SampleData.verifyStringResponse(stringBodyOptMin, "");
        String stringBodyOptMax = parametersApi.postStringBodyOptional(testStringBody, null);
        SampleData.verifyStringResponse(stringBodyOptMax, testStringBody);
    }

    @Test
    public void TestPathParameters() throws ApiException, IOException {
        ObjectNode parametersResponse1 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                null,
                //Boolean id3
                null,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        ObjectNode parametersResponse1Expected = JsonNodeFactory.instance.objectNode();
        parametersResponse1Expected.put("id1", "1");

        SampleData.verifyJsonResponse(parametersResponse1, parametersResponse1Expected);

        ObjectNode parametersResponse2 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                null,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        ObjectNode parametersResponse2Expected = JsonNodeFactory.instance.objectNode();
        parametersResponse2Expected.put("id1", "1");
        parametersResponse2Expected.put("id2", "two");

        SampleData.verifyJsonResponse(parametersResponse2, parametersResponse2Expected);

        ObjectNode parametersResponse3 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                true,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        ObjectNode parametersResponse3Expected = JsonNodeFactory.instance.objectNode();
        parametersResponse3Expected.put("id1", "1");
        parametersResponse3Expected.put("id2", "two");
        parametersResponse3Expected.put("id3", "true");

        SampleData.verifyJsonResponse(parametersResponse3, parametersResponse3Expected);


        ObjectNode parametersResponse4 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                true,
                //BigDecimal id4
                new BigDecimal("4"),
                //Boolean fail
                false);

        ObjectNode parametersResponse4Expected = JsonNodeFactory.instance.objectNode();
        parametersResponse4Expected.put("id1", "1");
        parametersResponse4Expected.put("id2", "two");
        parametersResponse4Expected.put("id3", "true");
        parametersResponse4Expected.put("id4", "4");

        SampleData.verifyJsonResponse(parametersResponse4, parametersResponse4Expected);
    }
}
