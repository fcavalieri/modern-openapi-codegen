package com.reportix.cellstore.client.tests;

import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.reportix.cellstore.client.ApiException;
import com.reportix.cellstore.model.ISequence;
import com.reportix.cellstore.model.IStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class SampleData
{
    public static JsonArray JsonArrayOf(String... elems){
        JsonArray array = new JsonArray();
        for (String elem: elems)
            array.add(elem);
        return array;
    }


    public static void verifyValidBinary(byte[] binaryResponse) throws IOException {
        ZipInputStream zipIn = new ZipInputStream(new ByteArrayInputStream(binaryResponse));
        ZipEntry entry = zipIn.getNextEntry();
        if (entry != null) {
            String text = null;
            try (Reader reader = new InputStreamReader(zipIn)) {
                text = CharStreams.toString(reader);
                if (!"Test String".equals(text))
                    throw new IOException("Cannot verify binary (1)");
            }
        }
        else
            throw new IOException("Cannot verify binary (2)");
    }

    public static JsonObject sampleJsonResponse(int id)
    {
        JsonObject ret = new JsonObject();
        ret.addProperty("id_field", id);
        ret.addProperty("string_field", "string");
        ret.addProperty("int_field", 1);
        ret.addProperty("bool_field", true);
        ret.addProperty("float_field", 3.14);
        ret.add("null_field", null);
        JsonArray arr = new JsonArray();
        arr.add("string");
        arr.add(1);
        arr.add(true);
        ret.add("array_field", arr);
        JsonObject obj = new JsonObject();
        obj.addProperty("utf8_string_field", "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。");
        ret.add("object_field", obj);
        return ret;
    }

    public static String sampleXMLResponse()
    {
        return "<?xml version = \"1.0\" encoding = \"utf-8\"?>\n" +
                "<!-- planes.xml - A document that lists ads for used airplanes -->\n" +
                "<planes_for_sale>\n" +
                "   <ad>\n" +
                "      <year>1977</year>\n" +
                "      <make>&c;</make>\n" +
                "      <model>Skyhawk</model>\n" +
                "   </ad>\n" +
                "</planes_for_sale>";
    }

    public static byte[] sampleBinaryBody()
    {
        byte[] ret = new byte[1024];
        Random rnd = new Random();
        rnd.nextBytes(ret);
        return ret;
    }

    public static String sampleString()
    {
        return "--Test-String-Body--Italian:però-German:Zürich-Hebrew:שׁמונה-Japanese:田中さんにあげて下さい。";
    }

    public static void verifyJsonResponse(JsonObject jsonResponse) throws IOException {
        JsonObject expected = sampleJsonResponse(0);
        if (!jsonResponse.toString().equals(expected.toString()))
            throw new IOException("Unexpected Json");
    }

    public static void verifyXMLResponse(String xmlResponse) throws IOException {
        String expected = sampleXMLResponse();
        if (!xmlResponse.equals(expected))
            throw new IOException("Unexpected Xml");
    }

    public static void verifyException(ApiException apiException, String endpoint) throws IOException {
        if (!apiException.getMessage().contains("Failing as requested"))
            throw new IOException("Expecting request to fail due to explicit request");

        if (apiException.getErrorCode() != 500)
            throw new IOException("Expecting error to contain http response code");
        String message = apiException.toString();
        if (!message.contains("CELLSTORE_EXCEPTION"))
            throw new IOException("Expecting error to contain raw response");
        if (!message.contains("com.reportix.xbrl.server.rest.gen.endpoints"))
            throw new IOException("Expecting error to contain stack trace");
        if (!message.contains("1.0.0-TESTS"))
            throw new IOException("Expecting error to contain library information");
        if (!message.contains("System has"))
            throw new IOException("Expecting error to contain system memory information");
        if (!message.contains("Application has an upper limit"))
            throw new IOException("Expecting error to contain application memory information");
        if (endpoint != null & !message.contains(endpoint))
            throw new IOException("Expecting error to contain call information");
        if (!message.contains("RequestTime"))
            throw new IOException("Expecting error to contain call request time");
        if (!message.contains("ResponseTime"))
            throw new IOException("Expecting error to contain call response time");
        if (!message.contains("authenticationProvider"))
            throw new IOException("Expecting error to contain configuration");
    }

    public static void verifyJsonResponse(JsonObject actualResponse, JsonObject expectedResponse) throws IOException {
        if (actualResponse == null && expectedResponse == null)
            return;
        if (!actualResponse.equals(expectedResponse))
            throw new IOException("Expected != Actual");
    }

    public static void verifyBinaryResponse(byte[] actualResponse, byte[] expectedResponse) throws IOException {
        if (!Arrays.equals(actualResponse, expectedResponse))
            throw new IOException("Expected != Actual");
    }

    public static void verifyStringResponse(String actualResponse, String expectedResponse) throws IOException {
        if (!actualResponse.equals(expectedResponse))
            throw new IOException("Expected != Actual");
    }

    public static void verifyJsonListResponse(ISequence<JsonObject, JsonObject> jsonListResponse, int amount) throws IOException {
        if (Math.max(amount, 0) != jsonListResponse.size())
            throw new IOException("Unexpected count");

        verifyJsonList(jsonListResponse.stream().collect(Collectors.toList()), jsonListResponse.getMetadata());
    }

    public static void verifyJsonStreamResponse(IStream<JsonObject, JsonObject> jsonStreamResponse, int amount) throws IOException {
        List<JsonObject> objectList = Lists.newArrayList(jsonStreamResponse.iterator());
        if (Math.max(amount, 0) != objectList.size())
            throw new IOException("Unexpected count");

        verifyJsonList(objectList, jsonStreamResponse.getMetadata());
    }

    public static void verifyFailedJsonStreamResponse(IStream<JsonObject, JsonObject> jsonStreamResponse, int minimum, int maximum, String endpoint) throws IOException {
        verifyJsonResponse(jsonStreamResponse.getMetadata(), sampleJsonResponse(-2));
        Iterator<JsonObject> en = jsonStreamResponse.iterator();
        for (int i = 0; i < minimum; ++i)
        {
            if (!en.hasNext())
                throw new IOException("Cannot get item " + i);
            verifyJsonResponse(en.next(), sampleJsonResponse(i));
        }
        for (int i = minimum; i <= maximum; ++i)
        {
            try
            {
                if (!en.hasNext())
                    throw new IOException("Cannot get item " + i);
                verifyJsonResponse(en.next(), sampleJsonResponse(i));
            }
            catch (RuntimeException e)
            {
                verifyException((ApiException)e.getCause(), endpoint);
                return;
            }
        }
    }

    private static void verifyJsonList(List<JsonObject> toList, JsonObject metadata) throws IOException {
        verifyJsonResponse(metadata, sampleJsonResponse(-2));
        for (int i=0; i<toList.size(); ++i)
        {
            verifyJsonResponse(toList.get(i), sampleJsonResponse(i));
        }
    }
}
