package com.reportix.cellstore.client.tests;

import com.google.gson.JsonObject;
import com.reportix.cellstore.client.ApiException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ParametersTest extends TestBase {

    /// <summary>
    /// Test that any kind of parameter is serialized, sent, received and echoed back correctly
    /// </summary>
    @Test
    public void TestParametersMin() throws ApiException, IOException {
        //Least amount of parameters
        JsonObject parametersResponseMin = parametersApi.getQueryParameters(
                //Boolean booleanMandatoryWithoutDefault
                true,
                //Integer integerMandatoryWithoutDefault
                1,
                //BigDecimal numberMandatoryWithoutDefault
                new BigDecimal("1234567890.123456789"),
                //String stringMandatoryWithoutDefault
                "str",
                //List<Boolean> booleanArrayMandatoryWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayMandatoryWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayMandatoryWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayMandatoryWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayMandatory
                Map.of("a:b", List.of("value_a_b", "value_a_b_2"),
                        "c:d", List.of("value_c_d", "value_c_d_2")),
                //Map<String, String> patternParameterSingleMandatory
                Map.of("e:f", "value_e_f",
                        "g:h", "value_g_h"),
                //int overriddenTypeMandatory
                1,
                //Boolean fail
                null,
                //Boolean booleanOptionalWithDefault
                null,
                //Boolean booleanOptionalWithoutDefault
                null,
                //Integer integerOptionalWithDefault
                null,
                //Integer integerOptionalWithoutDefault
                null,
                //BigDecimal numberOptionalWithDefault
                null,
                //BigDecimal numberOptionalWithoutDefault
                null,
                //String stringOptionalWithDefault
                null,
                //String stringOptionalWithoutDefault
                null,
                //List<Boolean> booleanArrayOptionalWithDefault
                null,
                //List<Boolean> booleanArrayOptionalWithoutDefault
                null,
                //List<Integer> integerArrayOptionalWithDefault
                null,
                //List<Integer> integerArrayOptionalWithoutDefault
                null,
                //List<BigDecimal> numberArrayOptionalWithDefault
                null,
                //List<BigDecimal> numberArrayOptionalWithoutDefault
                null,
                //List<String> stringArrayOptionalWithDefault
                null,
                //List<String> stringArrayOptionalWithoutDefault
                null,
                //Map<String, List<String>> patternParameterArrayOptional
                null,
                //Map<String, String> patternParameterSingleOptional
                null,
                //Integer overriddenTypeOptional
                null);

        JsonObject parametersResponseMinExpected = new JsonObject();

        parametersResponseMinExpected.add("boolean-hardcoded", SampleData.JsonArrayOf("true"));
        parametersResponseMinExpected.add("integer-hardcoded", SampleData.JsonArrayOf("3"));
        parametersResponseMinExpected.add("number-hardcoded", SampleData.JsonArrayOf("987654321.987654321"));
        parametersResponseMinExpected.add("string-hardcoded", SampleData.JsonArrayOf("value"));

        parametersResponseMinExpected.add("boolean-mandatory-without-default", SampleData.JsonArrayOf("true"));
        parametersResponseMinExpected.add("integer-mandatory-without-default", SampleData.JsonArrayOf("1"));
        parametersResponseMinExpected.add("number-mandatory-without-default", SampleData.JsonArrayOf("1234567890.123456789"));
        parametersResponseMinExpected.add("string-mandatory-without-default", SampleData.JsonArrayOf("str"));

        parametersResponseMinExpected.add("boolean-array-mandatory-without-default", SampleData.JsonArrayOf("true", "false"));
        parametersResponseMinExpected.add("integer-array-mandatory-without-default", SampleData.JsonArrayOf("1", "2"));
        parametersResponseMinExpected.add("number-array-mandatory-without-default", SampleData.JsonArrayOf("1234567890.123456789", "12345678901234567890.123456789"));
        parametersResponseMinExpected.add("string-array-mandatory-without-default", SampleData.JsonArrayOf("a", "b"));

        parametersResponseMinExpected.add("overridden-type-mandatory", SampleData.JsonArrayOf("1"));
        parametersResponseMinExpected.add("a:b", SampleData.JsonArrayOf("value_a_b", "value_a_b_2"));
        parametersResponseMinExpected.add("c:d", SampleData.JsonArrayOf("value_c_d", "value_c_d_2"));
        parametersResponseMinExpected.add("e:f", SampleData.JsonArrayOf("value_e_f"));
        parametersResponseMinExpected.add("g:h", SampleData.JsonArrayOf("value_g_h"));

        SampleData.verifyJsonResponse(parametersResponseMin, parametersResponseMinExpected);
    }

    @Test
    public void TestParametersMax() throws IOException, ApiException {
        //Maximum amount of parameters
        JsonObject parametersResponseMax = parametersApi.getQueryParameters(
                //Boolean booleanMandatoryWithoutDefault
                true,
                //Integer integerMandatoryWithoutDefault
                1,
                //BigDecimal numberMandatoryWithoutDefault
                new BigDecimal("1234567890.123456789"),
                //String stringMandatoryWithoutDefault
                "str",
                //List<Boolean> booleanArrayMandatoryWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayMandatoryWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayMandatoryWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayMandatoryWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayMandatory
                Map.of("a:b", List.of("value_a_b", "value_a_b_2"),
                        "c:d", List.of("value_c_d", "value_c_d_2")),
                //Map<String, String> patternParameterSingleMandatory
                Map.of("e:f", "value_e_f",
                        "g:h", "value_g_h"),
                //int overriddenTypeMandatory
                1,
                //Boolean fail
                false,
                //Boolean booleanOptionalWithDefault
                true,
                //Boolean booleanOptionalWithoutDefault
                true,
                //Integer integerOptionalWithDefault
                1,
                //Integer integerOptionalWithoutDefault
                1,
                //BigDecimal numberOptionalWithDefault
                new BigDecimal("1234567890"),
                //BigDecimal numberOptionalWithoutDefault
                new BigDecimal("12345678901234567890"),
                //String stringOptionalWithDefault
                "str",
                //String stringOptionalWithoutDefault
                "str",
                //List<Boolean> booleanArrayOptionalWithDefault
                List.of(true, false),
                //List<Boolean> booleanArrayOptionalWithoutDefault
                List.of(true, false),
                //List<Integer> integerArrayOptionalWithDefault
                List.of(1, 2),
                //List<Integer> integerArrayOptionalWithoutDefault
                List.of(1, 2),
                //List<BigDecimal> numberArrayOptionalWithDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<BigDecimal> numberArrayOptionalWithoutDefault
                List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                //List<String> stringArrayOptionalWithDefault
                List.of("a", "b"),
                //List<String> stringArrayOptionalWithoutDefault
                List.of("a", "b"),
                //Map<String, List<String>> patternParameterArrayOptional
                Map.of("i:j", List.of("value_i_j", "value_i_j_2"),
                        "k:l", List.of("value_k_l", "value_k_l_2")),
                //Map<String, String> patternParameterSingleOptional
                Map.of("m:n", "value_m_n",
                        "o:p", "value_o_p"),
                //Integer overriddenTypeOptional
                1);

        JsonObject parametersResponseMaxExpected = new JsonObject();
        parametersResponseMaxExpected.add("boolean-hardcoded", SampleData.JsonArrayOf("true"));
        parametersResponseMaxExpected.add("integer-hardcoded", SampleData.JsonArrayOf("3"));
        parametersResponseMaxExpected.add("number-hardcoded", SampleData.JsonArrayOf("987654321.987654321"));
        parametersResponseMaxExpected.add("string-hardcoded", SampleData.JsonArrayOf("value"));

        parametersResponseMaxExpected.add("fail", SampleData.JsonArrayOf("false"));

        parametersResponseMaxExpected.add("boolean-mandatory-without-default", SampleData.JsonArrayOf("true"));
        parametersResponseMaxExpected.add("boolean-optional-with-default", SampleData.JsonArrayOf("true"));
        parametersResponseMaxExpected.add("boolean-optional-without-default", SampleData.JsonArrayOf("true"));

        parametersResponseMaxExpected.add("integer-mandatory-without-default", SampleData.JsonArrayOf("1"));
        parametersResponseMaxExpected.add("integer-optional-with-default", SampleData.JsonArrayOf("1"));
        parametersResponseMaxExpected.add("integer-optional-without-default", SampleData.JsonArrayOf("1"));

        parametersResponseMaxExpected.add("number-mandatory-without-default", SampleData.JsonArrayOf("1234567890.123456789"));
        parametersResponseMaxExpected.add("number-optional-with-default", SampleData.JsonArrayOf("1234567890"));
        parametersResponseMaxExpected.add("number-optional-without-default", SampleData.JsonArrayOf("12345678901234567890"));

        parametersResponseMaxExpected.add("string-mandatory-without-default", SampleData.JsonArrayOf("str"));
        parametersResponseMaxExpected.add("string-optional-with-default", SampleData.JsonArrayOf("str"));
        parametersResponseMaxExpected.add("string-optional-without-default", SampleData.JsonArrayOf("str"));

        parametersResponseMaxExpected.add("boolean-array-mandatory-without-default", SampleData.JsonArrayOf("true", "false"));
        parametersResponseMaxExpected.add("boolean-array-optional-with-default", SampleData.JsonArrayOf("true", "false"));
        parametersResponseMaxExpected.add("boolean-array-optional-without-default", SampleData.JsonArrayOf("true", "false"));

        parametersResponseMaxExpected.add("integer-array-mandatory-without-default", SampleData.JsonArrayOf("1", "2"));
        parametersResponseMaxExpected.add("integer-array-optional-with-default", SampleData.JsonArrayOf("1", "2"));
        parametersResponseMaxExpected.add("integer-array-optional-without-default", SampleData.JsonArrayOf("1", "2"));

        parametersResponseMaxExpected.add("number-array-mandatory-without-default", SampleData.JsonArrayOf("1234567890.123456789", "12345678901234567890.123456789"));
        parametersResponseMaxExpected.add("number-array-optional-with-default", SampleData.JsonArrayOf("1234567890.123456789", "12345678901234567890.123456789"));
        parametersResponseMaxExpected.add("number-array-optional-without-default", SampleData.JsonArrayOf("1234567890.123456789", "12345678901234567890.123456789"));

        parametersResponseMaxExpected.add("string-array-mandatory-without-default", SampleData.JsonArrayOf("a", "b"));
        parametersResponseMaxExpected.add("string-array-optional-with-default", SampleData.JsonArrayOf("a", "b"));
        parametersResponseMaxExpected.add("string-array-optional-without-default", SampleData.JsonArrayOf("a", "b"));

        parametersResponseMaxExpected.add("overridden-type-optional", SampleData.JsonArrayOf("1"));
        parametersResponseMaxExpected.add("overridden-type-mandatory", SampleData.JsonArrayOf("1"));

        parametersResponseMaxExpected.add("i:j", SampleData.JsonArrayOf("value_i_j", "value_i_j_2"));
        parametersResponseMaxExpected.add("k:l", SampleData.JsonArrayOf("value_k_l", "value_k_l_2"));
        parametersResponseMaxExpected.add("m:n", SampleData.JsonArrayOf("value_m_n"));
        parametersResponseMaxExpected.add("o:p", SampleData.JsonArrayOf("value_o_p"));

        parametersResponseMaxExpected.add("a:b", SampleData.JsonArrayOf("value_a_b", "value_a_b_2"));
        parametersResponseMaxExpected.add("c:d", SampleData.JsonArrayOf("value_c_d", "value_c_d_2"));
        parametersResponseMaxExpected.add("e:f", SampleData.JsonArrayOf("value_e_f"));
        parametersResponseMaxExpected.add("g:h", SampleData.JsonArrayOf("value_g_h"));

        SampleData.verifyJsonResponse(parametersResponseMax, parametersResponseMaxExpected);

        //Maximum amount of parameters
    }

    @Test
    public void TestPatternParameters() throws IOException {
        //Invalid
        try
        {
            parametersApi.getQueryParameters(
                    true,
                    //Integer integerMandatoryWithoutDefault
                    1,
                    //BigDecimal numberMandatoryWithoutDefault
                    new BigDecimal("1234567890.123456789"),
                    //String stringMandatoryWithoutDefault
                    "str",
                    //List<Boolean> booleanArrayMandatoryWithoutDefault
                    List.of(true, false),
                    //List<Integer> integerArrayMandatoryWithoutDefault
                    List.of(1, 2),
                    //List<BigDecimal> numberArrayMandatoryWithoutDefault
                    List.of(new BigDecimal("1234567890.123456789"), new BigDecimal("12345678901234567890.123456789")),
                    //List<String> stringArrayMandatoryWithoutDefault
                    List.of("a", "b"),
                    //Map<String, List<String>> patternParameterArrayMandatory
                    Map.of("ab", List.of("value_a_b", "value_a_b_2"),
                            "c:d", List.of("value_c_d", "value_c_d_2")),
                    //Map<String, String> patternParameterSingleMandatory
                    Map.of("e:f", "value_e_f",
                            "g:h", "value_g_h"),
                    //int overriddenTypeMandatory
                    1,
                    //Boolean fail
                    null,
                    //Boolean booleanOptionalWithDefault
                    null,
                    //Boolean booleanOptionalWithoutDefault
                    null,
                    //Integer integerOptionalWithDefault
                    null,
                    //Integer integerOptionalWithoutDefault
                    null,
                    //BigDecimal numberOptionalWithDefault
                    null,
                    //BigDecimal numberOptionalWithoutDefault
                    null,
                    //String stringOptionalWithDefault
                    null,
                    //String stringOptionalWithoutDefault
                    null,
                    //List<Boolean> booleanArrayOptionalWithDefault
                    null,
                    //List<Boolean> booleanArrayOptionalWithoutDefault
                    null,
                    //List<Integer> integerArrayOptionalWithDefault
                    null,
                    //List<Integer> integerArrayOptionalWithoutDefault
                    null,
                    //List<BigDecimal> numberArrayOptionalWithDefault
                    null,
                    //List<BigDecimal> numberArrayOptionalWithoutDefault
                    null,
                    //List<String> stringArrayOptionalWithDefault
                    null,
                    //List<String> stringArrayOptionalWithoutDefault
                    null,
                    //Map<String, List<String>> patternParameterArrayOptional
                    null,
                    //Map<String, String> patternParameterSingleOptional
                    null,
                    //Integer overriddenTypeOptional
                    null);
            throw new IOException("Expecting pattern parameter validation to fail");
        }
        catch (ApiException e)
        {
            if (!e.getMessage().contains("match the regular expression"))
                throw new IOException("Expecting pattern parameter validation to fail, but another error occurred");
        }
    }

    /// <summary>
    /// Test that bodies are received correctly
    /// </summary>
    @Test
    public void TestBinaryBodies() throws IOException, ApiException {
        byte[] testBinaryBody = SampleData.sampleBinaryBody();

        byte[] binaryBody = parametersApi.postBinaryBody(testBinaryBody, null);
        SampleData.verifyBinaryResponse(binaryBody, testBinaryBody);
        byte[] binaryBodyOptMin = parametersApi.postBinaryBodyOptional(null, null);
        SampleData.verifyBinaryResponse(binaryBodyOptMin, new byte[0]);
        byte[] binaryBodyOptMax = parametersApi.postBinaryBodyOptional(testBinaryBody, null);
        SampleData.verifyBinaryResponse(binaryBodyOptMax, testBinaryBody);
    }

    @Test
    public void TestObjectBodies() throws IOException, ApiException {
        JsonObject testObjectBody = SampleData.sampleJsonResponse(0);

        JsonObject objectBody = parametersApi.postObjectBody(testObjectBody, null);
        SampleData.verifyJsonResponse(objectBody, testObjectBody);

        JsonObject objectBodyOptMin = parametersApi.postObjectBodyOptional(null, null);
        SampleData.verifyJsonResponse(objectBodyOptMin, null);

        JsonObject objectBodyOptMax = parametersApi.postObjectBodyOptional(testObjectBody, null);
        SampleData.verifyJsonResponse(objectBodyOptMax, testObjectBody);
    }

    @Test
    public void TestStringBodies() throws IOException, ApiException {
        String testStringBody = SampleData.sampleString();

        String stringBody = parametersApi.postStringBody(testStringBody, null);
        SampleData.verifyStringResponse(stringBody, testStringBody);
        String stringBodyOptMin = parametersApi.postStringBodyOptional(null, null);
        SampleData.verifyStringResponse(stringBodyOptMin, "");
        String stringBodyOptMax = parametersApi.postStringBodyOptional(testStringBody, null);
        SampleData.verifyStringResponse(stringBodyOptMax, testStringBody);
    }

    @Test
    public void TestPathParameters() throws ApiException, IOException {
        JsonObject parametersResponse1 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                null,
                //Boolean id3
                null,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        JsonObject parametersResponse1Expected = new JsonObject();
        parametersResponse1Expected.addProperty("id1", "1");

        SampleData.verifyJsonResponse(parametersResponse1, parametersResponse1Expected);

        JsonObject parametersResponse2 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                null,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        JsonObject parametersResponse2Expected = new JsonObject();
        parametersResponse2Expected.addProperty("id1", "1");
        parametersResponse2Expected.addProperty("id2", "two");

        SampleData.verifyJsonResponse(parametersResponse2, parametersResponse2Expected);

        JsonObject parametersResponse3 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                true,
                //BigDecimal id4
                null,
                //Boolean fail
                false);

        JsonObject parametersResponse3Expected = new JsonObject();
        parametersResponse3Expected.addProperty("id1", "1");
        parametersResponse3Expected.addProperty("id2", "two");
        parametersResponse3Expected.addProperty("id3", "true");

        SampleData.verifyJsonResponse(parametersResponse3, parametersResponse3Expected);


        JsonObject parametersResponse4 = parametersApi.getPathParameters(
                //Integer id1
                1,
                //String id2
                "two",
                //Boolean id3
                true,
                //BigDecimal id4
                new BigDecimal("4"),
                //Boolean fail
                false);

        JsonObject parametersResponse4Expected = new JsonObject();
        parametersResponse4Expected.addProperty("id1", "1");
        parametersResponse4Expected.addProperty("id2", "two");
        parametersResponse4Expected.addProperty("id3", "true");
        parametersResponse4Expected.addProperty("id4", "4");

        SampleData.verifyJsonResponse(parametersResponse4, parametersResponse4Expected);
    }
}
